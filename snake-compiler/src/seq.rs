

use crate::syntax::{Exp, FunDecl, ImmExp, Prim, SeqExp};
use crate::tag::is_binary_operation;

fn simple_exp_to_imm(e: &Exp<u32>) -> Option<ImmExp> {
    match e {
        Exp::Num(i, _) => Some(ImmExp::Num(*i)),
        Exp::Bool(b, _) => Some(ImmExp::Bool(*b)),
        Exp::Var(x, _) => Some(ImmExp::Var(x.clone())),
        _ => None,
    }
}

fn sequentialize_array_help(array: &Vec<Box<Exp<u32>>>, ann: u32) -> SeqExp<()> {
    let mut seq_array: Vec<ImmExp> = Vec::new();
    let mut bindings: Vec<(String, Exp<u32>)> = Vec::new();
    for (i, element) in array.iter().enumerate() {
        match simple_exp_to_imm(element) {
            Some(imm) => seq_array.push(imm),
            None => {
                let x: String = format!("#array_{}_element_{}", ann, i);
                bindings.push((x.clone(), element.as_ref().clone()));
                seq_array.push(ImmExp::Var(x.clone()));
            }
        };
    }
    bindings.iter().rev().fold(
        SeqExp::Prim(Prim::MakeArray, seq_array, ()),
        |acc, (x, def)| SeqExp::Let {
            var: x.clone(),
            bound_exp: Box::new(sequentialize(&def)),
            body: Box::new(acc),
            ann: (),
        },
    )
}

pub fn sequentialize(e: &Exp<u32>) -> SeqExp<()> {
    match e {
        Exp::Num(n, _) => SeqExp::Imm(ImmExp::Num(*n), ()),
        Exp::Var(x, _) => SeqExp::Imm(ImmExp::Var(x.clone()), ()),
        Exp::Bool(b, _) => SeqExp::Imm(ImmExp::Bool(*b), ()),
        Exp::If {
            cond,
            thn,
            els,
            ann,
        } => SeqExp::Let {
            var: format!("{}{:?}", "if_", ann),
            bound_exp: Box::new(sequentialize(cond)),
            body: Box::new(SeqExp::If {
                cond: ImmExp::Var(format!("{}{:?}", "if_", ann)),
                thn: Box::new(sequentialize(thn)),
                els: Box::new(sequentialize(els)),
                ann: (),
            }),
            ann: (),
        },
        Exp::Let {
            bindings,
            body,
            ann: _,
        } => {
            let mut last_seq_exp = Box::new(sequentialize(body));
            for (x, bound_exp) in bindings.iter().rev() {
                last_seq_exp = Box::new(SeqExp::Let {
                    var: x.clone(), // do we need to rename?
                    bound_exp: Box::new(sequentialize(bound_exp)),
                    body: last_seq_exp,
                    ann: (),
                });
            }
            *last_seq_exp
        }
        Exp::Prim(op, es, tag) => {
            if is_binary_operation(op.clone()) {
                let s_e1 = sequentialize(&es[0]);
                let s_e2 = sequentialize(&es[1]);
                let name1 = format!("#prim2_1_{}", tag);
                let name2 = format!("#prim2_2_{}", tag);
                SeqExp::Let {
                    var: name1.clone(),
                    bound_exp: Box::new(s_e1),
                    body: Box::new(SeqExp::Let {
                        var: name2.clone(),
                        bound_exp: Box::new(s_e2),
                        body: Box::new(SeqExp::Prim(
                            *op,
                            vec![ImmExp::Var(name1), ImmExp::Var(name2)],
                            (),
                        )),
                        ann: (),
                    }),
                    ann: (),
                }
            } else {
                match op {
                    Prim::ArraySet => {
                        let s_e1 = sequentialize(&es[0]);
                        let s_e2 = sequentialize(&es[1]);
                        let s_e3 = sequentialize(&es[2]);
                        let name1 = format!("#prim3_1_{}", tag);
                        let name2 = format!("#prim3_2_{}", tag);
                        let name3 = format!("#prim3_3_{}", tag);
                        SeqExp::Let {
                            var: name1.clone(),
                            bound_exp: Box::new(s_e1),
                            body: Box::new(SeqExp::Let {
                                var: name2.clone(),
                                bound_exp: Box::new(s_e2),
                                body: Box::new(SeqExp::Let {
                                    var: name3.clone(),
                                    bound_exp: Box::new(s_e3),
                                    body: Box::new(SeqExp::Prim(
                                        *op,
                                        vec![
                                            ImmExp::Var(name1),
                                            ImmExp::Var(name2),
                                            ImmExp::Var(name3),
                                        ],
                                        (),
                                    )),
                                    ann: (),
                                }),
                                ann: (),
                            }),
                            ann: (),
                        }
                    }
                    Prim::MakeArray => sequentialize_array_help(es, *tag),
                    _ => {// Unary operation
                        let s_e1 = sequentialize(&es[0]);
                        let name1 = format!("#prim1_1_{}", tag);
                        SeqExp::Let {
                            var: name1.clone(),
                            bound_exp: Box::new(s_e1),
                            body: Box::new(SeqExp::Prim(*op, vec![ImmExp::Var(name1)], ())),
                            ann: (),
                        }
                    }
                }
            }
        }
        Exp::ExternalCall {
            fun, args, is_tail, .. 
            // function or var is unique, don't need tag
        } => {
            let mut s_args = vec![];
            for (i, _) in args.iter().enumerate() {
                let name = format!("#call_{}_{}", fun.to_string(), i);
                s_args.push(ImmExp::Var(name.clone()));
            }
            let base_se = SeqExp::ExternalCall {
                fun: fun.clone(),
                args: s_args,
                is_tail: *is_tail,
                ann: (),
            };
            let mut last_seq_exp = base_se;
            for (i, e) in args.iter().enumerate().rev() {
                last_seq_exp = SeqExp::Let {
                    var: format!("#call_{}_{}", fun.to_string(), i),
                    bound_exp: Box::new(sequentialize(e)),
                    body: Box::new(last_seq_exp),
                    ann: (),
                }
            }
            last_seq_exp
        }
        Exp::InternalTailCall(f, es, _) => {
            let mut s_es = vec![];
            for (i, _) in es.iter().enumerate() {
                let name = format!("#call_{}_{}", f, i);
                s_es.push(ImmExp::Var(name.clone()));
            }
            let base_se = SeqExp::InternalTailCall(f.clone(), s_es, ());
            let mut last_seq_exp = base_se;
            for (i, e) in es.iter().enumerate().rev() {
                last_seq_exp = SeqExp::Let {
                    var: format!("#call_{}_{}", f, i),
                    bound_exp: Box::new(sequentialize(e)),
                    body: Box::new(last_seq_exp),
                    ann: (),
                }
            }
            last_seq_exp
        }
        Exp::FunDefs { decls, body, .. } => SeqExp::FunDefs {
            decls: decls
                .iter()
                .map(|decl| {
                    let mut new_params = vec![];
                    for param in &decl.parameters {
                        new_params.push(param.clone());
                    }
                    FunDecl {
                        name: decl.name.clone(),
                        parameters: new_params,
                        body: sequentialize(&decl.body),
                        ann: (),
                    }
                })
                .collect(),
            body: Box::new(sequentialize(body)),
            ann: (),
        },
        Exp::MakeClosure { arity, label, env, ann }=>{
            let s_env = sequentialize(env);
            let name = format!("#closure_{}_{}", label, ann);
            SeqExp::Let {
                var: name.clone(),
                bound_exp: Box::new(s_env),
                body: Box::new(SeqExp::MakeClosure {
                    arity: *arity,
                    label: label.clone(),
                    env: ImmExp::Var(name),
                    ann: (),
                }),
                ann: (),
            }
        }
        Exp::Lambda { .. }
        |Exp::Semicolon {..}|Exp::DirectCall(..)|Exp::Call(..)=> panic!("these should be eliminated"),
        Exp::ClosureCall(.. )=>panic!("should be elminate to get env with external call")
    }
}
