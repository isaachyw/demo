use crate::asm::instrs_to_string;
use crate::asm::Instr::Mov;
use crate::asm::{Arg32, Arg64, BinArgs, Instr, JmpArg, MemRef, MovArgs, Offset, Reg, Reg32};
use crate::check;
use crate::lift;
use crate::runtime_check::{check_arity, check_overflow, check_type, RuntimeErr, RuntimeType};
use crate::seq;
use crate::syntax::{Exp, FunDecl, ImmExp, Prim, SeqExp, SeqProg, SurfProg, VarOrLabel};
use crate::tag;
use core::panic;
use std::collections::{HashMap, HashSet};
type Tag=u32;
static MAX_SNAKE_INT: i64 = i64::MAX >> 1;
static MIN_SNAKE_INT: i64 = i64::MIN >> 1;
static MASK_BITS: u64 = 0b111;
static NUM_TAG: u64 = 0x00_00_00_00_00_00_00_01;
static BOOL_TAG: u32 = 0b111;
static ARRAY_TAG: u32 = 0b001;
static CLOSURE_TAG: u32 = 0b011;
static BOOL_MASK: u64 = 0x80_00_00_00_00_00_00_00;
static HEAP_MASK: u64 = 0xFF_FF_FF_FF_FF_FF_FF_F8;
static SNAKE_TRUE: u64 = 0xFF_FF_FF_FF_FF_FF_FF_FF;
static SNAKE_FALSE: u64 = 0x7F_FF_FF_FF_FF_FF_FF_FF;

type Space = i32;

#[derive(Debug, PartialEq, Eq)]
pub enum CompileErr<Span> {
    UnboundVariable {
        unbound: String,
        location: Span,
    },
    UndefinedFunction {
        undefined: String,
        location: Span,
    },
    // The Span here is the Span of the let-expression that has the two duplicated bindings
    DuplicateBinding {
        duplicated_name: String,
        location: Span,
    },

    Overflow {
        num: i64,
        location: Span,
    },

    DuplicateFunName {
        duplicated_name: String,
        location: Span, // the location of the 2nd function
    },

    DuplicateArgName {
        duplicated_name: String,
        location: Span,
    },
}

fn space_needed<Ann>(e: &SeqExp<Ann>) -> Space {
    match e {
        SeqExp::Imm(..) | SeqExp::Prim(..) => 0,
        SeqExp::If {
            cond: _, thn, els, ..
        } => std::cmp::max(space_needed(thn), space_needed(els)),
        SeqExp::Let {
            var: _,
            bound_exp,
            body,
            ..
        } => std::cmp::max(1 + space_needed(body), space_needed(bound_exp)),
        SeqExp::FunDefs { decls, body, .. } => {
            let mut max = space_needed(body);
            for decl in decls {
                max = std::cmp::max(max, space_needed(&decl.body));
            }
            max
        }
        SeqExp::InternalTailCall(..) => 0,
        SeqExp::ExternalCall {
            fun: _,
            args,
            is_tail,
            ann: _,
        } => {
            if *is_tail {
                0
            } else {
                args.len() as i32 + 1
            }
        }
        SeqExp::Semicolon { .. } => panic!("semicolon should be eliminated"),
        SeqExp::MakeClosure { .. } => 0,
    }
}

fn space_aligner<Ann>(e: &SeqExp<Ann>) -> Space {
    let base = space_needed(e);
    if base % 2 == 0 {
        8 * base + 8
    } else {
        8 * base
    }
}

fn print_helper(space: Space) -> Vec<Instr> {
    let mut instr: Vec<Instr> = vec![];
    instr.push(Instr::Mov(MovArgs::ToReg(Reg::Rdi, Arg64::Reg(Reg::Rax))));
    instr.push(Instr::Sub(BinArgs::ToReg(Reg::Rsp, Arg32::Signed(space))));
    instr.push(Instr::Call(JmpArg::Label("print_snake_val".to_string())));
    instr.push(Instr::Add(BinArgs::ToReg(Reg::Rsp, Arg32::Signed(space))));
    instr
}

fn get_imm_val(e: &ImmExp, env: &Vec<String>) -> Arg64 {
    match e {
        ImmExp::Num(n) => Arg64::Signed(*n << 1),
        ImmExp::Bool(b) => {
            if *b {
                Arg64::Unsigned(SNAKE_TRUE)
            } else {
                Arg64::Unsigned(SNAKE_FALSE)
            }
        }
        ImmExp::Var(identifier) => Arg64::Mem(MemRef {
            reg: Reg::Rsp,
            offset: Offset::Constant(
                -8 * (env.iter().position(|x| x == identifier).unwrap() as i32 + 1),
            ),
        }),
    }
}

fn compile_im_exp(imme: &ImmExp, env: &Vec<String>) -> Vec<Instr> {
    match imme {
        ImmExp::Num(n) => {
            vec![Instr::Mov(MovArgs::ToReg(Reg::Rax, Arg64::Signed(*n << 1)))]
        }
        ImmExp::Bool(b) => {
            if *b {
                vec![Instr::Mov(MovArgs::ToReg(
                    Reg::Rax,
                    Arg64::Unsigned(SNAKE_TRUE),
                ))]
            } else {
                vec![Instr::Mov(MovArgs::ToReg(
                    Reg::Rax,
                    Arg64::Unsigned(SNAKE_FALSE),
                ))]
            }
        }
        ImmExp::Var(identifier) => {
            vec![Instr::Mov(MovArgs::ToReg(
                Reg::Rax,
                Arg64::Mem(MemRef {
                    reg: Reg::Rsp,
                    offset: Offset::Constant(
                        -8 * (env.iter().position(|x| x == identifier).unwrap() as i32 + 1),
                    ),
                }),
            ))]
        }
    }
}
fn compile_seq_exp(e: &SeqExp<u32>, env: Vec<String>, space: Space) -> (Vec<Instr>, Vec<Instr>) {
    let mut instr_vec = vec![];
    let mut fun_vec = vec![];
    match e {
        SeqExp::Semicolon { .. } => panic!("semicolon should be eliminated"),
        SeqExp::MakeClosure {
            arity,
            label,
            env: fun_env,
            ann: _,
        } => {
            // store the arity
            instr_vec.push(Instr::Mov(MovArgs::ToReg(
                Reg::Rax,
                Arg64::Unsigned(*arity as u64),
            )));
            instr_vec.push(Instr::Mov(MovArgs::ToMem(
                MemRef {
                    reg: Reg::Rcx,
                    offset: Offset::Constant(0),
                },
                Reg32::Reg(Reg::Rax),
            )));
            // store the label
            instr_vec.push(Instr::Mov(MovArgs::ToReg(
                Reg::Rax,
                Arg64::Label(label.clone()),
            )));
            instr_vec.push(Instr::Mov(MovArgs::ToMem(
                MemRef {
                    reg: Reg::Rcx,
                    offset: Offset::Constant(8),
                },
                Reg32::Reg(Reg::Rax),
            )));
            // store the env
            instr_vec.push(Instr::Mov(MovArgs::ToReg(
                Reg::Rax,
                get_imm_val(fun_env, &env),
            )));
            instr_vec.push(Instr::Mov(MovArgs::ToMem(
                MemRef {
                    reg: Reg::Rcx,
                    offset: Offset::Constant(16),
                },
                Reg32::Reg(Reg::Rax),
            )));
            // store the address in rax
            instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::Rax, Arg64::Reg(Reg::Rcx))));
            instr_vec.push(Instr::Add(BinArgs::ToReg(
                Reg::Rax,
                Arg32::Unsigned(CLOSURE_TAG as u32),
            )));
            // move the heap pointer
            instr_vec.push(Instr::Add(BinArgs::ToReg(Reg::Rcx, Arg32::Unsigned(8 * 3))));
        }
        SeqExp::Imm(imx, _) => instr_vec.extend(compile_im_exp(imx, &env)),
        SeqExp::Let {
            var,
            bound_exp,
            body,
            ann: _,
        } => {
            instr_vec.append(&mut compile_seq_exp(bound_exp, env.clone(), space).0);
            fun_vec.append(&mut compile_seq_exp(bound_exp, env.clone(), space).1);
            instr_vec.push(Instr::Mov(MovArgs::ToMem(
                MemRef {
                    reg: Reg::Rsp,
                    offset: {
                        // if the variable is already in the environment, then we need to find the offset
                        // otherwise, we need to push the variable into the environment
                        Offset::Constant(match env.iter().position(|x| x == var) {
                            Some(n) => -8 * (n as i32 + 1),
                            None => -8 * (env.len() as i32 + 1),
                        })
                    },
                },
                Reg32::Reg(Reg::Rax),
            )));
            let mut new_env = env.clone();
            if !new_env.contains(var) {
                new_env.push(var.clone());
            }
            instr_vec.append(&mut compile_seq_exp(body, new_env.clone(), space).0);
            fun_vec.append(&mut compile_seq_exp(body, new_env, space).1);
        }
        SeqExp::If {
            cond,
            thn,
            els,
            ann,
        } => {
            instr_vec.append(&mut compile_im_exp(cond, &env));
            instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
            instr_vec.append(&mut check_type(
                Reg::Rdx,
                RuntimeType::Bool,
                RuntimeErr::IfErr,
            ));
            instr_vec.push(Instr::Mov(MovArgs::ToReg(
                Reg::R8,
                Arg64::Unsigned(SNAKE_FALSE),
            )));
            let else_label = format!("else_{:?}", ann);
            let end_label = format!("end_{:?}", ann);

            instr_vec.push(Instr::Cmp(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
            instr_vec.push(Instr::Je(JmpArg::Label(else_label.clone())));
            instr_vec.append(&mut compile_seq_exp(thn, env.clone(), space).0);
            instr_vec.push(Instr::Jmp(JmpArg::Label(end_label.clone())));
            // add label for else
            instr_vec.push(Instr::Label(else_label));
            instr_vec.append(&mut compile_seq_exp(els, env.clone(), space).0);
            // add label for end
            instr_vec.push(Instr::Label(end_label));
            fun_vec.append(&mut compile_seq_exp(thn, env.clone(), space).1);
            fun_vec.append(&mut compile_seq_exp(els, env.clone(), space).1);
        }
        SeqExp::Prim(op, exps, ann) => {
            match op {
                Prim::CheckArityAndUntag(arity) => {
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::R8,
                        get_imm_val(&exps[0], &env),
                    )));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::R8)))); // use extra copy of rax to check
                    instr_vec.extend(check_type(
                        Reg::Rdx,
                        RuntimeType::Closure,
                        RuntimeErr::ClosureCallErr,
                    ));
                    // substract the tag
                    instr_vec.push(Instr::Sub(BinArgs::ToReg(
                        Reg::R8,
                        Arg32::Unsigned(CLOSURE_TAG as u32),
                    )));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::R8,
                        Arg64::Mem(MemRef {
                            reg: Reg::R8,
                            offset: Offset::Constant(0),
                        }),
                    )));
                    // check arity
                    instr_vec.extend(check_arity(*arity, Reg::R8));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::Rax, Arg64::Reg(Reg::R8))));
                }
                Prim::GetCode => {
                    // move the compiled closure exp to rax
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        get_imm_val(&exps[0], &env),
                    )));
                    // don't need to substract the tag
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Mem(MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Constant(8),
                        }),
                    )));
                }
                Prim::GetEnv => {
                    // move the compiled closure exp to rax
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        get_imm_val(&exps[0], &env),
                    )));
                    // don't need to substract the tag
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Mem(MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Constant(16),
                        }),
                    )));
                }
                Prim::Add1 => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    instr_vec.push(Instr::Add(BinArgs::ToReg(Reg::Rax, Arg32::Signed(1 << 1))));
                    instr_vec.extend(check_overflow());
                }
                Prim::Sub1 => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    instr_vec.push(Instr::Sub(BinArgs::ToReg(Reg::Rax, Arg32::Signed(1 << 1))));
                    instr_vec.extend(check_overflow());
                }
                Prim::Not => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
                    instr_vec.append(&mut check_type(
                        Reg::Rdx,
                        RuntimeType::Bool,
                        RuntimeErr::NotBoolErr,
                    ));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::R8,
                        Arg64::Unsigned(BOOL_MASK),
                    )));
                    instr_vec.push(Instr::Xor(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                }
                Prim::Add => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    // store the left operand result into R8
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.append(&mut compile_im_exp(&exps[1], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    instr_vec.push(Instr::Add(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.extend(check_overflow());
                }
                Prim::Sub => {
                    instr_vec.append(&mut compile_im_exp(&exps[1], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    // store the left operand result into R8
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    instr_vec.push(Instr::Sub(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.extend(check_overflow());
                }
                Prim::Mul => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    // store the left operand result into R8
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.push(Instr::Sar(BinArgs::ToReg(Reg::R8, Arg32::Unsigned(1))));
                    instr_vec.append(&mut compile_im_exp(&exps[1], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::ArithErr,
                    ));
                    instr_vec.push(Instr::Sar(BinArgs::ToReg(Reg::Rax, Arg32::Unsigned(1))));
                    instr_vec.push(Instr::IMul(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.extend(check_overflow());
                    instr_vec.push(Instr::Shl(BinArgs::ToReg(Reg::Rax, Arg32::Unsigned(1))));
                }
                Prim::And => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
                    instr_vec.append(&mut check_type(
                        Reg::Rdx,
                        RuntimeType::Bool,
                        RuntimeErr::BinaryBoolErr,
                    ));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.append(&mut compile_im_exp(&exps[1], &env));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
                    instr_vec.append(&mut check_type(
                        Reg::Rdx,
                        RuntimeType::Bool,
                        RuntimeErr::BinaryBoolErr,
                    ));
                    instr_vec.push(Instr::And(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                }
                Prim::Or => {
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
                    instr_vec.append(&mut check_type(
                        Reg::Rdx,
                        RuntimeType::Bool,
                        RuntimeErr::BinaryBoolErr,
                    ));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.append(&mut compile_im_exp(&exps[1], &env));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to checkinstr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
                    instr_vec.append(&mut check_type(
                        Reg::Rdx,
                        RuntimeType::Bool,
                        RuntimeErr::BinaryBoolErr,
                    ));
                    instr_vec.push(Instr::Or(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                }
                Prim::Lt | Prim::Gt | Prim::Le | Prim::Ge | Prim::Eq => {
                    instr_vec.extend(compile_im_exp(&exps[1], &env));
                    match op {
                        Prim::Eq => {}
                        _ => {
                            instr_vec.append(&mut check_type(
                                Reg::Rax,
                                RuntimeType::Num,
                                RuntimeErr::CmpErr,
                            ));
                        }
                    }
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.append(&mut compile_im_exp(&exps[0], &env));
                    match op {
                        Prim::Eq => {}
                        _ => {
                            instr_vec.append(&mut check_type(
                                Reg::Rax,
                                RuntimeType::Num,
                                RuntimeErr::CmpErr,
                            ));
                        }
                    }
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(SNAKE_TRUE),
                    )));
                    instr_vec.push(match op {
                        Prim::Lt => Instr::Jl(JmpArg::Label(format!("less_than_{}", ann))),
                        Prim::Gt => Instr::Jg(JmpArg::Label(format!("greater_than_{}", ann))),
                        Prim::Le => Instr::Jle(JmpArg::Label(format!("less_equal_{}", ann))),
                        Prim::Ge => Instr::Jge(JmpArg::Label(format!("greater_equal_{}", ann))),
                        Prim::Eq => Instr::Je(JmpArg::Label(format!("equal_{}", ann))),
                        _ => panic!("invalid logic comparison"),
                    });
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(SNAKE_FALSE),
                    )));
                    instr_vec.push(Instr::Label(format!(
                        "{}_{}",
                        match op {
                            Prim::Lt => "less_than",
                            Prim::Gt => "greater_than",
                            Prim::Le => "less_equal",
                            Prim::Ge => "greater_equal",
                            Prim::Eq => "equal",
                            _ => panic!("invalid logic comparison"),
                        },
                        ann,
                    )));
                }
                Prim::IsNum => {
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::R8,
                        Arg64::Unsigned(NUM_TAG),
                    )));
                    instr_vec.push(Instr::And(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.push(Instr::Shl(BinArgs::ToReg(Reg::Rax, Arg32::Unsigned(63))));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::R8,
                        Arg64::Unsigned(SNAKE_TRUE),
                    )));
                    instr_vec.push(Instr::Xor(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                }
                Prim::IsBool | Prim::IsArray | Prim::IsFun => {
                    instr_vec.push(Instr::Comment(String::from("IsBool/IsArray/IsFun")));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::R8,
                        Arg64::Unsigned(MASK_BITS),
                    )));
                    instr_vec.push(Instr::And(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(
                        Reg::Rax,
                        Arg32::Unsigned(match op {
                            Prim::IsBool => BOOL_TAG,
                            Prim::IsArray => ARRAY_TAG,
                            Prim::IsFun => CLOSURE_TAG,
                            _ => panic!("is bool/array/fun failure"),
                        }),
                    )));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(SNAKE_FALSE),
                    )));
                    instr_vec.push(Instr::Jne(JmpArg::Label(format!("ISNOT_{}", ann))));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(SNAKE_TRUE),
                    )));
                    instr_vec.push(Instr::Label(format!("ISNOT_{}", ann)));
                }
                Prim::Print => {
                    instr_vec.extend(compile_im_exp(&exps[0], &env));
                    instr_vec.extend(print_helper(space));
                }
                Prim::Neq => {
                    instr_vec.extend(compile_im_exp(&exps[0], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::CmpErr,
                    ));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::R8, Arg64::Reg(Reg::Rax))));
                    instr_vec.extend(compile_im_exp(&exps[1], &env));
                    instr_vec.append(&mut check_type(
                        Reg::Rax,
                        RuntimeType::Num,
                        RuntimeErr::CmpErr,
                    ));
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(Reg::Rax, Arg32::Reg(Reg::R8))));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(SNAKE_TRUE),
                    )));
                    instr_vec.push(Instr::Jne(JmpArg::Label(format!("not_equal_{}", ann))));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(SNAKE_FALSE),
                    )));
                    instr_vec.push(Instr::Label(format!("not_equal_{}", ann)));
                }
                //////////////////    Array Operations    //////////////////
                Prim::Length => {
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rax, get_imm_val(&exps[0], &env))));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // use extra copy of rax to check
                    instr_vec.append(&mut check_type(
                        Reg::Rdx,
                        RuntimeType::Array,
                        RuntimeErr::LengthNonArrayErr,
                    ));
                    instr_vec.push(Instr::Sub(BinArgs::ToReg(
                        Reg::Rax,
                        Arg32::Unsigned(ARRAY_TAG),
                    )));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Mem(MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Constant(0),
                        }),
                    )));
                }
                Prim::MakeArray => {
                    // size
                    instr_vec.push(Instr::Comment("array".to_string()));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Unsigned(2 * exps.len() as u64),
                        // store 2* size so that don't need extra effort deal with internal num
                    )));
                    instr_vec.push(Instr::Mov(MovArgs::ToMem(
                        MemRef {
                            reg: Reg::Rcx,
                            offset: Offset::Constant(0),
                        },
                        Reg32::Reg(Reg::Rax),
                    )));
                    // data
                    for (i, ele) in exps.iter().enumerate() {
                        instr_vec.push(Instr::Mov(MovArgs::ToReg(
                            Reg::Rax,
                            get_imm_val(&ele, &env),
                        )));
                        instr_vec.push(Instr::Mov(MovArgs::ToMem(
                            MemRef {
                                reg: Reg::Rcx,
                                offset: Offset::Constant(8 * (i as i32 + 1)),
                            },
                            Reg32::Reg(Reg::Rax),
                        )));
                    }
                    // store the address in rax
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(Reg::Rax, Arg64::Reg(Reg::Rcx))));
                    // add tag
                    instr_vec.push(Instr::Add(BinArgs::ToReg(
                        Reg::Rax,
                        Arg32::Unsigned(ARRAY_TAG as u32),
                    )));
                    // move heap pointer
                    instr_vec.push(Instr::Add(BinArgs::ToReg(
                        Reg::Rcx,
                        Arg32::Unsigned(8 + 8 * exps.len() as u32),
                    )));
                    instr_vec.push(Instr::Comment("array end".to_string()));
                }
                Prim::ArrayGet => {
                    // first move the value of array and index to Rax and R8
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rax, get_imm_val(&exps[0], &env))));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::R8, get_imm_val(&exps[1], &env))));
                    // check array type and index type
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rdx, Arg64::Reg(Reg::Rax)))); // rax should not be modify when checking, thus use 14
                    instr_vec.push(Instr::Comment("array get..".to_string()));
                    instr_vec.extend(check_type(
                        Reg::Rdx,
                        RuntimeType::Array,
                        RuntimeErr::IndexNonArrayErr,
                    ));
                    instr_vec.extend(check_type(
                        Reg::R8,
                        RuntimeType::Num,
                        RuntimeErr::IndexNonNumErr,
                    ));
                    // check index out of bound
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rdi,
                        Arg64::Unsigned(RuntimeErr::IndexOutOfBoundsErr as u64),
                    )));
                    instr_vec.push(Instr::Sub(BinArgs::ToReg(
                        Reg::Rax,
                        Arg32::Unsigned(ARRAY_TAG as u32),
                    ))); // substract the tag
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(
                        Reg::R8,
                        Arg32::Mem(MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Constant(0),
                        }),
                    )));
                    instr_vec.push(Instr::Jge(JmpArg::Label(String::from("snake_error"))));
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(Reg::R8, Arg32::Signed(0))));
                    instr_vec.push(Instr::Jl(JmpArg::Label(String::from("snake_error"))));
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Mem(MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Computed {
                                reg: Reg::R8,
                                factor: 4,
                                constant: 8,
                            },
                        }),
                    )));
                }
                Prim::ArraySet => {
                    // first move the value of array and index , new value to Rax and R8, R9
                    instr_vec.push(Instr::Comment("array set".to_string()));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::Rax, get_imm_val(&exps[0], &env))));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::R8, get_imm_val(&exps[1], &env))));
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::R9, get_imm_val(&exps[2], &env))));
                    // check array type and index type
                    instr_vec.push(Mov(MovArgs::ToReg(Reg::R14, Arg64::Reg(Reg::Rax)))); // rax should not be modify when checking, thus use 14
                    instr_vec.extend(check_type(
                        Reg::R14,
                        RuntimeType::Array,
                        RuntimeErr::IndexNonArrayErr,
                    ));
                    instr_vec.push(Instr::Sub(BinArgs::ToReg(
                        Reg::Rax,
                        Arg32::Unsigned(ARRAY_TAG as u32),
                    )));
                    instr_vec.extend(check_type(
                        Reg::R8,
                        RuntimeType::Num,
                        RuntimeErr::IndexNonNumErr,
                    ));
                    // check index out of bound
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rdi,
                        Arg64::Unsigned(RuntimeErr::IndexOutOfBoundsErr as u64),
                    )));
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(
                        Reg::R8,
                        Arg32::Mem(MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Constant(0),
                        }),
                    )));
                    instr_vec.push(Instr::Jge(JmpArg::Label(String::from("snake_error"))));
                    instr_vec.push(Instr::Cmp(BinArgs::ToReg(Reg::R8, Arg32::Signed(0))));
                    instr_vec.push(Instr::Jl(JmpArg::Label(String::from("snake_error"))));
                    // set the new value
                    instr_vec.push(Instr::Mov(MovArgs::ToMem(
                        MemRef {
                            reg: Reg::Rax,
                            offset: Offset::Computed {
                                reg: Reg::R8,
                                factor: 4,
                                constant: 8,
                            },
                        },
                        Reg32::Reg(Reg::R9),
                    )));
                    // add the tag back
                    instr_vec.push(Instr::Add(BinArgs::ToReg(
                        Reg::Rax,
                        Arg32::Unsigned(ARRAY_TAG),
                    )));
                }
            }
        }
        SeqExp::FunDefs { decls, body, .. } => {
            let mut new_env = env.clone();
            for decl in decls {
                new_env.push(decl.name.clone());
            }
            instr_vec.append(&mut compile_seq_exp(body, new_env.clone(), space).0);
            fun_vec.append(&mut compile_seq_exp(body, new_env, space).1);
            for decl in decls {
                let mut param_env = decl.parameters.clone();
                param_env.append(&mut env.clone());
                println! {"param_env: {:?}", param_env};
                let mut new_body = vec![];
                new_body.push(Instr::Label(decl.name.clone()));
                new_body.append(&mut compile_seq_exp(&decl.body, param_env.clone(), space).0);
                fun_vec.append(&mut compile_seq_exp(&decl.body, param_env, space).1);
                new_body.push(Instr::Ret);
                fun_vec.append(&mut new_body);
            }
        }
        SeqExp::InternalTailCall(fun, args, ..) => {
            // load the arguments into the registers
            for (i, arg) in args.iter().enumerate() {
                instr_vec.append(&mut compile_im_exp(arg, &env));
                instr_vec.push(Instr::Mov(MovArgs::ToMem(
                    MemRef {
                        reg: Reg::Rsp,
                        offset: Offset::Constant(-8 * (i as i32 + 1)),
                    },
                    Reg32::Reg(Reg::Rax),
                )));
            }
            // tail call
            instr_vec.push(Instr::Jmp(JmpArg::Label(fun.clone())));
        }
        // don't classify tail or not.. can't solve non-tail call now
        SeqExp::ExternalCall {
            fun, args, is_tail, ..
        } => {
            instr_vec.push(Instr::Comment("external call  re".to_string()));
            // load the arguments into the registers
            for (i, arg) in args.iter().enumerate() {
                instr_vec.append(&mut compile_im_exp(arg, &env));
                instr_vec.push(Instr::Mov(MovArgs::ToMem(
                    MemRef {
                        reg: Reg::Rsp,
                        offset: Offset::Constant(-8 * (i as i32 + 2) - space),
                    },
                    Reg32::Reg(Reg::Rax),
                )));
            }
            // call the function
            instr_vec.push(Instr::Sub(BinArgs::ToReg(Reg::Rsp, Arg32::Signed(space))));
            match fun {
                VarOrLabel::Label(label) => {
                    instr_vec.push(Instr::Call(JmpArg::Label(label.clone())));
                }
                VarOrLabel::Var(identifier) => {
                    instr_vec.push(Instr::Mov(MovArgs::ToReg(
                        Reg::Rax,
                        Arg64::Mem(MemRef {
                            reg: Reg::Rsp,
                            offset: Offset::Constant(
                                -8 * (env.iter().position(|x| x == identifier).unwrap() as i32 + 1),
                            ),
                        }),
                    )));
                    instr_vec.push(Instr::Call(JmpArg::Reg(Reg::Rax)));
                }
            }
            instr_vec.push(Instr::Add(BinArgs::ToReg(Reg::Rsp, Arg32::Signed(space))));
        }
    }
    {
        (instr_vec, fun_vec)
    }
}

pub fn check_prog<Span>(p: &SurfProg<Span>) -> Result<(), CompileErr<Span>>
where
    Span: Clone,
{
    check::check_prog(p)
}

fn uniquify(e: &Exp<u32>) -> Exp<()> {
    tag::uniquify_helper(e, &mut HashMap::new(), &mut HashMap::new())
}

// Parse Calls into either DirectCall or ClosureCall, merge in the pass of uniquify
// fn eliminate_closures<Ann>(e: &Exp<Ann>) -> Exp<()> {
//     eliminate_closures_helper(e, &mut HashSet::new())
// }

// Identify which functions should be lifted to the top level
fn should_lift<Ann>(p: &Exp<Ann>) -> HashSet<String> {
    lift::should_lift(p)
}

// Lift some functions to global definitions
fn lambda_lift(p: &Exp<Tag>) -> (Vec<FunDecl<Exp<()>, ()>>, Exp<()>) {
    lift::lambda_lift(p)
}

fn sequentialize(e: &Exp<u32>) -> SeqExp<()> {
    seq::sequentialize(e)
}

fn seq_prog(decls: &[FunDecl<Exp<u32>, u32>], p: &Exp<u32>) -> SeqProg<()> {
    let main_p = sequentialize(p);
    SeqProg {
        funs: decls
            .iter()
            .map(|decl| {
                let mut new_params = vec![];
                for param in &decl.parameters {
                    new_params.push(param.clone());
                }
                FunDecl {
                    name: decl.name.clone(),
                    parameters: new_params,
                    body: sequentialize(&decl.body),
                    ann: (),
                }
            })
            .collect(),
        main: main_p,
        ann: (),
    }
}

fn compile_main(e: &SeqExp<u32>) -> (Vec<Instr>, Vec<Instr>) {
    let mut main_inst_vec = vec![];
    // main_inst_vec.push(Instr::Push(Arg32::Reg(Reg::Rcx)));
    main_inst_vec.push(Instr::Mov(MovArgs::ToReg(Reg::Rcx, Arg64::Reg(Reg::Rdi))));
    main_inst_vec.push(Instr::Add(BinArgs::ToReg(Reg::Rcx, Arg32::Unsigned(7))));
    main_inst_vec.push(Instr::Mov(MovArgs::ToReg(
        Reg::R8,
        Arg64::Unsigned(HEAP_MASK),
    )));
    main_inst_vec.push(Instr::And(BinArgs::ToReg(Reg::Rcx, Arg32::Reg(Reg::R8))));
    main_inst_vec.push(Instr::Comment(String::from("start main")));
    let (mut instr_vec, fun_vec) = compile_seq_exp(e, vec![], space_aligner(e));
    // instr_vec.push(Instr::Pop(Arg32::Reg(Reg::Rcx)));
    instr_vec.push(Instr::Ret);
    main_inst_vec.extend(instr_vec);
    (main_inst_vec, fun_vec)
}

fn compile_global_decls(e: &FunDecl<SeqExp<u32>, u32>) -> Vec<Instr> {
    let mut instr_vec = vec![];
    let this_env = e.parameters.clone();
    instr_vec.push(Instr::Label(e.name.clone()));
    instr_vec.append(&mut compile_seq_exp(&e.body, this_env, space_aligner(&e.body)).0);
    instr_vec.push(Instr::Ret);
    instr_vec
}

fn compile_to_instrs(p: &SeqExp<u32>) -> Vec<Instr> {
    let id_vec = vec![];
    let mut instr_vec = compile_seq_exp(p, id_vec, space_aligner(p)).0;
    instr_vec.push(Instr::Ret);
    instr_vec
}

pub fn compile_to_string<Span>(p: &SurfProg<Span>) -> Result<String, CompileErr<Span>>
where
    Span: Clone,
{
    let tagggg = tag::tag_exp(p, &mut 0);
    println!("origin exp: {:?}\n", tagggg);
    println!("****************************************");
    check_prog(p)?;
    let uniq_exp = tag::uniquify_helper(
        &tag::tag_exp(p, &mut 0),
        &mut HashMap::new(),
        &mut HashMap::new(),
    );
    let (glob_decl, main_p) = lift::lambda_lift(&tag::tag_exp(&uniq_exp, &mut 0));

    let mut tag_glob_decl = vec![];
    let mut counter = 0;
    glob_decl.iter().for_each(|decl| {
        tag_glob_decl.push(FunDecl {
            name: decl.name.clone(),
            parameters: decl.parameters.clone(),
            body: tag::tag_exp(&decl.body, &mut counter),
            ann: counter,
        });
    });
    println!("tagged global decl");
    for decl in &tag_glob_decl {
        println!("{:?}", decl);
    }

    let tag_main_p = tag::tag_exp(&main_p, &mut counter);
    println!("tagged main\n{:?}", tag_main_p);
    let seq_main_p = seq_prog(&tag_glob_decl, &tag_main_p);
    let tag_seq_main_p = SeqProg {
        funs: seq_main_p
            .funs
            .iter()
            .map(|decl| FunDecl {
                name: decl.name.clone(),
                parameters: decl.parameters.clone(),
                body: tag::tag_seq_exp(&decl.body, &mut counter),
                ann: counter,
            })
            .collect(),
        main: tag::tag_seq_exp(&seq_main_p.main, &mut counter),
        ann: counter,
    };
    let (main_instr, fun_instr) = compile_main(&tag_seq_main_p.main);
    Ok(format!(
        "\
section .data
HEAP:   times 1024 dq 0
section .text
        global start_here
        extern print_snake_val
        extern snake_error
start_here:
{}

notlift_defs:
{}

global_defs:
{}
",
        instrs_to_string(&main_instr),
        instrs_to_string(&fun_instr),
        instrs_to_string(
            &tag_seq_main_p
                .funs
                .iter()
                .map(|decl| compile_global_decls(&decl))
                .flatten()
                .collect::<Vec<Instr>>()
        )
    ))
    // add tag
    // uniquify and desuguaring the semicolon as well as lambda
    // elminate closures(classify call to direct call or closure call)

    // tag again?
    // lift the function to the top level as well as makeclosure or classify direct call to ecall or icall
    // sequentialize the program (desugar the callclosure to ecall)
    // code generation
}
