use crate::compile::CompileErr;
use crate::syntax::{Exp, FunDecl, SurfProg};

static MAX_SNAKE_INT: i64 = i64::MAX >> 1;
static MIN_SNAKE_INT: i64 = i64::MIN >> 1;

fn check_exp<Span>(e: &SurfProg<Span>, var_env: &Vec<String>) -> Result<(), CompileErr<Span>>
where
    Span: Clone,
{
    match e {
        Exp::Num(n, ann) => {
            // check for overflow
            if *n <= MAX_SNAKE_INT && *n >= MIN_SNAKE_INT {
                Ok(())
            } else {
                Err(CompileErr::Overflow {
                    num: *n,
                    location: ann.clone(),
                })
            }
        }
        Exp::Var(x, loc) => {
            if var_env.contains(x) {
                Ok(())
            } else {
                Err(CompileErr::UnboundVariable {
                    unbound: x.clone(),
                    location: loc.clone(),
                })
            }
        }
        Exp::Bool(..) => Ok(()),
        Exp::Let {
            bindings,
            body,
            ann,
        } => {
            // can shadow, but not duplicate
            let mut new_env = vec![];
            for (x, _) in bindings {
                if new_env.contains(x) {
                    return Err(CompileErr::DuplicateBinding {
                        duplicated_name: x.clone(),
                        location: ann.clone(),
                    });
                }
                new_env.push(x.clone());
            }
            let mut this_env = var_env.clone();
            for (x, e) in bindings {
                match check_exp(e, &this_env) {
                    Ok(()) => {}
                    Err(e) => return Err(e),
                }
                if !this_env.contains(x) {
                    // var_env.push(x.clone());
                    this_env.push(x.clone());
                }
            }
            check_exp(body, &this_env)
        }
        Exp::Prim(_, es, _) => {
            for e in es {
                match check_exp(e, var_env) {
                    Ok(()) => {}
                    Err(e) => return Err(e),
                }
            }
            Ok(())
        }
        Exp::If {
            cond,
            thn,
            els,
            ann: _,
        } => {
            match check_exp(cond, var_env) {
                Ok(()) => {}
                Err(e) => return Err(e),
            }
            match check_exp(thn, var_env) {
                Ok(()) => {}
                Err(e) => return Err(e),
            }
            match check_exp(els, var_env) {
                Ok(()) => {}
                Err(e) => return Err(e),
            }
            Ok(())
        }
        Exp::FunDefs {
            decls,
            body,
            ann: _,
        } => {
            let mut env_temp_fun = Vec::new();
            let mut this_env = var_env.clone();
            for FunDecl {
                name,
                parameters,
                body: _,
                ann,
            } in decls.iter()
            {
                let mut env_temp_arg = Vec::new();
                if env_temp_fun.contains(name) {
                    return Err(CompileErr::DuplicateFunName {
                        duplicated_name: name.clone(),
                        location: ann.clone(),
                    });
                } else {
                    env_temp_fun.push(name.clone());
                }
                for arg in parameters.iter() {
                    if env_temp_arg.contains(arg) {
                        return Err(CompileErr::DuplicateArgName {
                            duplicated_name: arg.clone(),
                            location: ann.clone(),
                        });
                    } else {
                        env_temp_arg.push(arg.clone());
                    }
                }
                this_env.push(name.clone());
            }
            for FunDecl {
                parameters, body, ..
            } in decls.iter()
            {
                let mut env_clone = this_env.clone();
                for arg in parameters.iter() {
                    env_clone.push(arg.clone());
                }
                check_exp(body, &env_clone)?;
            }
            check_exp(body, &this_env)
        }
        Exp::Call(_f, es, _ann) => {
            for e in es {
                check_exp(e, var_env)?;
            }
            check_exp(_f, var_env)
        }
        Exp::Lambda {
            parameters,
            body,
            ann,
        } => {
            let mut env_temp = Vec::new();
            for arg in parameters.iter() {
                if env_temp.contains(arg) {
                    return Err(CompileErr::DuplicateArgName {
                        duplicated_name: arg.clone(),
                        location: ann.clone(),
                    });
                } else {
                    env_temp.push(arg.clone());
                }
            }
            let mut env_clone = var_env.clone();
            for arg in parameters.iter() {
                env_clone.push(arg.clone());
            }
            check_exp(body, &env_clone)
        }
        Exp::Semicolon { e1, e2, ann: _ } => {
            match check_exp(e1, var_env) {
                Ok(()) => {}
                Err(e) => return Err(e),
            }
            match check_exp(e2, var_env) {
                Ok(()) => {}
                Err(e) => return Err(e),
            }
            Ok(())
        }
        Exp::MakeClosure { .. }
        | Exp::ClosureCall { .. }
        | Exp::DirectCall { .. }
        | Exp::ExternalCall { .. }
        | Exp::InternalTailCall(..) => {
            panic!("Don't appear in static checking")
        }
    }
}

pub fn check_prog<Span>(p: &SurfProg<Span>) -> Result<(), CompileErr<Span>>
where
    Span: Clone,
{
    check_exp(p, &vec![])
}
