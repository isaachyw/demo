use crate::lift::extract_fun_name;
use crate::syntax::{Exp, FunDecl, ImmExp, Prim, SeqExp};
use std::collections::HashMap;
type Tag = u32;

pub fn is_binary_operation(prim: Prim) -> bool {
    use Prim::*;
    matches!(
        prim,
        Add | Sub | Mul | And | Or | Lt | Gt | Le | Ge | Eq | Neq | ArrayGet
    )
}
pub fn tag_exp<Span>(e: &Exp<Span>, counter: &mut Tag) -> Exp<Tag> {
    let cur_tag = *counter;
    *counter += 1;
    match e {
        Exp::Num(n, _) => Exp::Num(*n, cur_tag),
        Exp::Var(x, _) => Exp::Var(x.clone(), cur_tag),
        Exp::Bool(b, _) => Exp::Bool(*b, cur_tag),
        Exp::Prim(p, es, _) => {
            let mut new_es = vec![];
            for e in es {
                new_es.push(Box::new(tag_exp(e, counter)));
            }
            Exp::Prim(*p, new_es, cur_tag)
        }
        Exp::Let {
            bindings,
            body,
            ann: _,
        } => {
            let mut new_bindings = vec![];
            for (x, e) in bindings {
                new_bindings.push((x.clone(), tag_exp(e, counter)));
            }
            Exp::Let {
                bindings: new_bindings,
                body: Box::new(tag_exp(body, counter)),
                ann: cur_tag,
            }
        }
        Exp::If {
            cond,
            thn,
            els,
            ann: _,
        } => Exp::If {
            cond: Box::new(tag_exp(cond, counter)),
            thn: Box::new(tag_exp(thn, counter)),
            els: Box::new(tag_exp(els, counter)),
            ann: cur_tag,
        },
        Exp::FunDefs {
            decls,
            body,
            ann: _,
        } => {
            let mut new_decls = vec![];
            for decl in decls {
                new_decls.push(FunDecl {
                    name: decl.name.clone(),
                    parameters: decl.parameters.clone(),
                    body: (tag_exp(&decl.body, counter)),
                    ann: cur_tag,
                });
            }
            Exp::FunDefs {
                decls: new_decls,
                body: Box::new(tag_exp(body, counter)),
                ann: cur_tag,
            }
        }
        Exp::Call(fun, args, _ann) => {
            let mut new_args = vec![];
            for arg in args {
                new_args.push(tag_exp(arg, counter));
            }
            Exp::Call(Box::new(tag_exp(fun, counter)), new_args, cur_tag)
        }
        Exp::Lambda {
            parameters,
            body,
            ann: _,
        } => Exp::Lambda {
            parameters: parameters.clone(),
            body: Box::new(tag_exp(&body, counter)),
            ann: cur_tag,
        },
        Exp::Semicolon { e1, e2, ann: _ } => Exp::Semicolon {
            e1: Box::new(tag_exp(e1, counter)),
            e2: Box::new(tag_exp(e2, counter)),
            ann: cur_tag,
        },
        Exp::MakeClosure {
            arity,
            label,
            env,
            ann: _,
        } => Exp::MakeClosure {
            arity: *arity,
            label: label.clone(),
            env: Box::new(tag_exp(env, counter)),
            ann: cur_tag,
        },
        Exp::ClosureCall(fun, args, _ann) => {
            let mut new_args = vec![];
            for arg in args {
                new_args.push(tag_exp(arg, counter));
            }
            Exp::ClosureCall(Box::new(tag_exp(fun, counter)), new_args, cur_tag)
        }
        Exp::DirectCall(fun, args, _ann) => {
            let mut new_args = vec![];
            for arg in args {
                new_args.push(tag_exp(arg, counter));
            }
            Exp::DirectCall(fun.clone(), new_args, cur_tag)
        }
        Exp::InternalTailCall(fun, args, _ann) => {
            let mut new_args = vec![];
            for arg in args {
                new_args.push(tag_exp(arg, counter));
            }
            Exp::InternalTailCall(fun.clone(), new_args, cur_tag)
        }
        Exp::ExternalCall {
            fun,
            args,
            is_tail,
            ann: _,
        } => {
            let mut new_args = vec![];
            for arg in args {
                new_args.push(tag_exp(arg, counter));
            }
            Exp::ExternalCall {
                fun: fun.clone(),
                args: new_args,
                is_tail: *is_tail,
                ann: cur_tag,
            }
        }
    }
}

pub fn tag_seq_exp<Span>(e: &SeqExp<Span>, counter: &mut Tag) -> SeqExp<Tag> {
    let cur_tag = *counter;
    *counter += 1;
    match e {
        SeqExp::Imm(ie, _) => match ie {
            ImmExp::Num(n) => SeqExp::Imm(ImmExp::Num(*n), cur_tag),
            ImmExp::Var(x) => SeqExp::Imm(ImmExp::Var(x.clone()), cur_tag),
            ImmExp::Bool(b) => SeqExp::Imm(ImmExp::Bool(*b), cur_tag),
        },
        SeqExp::Prim(p, ies, _) => {
            let mut new_ies = vec![];
            for ie in ies {
                match ie {
                    ImmExp::Num(n) => new_ies.push(ImmExp::Num(*n)),
                    ImmExp::Var(x) => new_ies.push(ImmExp::Var(x.clone())),
                    ImmExp::Bool(b) => new_ies.push(ImmExp::Bool(*b)),
                }
            }
            SeqExp::Prim(*p, new_ies, cur_tag)
        }
        SeqExp::Let {
            var,
            bound_exp,
            body,
            ann: _,
        } => SeqExp::Let {
            var: var.clone(),
            bound_exp: Box::new(tag_seq_exp(bound_exp, counter)),
            body: Box::new(tag_seq_exp(body, counter)),
            ann: cur_tag,
        },
        SeqExp::If {
            cond,
            thn,
            els,
            ann: _,
        } => SeqExp::If {
            cond: match cond {
                ImmExp::Num(n) => ImmExp::Num(*n),
                ImmExp::Var(x) => ImmExp::Var(x.clone()),
                ImmExp::Bool(b) => ImmExp::Bool(*b),
            },
            thn: Box::new(tag_seq_exp(thn, counter)),
            els: Box::new(tag_seq_exp(els, counter)),
            ann: cur_tag,
        },
        SeqExp::ExternalCall {
            fun, args, is_tail, ..
        } => SeqExp::ExternalCall {
            fun: fun.clone(),
            args: args.clone(),
            is_tail: *is_tail,
            ann: cur_tag,
        },
        SeqExp::InternalTailCall(fun, args, ..) => {
            SeqExp::InternalTailCall(fun.clone(), args.clone(), cur_tag)
        }
        SeqExp::FunDefs { decls, body, .. } => {
            let mut new_decls = vec![];
            for decl in decls {
                new_decls.push(FunDecl {
                    name: decl.name.clone(),
                    parameters: decl.parameters.clone(),
                    body: (tag_seq_exp(&decl.body, counter)),
                    ann: cur_tag,
                });
            }
            SeqExp::FunDefs {
                decls: new_decls,
                body: Box::new(tag_seq_exp(body, counter)),
                ann: cur_tag,
            }
        }
        SeqExp::Semicolon { e1, e2, .. } => SeqExp::Semicolon {
            e1: Box::new(tag_seq_exp(e1, counter)),
            e2: Box::new(tag_seq_exp(e2, counter)),
            ann: cur_tag,
        },
        SeqExp::MakeClosure {
            arity, label, env, ..
        } => SeqExp::MakeClosure {
            arity: *arity,
            label: label.clone(),
            env: env.clone(),
            ann: cur_tag,
        },
    }
}

pub fn uniquify_helper(
    e: &Exp<u32>,
    var_name_map: &HashMap<String, String>,
    fun_name_map: &HashMap<String, String>,
) -> Exp<()> {
    // uniquify the variable name using the tag as well as desugar the lambda expression
    match e {
        Exp::Num(n, _) => Exp::Num(*n, ()),
        Exp::Var(x, _) => Exp::Var(var_name_map.get(x).unwrap().clone(), ()),
        Exp::Bool(b, _) => Exp::Bool(*b, ()),
        Exp::Prim(p, es, _) => {
            let mut new_es = vec![];
            for e in es {
                new_es.push(Box::new(uniquify_helper(e, var_name_map, fun_name_map)));
            }
            Exp::Prim(*p, new_es, ())
        }
        Exp::Let {
            bindings,
            body,
            ann,
        } => {
            let mut new_bindings = vec![];
            let mut new_name_map = var_name_map.clone();
            for (x, e) in bindings {
                let new_x = format!("var_{}{}", x, ann);
                new_bindings.push((
                    new_x.clone(),
                    uniquify_helper(e, &new_name_map, &fun_name_map),
                ));
                new_name_map.insert(x.clone(), new_x.clone());
            }
            Exp::Let {
                bindings: new_bindings,
                body: Box::new(uniquify_helper(body, &new_name_map, &fun_name_map)),
                ann: (),
            }
        }
        Exp::If {
            cond,
            thn,
            els,
            ann: _,
        } => Exp::If {
            cond: Box::new(uniquify_helper(cond, var_name_map, fun_name_map)),
            thn: Box::new(uniquify_helper(thn, var_name_map, fun_name_map)),
            els: Box::new(uniquify_helper(els, var_name_map, fun_name_map)),
            ann: (),
        },
        Exp::Call(f, es, _) => {
            let mut new_es = vec![];
            for e in es {
                new_es.push(uniquify_helper(e, var_name_map, fun_name_map));
            }
            match extract_fun_name(&f) {
                Some(name) => {
                    if fun_name_map.contains_key(&name) {
                        let new_f = fun_name_map.get(&name).unwrap().clone();
                        Exp::DirectCall(new_f.clone(), new_es.clone(), ())
                    } else {
                        let new_f = Box::new(uniquify_helper(f, var_name_map, fun_name_map));
                        Exp::ClosureCall(new_f.clone(), new_es.clone(), ())
                    }
                }
                None => {
                    let new_f = Box::new(uniquify_helper(f, var_name_map, fun_name_map));
                    Exp::ClosureCall(new_f.clone(), new_es.clone(), ())
                }
            }
        }
        Exp::FunDefs { decls, body, ann } => {
            let mut new_decls = vec![];
            let new_var_name_map = var_name_map.clone();
            let mut new_fun_name_map = fun_name_map.clone();
            for decl in decls {
                let new_name = format!("func_{}{}", decl.name, ann);
                new_fun_name_map.insert(decl.name.clone(), new_name.clone());
            }
            for decl in decls {
                let mut each_var_name_map = new_var_name_map.clone();
                let each_fun_name_map = new_fun_name_map.clone();
                let mut rename_params = vec![];
                for params in &decl.parameters {
                    let new_name = format!("args_{}{}", params, ann);
                    each_var_name_map.insert(params.clone(), new_name.clone());
                    rename_params.push(new_name.clone());
                }
                new_decls.push(FunDecl {
                    name: new_fun_name_map.get(&decl.name).unwrap().clone(),
                    parameters: rename_params,
                    body: uniquify_helper(&decl.body, &each_var_name_map, &each_fun_name_map),
                    ann: (),
                });
            }
            Exp::FunDefs {
                decls: new_decls,
                body: Box::new(uniquify_helper(body, &new_var_name_map, &new_fun_name_map)),
                ann: (),
            }
        }
        Exp::Lambda {
            parameters,
            body,
            ann,
        } => {
            let mut unique_params = vec![];
            let uniq_fun_name = format!("lambda_{}", ann);
            let mut new_var_name_map = var_name_map.clone();
            for param in parameters {
                let new_name = format!("lambda_args_{}{}", param, ann);
                unique_params.push(new_name.clone());
                new_var_name_map.insert(param.clone(), new_name.clone());
            }
            let uniq_decls = vec![FunDecl {
                name: uniq_fun_name.clone(),
                parameters: unique_params.clone(),
                body: uniquify_helper(body, &new_var_name_map, fun_name_map),
                ann: (),
            }];
            Exp::FunDefs {
                decls: uniq_decls,
                body: Box::new(Exp::Var(uniq_fun_name.clone(), ())),
                ann: (),
            }
        }
        Exp::Semicolon { e1, e2, ann } => {
            // desugar to let expression
            let new_var_name = format!("semicolon_{}", ann);
            let new_var_name_map = var_name_map.clone();
            let new_e1 = uniquify_helper(e1, &new_var_name_map, fun_name_map);
            let new_e2 = uniquify_helper(e2, &new_var_name_map, fun_name_map);
            Exp::Let {
                bindings: vec![(new_var_name.clone(), new_e1)],
                body: Box::new(new_e2),
                ann: (),
            }
        }
        Exp::MakeClosure { .. }
        | Exp::ClosureCall { .. }
        | Exp::DirectCall { .. }
        | Exp::ExternalCall { .. }
        | Exp::InternalTailCall(..) => {
            panic!("Don't appear in uniquify_helper just after static check")
        }
    }
}
