use crate::syntax::Prim;
use crate::syntax::{Exp, FunDecl, VarOrLabel};
use std::{
    collections::{HashMap, HashSet},
    vec,
};

type Tag = u32;

// should_lift helper, indicate whether a function is called in a non-tail position
// all function should be lifted, only to indicate call can be internal tail call to save stack space
fn should_lift_helper<Ann>(p: &Exp<Ann>, is_tail: bool, set: &mut HashSet<String>) {
    match p {
        Exp::DirectCall(f, ..) => {
            if !is_tail {
                set.insert(f.clone());
            }
        }
        Exp::Call(..)
        | Exp::MakeClosure { .. }
        | Exp::InternalTailCall(..)
        | Exp::ExternalCall { .. }
        | Exp::Semicolon { .. } => {
            panic!("should not appear in should_lift_helper");
        }
        Exp::Num(_, _) | Exp::Bool(_, _) | Exp::Var(_, _) | Exp::ClosureCall(..) => {}
        Exp::Prim(_, es, _) => {
            for e in es {
                should_lift_helper(e, false, set);
            }
        }
        Exp::If { cond, thn, els, .. } => {
            should_lift_helper(cond, false, set);
            should_lift_helper(thn, is_tail, set);
            should_lift_helper(els, is_tail, set);
        }
        Exp::Let { bindings, body, .. } => {
            for (_, e) in bindings {
                should_lift_helper(e, false, set);
            }
            should_lift_helper(body, is_tail, set);
        }
        Exp::FunDefs { decls, body, .. } => {
            // make a copy of the old set
            let new_set = set.clone();
            should_lift_helper(body, is_tail, set);
            // if the set is update, mark all decls as should_lift
            if new_set != *set {
                for decl in decls {
                    set.insert(decl.name.clone());
                }
            }
            for decl in decls {
                should_lift_helper(&decl.body, is_tail, set);
            }
            if new_set != *set {
                for decl in decls {
                    set.insert(decl.name.clone());
                }
            }
            for decl in decls {
                // is decl.name start with "lambda_" add the name to set
                if decl.name.starts_with("lambda_") {
                    set.insert(decl.name.clone());
                }
            }
        }
        Exp::Lambda { .. } => {
            panic!("lambda should not appear in should lift")
        } // must be lifted later
    }
}

// Identify which functions should be lifted to the top level
pub fn should_lift<Ann>(p: &Exp<Ann>) -> HashSet<String> {
    // a function should be lifted if it is called at least once in a non-tail position
    let mut set = HashSet::new();
    should_lift_helper(p, true, &mut set);
    set
}

// a hash map, key is func name, value is vec of string indicate the extra parameter (scope params)
fn lambda_lift_helper(
    p: &Exp<Tag>,
    should_lift_funct: &HashSet<String>, // just for decide internal or external call
    lift_func_params: &HashMap<String, Vec<String>>,
    var_env: &Vec<String>, // should also include the name of the def function
    is_tail: bool,
) -> (Vec<FunDecl<Exp<()>, ()>>, Exp<()>) {
    // return the tuple with global decl and main body
    match p {
        Exp::Semicolon { .. }
        | Exp::Lambda { .. }
        | Exp::MakeClosure { .. }
        | Exp::Call(..)
        | Exp::InternalTailCall(..)
        | Exp::ExternalCall { .. } => {
            panic!("should not appear in lambda_lift_helper");
        }
        Exp::ClosureCall(FunDecl, Args, ..) => {
            // fundecl is a closure
            let mut global_decls = vec![];
            let mut new_args = vec![];
            for arg in Args {
                let (new_global_decls, new_arg) =
                    lambda_lift_helper(arg, should_lift_funct, lift_func_params, var_env, false);
                for new_decl in new_global_decls {
                    global_decls.push(new_decl);
                }
                new_args.push(new_arg);
            }
            let (new_global_decls, new_body_fun) = lambda_lift_helper(
                &FunDecl,
                should_lift_funct,
                lift_func_params,
                var_env,
                is_tail,
            );
            global_decls.extend(new_global_decls);
            let new_ecall = Exp::ExternalCall {
                fun: VarOrLabel::Var("code_ptr".to_string()),
                args: vec![vec![Exp::Var("env".to_string(), ())], new_args.clone()].concat(),
                is_tail: is_tail,
                ann: (),
            };
            let env_exp = Exp::Let {
                bindings: vec![(
                    "env".to_string(),
                    Exp::Prim(
                        Prim::GetEnv,
                        vec![Box::new(Exp::Var("untagged".to_string(), ()))],
                        (),
                    ),
                )],
                body: Box::new(new_ecall),
                ann: (),
            };
            let code_ptr_exp = Exp::Let {
                bindings: vec![(
                    "code_ptr".to_string(),
                    Exp::Prim(
                        Prim::GetCode,
                        vec![Box::new(Exp::Var("untagged".to_string(), ()))],
                        (),
                    ),
                )],
                body: Box::new(env_exp),
                ann: (),
            };
            let untagged_exp = Exp::Let {
                bindings: vec![(
                    "untagged".to_string(),
                    Exp::Prim(
                        Prim::CheckArityAndUntag(Args.len()),
                        vec![Box::new(new_body_fun)],
                        (),
                    ),
                )],
                body: Box::new(code_ptr_exp),
                ann: (),
            };
            // println!("closure call body \n {:?} \n", untagged_exp.clone());
            // transfer the call_closure(f,args) to
            // let untagged = check_arity_and_untag(f, args.len()) in
            // let code_ptr = get_code(untagged) in
            // let env      = get_env(untagged) in
            // ecall(code_ptr, (env; args))

            (global_decls, untagged_exp)
        }
        Exp::FunDefs { decls, body, ann } => {
            let env_name = "env".to_string(); // FIXME:should this be unique?
            let mut global_decls = vec![];
            let mut new_lift_func_params = lift_func_params.clone();
            let mut origin_func_params: HashMap<String, Vec<String>> = HashMap::new();
            // grab all fun name for closure call
            let mut cur_level_func_names: Vec<String> = vec![];
            for decl in decls {
                cur_level_func_names.push(decl.name.clone());
            }
            // push all the extra captured var in to new func params and origin func params
            for decl in decls {
                new_lift_func_params.insert(decl.name.clone(), vec![]);
                origin_func_params.insert(decl.name.clone(), decl.parameters.clone());
                for var in var_env {
                    if !decl.parameters.contains(var) {
                        new_lift_func_params
                            .get_mut(&decl.name)
                            .unwrap()
                            .push(var.clone());
                    }
                }
            }

            for decl in decls {
                let mut new_params = decl.parameters.clone();
                for extra_var in new_lift_func_params.get(&decl.name).unwrap() {
                    new_params.push(extra_var.clone());
                }

                let (new_global_decls, new_body) = lambda_lift_helper(
                    &decl.body,
                    should_lift_funct,
                    &new_lift_func_params,
                    &new_params,
                    is_tail,
                );
                global_decls.push({
                    FunDecl {
                        name: decl.name.clone(),
                        parameters: new_params,
                        body: new_body,
                        ann: (),
                    }
                });
                let extra_params = new_lift_func_params.get(&decl.name).unwrap();
                // create a vector of exp contain array get env 0,1,2,...till the length of extra_params
                let origin_para_exp: Vec<Exp<()>> = origin_func_params
                    .get(&decl.name)
                    .unwrap()
                    .iter()
                    .map(|x| Exp::Var(x.clone(), ()))
                    .collect();
                let mut env_get_exp = vec![];
                for i in 0..extra_params.len() {
                    env_get_exp.push(Exp::Prim(
                        crate::syntax::Prim::ArrayGet,
                        vec![
                            Box::new(Exp::Var(env_name.clone(), ())),
                            Box::new(Exp::Num(i as i64, ())),
                        ],
                        (),
                    ));
                }
                // push the closure version
                let exp = FunDecl {
                    name: decl.name.clone() + &"_closure".to_string(),
                    parameters: [vec![env_name.clone()], decl.parameters.clone()].concat(),
                    body: Exp::ExternalCall {
                        fun: VarOrLabel::Label(decl.name.clone()),
                        args: [env_get_exp, origin_para_exp].concat(),
                        is_tail: is_tail,
                        ann: (),
                    },
                    ann: (),
                };
                // println!("closure version of {:?}", exp.clone());
                global_decls.push(exp.clone());
                global_decls.extend(new_global_decls); // add the lifted function to global decls
            }

            let (new_global_decls, new_body) = lambda_lift_helper(
                body,
                should_lift_funct,
                &new_lift_func_params,
                &var_env,
                is_tail,
            );
            global_decls.extend(new_global_decls); // add the new lifted function from body and grab the new body
                                                   // append the new binding of closure and env as well as the function to be make closure to the body
                                                   // crete a new make array expression, the name should be the env_name and the exps shoud be the extra params while all the fun name should be set to 0/null in case recursion
            let mut make_array_exp: Vec<Box<Exp<()>>> = vec![];
            for extra_var in var_env {
                make_array_exp.push(Box::new(Exp::Var(extra_var.clone(), ())));
            }
            // add the place holder in the make arrexp with length of cur_level_func_names
            for _ in 0..cur_level_func_names.len() {
                make_array_exp.push(Box::new(Exp::Num(0, ())));
            }
            let mut make_closure_exp: Vec<(String, Exp<()>)> = vec![];
            for fun in cur_level_func_names.clone() {
                make_closure_exp.push((
                    fun.clone(),
                    Exp::MakeClosure {
                        arity: origin_func_params.get(&fun).unwrap().len(),
                        label: fun.clone() + &"_closure".to_string(),
                        env: Box::new(Exp::Var(env_name.clone(), ())),
                        ann: (),
                    },
                ));
            }
            let mut final_bindings: Vec<(String, Exp<()>)> = vec![];
            // bind the env
            final_bindings.push((
                env_name.clone(),
                Exp::Prim(crate::syntax::Prim::MakeArray, make_array_exp, ()),
            ));
            final_bindings.extend(make_closure_exp); // bind the extra closure
            let mut set_env_exps :Vec<(String, Exp<()>)>=Vec::new();
            // set the closure of the function in the new body env[3]:=f...start from the length of var_env end at the length of var_env + cur_level_func_names.len(),use semicolon to combine the binding_body and the new_body
            for (i,fun_name) in cur_level_func_names.clone().iter().enumerate() {
                set_env_exps.push(
                    (
                        format!("dum_env[{}]_{}",var_env.len()+i,ann),
                        Exp::Prim(
                            crate::syntax::Prim::ArraySet,
                            vec![
                                Box::new(Exp::Var(env_name.clone(), ())),
                                Box::new(Exp::Num((var_env.len()+i) as i64, ())),
                                Box::new(Exp::Var(fun_name.clone(), ())),
                            ],
                            (),
                        ),
                    )
                );
            }
            let mut final_body = Exp::Let { bindings: set_env_exps, body: Box::new(new_body), ann: () };

            println!("binding_body {:?}\n\n", final_body.clone());
            let binding_body = Exp::Let {
                bindings: final_bindings,
                body: Box::new(final_body),
                ann: (),
            };
            
            (global_decls, binding_body)
        }
        Exp::DirectCall(f, es, ..) => {
            let mut global_decls = vec![];
            let mut new_es = vec![];
            for e in es {
                let (new_global_decls, new_e) =
                    lambda_lift_helper(e, should_lift_funct, &lift_func_params, var_env, false);
                for new_decl in new_global_decls {
                    global_decls.push(new_decl);
                }
                new_es.push(new_e);
            }
            if should_lift_funct.contains(f) {
                // add the var in lift_func_params to the new_es
                for var in lift_func_params.get(f).unwrap() {
                    new_es.push(Exp::Var(var.clone(), ()));
                }
                (
                    global_decls,
                    Exp::ExternalCall {
                        fun: VarOrLabel::Label(f.clone()),
                        args: new_es,
                        is_tail: is_tail,
                        ann: (),
                    },
                )
            } else {
                let mut call_params = vec![];
                call_params.append(&mut new_es);
                for var in var_env {
                    call_params.push(Exp::Var(var.clone(), ()));
                }
                (
                    global_decls,
                    Exp::InternalTailCall(f.clone(), call_params, ()),
                )
            }
        }
        Exp::Num(n, _) => (vec![], Exp::Num(*n, ())),
        Exp::Bool(b, _) => (vec![], Exp::Bool(*b, ())),
        Exp::Var(x, _) => (vec![], Exp::Var(x.clone(), ())),
        Exp::Prim(p, es, _) => {
            let mut global_decls = vec![];
            let mut new_es = vec![];
            for e in es {
                let (new_global_decls, new_e) =
                    lambda_lift_helper(e, should_lift_funct, lift_func_params, var_env, false);
                for new_decl in new_global_decls {
                    global_decls.push(new_decl);
                }
                new_es.push(Box::new(new_e));
            }
            (global_decls, Exp::Prim(*p, new_es, ()))
        }
        Exp::If { cond, thn, els, .. } => {
            let (new_global_decls_cond, new_cond) =
                lambda_lift_helper(cond, should_lift_funct, lift_func_params, var_env, false);
            let (new_global_decls_thn, new_thn) =
                lambda_lift_helper(thn, should_lift_funct, lift_func_params, var_env, is_tail);
            let (new_global_decls_els, new_els) =
                lambda_lift_helper(els, should_lift_funct, lift_func_params, var_env, is_tail);
            let mut global_decls = vec![];
            for new_decl in new_global_decls_cond {
                global_decls.push(new_decl);
            }
            for new_decl in new_global_decls_thn {
                global_decls.push(new_decl);
            }
            for new_decl in new_global_decls_els {
                global_decls.push(new_decl);
            }
            (
                global_decls,
                Exp::If {
                    cond: Box::new(new_cond),
                    thn: Box::new(new_thn),
                    els: Box::new(new_els),
                    ann: (),
                },
            )
        }
        Exp::Let { bindings, body, .. } => {
            let mut global_decls = vec![];
            let mut new_bindings = vec![];
            let mut new_var_env = var_env.clone();
            for (x, e) in bindings {
                let (new_global_decls, new_e) =
                    lambda_lift_helper(e, should_lift_funct, lift_func_params, var_env, false);
                for new_decl in new_global_decls {
                    global_decls.push(new_decl);
                }
                new_bindings.push((x.clone(), new_e));
                new_var_env.push(x.clone());
            }
            let (new_global_decls_body, new_body) = lambda_lift_helper(
                body,
                should_lift_funct,
                lift_func_params,
                &new_var_env,
                is_tail,
            );
            for new_decl in new_global_decls_body {
                global_decls.push(new_decl);
            }
            (
                global_decls,
                Exp::Let {
                    bindings: new_bindings,
                    body: Box::new(new_body),
                    ann: (),
                },
            )
        }
    }
}

// Lift some functions to global definitions
pub fn lambda_lift(p: &Exp<Tag>) -> (Vec<FunDecl<Exp<()>, ()>>, Exp<()>) {
    let should_lift_func = should_lift(p);
    let new_p = p.clone();
    let (glob_decl, main_p) =
        lambda_lift_helper(&new_p, &should_lift_func, &HashMap::new(), &vec![], true);
    (glob_decl, main_p)
}

pub fn extract_fun_name<Ann>(e: &Exp<Ann>) -> Option<String> {
    match e {
        Exp::Var(name, _) => Some(name.clone()),
        _ => None,
    }
}
