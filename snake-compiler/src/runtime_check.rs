use crate::{
    asm::{Arg32, Arg64, BinArgs, Instr, JmpArg, MovArgs, Reg},
    check,
};

static NUM_TAG: u64 = 0x00_00_00_00_00_00_00_01;
static MASK_BITS: u64 = 0b111;
static BOOL_TAG: u32 = 0b111;
static ARRAY_TAG: u32 = 0b001;
static CLOSURE_TAG: u32 = 0b011;
pub enum RuntimeErr {
    ArithErr,
    CmpErr,
    OverflowErr,
    BinaryBoolErr,
    NotBoolErr,
    IfErr,
    IndexNonArrayErr,
    IndexNonNumErr,
    IndexOutOfBoundsErr,
    LengthNonArrayErr,
    ClosureCallErr,
    ArityErr,
}

pub enum RuntimeType {
    Num,
    Bool,
    Array,
    Closure,
}

pub fn check_arity(arity: usize, reg: Reg) -> Vec<Instr> {
    let mut instr: Vec<Instr> = Vec::new();
    instr.push(Instr::Mov(MovArgs::ToReg(
        Reg::Rbx,
        Arg64::Unsigned(arity as u64),
    )));
    instr.push(Instr::Cmp(BinArgs::ToReg(reg, Arg32::Reg(Reg::Rbx))));
    instr.push(Instr::Mov(MovArgs::ToReg(
        Reg::Rdi,
        Arg64::Unsigned(RuntimeErr::ArityErr as u64),
    )));
    instr.push(Instr::Jne(JmpArg::Label("snake_error".to_string())));
    instr
}

pub fn check_overflow() -> Vec<Instr> {
    let mut instr: Vec<Instr> = Vec::new();
    instr.push(Instr::Mov(MovArgs::ToReg(
        Reg::Rdi,
        Arg64::Unsigned(RuntimeErr::OverflowErr as u64),
    )));
    instr.push(Instr::Jo(JmpArg::Label("snake_error".to_string())));
    instr
}

pub fn check_type(reg: Reg, runtime_t: RuntimeType, potential_error: RuntimeErr) -> Vec<Instr> {
    let mut check_instrs: Vec<Instr> = vec![];
    let tem_reg = Reg::Rsi;
    match runtime_t {
        RuntimeType::Num => {
            check_instrs.push(Instr::Mov(MovArgs::ToReg(
                tem_reg,
                Arg64::Unsigned(NUM_TAG),
            )));
            check_instrs.push(Instr::Test(BinArgs::ToReg(reg, Arg32::Reg(tem_reg))));
            check_instrs.push(Instr::Mov(MovArgs::ToReg(
                Reg::Rdi,
                Arg64::Unsigned(potential_error as u64),
            )));
            check_instrs.push(Instr::Jnz(JmpArg::Label("snake_error".to_string())));
        }
        RuntimeType::Array | RuntimeType::Closure | RuntimeType::Bool => {
            check_instrs.push(Instr::Comment("check arrary".to_string()));
            check_3_bits_type(reg, runtime_t, potential_error, &mut check_instrs);
            check_instrs.push(Instr::Comment("check arrayend".to_string()));
        }
    }
    check_instrs
}

fn check_3_bits_type(
    reg: Reg,
    rtype: RuntimeType,
    err_code: RuntimeErr,
    check_instrs: &mut Vec<Instr>,
) {
    let tem_reg = Reg::Rsi;
    check_instrs.push(Instr::Mov(MovArgs::ToReg(
        tem_reg,
        Arg64::Unsigned(MASK_BITS as u64),
    )));
    check_instrs.push(Instr::And(BinArgs::ToReg(reg, Arg32::Reg(tem_reg))));

    check_instrs.push(Instr::Cmp(BinArgs::ToReg(
        reg,
        Arg32::Unsigned(match rtype {
            RuntimeType::Bool => BOOL_TAG,
            RuntimeType::Array => ARRAY_TAG,
            RuntimeType::Closure => CLOSURE_TAG,
            RuntimeType::Num => panic!("should not be num in check 3 bit type"),
        }),
    )));

    check_instrs.push(Instr::Mov(MovArgs::ToReg(
        Reg::Rdi,
        Arg64::Unsigned(err_code as u64),
    )));
    // check_instrs.push(Instr::Pop(Arg32::Reg(Reg::Rcx)));
    check_instrs.push(Instr::Jne(JmpArg::Label("snake_error".to_string())));
    // check_instrs.push(Instr::Push(Arg32::Reg(Reg::Rcx)));
}
