use std::{collections::HashSet, hash::Hash};

#[repr(C)]
#[derive(PartialEq, Eq, Copy, Clone)]
struct SnakeVal(u64);

static INT_TAG: u64 = 0x00_00_00_00_00_00_00_01;
static SNAKE_TRU: SnakeVal = SnakeVal(0xFF_FF_FF_FF_FF_FF_FF_FF);
static SNAKE_FLS: SnakeVal = SnakeVal(0x7F_FF_FF_FF_FF_FF_FF_FF);
static ARRAY_TAG: u64 = 0b001;
static CLOSURE_TAG: u64 = 0b011;
static MASK_BITS: u64 = 0b111;

#[repr(C)]
struct SnakeArray {
    size: u64,
    elts: *const SnakeVal,
}

enum RuntimeErr {
    ArithErr,
    CmpErr,
    OverflowErr,
    BinaryBoolErr,
    NotBoolErr,
    IfErr,
    IndexNonArrayErr,
    IndexNonNumErr,
    IndexOutOfBoundsErr,
    LengthNonArrayErr,
    ClosureCallErr,
    ArityErr,
}

impl RuntimeErr {
    fn from_u64(value: u64) -> Option<Self> {
        match value {
            0 => Some(Self::ArithErr),
            1 => Some(Self::CmpErr),
            2 => Some(Self::OverflowErr),
            3 => Some(Self::BinaryBoolErr),
            4 => Some(Self::NotBoolErr),
            5 => Some(Self::IfErr),
            6 => Some(Self::IndexNonArrayErr),
            7 => Some(Self::IndexNonNumErr),
            8 => Some(Self::IndexOutOfBoundsErr),
            9 => Some(Self::LengthNonArrayErr),
            10 => Some(Self::ClosureCallErr),
            11 => Some(Self::ArityErr),
            _ => None,
        }
    }
}

/* You can use this function to cast a pointer to an array on the heap
 * into something more convenient to access
 *
 */
fn load_snake_array(p: *const u64) -> SnakeArray {
    unsafe {
        let size = *p;
        SnakeArray {
            size,
            elts: std::mem::transmute(p.add(1)),
        }
    }
}

static mut HEAP: [u64; 100000] = [0; 100000];

#[link(name = "compiled_code", kind = "static")]
extern "sysv64" {

    // The \x01 here is an undocumented feature of LLVM that ensures
    // it does not add an underscore in front of the name.
    #[link_name = "\x01start_here"]
    fn compiled_code(heap: *mut u64) -> SnakeVal;
}

// reinterprets the bytes of an unsigned number to a signed number
fn unsigned_to_signed(x: u64) -> i64 {
    i64::from_le_bytes(x.to_le_bytes())
}

fn sprint_snake_val(x: SnakeVal, mut env: HashSet<u64>) -> String {
    if x.0 & INT_TAG == 0 {
        // it's a number
        format!("{}", unsigned_to_signed(x.0) >> 1)
    } else if x == SNAKE_TRU {
        String::from("true")
    } else if x == SNAKE_FLS {
        String::from("false")
    } else if x.0 & MASK_BITS == CLOSURE_TAG {
        String::from("<closure>")
    } else if x.0 & MASK_BITS == ARRAY_TAG {
        if env.contains(&x.0) {
            return String::from("<loop>");
        } else {
            env.insert(x.0.clone());
        }
        let address = x.0 - ARRAY_TAG;
        let pointer = address as *const u64;
        let arr_len = unsafe { *pointer >> 1 };
        let mut arr: String = String::from("[");
        for i in 1..=arr_len {
            let element = unsafe { *((address + i * 8) as *const u64) };
            if i == 1 {
                arr = format!(
                    "{}{}",
                    arr,
                    sprint_snake_val(SnakeVal(element), env.clone())
                );
            } else {
                arr = format!(
                    "{}, {}",
                    arr,
                    sprint_snake_val(SnakeVal(element), env.clone())
                );
            }
        }
        arr = format!("{}{}", arr, "]");
        format!("{}", arr)
    } else {
        format!("Invalid snake value 0x{:x}", x.0)
    }
}

#[export_name = "\x01print_snake_val"]
extern "sysv64" fn print_snake_val(v: SnakeVal) -> SnakeVal {
    println!("{}", sprint_snake_val(v, HashSet::new()));
    v
}

/* Implement the following error function. You are free to change the
 * input and output types as needed for your design.
 *
**/
#[export_name = "\x01snake_error"]
extern "sysv64" fn snake_error(e: u64) -> ! {
    match RuntimeErr::from_u64(e) {
        Some(err) => {
            // Print the error message
            match err {
                RuntimeErr::ArithErr => eprintln!("arithmetic expected a number!"),
                RuntimeErr::CmpErr => eprintln!("comparison expected a number!"),
                RuntimeErr::OverflowErr => eprintln!("overflow!"),
                RuntimeErr::BinaryBoolErr => eprintln!("logic expected a boolean(binary op)!"),
                RuntimeErr::NotBoolErr => eprintln!("logic expected a boolean(not)!"),
                RuntimeErr::IfErr => eprintln!("if expected a boolean!"),
                RuntimeErr::IndexNonArrayErr => eprintln!("indexed into non-array!"),
                RuntimeErr::IndexNonNumErr => eprintln!("index not a number!"),
                RuntimeErr::IndexOutOfBoundsErr => eprintln!("index out of bounds!"),
                RuntimeErr::LengthNonArrayErr => eprintln!("length called with non-array!"),
                RuntimeErr::ClosureCallErr => eprintln!("called a non-function!"),
                RuntimeErr::ArityErr => eprintln!("wrong number of arguments!"),
            }
        }
        None => eprintln!("Unknown error code: {}", e),
    }
    std::process::exit(1);
}

fn main() {
    let output = unsafe { compiled_code(HEAP.as_mut_ptr()) };
    println!("{}", sprint_snake_val(output, HashSet::new()));
}
