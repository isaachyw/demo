#!/bin/bash

# Create a temporary directory for the zip operation
temp_dir=$(mktemp -d)

# Copy the required directories to the temporary directory
cp -r Cargo.toml src/ examples/ tests/ runtime/ "$temp_dir"

# Navigate to the temporary directory
cd "$temp_dir"

# Find and delete files with the specified extensions
find . \( -name "*.o" -o -name "*.a" -o -name "*.s" -o -name "*.exe" \) -delete

# Zip the remaining files
zip -r submission.zip .

# Move the zip file back to the original directory
mv submission.zip "${OLDPWD}"

# Clean up: Go back to original directory and remove temporary directory
cd "${OLDPWD}"
rm -rf "$temp_dir"
