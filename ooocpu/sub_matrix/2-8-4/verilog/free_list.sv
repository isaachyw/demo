/*
make it a CAM, and use head-tail pointer to maintain the issued free register
calm down the linter

NOTE:internal forwaring: prioritize the out reg from retired_reg_in
TESTNOTE: expose the next_stack_ptr to avoid wait for the next cycle, should set reg_needed to 0 when reset
*/
`include "verilog/sys_defs.svh"

module free_list (
    input clock,
    input reset,
    // dispatch
    input [$clog2(`N):0] reg_needed,

    // retire or rollback, both come from rob
    // note: retire and rollback can both add to free list
    input FREE_LIST_ENTRY [`N:0] retired_reg_in,


    //---------------------------------------------------------//


    // dispatch
    output FREE_LIST_ENTRY [`N-1:0] free_reg_out
    `ifdef DEBUG
    ,output DEBUG_FREE_LIST dbg
    `endif

);

  logic [$clog2(`N)+1:0] retired_num;
  logic [$clog2(`N)+1:0] dispatch_num; 

  PHYS_REG_T [(`FREE_LIST_SIZE)-1:0] data, next_data;
  // logic [$clog2(`FREE_LIST_SIZE)-1:0] stack_ptr, next_stack_ptr;
  logic [$clog2(`FREE_LIST_SIZE):0] stack_ptr, next_stack_ptr;

  // retired_number is how many valid bit in the retired_reg_in
  // logic [`N:0] debug_retired_num;
  // always_comb begin

  //     $display("@@@ [time: %d] reg_needed: %d", $time, reg_needed);
  //     $display("@@@ [time: %d] retired_num: %d", $time, retired_num);
  // end
  always_comb begin : cal_dispatch_num_retired_num

    // debug:
      // $display("time: [%d]\tThe incoming retired physical registers are: | %d (valid: %d) | %d (valid: %d) | %d (valid: %d)", $time,
      //           retired_reg_in[0].phy_reg_idx, retired_reg_in[0].valid,
      //           retired_reg_in[1].phy_reg_idx, retired_reg_in[1].valid,
      //           retired_reg_in[2].phy_reg_idx, retired_reg_in[2].valid
      // );



    dispatch_num = 0;
    retired_num  = 0;
    // NOTE: the input retired should be ordered, i.e : valid, valid, invalid
    for (integer i = 0; i <= `N; i = i + 1) begin
      if (retired_reg_in[i].valid) begin
        retired_num = i+1;
      end
      if (reg_needed > i) begin
        dispatch_num = i+1;
      end
    end
    next_stack_ptr = stack_ptr + (dispatch_num - retired_num);
    // assert(next_stack_ptr < `FREE_LIST_SIZE) else 
    //   $display("time: [%d]\tThe stack pointer is: %d, the dispatch_num is: %d, the retired_num is: %d", $time, next_stack_ptr, dispatch_num, retired_num);
  end


  always_comb begin
    next_data = data;
    free_reg_out = '0;
    // won't overflow forever,
    // assert (stack_ptr + reg_needed < `FREE_LIST_SIZE);  // might not work due to overflow..
    if (retired_num>=dispatch_num) begin// provide the needed with the retired and set the remaining retired num back to data
      for (integer i = 0; i < retired_num; i = i + 1) begin
        if (i < dispatch_num) begin
          free_reg_out[i] = retired_reg_in[i];
        end else begin
          next_data[stack_ptr-(i+1-dispatch_num)] = retired_reg_in[i].phy_reg_idx;
        end
      end
    end else begin
      for (integer i = 0; i < dispatch_num; i = i + 1) begin
        if (i < retired_num) begin
          free_reg_out[i] = retired_reg_in[i];
        end else begin
          free_reg_out[i] = {data[stack_ptr+i-retired_num], 1'b1};
        end
      end
    end
    `ifdef DEBUG
    for (integer i=0;i<`N;i++) begin
      $display("%p giving",free_reg_out[i]);
    end
    `endif
  end
// `ifdef DEBUG
//     assign dbg.stack_ptr_out = next_stack_ptr;
//     assign dbg.data_out = next_data;
// `endif
  always_ff @(posedge clock) begin
    if (reset) begin
      stack_ptr <= 0;
      for (PHYS_REG_T i = 0; i < `FREE_LIST_SIZE; i = i + 1) begin
        data[i] <= i + `ARCH_REG_SZ;
      end
    end else begin
      stack_ptr <= next_stack_ptr;
      data <= next_data;
    end
  end
endmodule
