`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"

module alu (
    input             clock,
    input             reset,
    input  ALU_INPUT  alu_input,
    input ROB_REF_T rob_rollback_idx,
    input logic rollback_not_issued,
    input logic curr_cdb_stall,

    output ALU_OUTPUT alu_output
);

  logic next_valid;

  always_comb begin
    next_valid = alu_input.has_input;
    if (rob_rollback_idx.valid & (rob_rollback_idx == alu_input.next_rob_idx)) begin
    // if (rob_rollback_idx.valid & alu_input.next_rob_idx.valid & (alu_input.next_rob_idx.rob_index == rob_rollback_idx.rob_index)) begin
      next_valid = 0;
    end
  end

  logic [`XLEN-1:0] opa_mux_out;
  logic [`XLEN-1:0] opb_mux_out;
  logic signed [`XLEN-1:0] signed_opa_mux_out;
  logic signed [`XLEN-1:0] signed_opb_mux_out;

  logic signed [`XLEN-1:0] signed_phy_reg_val_a;
  logic signed [`XLEN-1:0] signed_phy_reg_val_b;

  logic [`XLEN-1:0] next_result;  // internal "pure alu" result
  logic [`XLEN-1:0] next_ucbr_result;  // alu result for unconditional branch dest reg

  BRANCH_OUTCOME next_br_outcome; // for output
  BRANCH_FEEDBACK next_br_feedback;
  BRANCH_DIRECTION_T alu_br_direction;
  logic alu_br_valid;

  assign alu_br_valid = (alu_input.has_input) & (alu_input.func == BRANCH_UNCOND | alu_input.func == BRANCH_COND);
  assign signed_phy_reg_val_a = alu_input.phy_reg_val_a;
  assign signed_phy_reg_val_b = alu_input.phy_reg_val_b;
  assign signed_opa_mux_out = opa_mux_out;
  assign signed_opb_mux_out = opb_mux_out;

  always_comb begin: drive_mux_out
    // $display("ALU_INPUT: %d %d, ALU OUTPUT: %d", alu_input.phy_reg_val_a, alu_input.phy_reg_val_b, alu_output.result);

    opa_mux_out = `XLEN'hdeadbeef;
    opb_mux_out = `XLEN'hdeadbeef;
    case (alu_input.opa_select)
      OPA_IS_RS1:  opa_mux_out = alu_input.phy_reg_val_a;
      OPA_IS_NPC:  opa_mux_out = alu_input.npc;
      OPA_IS_PC:   opa_mux_out = alu_input.pc;
      OPA_IS_ZERO: opa_mux_out = 0;
    endcase
    case (alu_input.opb_select)
      OPB_IS_RS2:   opb_mux_out = alu_input.phy_reg_val_b;
      OPB_IS_I_IMM: opb_mux_out = `RV32_signext_Iimm(alu_input.inst);
      OPB_IS_S_IMM: opb_mux_out = `RV32_signext_Simm(alu_input.inst);
      OPB_IS_B_IMM: opb_mux_out = `RV32_signext_Bimm(alu_input.inst);
      OPB_IS_U_IMM: opb_mux_out = `RV32_signext_Uimm(alu_input.inst);
      OPB_IS_J_IMM: opb_mux_out = `RV32_signext_Jimm(alu_input.inst);
    endcase
  end

  always_comb begin: compute_pure_alu_result
    case (alu_input.func)
      ALU_ADD:  begin 
        next_result = opa_mux_out + opb_mux_out;

        //DEBUG
        if(alu_input.pc == 'h2d0) begin
          $display("Time: %d\tHere is pc%h ADD instruction where dest is x15, the result is: %h", $time, alu_input.pc, next_result);
        end

        if(alu_input.pc == 212) begin
          $display("Time: %d\tHere is pc%h ADD instruction where dest is x8, the result is: %h", $time, alu_input.pc, next_result);
        end

        if(alu_input.pc == 180) begin
          $display("Time: %d\tHere is pc%h ADDI instruction where dest is x2, the result is: %h", $time, alu_input.pc, next_result);
        end
      end
      BRANCH_COND:  next_result = opa_mux_out + opb_mux_out;
      BRANCH_UNCOND:  begin
        next_result = opa_mux_out + opb_mux_out;
        next_ucbr_result = alu_input.pc + 3'b100;
        //DEBUG
        if(alu_input.pc == 52 || alu_input.pc == 68) begin
          $display("Time: %d\tHere is JAL instruction with pc%h, my result is: %h", $time, alu_input.pc, next_ucbr_result);
        end
        if(alu_input.pc == 'h2d0) begin
          $display("Time: %d\tHere is JALR instruction with pc%h, my result is: %h", $time, alu_input.pc, next_result);
        end
      end
      ALU_SUB:  next_result = opa_mux_out - opb_mux_out;
      ALU_AND:  next_result = opa_mux_out & opb_mux_out;
      ALU_SLT:  next_result = signed_opa_mux_out < signed_opb_mux_out;
      ALU_SLTU: next_result = opa_mux_out < opb_mux_out;
      ALU_OR:   next_result = opa_mux_out | opb_mux_out;
      ALU_XOR:  next_result = opa_mux_out ^ opb_mux_out;
      ALU_SRL:  next_result = opa_mux_out >> opb_mux_out[4:0];
      ALU_SLL:  next_result = opa_mux_out << opb_mux_out[4:0];
      ALU_SRA:  next_result = signed_opa_mux_out >>> opb_mux_out[4:0];  // arithmetic from logical shift
      NOOP:     next_result = 0;
      HALT: begin     next_result = 0; $display("halt!!!"); end
      default:  next_result = `XLEN'hfacebeec;  // here to prevent latches
    endcase
    // $display("ALU input: %p", alu_input);
    // $display("next_result: %h", next_result);

  end

  always_comb begin: compute_br_direction
  
    if (alu_input.func == BRANCH_UNCOND) begin
      
      
      alu_br_direction = TAKEN;
    end else if (alu_input.func == BRANCH_COND) begin
      `ifdef DEBUG
    $display(" signed_phy_reg_val_a: %b \t signed_phy_reg_val_b: %b \t (signed_phy_reg_val_a    >= signed_phy_reg_val_b) ", 
      signed_phy_reg_val_a,
      signed_phy_reg_val_b,
      (signed_phy_reg_val_a    >= signed_phy_reg_val_b) );
      $display("alu_input.phy_reg_val_a: %b \t alu_input.phy_reg_val_b: %b", alu_input.phy_reg_val_a, alu_input.phy_reg_val_b);
      $display("time: %d\t inside branch cond. PC: %d", $time, alu_input.pc);
      `endif
      case (alu_input.inst.b.funct3)
        // BEQ
        3'b000:  begin alu_br_direction = (signed_phy_reg_val_a    == signed_phy_reg_val_b)    ? TAKEN : NOT_TAKEN;
                      `ifdef DEBUG
                       $display("Here is the BEQ branch prediction outcome: %p, because 2 values are: %p %p", alu_br_direction, signed_phy_reg_val_a, signed_phy_reg_val_b);
                      `endif
                 end
        // BNE
        3'b001:  begin alu_br_direction = (signed_phy_reg_val_a    != signed_phy_reg_val_b)    ? TAKEN : NOT_TAKEN;
                       `ifdef DEBUG
                       $display("Here is the Bne branch prediction outcome: %p, because 2 values are: %p %p", alu_br_direction, signed_phy_reg_val_a, signed_phy_reg_val_b);
                       `endif
                 end
        // BLT
        3'b100:  begin
                  alu_br_direction = (signed_phy_reg_val_a    <  signed_phy_reg_val_b)    ? TAKEN : NOT_TAKEN;

                  //DEBUG
                  if(alu_input.pc == 720) begin
                    $display("Time: %d\tHere is the BLT(pc%h) branch prediction outcome: %p, because 2 values are: %h %h", $time, alu_input.pc, alu_br_direction, signed_phy_reg_val_a, signed_phy_reg_val_b);
                  end
                 end
        // BGE
        3'b101:  begin
                    alu_br_direction = (signed_phy_reg_val_a    >= signed_phy_reg_val_b)    ? TAKEN : NOT_TAKEN;

                    //DEBUG
                    if(alu_input.pc == 560) begin
                      $display("Time: %d\tHere is pc230 BGE, my phy_reg_val_a: %h, my phy_reg_val_b: %h", $time, signed_phy_reg_val_a, signed_phy_reg_val_b);
                    end
                 end
        // BLTU
        3'b110:  alu_br_direction = (alu_input.phy_reg_val_a <  alu_input.phy_reg_val_b) ? TAKEN : NOT_TAKEN;
        // BGEU
        3'b111:  alu_br_direction = (alu_input.phy_reg_val_a >= alu_input.phy_reg_val_b) ? TAKEN : NOT_TAKEN;
        default: alu_br_direction = NOT_TAKEN;
      endcase
    end else begin
      alu_br_direction = NOT_TAKEN;
    end
  end

  always_comb begin: conclude_br_output
    next_br_outcome = {
      direction: alu_br_direction,
      target: next_result,
      valid: alu_br_valid
    };
    next_br_feedback = {
      location: alu_input.pc,
      npc: alu_input.npc,
      reality: next_br_outcome,
      predict: alu_input.predict,
      valid: alu_br_valid
    };
  end

  always_ff @(posedge clock) begin
    if (reset) begin
      alu_output <= 0;
    end else begin
      if (rollback_not_issued) begin
        if (rob_rollback_idx.valid & (rob_rollback_idx == alu_output.rob_idx)) begin
          alu_output <= 0;
        end
      end else begin
        // if (alu_input.cdb_stall) begin
        if (curr_cdb_stall) begin
          // if not to be canceled, do nothing, keep the current value
          if (rob_rollback_idx.valid & (rob_rollback_idx == alu_output.rob_idx)) begin
            alu_output <= 0;
          end
        end else begin
          if (alu_input.has_input) begin
            // Update with new value
            if(alu_input.func == BRANCH_UNCOND) begin
              alu_output <= {
                result: next_ucbr_result,
                valid: next_valid,
                has_dest: alu_input.next_has_dest,
                br_feedback: next_br_feedback,
                rob_idx: alu_input.next_rob_idx,
                dst_reg_idx: alu_input.next_dst_reg_idx
                `ifdef DEBUG
                ,rs_entry: alu_input.rs_entry
                `endif
              };
            end else begin
              alu_output <= {
                result: next_result,
                valid: next_valid,
                has_dest: alu_input.next_has_dest,
                br_feedback: next_br_feedback,
                rob_idx: alu_input.next_rob_idx,
                dst_reg_idx: alu_input.next_dst_reg_idx
                `ifdef DEBUG
                ,rs_entry: alu_input.rs_entry
                `endif
              };
            end
          end else begin
            // no new input, not because of rollback
            alu_output <= 0;
          end
        end
      end
    end
  end

endmodule  // alu