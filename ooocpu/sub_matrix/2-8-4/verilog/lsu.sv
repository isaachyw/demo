`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"

module lsu(
    input clock,
    input reset,
    input MEM_INPUT [`N-1:0] mems_input,
    input logic [`N-1:0] lsq_issue_approved,

    output logic [`N-1:0] av_mask,
    output LSQ_ISSUE_REQUEST [`N-1:0] outgoing_req
);

    MEM_OP_T [`N-1:0] mems_op;
    MEM_SIZE [`N-1:0] mems_size;
    REG_DATA_T [`N-1:0] mems_data;
    MEM_ADDR_T [`N-1:0] mems_addr;
    LSQ_ISSUE_REQUEST [`N-1:0] next_outgoing_req;
    LSQ_REQUEST_BASE [`N-1:0] lsq_reqs_base;

    logic [`N-1][`XLEN-1:0] opas_mux_out;
    logic [`N-1][`XLEN-1:0] opbs_mux_out;

    always_comb begin: size_sel
        mems_size = 0;
        for (int i = 0; i < `N; i++) begin
            mems_op[i] = (mems_input[i].rs_entry.inst.func == MEM_RD) ? MEM_READ : MEM_WRITE;
            if (mems_input[i].rs_entry.inst.func == MEM_RD) begin
                case (mems_input[i].rs_entry.inst.inst.i.funct3)
                    3'b000: mems_size[i] = BYTE;
                    3'b001: mems_size[i] = HALF;
                    3'b010: mems_size[i] = WORD;
                    3'b100: mems_size[i] = BYTE;
                    3'b101: mems_size[i] = HALF;
                endcase
            end else if (mems_input[i].rs_entry.inst.func == MEM_WR) begin
                case(mems_input[i].rs_entry.inst.inst.s.funct3)
                    3'b000: mems_size[i] = BYTE;
                    3'b001: mems_size[i] = HALF;
                    3'b101: mems_size[i] = WORD;
                endcase
            end
        end
    end

    always_comb begin
        lsq_reqs_base = 0;
        for (int i = 0; i < `N; i++) begin
            if (mems_input[i].rs_entry.inst.fu == MEM & mems_input[i].rs_entry.valid) begin
                lsq_reqs_base[i] = {
                    rob_idx:        mems_input[i].rs_entry.rob_ref.rob_index,
                    valid:          1,
                    mem_op:         mems_op[i],
                    size:           mems_size[i],
                    is_ld_signed:   mems_input[i].rs_entry.inst.is_ld_signed
                };
            end
        end
    end

    always_comb begin: drive_mux_out
        opas_mux_out = 0;
        opbs_mux_out = 0;
        for (int i = 0; i < `N; i++) begin
            opas_mux_out[i] = mems_input[i].phy_reg_val_a;
            case (mems_input[i].opb_select)
                OPB_IS_I_IMM: opbs_mux_out[i] = `RV32_signext_Iimm(mems_input[i].inst);
                OPB_IS_S_IMM: opbs_mux_out[i] = `RV32_signext_Simm(mems_input[i].inst);
            endcase
            mems_addr[i] = opas_mux_out[i] + opbs_mux_out[i];
            mems_data[i] = mems_input[i].phy_reg_val_b;
        end
    end

    always_comb begin

    end

    always_comb begin
        av_mask = 0;
        for (int i = 0; i < `N; i++) begin
            if (lsq_issue_approved[i] | ~outgoing_req[i].basic_info.valid) begin
                av_mask[i] = 1;
            end
        end
    end

    always_comb begin
        next_outgoing_req = outgoing_req;
        for (int i = 0; i < `N; i++) begin
            if (mems_input[i].valid) begin
                next_outgoing_req[i] = {
                    basic_info:     lsq_reqs_base[i],
                    memory_addr:    mems_addr[i],
                    data:           mems_data[i],
                    dst_reg_idx:    mems_input[i].dst_reg_idx,
                    pc:             mems_input[i].pc
                };
            end
        end
    end

    always_ff @(posedge clock) begin
        if (reset) begin
            outgoing_req <= 0;
        end else begin
            outgoing_req <= next_outgoing_req;
        end
    end

endmodule
