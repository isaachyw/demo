`include "verilog/sys_defs.svh"

module mult_stage (
  input clock,
  input reset,
  input start,
  input cdb_stall,
  input [2*`XLEN-1:0] raw_mplier,
  input [2*`XLEN-1:0] raw_mcand,
  input [2*`XLEN-1:0] prev_sum,
  input MULT_STAGE_PASS mult_stage_pass_in,
  input ROB_REF_T stage_rob_rollback_idx,
  input stage_rollback_not_issued,
  input stage_curr_cdb_stall,

  output logic [2*`XLEN-1:0] next_mplier,
  output logic [2*`XLEN-1:0] next_mcand,
  output logic [2*`XLEN-1:0] product_sum,
  output MULT_STAGE_PASS mult_stage_pass_out,
  output logic done
);

  parameter SHIFT = (2*`XLEN)/`MULT_STAGES;

  logic [2*`XLEN-1:0] shifted_mplier;
  logic [2*`XLEN-1:0] partial_product;
  logic [2*`XLEN-1:0] shifted_mcand;
  logic next_done;

  always_comb begin
    next_done = start;
    if (stage_rob_rollback_idx.valid & (stage_rob_rollback_idx == mult_stage_pass_in.rob_idx)) begin
    // if ((stage_rob_rollback_idx.valid) & (mult_stage_pass_in.rob_idx.valid) & (stage_rob_rollback_idx.rob_index == mult_stage_pass_in.rob_idx.rob_index)) begin
      next_done = 0;
    end
  end

  assign partial_product = raw_mplier[SHIFT-1:0] * raw_mcand; // take lower bits of raw_mplier and shift it to the right
  assign shifted_mplier = {SHIFT'('b0), raw_mplier[2*`XLEN-1:SHIFT]};
  assign shifted_mcand = {raw_mcand[2*`XLEN-1-SHIFT:0], SHIFT'('b0)};

  always_ff @(posedge clock) begin
    if (reset) begin
      done <= 1'b0;
    end else begin
      if (stage_rollback_not_issued) begin
        if (stage_rob_rollback_idx.valid & (stage_rob_rollback_idx == mult_stage_pass_out.rob_idx)) begin
          done <= 0;
        end
      end else begin
        if (stage_curr_cdb_stall) begin
          // if not to be canceled, do nothing, hold current values
          if (stage_rob_rollback_idx.valid & (stage_rob_rollback_idx == mult_stage_pass_out.rob_idx)) begin
            done <= 0;
          end
          `ifdef DEBUG
          $display("time: %d\t mult stall pc: %h done: %b", $time, mult_stage_pass_in.rs_entry.inst.pc, done);
          `endif
        end else begin
          // propagate
          done                  <= next_done;
          product_sum           <= prev_sum + partial_product;
          next_mplier           <= shifted_mplier;
          next_mcand            <= shifted_mcand;
          mult_stage_pass_out   <= mult_stage_pass_in;
        end
      end
    end
  end
endmodule

module mult (
  input clock,
  input reset,
  input MULT_INPUT mult_input,
  input ROB_REF_T rob_rollback_idx,
  input logic rollback_not_issued,
  input logic curr_cdb_stall,

  output MULT_OUTPUT mult_output
  `ifdef DEBUG
  ,output [2*`XLEN-1:0] debug_product
  ,output [2*`XLEN-1:0] debug_extended_mcand
  ,output [2*`XLEN-1:0] debug_extended_mplier
  `endif
);

  logic [2*`XLEN-1:0] raw_product;

  logic [2*`XLEN-1:0] extended_mcand;
  logic [2*`XLEN-1:0] extended_mplier;

  logic [`XLEN-1:0] raw_mcand;
  logic [`XLEN-1:0] raw_mplier;

  logic [`XLEN-1:0] output_result;
  logic output_done;

  `ifdef DEBUG
    assign debug_product = raw_product;
    assign debug_extended_mcand = extended_mcand;
    assign debug_extended_mplier = extended_mplier;
  `endif
  assign raw_mcand = mult_input.opa;
  assign raw_mplier = mult_input.opb;

  MULT_STAGE_PASS output_mult_stage_pass;
  MULT_STAGE_PASS input_mult_stage_pass;

  always_comb begin
    input_mult_stage_pass = {
      rob_idx: mult_input.next_rob_idx,
      dst_reg_idx: mult_input.next_dst_reg_idx,
      func: mult_input.func
      `ifdef DEBUG
      ,rs_entry: mult_input.rs_entry
      `endif
    };
  end

  always_comb begin
    case (mult_input.func)
      MULT_MUL: begin
        extended_mcand = $signed(raw_mcand);
        extended_mplier = $signed(raw_mplier);
      end
      MULT_MULH: begin
        extended_mcand = $signed(raw_mcand);
        extended_mplier = $signed(raw_mplier);
      end
      MULT_MULHSU: begin
        extended_mcand = $signed(raw_mcand);
        extended_mplier = $unsigned(raw_mplier);
      end
      MULT_MULHU: begin
        extended_mcand = $unsigned(raw_mcand);
        extended_mplier = $unsigned(raw_mplier);
      end
      default: begin
        extended_mcand = 0;
        extended_mplier = 0;
      end
    endcase
  end

  always_comb begin
    case (output_mult_stage_pass.func)
      MULT_MUL: begin
        output_result = raw_product[`XLEN-1:0];
      end
      MULT_MULH: begin
        output_result = raw_product[2*`XLEN-1:`XLEN];
      end
      MULT_MULHSU: begin
        output_result = raw_product[2*`XLEN-1:`XLEN];
      end
      MULT_MULHU: begin
        output_result = raw_product[2*`XLEN-1:`XLEN];
      end
      default: begin
        output_result = 0;
      end
    endcase
  end

  always_comb begin
    // $display("[MULT_RS] %p", mult_input.rs_entry);
    mult_output = {
      result:      output_result,
      done:        output_done,
      rob_idx:     output_mult_stage_pass.rob_idx,
      dst_reg_idx: output_mult_stage_pass.dst_reg_idx
      
      `ifdef DEBUG
      ,rs_entry:   output_mult_stage_pass.rs_entry
      `endif 
    };

    `ifdef DEBUG
      $display("time: [%d]\tMULT_DONE: %d\tFU_MULT\tPC:%h\twrite arch reg #%d, phys reg #%d result: %h", $time, mult_output.done,  mult_output.rs_entry.inst.pc, 
                    mult_output.rs_entry.inst.inst.r.rd, 
                    mult_output.rs_entry.dest_reg,
                    mult_output.result);
    `endif

  end

  logic [`MULT_STAGES-2:0] internal_dones;
  logic [(2*`XLEN*(`MULT_STAGES-1))-1:0] internal_mcands;
  logic [(2*`XLEN*(`MULT_STAGES-1))-1:0] internal_mpliers;
  logic [(2*`XLEN*(`MULT_STAGES-1))-1:0] internal_product_sums;
  logic [2*`XLEN-1:0] mcand_out;  // unused, just for wiring
  logic [2*`XLEN-1:0] mplier_out;  // unused, just for wiring

  MULT_STAGE_PASS [`MULT_STAGES-2:0] internal_mult_stage_passes;

  // instantiate an array of mult_stage modules
  // this uses concatenation syntax for internal wiring, see lab 2 slides
  mult_stage mstage[`MULT_STAGES-1:0] (
      .clock                    (clock),
      .reset                    (reset),
      .start                    ({internal_dones, mult_input.start}),           // forward prev done as next start
      .prev_sum                 ({internal_product_sums, {2*`XLEN{1'b0}}}),    // start the sum at 0
      .raw_mplier               ({internal_mpliers, extended_mplier}),
      .raw_mcand                ({internal_mcands, extended_mcand}),
      .mult_stage_pass_in       ({internal_mult_stage_passes, input_mult_stage_pass}),
      .stage_rob_rollback_idx   (rob_rollback_idx),
      .stage_rollback_not_issued(rollback_not_issued),
      .stage_curr_cdb_stall     (curr_cdb_stall),
      .product_sum              ({raw_product, internal_product_sums}),
      .next_mplier              ({mplier_out, internal_mpliers}),
      .next_mcand               ({mcand_out, internal_mcands}),
      .done                     ({output_done, internal_dones}),             // done when the final stage is done
      .mult_stage_pass_out      ({output_mult_stage_pass, internal_mult_stage_passes})
  );

  `ifdef DEBUG
  always_comb begin : debug
    $display("time: [%d], compact output done is %d, interal dones are: %b", $time, mult_output.done, internal_dones);
  end
  `endif
endmodule