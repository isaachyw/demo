`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"

// Rollback Mode:
// When any valid hazard_ref passed in, the ROB should flip to an rollback mode, 
// rolling back from the instruction that is closest to the head_reg pointer.
// In rollback mode, on every cycle A SINGLE instruction is reverted (removed from table),
// until the tail_reg pointer is before the offending branch instruction. For every reverted
// instruction, it emits the instruction's ref type out from the cancellation_broadcast port.
module rob (
    input clock,
    input reset,

    // Dispatch:
    // For populating new instructions, should be 0 when in rollback mode
    output NUM_INST_T available_entry_sig,
    input ROB_PACKET [`N-1:0] new_inst_supplies,
    // This is the handle that can use to refer to instructions in rob
    output ROB_REF_T [`N-1:0] new_inst_refs,

    // INST in RS

    // Execute (end of Execute stage, beginning of Complete stage):
    input ROB_REF_T [`N-1:0] executed_inst_rob_index,

    // Retire
    input logic stall_store_retire,               // stall retire when store queue asks
    output ROB_PACKET [`N-1:0] retired_inst,
    output NUM_INST_T num_retire_load_store_inst, // for update load store queue

    // Hazard (branch squash)
    input PC_T [`N-1:0] hazard_target,
    input BRANCH_OUTCOME [`N-1:0] branch_outcomes_fu_man,
    input ROB_REF_T [`N-1:0] hazard_ref,
    // Cancel(/rollback) one instruction at a time if hazard
    output ROB_REF_T cancellation_broadcast,
    // Map table access for rollback
    output MAP_TABLE_WRITE_COMMAND rollback_mt_write,
    // free physical reg when rollback
    output PHYS_REG_T phy_reg_to_free,
    // for update PC
    output BRANCH_OUTCOME hazard_branch_outcome,
    // for FU rollback
    output ROB_REF_T fu_rollback_rob_index,
    output PC_T rollback_pc,
    // for load store queue
    output logic rollback_load_store_inst

    // DEBUG
    `ifdef DEBUG
    ,output DEBUG_ROB dbg
    ,input [`PHYS_REG_SZ-1:1][`XLEN-1:0] regfile_in
    `endif

);
/****** registers *******/
  logic [$clog2(`ROB_SZ):0] head_reg, tail_reg; // Give head_reg and tail_reg extra bit to prevent overflow
  ROB_ENTRY [`ROB_SZ-1:0] ROB_content ;
  /** branch rollback **/
  logic rollback_enabled_reg;
  logic [$clog2(`ROB_SZ):0] rollback_branch_index_reg;
  NUM_INST_T available_entry_reg;


/****** wires *******/
  // ROB_ENTRY [`N-1:0] next_rob_entry ;
  ROB_ENTRY [`ROB_SZ-1:0] next_ROB_content;
  NUM_INST_T available_entry_normal_sig;

  logic signed [$clog2(`N)+1:0] tail_offset; // b/c rollback, need tail_reg to be signed
  logic [$clog2(`N):0] head_offset;
  logic [$clog2(`ROB_SZ):0] next_head, next_tail;  // avoid overflow
  logic [$clog2(`ROB_SZ):0] next_head_real, next_tail_real;
  logic [$clog2(`ROB_SZ):0] next_valid_ROB_entry_cnt_wire;
  logic [$clog2(`ROB_SZ):0] rob_entry_cnt;
  RETIRED_INST_T [`N-1:0] retired_inst_rob_index_array_sig;

  /** branch rollback **/
  logic next_rollback_enabled_wire;
  logic [$clog2(`ROB_SZ):0]   rollback_branch_index_wire;
  logic   edge_case_rollback_index_is_tail;

  /// debug print
  // reset
  // move head_reg and tail_reg
  always_ff @(posedge clock) begin
    if (reset) begin
      head_reg  <= 0;
      tail_reg  <= 0;
      available_entry_reg <= `ROB_SZ;
      rollback_enabled_reg <= 0;
      rollback_branch_index_reg <= 0;
      ROB_content <= '0;
    end else begin
      head_reg <= next_head_real;
      tail_reg <= next_tail_real;
      available_entry_reg <= available_entry_normal_sig;
      rollback_enabled_reg <= next_rollback_enabled_wire; /// TODO: check this out
      rollback_branch_index_reg <= rollback_branch_index_wire;
      ROB_content <= next_ROB_content;
    end



  end


  /******************
                       combinational logic
                                           ******************/
// logic [$clog2(`ROB_SZ):0] [`N-1:0] tmp_rob_index_head;
logic [`N-1 : 0] [$clog2(`ROB_SZ):0] tmp_rob_index_head;
logic [`N-1 : 0] [$clog2(`ROB_SZ):0] tmp_rob_index_tail;
// logic [$clog2(`ROB_SZ):0] [`N-1:0] tmp_rob_index_tail;
// logic [$clog2(`ROB_SZ):0] [`N-1:0]tmp_rob_index_tail_tmp;

// logic [$clog2(`ROB_SZ):0] tail_update_dbg;

always_comb begin: locate_tail
  tmp_rob_index_tail = '0;
  `ifdef  DEBUG
  for (int i=0;i<`N;i++) begin
    $display("time: %d [ROB]BEFORE tmp_rob_index_tail[%d]: %d\tROB_SZ: %d\ttail_reg: %d", $time, i, tmp_rob_index_tail[i], `ROB_SZ, tail_reg);
  end
  `endif
  for (int i = 0; i < `N; i++) begin
    if (available_entry_reg == `ROB_SZ) begin
      tmp_rob_index_tail[i] = ((tail_reg + i) >= `ROB_SZ) ? (tail_reg + i - `ROB_SZ) : (tail_reg + i);
    end else begin
      tmp_rob_index_tail[i] = ((tail_reg + i + 1) >= `ROB_SZ) ? (tail_reg + i + 1 - `ROB_SZ) : (tail_reg + i + 1);
      // $display("time: %d [ROB]DEBUG tmp_rob_index_tail[%d]: %d\tROB_SZ: %d\tavailable_entry_reg:%d\ttail_reg: %d\tomg: %d", $time, i, tmp_rob_index_tail[i], `ROB_SZ, available_entry_reg, tail_reg, tail_reg + i + 1);
    end
    // $display("time: %d [ROB]INSIDE tmp_rob_index_tail[%d]: %d\tROB_SZ: %d\tavailable_entry_reg:%d\ttail_reg: %d\tCalculation: %d", $time, i, tmp_rob_index_tail[i], `ROB_SZ, available_entry_reg, tail_reg, tail_reg + i + 1);
  end

  `ifdef DEBUG
  for (int i=0;i<`N;i++) begin
    $display("time: %d [ROB]AFTER tmp_rob_index_tail[%d]: %d\tROB_SZ: %d\ttail_reg: %d\t(tail_reg + i + 1): %d\t(tail_reg + i + 1) >= `ROB_SZ: %d", $time, i, tmp_rob_index_tail[i], `ROB_SZ, tail_reg, (tail_reg + i + 1), ((tail_reg + i + 1) >= `ROB_SZ));
  end
  `endif
end

logic [`N-1:0]  allow_retire;
logic [`N-1:0]  rollback_sig_tmp;
logic           has_retired_one_store;
logic [`N-1:0]  is_store_inst;
logic [`N-1:0]  is_load_inst;
always_comb begin: update_next_ROB_content
  next_ROB_content = ROB_content;
  /** retire stage **/
  // initialize head_offset
  head_offset = 0;
  tmp_rob_index_head = '0;
  retired_inst = '0;
  allow_retire = '0;
  rollback_sig_tmp = '0;
  has_retired_one_store = '0;
  is_store_inst = '0;
  is_load_inst = '0;
  num_retire_load_store_inst = 0;
  for (int unsigned i = 0; i < `N; i++) begin
    tmp_rob_index_head[i] = ((head_reg + i) >= `ROB_SZ) ? (head_reg + i - `ROB_SZ) : (head_reg + i);
    // if ROB is in rollback stage then cannot retire the branch which is rollback target

    rollback_sig_tmp[i] = ( ( (rollback_enabled_reg | next_rollback_enabled_wire) & (tmp_rob_index_head[i] == rollback_branch_index_wire) ) == 0);
    casez(ROB_content[tmp_rob_index_head[i]].inst.inst)
      `RV32_SB, `RV32_SH, `RV32_SW: begin
          is_store_inst[i] = 1;
      end
      `RV32_LB, `RV32_LH, `RV32_LW, `RV32_LBU, `RV32_LHU: begin
          is_load_inst[i] = 1;
      end
      default: begin
          is_store_inst[i] = 0;
          is_load_inst[i] = 0;
      end
    endcase

    allow_retire[i] =  rollback_sig_tmp[i] &
                       (ROB_content[tmp_rob_index_head[i]].has_completed == 1) &
                       (ROB_content[tmp_rob_index_head[i]].valid == 1);
    `ifdef DEBUG
    $display("allow retire[%d]: %d",i, allow_retire[i]);
    `endif
    if (is_store_inst[i] == 1) begin
      if ( (has_retired_one_store == 1) | (stall_store_retire == 1) ) begin
        allow_retire[i] = 0;
      end
    end
    `ifdef DEBUG
    $display("time: %d\tallow retire[%d]: %d\tis_store_inst: %d\tis_load_inst: %d",$time,i, allow_retire[i], is_store_inst[i], is_load_inst[i]);
    `endif
    if ( allow_retire[i] ) begin
      head_offset = i + 1;
      if (is_load_inst[i] | is_store_inst[i]) begin
        num_retire_load_store_inst = num_retire_load_store_inst + 1;    // can this sythnesize?
      end
      // retire must be in order
      retired_inst[i] = '{
        inst: ROB_content[tmp_rob_index_head[i]].inst,
        newT: ROB_content[tmp_rob_index_head[i]].newT,
        oldT: ROB_content[tmp_rob_index_head[i]].oldT,
        arch_reg_idx: ROB_content[tmp_rob_index_head[i]].arch_reg_idx,
        valid: 1
      };
      next_ROB_content[tmp_rob_index_head[i]] = 0; // clear retired slot

    `ifdef DEBUG
      $display("time: %d ROB RETIRE INSIDE: PC: %h | retired write arch reg: %d | value: %h", $time, retired_inst[i].inst.pc, retired_inst[i].arch_reg_idx, regfile_in[retired_inst[i].newT]);
    `endif 
      // can't retire anything after WFI
      if (retired_inst[i].inst.inst == `WFI) begin
        break;
      end
      if (is_store_inst[i] == 1) begin
        has_retired_one_store = 1;
      end

    end else begin
      // all instruction after this point cannot retire (some inst in the middle is stuck)
      break;
    end

  end

  /** complete stage **/
  // if (~rollback_enabled_reg) begin
  for (int unsigned i = 0; i < `N; i++) begin
    if (executed_inst_rob_index[i].valid & next_ROB_content[executed_inst_rob_index[i].rob_index].valid ) begin
      next_ROB_content[executed_inst_rob_index[i].rob_index].has_completed = 1; 
      `ifdef DEBUG
        $display("time: %d\texecuted_inst_rob_index[%d].valid: %d\trob_idx: %d", $time,i, executed_inst_rob_index[i].valid, executed_inst_rob_index[i].rob_index);
      `endif  
    end

  end


  /** rollback stage **/

  for (int unsigned i=0; i < `N; i++) begin
    if (hazard_ref[i].valid) begin
      assert(branch_outcomes_fu_man[i].valid);
      next_ROB_content[hazard_ref[i].rob_index].branch_outcome = branch_outcomes_fu_man[i];
      next_ROB_content[hazard_ref[i].rob_index].hazard_target = hazard_target[i];
      `ifdef DEBUG
      $display("[ROB BRANCH INPUT]\ttime: %d branch_outcomes_fu_man[%d].valid: %d | target: %h | direction: %p", $time, i, branch_outcomes_fu_man[i].valid, branch_outcomes_fu_man[i].target, branch_outcomes_fu_man[i].direction);
      `endif
    end
  end

  if (rollback_enabled_reg) begin
      // when first encountering of rollback branch, set its completed bit to one so that after rollback it can be retired immediately
      next_ROB_content[rollback_branch_index_reg].has_completed = 1;

      // clear instructions to be undo
      if (edge_case_rollback_index_is_tail == 0) begin
        next_ROB_content[tail_reg] = '0;
      end
  end

  /** dispatch stage **/
  tail_offset = 0;
  new_inst_refs = '0;
  for (int i = 0; i < `N; i++) begin
    if (new_inst_supplies[i].valid == 1) begin
      `ifdef DEBUG
        $display("time:%d\t[ROB]\tnew_inst_supplies[%d].pc: %h",$time, i, new_inst_supplies[i].inst.pc, i);
        // $display("fu: %p\tfunc: %p",new_inst_supplies[i].inst.fu, new_inst_supplies[i].inst.func);
      // $display("rob: [i:%d]\ttmp_rob_index_tail[i]: %d", i, tmp_rob_index_tail[i]);
      `endif
      next_ROB_content[tmp_rob_index_tail[i]].inst = new_inst_supplies[i].inst;
      // Not determined until end of EXE stage
      next_ROB_content[tmp_rob_index_tail[i]].branch_outcome = '0;
      next_ROB_content[tmp_rob_index_tail[i]].oldT = new_inst_supplies[i].oldT;
      next_ROB_content[tmp_rob_index_tail[i]].newT = new_inst_supplies[i].newT;
      next_ROB_content[tmp_rob_index_tail[i]].arch_reg_idx = new_inst_supplies[i].arch_reg_idx;
      next_ROB_content[tmp_rob_index_tail[i]].rob_index = tmp_rob_index_tail[i];
      next_ROB_content[tmp_rob_index_tail[i]].valid = 1;
      next_ROB_content[tmp_rob_index_tail[i]].has_executed  = 0;
      next_ROB_content[tmp_rob_index_tail[i]].has_completed = 0;
      tail_offset =  i + 1;
      // output newly created ROB index
      new_inst_refs[i].rob_index = tmp_rob_index_tail[i];
      new_inst_refs[i].valid     = 1;
    end else break;
  end


end




  /* branch resolution */

  // find branch closest to head_reg
  logic signed [$clog2(`ROB_SZ)+1:0] tail_offset_rollback;
  logic [$clog2(`ROB_SZ)+1:0] earliest_brach_index_diff_wire;
  logic [$clog2(`ROB_SZ)+1:0] current_brach_index_diff_wire;
  logic [$clog2(`ROB_SZ)+1:0] in_rollback_index_diff_wire;
  logic [$clog2(`ROB_SZ)+1:0] cmp_tmp;
  logic                       elongated_rollback_enabled_reg; // when rollbck index need to be updated during rollback (younger branch hazard)
  logic                       hazard_detected ;

  always_comb begin
    earliest_brach_index_diff_wire = `ROB_SZ + 1;
    current_brach_index_diff_wire  = 0;
    hazard_detected                = 0;
    elongated_rollback_enabled_reg = 0;

    // if (rollback_enabled_reg == 0) begin
    //     for (int i = 0; i < `N; i++) begin
    //       // $display("rob: [i:%d]\thazard_ref[i].rob_index: %d, valid: %d", i, hazard_ref[i].rob_index, hazard_ref[i].valid);
    //       if (hazard_ref[i].valid) begin
    //           hazard_detected            = 1;
    //           current_brach_index_diff_wire = (hazard_ref[i].rob_index >= head_reg) ? (hazard_ref[i].rob_index - head_reg) : (`ROB_SZ - (head_reg - hazard_ref[i].rob_index));
    //           earliest_brach_index_diff_wire = (earliest_brach_index_diff_wire < current_brach_index_diff_wire) ? earliest_brach_index_diff_wire : current_brach_index_diff_wire;
    //           // $display("rob: [i:%d]\thazard_ref[i].rob_index: %d",i, hazard_ref[i].rob_index);
    //           // $display("rob: current_brach_index_diff_wire: %d\tearliest_brach_index_diff_wire: %d",current_brach_index_diff_wire,earliest_brach_index_diff_wire);
    //       end
    //     end
    // end

    for (int i = 0; i < `N; i++) begin
      // $display("rob: [i:%d]\thazard_ref[i].rob_index: %d, valid: %d", i, hazard_ref[i].rob_index, hazard_ref[i].valid);
      if (hazard_ref[i].valid) begin
        if (rollback_enabled_reg == 0) begin
            hazard_detected            = 1;
            current_brach_index_diff_wire = (hazard_ref[i].rob_index >= head_reg) ? (hazard_ref[i].rob_index - head_reg) : (`ROB_SZ - (head_reg - hazard_ref[i].rob_index));
            earliest_brach_index_diff_wire = (earliest_brach_index_diff_wire < current_brach_index_diff_wire) ? earliest_brach_index_diff_wire : current_brach_index_diff_wire;
        end else begin
            current_brach_index_diff_wire = (hazard_ref[i].rob_index >= head_reg) ? (hazard_ref[i].rob_index - head_reg) : (`ROB_SZ - (head_reg - hazard_ref[i].rob_index));
            in_rollback_index_diff_wire = (rollback_branch_index_reg >= head_reg) ? (rollback_branch_index_reg - head_reg) : (`ROB_SZ - (head_reg - rollback_branch_index_reg));
            if (current_brach_index_diff_wire < in_rollback_index_diff_wire) begin
               hazard_detected = 1;
               elongated_rollback_enabled_reg = 1;
               `ifdef DEBUG
                $display("time: %d\t[ROB] younger branch hazard later detected at rob idx: %d", $time, hazard_ref[i].rob_index);
               `endif 
            end
            cmp_tmp = (current_brach_index_diff_wire < in_rollback_index_diff_wire) ? current_brach_index_diff_wire : in_rollback_index_diff_wire;
            earliest_brach_index_diff_wire = (earliest_brach_index_diff_wire < cmp_tmp) ? earliest_brach_index_diff_wire : cmp_tmp;
        end

        // $display("rob: [i:%d]\thazard_ref[i].rob_index: %d",i, hazard_ref[i].rob_index);
        // $display("rob: current_brach_index_diff_wire: %d\tearliest_brach_index_diff_wire: %d",current_brach_index_diff_wire,earliest_brach_index_diff_wire);
      end
    end
    `ifdef DEBUG
    for (int i = 0; i < `N; i++) begin
        if (hazard_ref[i].valid) $display("time: %d has branch hazard at rob idx: %d", $time, hazard_ref[i].rob_index);
    end
    `endif
  end

  // determine branch rollback index
  assign  rollback_branch_index_wire = (hazard_detected == 0) ?                                    rollback_branch_index_reg:
                                       ((head_reg + earliest_brach_index_diff_wire ) >= `ROB_SZ) ? head_reg + earliest_brach_index_diff_wire - `ROB_SZ : 
                                                                                                   head_reg + earliest_brach_index_diff_wire;

  // for FU rollback
  assign fu_rollback_rob_index = (rollback_enabled_reg) ? {rob_index: tail_reg, valid: 1} : '0;


  always_comb begin
    tail_offset_rollback       = '0;
    next_rollback_enabled_wire = rollback_enabled_reg;


    // if @param earliest_brach_index_diff_wire has changed, then there must be some branch to be squashed
    if (hazard_detected == 1) begin
        next_rollback_enabled_wire = 1;
    end



  /* can affect critical path (change rollback_enabled_reg)*/
  cancellation_broadcast = '0;
  rollback_mt_write = '0;
  phy_reg_to_free = '0;
  hazard_branch_outcome = '0;
  edge_case_rollback_index_is_tail = '0;
  rollback_pc = '0;
   rollback_load_store_inst = '0;
  if (rollback_enabled_reg == 1) begin
      if ( (tail_reg == rollback_branch_index_reg) & (elongated_rollback_enabled_reg == 0) ) begin
          /// edge case: current tail is the branch to rollback
          next_rollback_enabled_wire = 0;
          hazard_branch_outcome = ROB_content[rollback_branch_index_reg].branch_outcome;
          rollback_pc = ROB_content[rollback_branch_index_reg].inst.pc;
          tail_offset_rollback = '0;
          edge_case_rollback_index_is_tail = 1;
      end else begin
        assert(head_reg != tail_reg) // ROB cannot be empty if there are branch to be rolled back
        /// rollback current tail_reg
        /// No need to check relative position of head_reg and tail_reg, b/c tail_reg always moves upwards
        /// but need to take care of special cases: tail_reg at beginning of ROB
        // to RS
        cancellation_broadcast.valid = 1;
        cancellation_broadcast.rob_index = tail_reg;
        // to map table
        rollback_mt_write.phy_reg_idx = ROB_content[tail_reg].oldT;
        rollback_mt_write.arch_reg_idx = ROB_content[tail_reg].arch_reg_idx;
        rollback_mt_write.ready = 1;    // TODO: check this out!
        rollback_mt_write.valid = (ROB_content[tail_reg].arch_reg_idx == 0) ? 0 : 1;
        // to free list
        phy_reg_to_free = ROB_content[tail_reg].newT;
        // to load store queue if current is load or store
        casez(ROB_content[tail_reg].inst.inst)
          `RV32_SB, `RV32_SH, `RV32_SW,
          `RV32_LB, `RV32_LH, `RV32_LW, `RV32_LBU, `RV32_LHU: begin
              rollback_load_store_inst = 1;
          end
          default: begin
              rollback_load_store_inst = 0;
          end
        endcase
        // update tail_reg offset
        tail_offset_rollback = (tail_reg == 0) ? `ROB_SZ - 1 : -1; // round to bottom if tail_reg is currently at top
        // terminate rollback if next cycle tail_reg will be at branch
        if ((tail_reg + tail_offset_rollback == rollback_branch_index_reg) & (elongated_rollback_enabled_reg == 0) ) begin
            next_rollback_enabled_wire = 0;
            hazard_branch_outcome = ROB_content[rollback_branch_index_reg].branch_outcome;
            hazard_branch_outcome.target = ROB_content[rollback_branch_index_reg].hazard_target;
            rollback_pc = ROB_content[rollback_branch_index_reg].inst.pc;
        end
    end

    if (elongated_rollback_enabled_reg == 1) begin
        next_rollback_enabled_wire = 1;
    end

    `ifdef DEBUG
    $display("[ROB ROLLBACK] [time:%d]\tPC:%d\tfunc:%p", $time, ROB_content[tail_reg].inst.pc, ROB_content[tail_reg].inst.func);
    `endif
  end

end


  logic [$clog2(`ROB_SZ)+1:0]  next_tail_overflow;
  always_comb begin : update_next_tail
    if ((tail_offset == 0) & (rollback_enabled_reg == 0)) next_tail_overflow = tail_reg; // no instruction went into ROB during time not in rollback
    else if (available_entry_reg == `ROB_SZ) next_tail_overflow = tail_reg + tail_offset - 1;
    else if (rollback_enabled_reg == 1)  next_tail_overflow = tail_reg + tail_offset_rollback;
    else next_tail_overflow = tail_reg + tail_offset;
    // $display("rollback_enabled_reg: %d\tnext_tail_overflow: %d\ttail_offset_rollback: %d\ttail_reg: %d\ttail_reg + tail_offset_rollback: %d",rollback_enabled_reg, next_tail_overflow,tail_offset_rollback, tail_reg,tail_reg + tail_offset_rollback);
    next_tail = (next_tail_overflow >= `ROB_SZ) ? next_tail_overflow - `ROB_SZ : next_tail_overflow;
  end

  always_comb begin: update_next_head
    next_head = (head_reg + head_offset) >=`ROB_SZ? (head_reg + head_offset - `ROB_SZ) : (head_reg + head_offset);
    // make sure when retiring instructions, head_reg doesn't go ahead of tail_reg if next cycle all ROB entries will be freed
    // if (available_entry_reg == `ROB_SZ) next_head = next_tail;
  end

  always_comb begin: update_real
    next_head_real = head_reg;
    next_tail_real = tail_reg;
    if (available_entry_normal_sig == `ROB_SZ) begin
      next_head_real = next_tail;
      next_tail_real = next_tail;
    end else begin
      next_head_real = next_head;
      next_tail_real = next_tail;
    end
  end

  // check whether the rob is actually is_uninitialized_reg since last reset -- alaric

  // calculate valid ROB entry count: based on current state (not next)
  assign available_entry_normal_sig = (edge_case_rollback_index_is_tail == 0) ? available_entry_reg + head_offset - tail_offset + rollback_enabled_reg :
                                                                                available_entry_reg + head_offset - tail_offset;
  assign available_entry_sig = (rollback_enabled_reg) ? 0 : available_entry_reg;
  

  `ifdef DEBUG
  assign dbg.ROB_content_out      = ROB_content;
  assign dbg.head_out             = head_reg;
  assign dbg.tail_out             = tail_reg;
  assign dbg.next_rob_entry_out   = next_ROB_content;
  assign dbg.next_head_out        = next_head_real;
  assign dbg.next_tail_out        = next_tail_real;
  assign dbg.tail_offset_out      = tail_offset;
  assign dbg.rollback_branch_index_reg_out = rollback_branch_index_reg;
  assign dbg.rollback_enabled_reg_out = rollback_enabled_reg;
  assign dbg.available_entry_reg_out = available_entry_reg;
  assign dbg.next_rollback_enabled_wire_out = next_rollback_enabled_wire;
  assign dbg.rollback_branch_index_reg = rollback_branch_index_reg;
  assign dbg.hazard_detected = hazard_detected;
  assign dbg.tail_offset_rollback = tail_offset_rollback;

  always_comb begin
    $display("time: %d [ROB CONTENT] head: %d | tail: %d | available_entry_reg: %d | available_entry_sig: %d | available_entry_normal_sig: %d | is_rollback_reg: %d | rollback_wire: %d",$time, 
    next_head_real, next_tail_real, available_entry_reg, available_entry_sig, available_entry_normal_sig, rollback_enabled_reg, next_rollback_enabled_wire);
    $display("head_reg: %d | tail_reg: %d", head_reg, tail_reg);
    for (int i = 0; i < `ROB_SZ; i++) begin
        if (next_ROB_content[i].valid) begin
            $display("idx: %d\tdecodede_inst.pc: %h\tarch_reg:%d\tT_new:%d\tT_old:%d\tinst: %d\tfunc: %p\tfu: %p\tcompleted: %d",
             next_ROB_content[i].rob_index, 
             next_ROB_content[i].inst.pc, 
             next_ROB_content[i].inst.inst.r.rd, 
             next_ROB_content[i].newT, 
             next_ROB_content[i].oldT,
             next_ROB_content[i].inst.inst, 
             next_ROB_content[i].inst.func, 
             next_ROB_content[i].inst.fu, 
             next_ROB_content[i].has_completed
             );
        end
    end
  end
  `endif

endmodule