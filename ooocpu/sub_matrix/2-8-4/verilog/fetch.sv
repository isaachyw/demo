`timescale 1ns / 100ps
`include "verilog/sys_defs.svh"

module fetch (
    input clock,
    input reset,
    output FETCH_RES [`N-1:0] fetch_res_out,  // come from inst_buffer

    // To mmu, by pass the icache signal
    output logic [      1:0] fetch2Imem_command,
    output logic [`XLEN-1:0] fetch2Imem_addr,

    // From mmu, by pass the icache signal
    input [3:0] Imem2fetch_response,  // Should be zero unless there is a response
    input [63:0] Imem2fetch_data,
    input [3:0] Imem2fetch_tag,
    input d_request,  // Whether dcache is requesting memory, from mmu
    /*************  DISPATCH **************/
    input logic [$clog2(`INST_BUFFER_SZ):0] dispatch_count,


    /**************  BACKEND **************/
    input BRANCH_OUTCOME rollback,
    input PC_T rollback_pc,
    // use the rollback.valid to clear the inst_buffer
    // use the rollback.target act as next start pc
    input BP_UPDATE [`N-1:0] branch_update
    `ifdef DEBUG
    , output FETCH_RES [`N-1:0] to_inst_buffer
    , output FETCH_RES [`N-1:0] next_fetch_out
    , output jump_unresolved_out
    , output next_jump_unresolved_out
    , output FETCH_RES [`N-1:0]fetch_no_pred_out
    , output logic [`N-1:0] Icache_valid_out_debug
    `endif

);
  logic [$clog2(`N)-1:0] jump_inst_idx_reg, next_jump_inst_idx;// log the last jump inst idx, so that
  // the next fetch should be fetch_no_pred until that's idx's valid is 1 to turn off the resolved flag
  logic jump_unresolved, next_jump_unresolved, jump_valid_fetch;
  FETCH_RES [`N-1:0]
      fetch_reg,fetch_ref, fetch_no_pred, next_fetch;  // store the current fetched pc and next pc
  logic [`N-1:0] icache_valids;
  INST [`N-1:0] icache_insts;
  BP_QUERY [`N-1:0] bp_query;
  BP_OUTPUT [`N-1:0] bp_output;
  FETCH_RES [`N-1:0] fetch_res,fetch_res_final;  // output to inst buffer

  logic [$clog2(`INST_BUFFER_SZ)+1:0] slot_num_to_fill;
  PC_T start_n_jump,start_addr;
  logic [$clog2(`INST_BUFFER_SZ):0] available_slot_num;

  //assign the valid bit to the fetch_res based on icache valids as well as the previous icache_valids
`ifdef DEBUG
assign to_inst_buffer = fetch_res_final;
assign next_fetch_out = next_fetch;
assign jump_unresolved_out = jump_unresolved;
assign next_jump_unresolved_out = next_jump_unresolved;
assign fetch_no_pred_out = fetch_no_pred;
always_comb begin
  Icache_valid_out_debug = icache_valids;
end
`endif
  always_comb begin : assign_bp_query
    bp_query = '0;
    for (integer i = 0; i < `N; i++) begin
      bp_query[i].pc = fetch_reg[i].pc;
      bp_query[i].valid = 1;
    end
  end

  always_comb begin
    jump_valid_fetch = 1;
    for(integer i=0;i<=jump_inst_idx_reg;i++)begin
      if(!icache_valids[i])begin
        jump_valid_fetch = 0;
        break;
      end
    end
    if(jump_inst_idx_reg>=slot_num_to_fill)begin
      jump_valid_fetch = 0;
    end
  end

  always_comb begin
    fetch_ref = fetch_reg;
    foreach(fetch_ref[i])begin
      fetch_ref[i].inst = icache_insts[i];
      fetch_ref[i].branch_prediction = bp_output[i];
    end
    if(jump_unresolved&&~jump_valid_fetch)begin// if jump unresolved, and it is not valid to give the jump insturction, then don't give anyone
      for(integer i=0;i<`N;i++)begin
        fetch_ref[i].valid=0;
      end
    end
    else begin
      for (integer i = 0; i < `N; i++) begin
        fetch_ref[i].valid = icache_valids[i];
        if(!fetch_ref[i].valid)begin// if one is invalid, the following should be invalid
          for(integer j=i+1;j<`N;j++)begin
            fetch_ref[j].valid=0;
          end
          break;
        end
      end
  end
  end

  always_comb begin 
    fetch_res = fetch_ref;
    for(integer i=0;i<`N;i++)begin
      if(bp_output[i].direction)begin
        for(integer j=i+1;j<`N;j++)begin // if one is taken, the following should be invalid
          fetch_res[j].valid=0;
        end
        break;
      end
      else if(i==slot_num_to_fill)begin
        for(integer j=i;j<`N;j++)begin // if one is taken, the following should be invalid
          fetch_res[j].valid=0;
        end
        break;
      end
    end
  end

  always_comb begin 
    fetch_res_final=fetch_res;
    if(next_jump_unresolved)begin
      for(integer i=0;i<`N;i++)begin
        fetch_res_final[i].valid=0;
      end
    end
  end

/****************** next pc calculation ******************/
  always_comb begin : cal_start_addr
    // should always equal last valid res pc + 4
    start_n_jump = fetch_res_final[0].pc;
      for (integer i = 0; i < `N; i++) begin
        if (fetch_res_final[i].valid) begin
          if(!bp_output[i].direction)begin
            start_n_jump = fetch_res[i].pc + 4;
          end
        end
      end
      start_addr=start_n_jump;
      for(integer i=0;i<`N;i++)begin
        if(bp_output[i].direction&&fetch_res_final[i].valid)begin
          start_addr = bp_output[i].target;
          break;
        end
      end

  end

  assign slot_num_to_fill = available_slot_num >= `N ? `N : available_slot_num;
  always_comb begin : determin_fetch_no_pred
    // determine based on fetch_res, as well as slot_num
    // ensure the next_fetch num not exceed the available_slot_num
    // in fact, the next_fetch num should be at most available_slot_num
    fetch_no_pred = fetch_reg;
    for (integer i = 0; i < `N; i++) begin
      fetch_no_pred[i].valid =1;
      fetch_no_pred[i].pc = start_addr + i * 4;
    end
  end

  always_comb begin : determine_jump_unresolved
    next_jump_inst_idx = jump_inst_idx_reg;
    next_jump_unresolved = jump_unresolved;
    if (jump_unresolved)begin
      next_jump_unresolved = ~fetch_res[jump_inst_idx_reg].valid;
    end
    else begin// detect new jump unresolved
      for(integer i=0;i<`N;i++)begin
        if(bp_output[i].direction&&~fetch_res_final[i].valid)begin
          next_jump_unresolved = 1;
          next_jump_inst_idx = i;
          break;
        end
      end
    end
  end

  always_comb begin : determin_next_fetch
    // finally determine next fetch valid and pc based on the bp_output
    next_fetch = fetch_reg;
    if(next_jump_unresolved)begin
      next_fetch=fetch_reg;// if jump unresolved, keep the fetch_reg
    end
    // if first unresolved jump or no unresloved at all, set next based on prediction
    else begin// jump inst will be dispatch, good to go target
      next_fetch = fetch_no_pred;
    end
  end

  icache icache_inst (
      .clock(clock),
      .reset(reset),
      .d_request(d_request),
      .Imem2proc_response(Imem2fetch_response),
      .Imem2proc_data(Imem2fetch_data),
      .Imem2proc_tag(Imem2fetch_tag),
      .proc2Icache_addr(fetch_reg),
      .proc2Imem_command(fetch2Imem_command),
      .proc2Imem_addr(fetch2Imem_addr),
      .Icache_valid_out(icache_valids),
      .Icache_inst_out(icache_insts)
`ifdef DEBUG,
      .icache_data_out()
`endif
  );
  branch_predictor bp (
      .clock(clock),
      .reset(reset),
      .bp_query(bp_query),
      .bp_output(bp_output),
      .bp_update(branch_update)
`ifdef DEBUG,
      .bh_table_out(),
      .btb_table_out()
`endif
  );
  inst_buffer instruction_buffer (
      .clock(clock),
      .reset(reset),
      .rollback(rollback.valid),
      .fetch_res(fetch_res_final),
      .dispatch_count(dispatch_count),  // use for free up inst_buffer
      .available_slot_num(available_slot_num),
      .fetch_res_out(fetch_res_out)
`ifdef DEBUG,
      .debug_head(),
      .debug_size(),
      .debug_instruction_buffer()
`endif
  );

`ifdef DEBUG
always_comb begin
    $display("%d time: %4d | result:{resV:%b%b%b|icache:%b%b%b |pc: %3h, %3h, %3h |next:%3h, %3h, %3h |no_pred:%3h%3h%3h|r: %b%b|t:tt%btt%btt%b|idx:%d,%b |inst:%h,%h,%h}"
       ,slot_num_to_fill,$time, to_inst_buffer[0].valid,to_inst_buffer[1].valid,
      to_inst_buffer[2].valid,
      icache_valids[0],icache_valids[1],icache_valids[2],
       to_inst_buffer[0].pc,to_inst_buffer[1].pc,to_inst_buffer[2].pc
      ,next_fetch[0].pc,next_fetch[1].pc,next_fetch[2].pc,
      fetch_no_pred[0].pc,fetch_no_pred[1].pc,fetch_no_pred[2].pc,
      jump_unresolved,next_jump_unresolved,bp_output[0].direction,bp_output[1].direction,bp_output[2].direction,
      jump_inst_idx_reg,jump_valid_fetch,
      fetch_res_final[0].inst,fetch_res_final[1].inst,fetch_res_final[2].inst);
end
`endif


  always_ff @(posedge clock) begin
    // `ifdef DEBUG
    // $display("%d time: %4d | result:{resV:%b%b%b|icache:%b%b%b |pc: %3h, %3h, %3h |next:%3h, %3h, %3h |no_pred:%3d%3d%3d|r: %b%b|t:tt%btt%btt%b|idx:%d,%b |inst%d, %d}"
    //    ,slot_num_to_fill,$time, to_inst_buffer[0].valid,to_inst_buffer[1].valid,
    //   to_inst_buffer[2].valid,
    //   icache_valids[0],icache_valids[1],icache_valids[2],
    //    to_inst_buffer[0].pc,to_inst_buffer[1].pc,to_inst_buffer[2].pc
    //   ,next_fetch[0].pc,next_fetch[1].pc,next_fetch[2].pc,
    //   fetch_no_pred[0].pc,fetch_no_pred[1].pc,fetch_no_pred[2].pc,
    //   jump_unresolved,next_jump_unresolved,bp_output[0].direction,bp_output[1].direction,bp_output[2].direction,
    //   jump_inst_idx_reg,jump_valid_fetch,
    //   fetch_res_final[0].inst,fetch_res_final[1].inst);
    // `endif
    if (reset||rollback.valid) begin
      `ifdef DEBUG
      if (rollback.valid)begin
        $display("rollllll %b,%d",rollback.direction,rollback_pc);
      end
      `endif
      jump_unresolved <= 0;
      jump_inst_idx_reg <= 0;
      for (integer i = 0; i < `N; i++) begin
        fetch_reg[i].valid <= 1;
        fetch_reg[i].branch_prediction <= 0;
        fetch_reg[i].pc <= rollback.valid? (rollback.direction? rollback.target + 4 * i :rollback_pc + 4+4*i): 4 * i;
        fetch_reg[i].inst <= 0;
      end
    end else begin
      fetch_reg <= next_fetch;
      jump_unresolved <= next_jump_unresolved;
      jump_inst_idx_reg <= next_jump_inst_idx;
    end
  end
endmodule