`include "verilog/sys_defs.svh"

// Pure combinational

module mmu(
    input        [3:0]       mem2mmu_response,
    input        [3:0]       mem2mmu_tag,
    input        [63:0]      mem2mmu_data,

    output logic [1:0]       mmu2mem_command,
    output logic [`XLEN-1:0] mmu2mem_addr,
    output logic [63:0]      mmu2mem_data,

    output logic [3:0]       mmu2icache_response,
    output logic [3:0]       mmu2icache_tag,
    output logic [63:0]      mmu2icache_data,

    input        [1:0]       icache2mmu_command,
    input        [`XLEN-1:0] icache2mmu_addr,

    output logic [3:0]       mmu2dcache_response,
    output logic [3:0]       mmu2dcache_tag,
    output logic [63:0]      mmu2dcache_data,

    input        [1:0]       dcache2mmu_command,
    input        [`XLEN-1:0] dcache2mmu_addr,
    input        [63:0]      dcache2mmu_data,

    output logic             d_request
);

    assign d_request = dcache2mmu_command != BUS_NONE;//FIXME: change back when dcache is ready

    //pass through
    assign  mmu2icache_response = d_request?0:mem2mmu_response;
    assign  mmu2dcache_response = mem2mmu_response;

    assign  mmu2icache_tag = mem2mmu_tag;
    assign  mmu2dcache_tag = mem2mmu_tag;

    assign  mmu2icache_data = mem2mmu_data;
    assign  mmu2dcache_data = mem2mmu_data;

    always_comb begin
        if (dcache2mmu_command != BUS_NONE) begin
            mmu2mem_command = dcache2mmu_command;
            mmu2mem_addr = dcache2mmu_addr;
            mmu2mem_data = dcache2mmu_data;
        end
        else if (icache2mmu_command != BUS_NONE) begin
            mmu2mem_command = icache2mmu_command;
            mmu2mem_addr = icache2mmu_addr;
            mmu2mem_data = 0;
        end
        else begin
            mmu2mem_command = BUS_NONE;
            mmu2mem_addr = 0;
            mmu2mem_data = 0;
        end

        `ifdef  DEBUG
        $display("mem command: %p", mmu2mem_command);
         if (mmu2mem_command == BUS_STORE) begin
             $display("time: %d \t write mem addr: %h val: %d", $time, dcache2mmu_addr, mem2mmu_data);
         end
        `endif

    end


endmodule
