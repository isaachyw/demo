`timescale 1ns / 100ps

`include "verilog/sys_defs.svh"
// a bimodal branch predictor support 3-way branch prediction
// coupled with branch target buffer
// fetch unit should discard post-instruction if one of the prediction is taken
module branch_predictor (
    input clock,
    input reset,

    //fetch
    input  BP_QUERY  [`N-1:0] bp_query,
    output BP_OUTPUT [`N-1:0] bp_output,

    //update, come from execute
    input BP_UPDATE [`N-1:0] bp_update

    `ifdef DEBUG
    ,output BP_STATE [`BHT_SZ-1:0] bh_table_out,
    output BTB_ENTRY [`BTB_SET-1:0] btb_table_out
    `endif
);
  logic last_miss_resolved;// the first instruction predicted to be taken has valid in the icache

  BP_STATE [`BHT_SZ-1:0] bh_table, updated_bh_table;
  BTB_ENTRY [`BTB_SET-1:0] btb_table, updated_btb_table;// enable update and predict in same cycle
  // update the branch predictor
  logic [`BTB_SET-1:0] btb_valid,next_btb_valid;
  logic [`N-1:0] btb_hit;
  PC_T [`N-1:0] btb_hit_pc;
  BRANCH_DIRECTION_T [`N-1:0] bp_direction;

`ifdef DEBUG
  assign bh_table_out = updated_bh_table;
  assign btb_table_out = updated_btb_table;
`endif


always_comb begin : gen_bp_updater
    updated_bh_table = bh_table;
    foreach (bp_update[i]) begin
      if (bp_update[i].valid) begin
        priority case (bh_table[bp_update[i].pc[`BHT_W+1:2]])
          STRONG_NT: updated_bh_table[bp_update[i].pc[`BHT_W+1:2]] = bp_update[i].branch_outcome.direction ? WEAK_NT : STRONG_NT;
          WEAK_NT:   updated_bh_table[bp_update[i].pc[`BHT_W+1:2]] = bp_update[i].branch_outcome.direction ? WEAK_T  : STRONG_NT;
          WEAK_T:    updated_bh_table[bp_update[i].pc[`BHT_W+1:2]] = bp_update[i].branch_outcome.direction ? STRONG_T: WEAK_NT;
          STRONG_T:  updated_bh_table[bp_update[i].pc[`BHT_W+1:2]] = bp_update[i].branch_outcome.direction ? STRONG_T: WEAK_T;
        endcase
      end
    end
end


  // update the branch target buffer
  always_comb begin  // TODO: check the width match
    updated_btb_table = btb_table;
    next_btb_valid = btb_valid;
    for (integer i = 0; i < `N; i++) begin
      if (bp_update[i].valid & bp_update[i].branch_outcome.direction==TAKEN) begin
        updated_btb_table[bp_update[i].pc[`BTB_IDX_W+1:2]].target = bp_update[i].branch_outcome.target;
        updated_btb_table[bp_update[i].pc[`BTB_IDX_W+1:2]].tag  = bp_update[i].pc[`BTB_TAG_W+`BTB_IDX_W+1:`BTB_IDX_W+2];
        next_btb_valid[bp_update[i].pc[`BTB_IDX_W+1:2]] = 1;
      end
    end
  end



  // make prediction
  always_comb begin : prediction_direction
    bp_direction = '0;
    for (integer i = 0; i < `N; i++) begin
      if (bp_query[i].valid) begin
        bp_direction[i] = updated_bh_table[bp_query[i].pc[`BHT_W+1:2]] == STRONG_T ||
        updated_bh_table[bp_query[i].pc[`BHT_W+1:2]] == WEAK_T;
      end
    end
  end

  always_comb begin : prediction_target
    btb_hit = '0;
    btb_hit_pc = '0;
    for (integer i = 0; i < `N; i++) begin
      if (bp_query[i].valid) begin
        btb_hit[i] = (updated_btb_table[bp_query[i].pc[`BTB_IDX_W+1:2]].tag == bp_query[i].pc[`BTB_TAG_W+`BTB_IDX_W+1:`BTB_IDX_W+2])
         &&next_btb_valid[bp_query[i].pc[`BTB_IDX_W+1:2]];
        btb_hit_pc[i] = updated_btb_table[bp_query[i].pc[`BTB_IDX_W+1:2]].target;
      end
    end
  end

  always_comb begin : setoutput
    bp_output = '0;
    for (integer i = 0; i < `N; i++) begin
      if (bp_query[i].valid) begin
        bp_output[i].valid = 1;
        bp_output[i].direction = bp_direction[i] && btb_hit[i];
        bp_output[i].target = btb_hit[i] ? btb_hit_pc[i] : bp_query[i].pc + 4;
      end
    end
  end



  always_ff @(posedge clock) begin
    if (reset) begin
      btb_table  <= '0;
      btb_valid  <= '0;
      // init the bh_table to weak not taken
      foreach (bh_table[i]) begin
        bh_table[i] <= WEAK_NT;
      end
    end else begin
      btb_valid <= next_btb_valid;
      bh_table  <= updated_bh_table;
      btb_table <= updated_btb_table;
    end
  end
endmodule
