`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"
module pipeline (
    input clock,
    input reset,

    output EXCEPTION_CODE    pipeline_error_status,
    output logic [3:0] pipeline_completed_insts,

    output CACHE_LINE [`DC_N_BLKS-1:0] dcache_contents,

    // Mmeory protocal from P3
    input [ 3:0] mem2proc_response,  // Tag from memory about current request
    input [63:0] mem2proc_data,      // Data coming back from memory
    input [ 3:0] mem2proc_tag,       // Tag from memory about current reply

    output logic    [      1:0] proc2mem_command,  // Command sent to memory
    output logic    [`XLEN-1:0] proc2mem_addr,     // Address sent to memory
    output logic    [     63:0] proc2mem_data     // Data sent to memory

    `ifdef  DEBUG
    ,output logic [`PHYS_REG_SZ-1:1][`XLEN-1:0] DBG_RF
    ,output DEBUG_MAP_TABLE debug_map_table
    `endif
    // output logic take_branch,
    // output PC_T branch_target
);
always_comb begin
  pipeline_completed_insts = 0;
  for (int i = 0; i < `N; i++) begin
    if (retired_inst[i].valid) begin
      pipeline_completed_insts = pipeline_completed_insts + 1;
    end
  end
  $monitor("pipeline_completed_insts = %d", pipeline_completed_insts);
end
  // Debug
`ifdef DEBUG
  DEBUG_FETCH DBG_FET;
  DEBUG_DECODER DBG_DEC;
  DEBUG_ROB DBG_ROB;
  DEBUG_RS DBG_RS;
  DEBUG_FU_MANAGER DBG_FU;
  RS_ENTRY [`RS_SZ-1:0] cleared_rs_entries_out;
`endif

  logic stall_store_retire;
  
  logic [$clog2(`INST_BUFFER_SZ):0] dispatch_count, dispatch_count_tmp;
  NUM_INST_T  num_rob_ready;
  NUM_INST_T_RS num_rs_ready;
  logic [`N-1:0] num_fetch_res_valid;
  NUM_INST_T tmp_can_fetch_cnt;
  FETCH_RES [`N-1:0] dispatch_in,fetch_res;


  always_comb begin
    tmp_can_fetch_cnt = '0;
    num_fetch_res_valid = '0;
    dispatch_count_tmp = '0;
    if (num_rob_ready < num_rs_ready ) begin
      dispatch_count_tmp = (num_rob_ready < `N) ? num_rob_ready : `N;
    end else begin
      dispatch_count_tmp = (num_rs_ready < `N) ? num_rs_ready : `N;
    end

    for (int i = 0; i < `N; i++) begin
      if (fetch_res[i].valid) begin
        num_fetch_res_valid += 1;
      end
    end

    dispatch_count = (dispatch_count_tmp < num_fetch_res_valid) ? dispatch_count_tmp : num_fetch_res_valid;

    dispatch_in = fetch_res;
    for(int i=0;i<`N;i++)begin
      if(i>=dispatch_count)begin
        dispatch_in[i].valid = 0;
      end
    end
    `ifdef DEBUG
    $display("[AVAIL]time:%d\trob_avail:%d\trs_avail:%d\tdispatch_count:%d\tdispatch_count_tmp: %d", $time, num_rob_ready, num_rs_ready, dispatch_count, dispatch_count_tmp);

    for (int i = 0;i < `N; i++) begin
      $display("[DISPATCH IN]\ttime: %d\tpc: %h\tinst: %d\tvalid: %d", $time, dispatch_in[i].pc,dispatch_in[i].inst, dispatch_in[i].valid);
      $display("[FETCH RES]\ttime: %d\tpc: %h\tinst: %d\tvalid: %d", $time, fetch_res[i].pc,fetch_res[i].inst, fetch_res[i].valid);
    end
    `endif
  end

    // to LSQ
  logic start_cleaning_up;
  logic dcache_clean;



  logic [1:0] fetch2Imem_command;
  logic [`XLEN-1:0] fetch2Imem_addr;
  logic [3:0] Imem2fetch_response;
  logic [63:0] Imem2fetch_data;
  logic [3:0] Imem2fetch_tag;
  PC_T rollback_pc;
  BRANCH_OUTCOME hazard_branch_outcome;
  BRANCH_OUTCOME [`N-1:0] branch_outcomes;
  BP_UPDATE [`N-1:0] bp_updates;
  fetch _m_fetch (
      .clock(clock),
      .reset(reset),
      .fetch_res_out(fetch_res),// main instruction port
      // memory
      .fetch2Imem_command(fetch2Imem_command),
      .fetch2Imem_addr(fetch2Imem_addr),
      .Imem2fetch_response(Imem2fetch_response),
      .Imem2fetch_data(Imem2fetch_data),
      .Imem2fetch_tag(Imem2fetch_tag),
      .d_request(d_request),
      // dispatch
      .dispatch_count(dispatch_count),
      // backend
      .rollback(hazard_branch_outcome),
      .rollback_pc(rollback_pc),
      .branch_update(bp_updates)
      `ifdef DEBUG
      // .jump_inst_idx_reg_out()
      `endif
  );

  // Decoder:
  // ROB:
  ROB_PACKET [`N-1:0] decoded_insts_to_ROB;
  PHYS_REG_T [`N:0] freelist_return_reg;
  MAP_TABLE_READ_COMMAND [`N-1:0] decode_dest_map_read_req;
  MAP_TABLE_ENTRY [`N-1:0] decode_dest_map_read_res;
  MAP_TABLE_WRITE_COMMAND [`N-1:0] decode_map_write_command;
  // RS:
  MAP_TABLE_READ_COMMAND [`N-1:0][1:0] decode_operands_map_read_req;
  MAP_TABLE_ENTRY [`N-1:0][1:0] decode_operands_map_read_res;
  RS_INPUT [`N-1:0] rs_new_inst;


  // ROB:
  // ROB_PACKET [`N-1:0] new_inst_supplies, output from decoder
  ROB_REF_T  [`N-1:0] dispatch_rob_refs; // input to RS
  // ROB_REF_T  [`N-1:0] completing_inst; output from FU
  ROB_PACKET [`N-1:0] retired_inst;
  PHYS_REG_T phy_reg_to_free_rollback;

  // Roll back control:
  ROB_REF_T ROB_cancellation_to_RS;
  MAP_TABLE_WRITE_COMMAND rollback_mt_write_cmd;
  ROB_REF_T [`N-1:0] hazard_ref_wire;
  ROB_REF_T fu_rollback_rob_index;
  PC_T [`N-1:0] hazard_target;

  CDB_ENTRY [`CDB_W-1:0] FU_cdb_entries_to_RS_MP;
  RS_ENTRY [`RS_SZ-1:0] rs_entries_out;
  logic [`RS_SZ-1:0] ready_issue;
  logic [`RS_SZ-1:0] fu_feedback_mask;
  NUM_INST_T_RS fu_feedback_num;
  ROB_REF_T cancel_ref;

  REG_READ_REQ [`REG_PORT_WIDTH-1:0][1:0] read_reg_req;
  REG_READ_RESPONSE [`REG_PORT_WIDTH-1:0][1:0] read_reg_response;
  ROB_REF_T[`N-1:0] FU_completed_inst_to_ROB;
  REG_WRITE_REQ [`N-1:0] write_reg_req;
  BRANCH_FEEDBACK[`N-1:0] branch_feedbacks;
  ROB_REF_T[`N-1:0] hazard_trigger;
  logic wfi_received_reg, wfi_received_sig;
  // CDB_ENTRY [`CDB_W-1:0] cdb_entries_wire;

  always_comb begin : set_br_outcome
    branch_outcomes = '0;
    for(int i=0;i<`N;i++)begin
      branch_outcomes[i]=branch_feedbacks[i].reality;
    end
  end

always_comb begin
    // determine_pipeline_error_status
    wfi_received_sig = wfi_received_reg;
    start_cleaning_up = 0;
    for (int i = 0; i <`N; i++) begin
      if (retired_inst[i].valid & wfi_received_reg == 0) begin
          `ifdef DEBUG
          $display("[time: %d]\tretired inst %d: | pcd:%d | pch: %h", $time, i, retired_inst[i].inst.pc +4, retired_inst[i].inst.pc );
          `endif
          if(retired_inst[i].inst.inst == `WFI) begin $display("wfi!!!!!") ; wfi_received_sig = 1; end
          else wfi_received_sig = 0;
      end
  end
    `ifdef ENABLE_CACHE_CLEANUP
    pipeline_error_status = (dcache_clean & wfi_received_reg) ? HALTED_ON_WFI : NO_ERROR;
    // send clean msg to dcache
    start_cleaning_up = wfi_received_reg;
    `else
    pipeline_error_status = (wfi_received_reg & ~stall_store_retire) ? HALTED_ON_WFI : NO_ERROR;
    `endif
    
end

always_ff @(posedge clock) begin
  if (reset) begin
    wfi_received_reg <= 0;
  end else begin
    wfi_received_reg <= wfi_received_sig;
  end
end

  dispatch_stage _m_dispatch_stage (
    // input
    .clock(clock),
    .reset(reset),
    .instructions_to_decode(dispatch_in),

    /// ROB
    .dest_read_req(decode_dest_map_read_req),
    .dest_read_res(decode_dest_map_read_res),
    .map_table_updates(decode_map_write_command),
    .rob_result(decoded_insts_to_ROB),

    /// RS
    .rob_refs(dispatch_rob_refs),
    .operands_read_req(decode_operands_map_read_req),
    .operands_read_res(decode_operands_map_read_res),
    .rs_result(rs_new_inst),

    .return_reg(freelist_return_reg)

      // Debug
      `ifdef DEBUG, .dbg_out(DBG_DEC) `endif
    );


  // LSQ:
  LSQ_REQUEST_BASE [`N-1:0] mem_dispatch_lsq;
  mem_dispatch mem_dis0 [`N-1:0] (
    .mem_input(rs_new_inst),
    .mem_dispatch_lsq(mem_dispatch_lsq)
  );

  logic [$clog2(`N)+1 : 0] fr_ret_reg_i;
  always_comb begin
    freelist_return_reg = '0;
    fr_ret_reg_i = 0;
    for (int i = 0; i < `N+1; i++) begin
      if(retired_inst[i].valid ) begin
        if( ~(retired_inst[i].oldT == 0) ) begin
          freelist_return_reg[fr_ret_reg_i] = retired_inst[i].oldT;
          fr_ret_reg_i = fr_ret_reg_i + 1;
        end
      end else if ( ~(phy_reg_to_free_rollback==0) ) begin
        freelist_return_reg[fr_ret_reg_i] = phy_reg_to_free_rollback;
        break;
      end
    end

    `ifdef DEBUG
    for (int i = 0; i < `N; i++) begin
      $display("[RETURN TO FREE LIST]\t[time: %d]\tfreelist_return_reg: %d", $time, freelist_return_reg[i]);
    end
    `endif

  end


  always_comb begin: determine_taken_branches_updates
    hazard_ref_wire = '0;
    bp_updates = '0;
    for (int i = 0; i < `N; i++) begin
      if(branch_feedbacks[i].valid)begin
      if((branch_feedbacks[i].predict != branch_feedbacks[i].reality.direction)
      ||(branch_feedbacks[i].reality.direction&&( branch_feedbacks[i].reality.target!=branch_feedbacks[i].npc))
      ) begin
        `ifdef DEBUG
        $display("time: %d\thazardooo\t pc:", $time,branch_feedbacks[i].location);
        if(branch_feedbacks[i].valid && branch_feedbacks[i].reality.direction&&( branch_feedbacks[i].reality.target!=branch_feedbacks[i].npc))begin
          $display("+++++target: %h, npc: %h",branch_feedbacks[i].reality.target,branch_feedbacks[i].npc);
        end
        `endif
        hazard_ref_wire[i].valid = 1;
        hazard_ref_wire[i].rob_index = FU_completed_inst_to_ROB[i].rob_index;
        hazard_target[i]=branch_feedbacks[i].reality.target;
      end
      bp_updates[i].valid = branch_feedbacks[i].valid;
      bp_updates[i].pc = branch_feedbacks[i].location;
      bp_updates[i].branch_outcome = branch_feedbacks[i].reality;
    end
    end
  end

  NUM_INST_T num_retire_load_store_inst;
  logic rollback_load_store_inst;


  rob _m_rob (
      .clock(clock),
      .reset(reset),

      .available_entry_sig(num_rob_ready),
      .new_inst_supplies(decoded_insts_to_ROB),
      .new_inst_refs(dispatch_rob_refs),

      .executed_inst_rob_index(FU_completed_inst_to_ROB),
      .stall_store_retire(stall_store_retire), // for lsq
      
      .retired_inst(retired_inst),
      .num_retire_load_store_inst(num_retire_load_store_inst), // for lsq

      .hazard_target(hazard_target),
      .branch_outcomes_fu_man(branch_outcomes),
      .hazard_ref(hazard_ref_wire),
      .cancellation_broadcast(ROB_cancellation_to_RS),
      .rollback_mt_write(rollback_mt_write_cmd),
      .phy_reg_to_free(phy_reg_to_free_rollback),
      .hazard_branch_outcome(hazard_branch_outcome),
      .fu_rollback_rob_index(fu_rollback_rob_index),
      .rollback_pc(rollback_pc),
      .rollback_load_store_inst(rollback_load_store_inst) // for lsq

      // Debug
`ifdef DEBUG,
      .dbg(DBG_ROB),
      .regfile_in(DBG_RF)
`endif
  );


  always_comb begin
    // rs_new_inst = '0;
    // for (integer i = 0; i < `N; i++) begin
    //   if (decoded_insts_to_ROB[i].valid) begin
    //     rs_new_inst[i] = {
    //       // output from decoder:
    //       inst: decoded_insts_to_ROB[i].inst,
    //       // TODO: fix this
    //       operands_status: '0,
    //       dest_reg: decoded_insts_to_ROB[i].newT,
    //       // output from ROB
    //       rob_ref: dispatch_rob_refs[i],
    //       valid: 1
    //     };
    //   end
    // end
      `ifdef DEBUG
      $display("[RS_INPUT]time: %d -----rs_new_inst----- ", $time);
      for (int i = 0; i < `N; i++) begin
        if (rs_new_inst[i].valid) begin
          $display("rs_new_inst.inst: %d\tfunc: %p\tfu: %p\tpc: %h\topa:%d\topb: %d",
          rs_new_inst[i].inst.inst, rs_new_inst[i].inst.func, rs_new_inst[i].inst.fu, rs_new_inst[i].inst.pc, 
          rs_new_inst[i].operands_status[0].phy_reg_idx, rs_new_inst[i].operands_status[1].phy_reg_idx);
        end
      end
      `endif
  end


  rs _m_reservation_st (
      .clock(clock),
      .reset(reset),

      // Dispatch
      .new_inst_supplies(rs_new_inst),
      .cdb_entries(FU_cdb_entries_to_RS_MP),
      .num_entry_available(num_rs_ready),

      // Issue:
      .rs_entries_out(rs_entries_out),
      .ready_issue(ready_issue),
      .fu_feedback_mask(fu_feedback_mask),
      .fu_feedback_num(fu_feedback_num),

      // Cancelation
      .cancel_ref(ROB_cancellation_to_RS)
  );


  // always_comb begin
  //   for (integer i = 0; i < `N; i++) begin
  //     cdb_entries[i] = {cdb_entries_wire[i].phy_reg_idx, cdb_entries_wire[i].valid};
  //   end
  // end

  LSQ_ENTRY [`ROB_SZ - 1 : 0] lsq_content;
  LSQ_ISSUE_REQUEST [`N - 1 : 0] lsq_issue_request;
  // logic [`N - 1 : 0] issue_approved;
  logic [`N-1:0][$clog2(`ROB_SZ)+1:0] completed_entry;
  logic [`N-1:0] completed_entry_valid;



  fu_manager _m_fu_manager (
      .clock(clock),
      .reset(reset),
      // rob rollback index:
      .rob_rollback_idx(fu_rollback_rob_index),

      // input the RS output
      .rs_entries_in_wire(rs_entries_out),
      .rs_ready_in_wire(ready_issue),
      .fu_feedback_mask(fu_feedback_mask),
      .fu_feedback_num(fu_feedback_num),

      // execute:
      .read_reg_req(read_reg_req),
      .read_reg_response(read_reg_response),

      // Complete:
      .completed_inst(FU_completed_inst_to_ROB),
      .write_reg_req(write_reg_req),
      .branch_feedbacks(branch_feedbacks),

      // Cancellation
      // output cdb_entries
      .cdb_entries_wire(FU_cdb_entries_to_RS_MP),

      //LSQ
      .lsq_content(lsq_content),
      .lsq_issue_request(lsq_issue_request),
      // .issue_approved(issue_approved),
      .completed_entry(completed_entry),
      .completed_entry_valid(completed_entry_valid)
  );


  // May not used?
  logic [$clog2(`ROB_SZ) + 1 : 0] lsq_head;
  logic [$clog2(`ROB_SZ) + 1 : 0] lsq_content_size;

  // LSQ->MMU
  logic [1:0]  proc2Dmem_command;
  logic [`XLEN-1:0] proc2Dmem_addr;
  logic [63:0] proc2Dmem_data;

  logic [3:0]  Dmem2proc_response;
  logic [63:0] Dmem2proc_data;
  logic [3:0]  Dmem2proc_tag;


  load_store_queue lsq0(
    .clock(clock),
    .reset(reset),

    // // TODO: get it from dispatch
    // .pc(pc),

    .dcache_contents(dcache_contents),

    // From memory (only used by DCache)
    .Dmem2proc_response(Dmem2proc_response), // Should be zero unless there is a response
    .Dmem2proc_data(Dmem2proc_data),
    .Dmem2proc_tag(Dmem2proc_tag),

    // To memory  (only used by DCache)
    .proc2Dmem_command(proc2Dmem_command),
    .proc2Dmem_addr(proc2Dmem_addr),
    .proc2Dmem_data(proc2Dmem_data),

    // Dispatch: Reserve Spots for load and store
    .dispatch_request(mem_dispatch_lsq), // TODO: dispatch stage provides

    // Issue / Execute: Issue load (possible reject: reflect in output as not issue_success) and store (guarenteed by LSQ to success)
    .issue_request(lsq_issue_request),
    // .issue_approved(issue_approved), // return in-cycle issue success (combinational logic)

    // Complete
    .complete_request(completed_entry), // LSQ INDEX (NOT COUNT FROM HEAD, NOT ROB INDEX)
    .complete_request_valid(completed_entry_valid),

    // Retire: PLEASE CONSIDER stall_store_retire BEFORE SETTING num_retire_load_store_inst
    // only contain the count of load/store inst of the total inst retiring!!!!! 
    // must contain at most 1 store per cycle (if stall_store_retire == 0) to retire: 
    //      stall_store_retire == 0 && Store Store Load -> can only retire first Store
    //      stall_store_retire == 0 && Store Load Load -> can retire all three
    //      stall_store_retire == 1 && Store Store Load -> can NOT retire ANYTHING!!!
    //      stall_store_retire == 1 && Load Load Store -> can retire first two loads
    .num_retire_load_store_inst(num_retire_load_store_inst),

    // Rollback
    // PLEASE ONLY CONTAIN LOAD / STORE
    .rollback_load_store_inst(rollback_load_store_inst), // at most one

    // Output of the whole Load Store Queue Structure
    .LSQ_content(lsq_content),
    .lsq_head(lsq_head), // maximum lsq_head possible is `ROB_SZ - 1, 
    .lsq_content_size(lsq_content_size), // maximum lsq_content_size possible is `ROB_SZ, 
    .stall_store_retire(stall_store_retire), // a wire directly from DCache without changes

    // WFI
    .start_cleaning_up(start_cleaning_up),
    .dcache_clean(dcache_clean)
);





  map_table _m_map_table (
      .clock(clock),
      .reset(reset),

      // decode
      .decode_dest_read_request(decode_dest_map_read_req),
      .decode_dest_read_response(decode_dest_map_read_res),
      .decode_map_write_command(decode_map_write_command),
      .decode_operands_read_request(decode_operands_map_read_req),
      .decode_operands_read_response(decode_operands_map_read_res),

      // fu_manager completion, input
      .cdb_entries(FU_cdb_entries_to_RS_MP),
      // ROLL back control from ROB
      .rollback_mt_write(rollback_mt_write_cmd)


      // Debug
      `ifdef DEBUG,
      .dbg_out(debug_map_table)
      `endif
  );

  regfile _m_reg_file (
      .clock(clock),
      .reset(reset),
      // commands
      .read_request(read_reg_req),
      .read_response(read_reg_response),
      .write_request(write_reg_req)

      // Debug
      `ifdef DEBUG,
      .registers_out(DBG_RF)
      `endif
  );

  // Alltogether memory bus

  mmu _m_mmu (
    .mem2mmu_response(mem2proc_response),
    .mem2mmu_tag(mem2proc_tag),
    .mem2mmu_data(mem2proc_data),

    .mmu2mem_command(proc2mem_command),
    .mmu2mem_addr(proc2mem_addr),
    .mmu2mem_data(proc2mem_data),

    .mmu2icache_response(Imem2fetch_response),
    .mmu2icache_tag(Imem2fetch_tag),
    .mmu2icache_data(Imem2fetch_data),

    .icache2mmu_command(fetch2Imem_command),
    .icache2mmu_addr(fetch2Imem_addr),

    // TODO: dcache
    .mmu2dcache_response(Dmem2proc_response),
    .mmu2dcache_tag(Dmem2proc_tag),
    .mmu2dcache_data(Dmem2proc_data),

    .dcache2mmu_command(proc2Dmem_command),
    .dcache2mmu_addr(proc2Dmem_addr),
    .dcache2mmu_data(proc2Dmem_data),

    .d_request(d_request)
  );

endmodule  // pipeline