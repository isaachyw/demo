`include "verilog/sys_defs.svh"

module testbench;
    logic clock;

    // coming from RS @issue
    REG_READ_REQ[1:0][`N-1:0] read_request_in;
    REG_READ_RESPONSE[1:0][`N-1:0] read_response_in;

    // coming from FU @complete
    REG_WRITE_REQ[`N-1:0] write_request_in;
    logic wr_en;

    regfile regfile0(
        .clock(clock),
        .read_request(read_request_in),
        .read_response(read_response_in),
        .write_request(write_request_in),
        .wr_en(wr_en)
    );

    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    initial begin
        $display("Start testing...");
        clock = 0;

        @(negedge clock)

        @(negedge clock)
        $display("@@@Passed!");
        $finish;
    end

endmodule`include "verilog/sys_defs.svh"

module testbench;
    logic clock;
    logic reset;

    // coming from RS @issue
    REG_READ_REQ[1:0][`N-1:0] read_request_in;
    REG_READ_RESPONSE[1:0][`N-1:0] read_response_out;

    // coming from FU @complete
    REG_WRITE_REQ[`N-1:0] write_request_in;

    regfile regfile0(
        .clock(clock),
        .reset(reset),
        .read_request(read_request_in),
        .read_response(read_response_out),
        .write_request(write_request_in)
    );

    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    initial begin
        $display("Start testing...");
        reset = 1;
        clock = 0;

        @(negedge clock)
        $display("Test basic logic: write first then read...");
        reset = 0;

        for(int i = 0; i < `N; i++) begin
            write_request_in[i] = {
                reg_ref: 1 + i,
                data: 100 + i,
                valid: 1
            };
        end

        @(negedge clock)

        for(int i = 0; i < `N; i++) begin
            read_request_in[0][i] = {
                reg_ref: 1 + i,
                valid: 1
            };
            read_request_in[1][i] = {
                reg_ref: 1 + i,
                valid: 1
            };
        end

        @(negedge clock)
        for(int i = 0; i < `N; i++) begin
            for(int j = 0; j < 2; j++) begin
                $display("Regfile{physical_address: %d, data: %d}\n", read_request_in[j][i].reg_ref, read_response_out[j][i].data);
            end
        end

        @(negedge clock)
        $display("Test forward logic: internal fowarding...");

        for(int i = 0; i < `N; i++) begin
            write_request_in[i] = {
                reg_ref: 1 + i,
                data: 200 + i,
                valid: 1
            };
        end

        for(int i = 0; i < `N; i++) begin
            read_request_in[0][i] = {
                reg_ref: 1 + i,
                valid: 1
            };
            read_request_in[1][i] = {
                reg_ref: 1 + i,
                valid: 1
            };
        end

        @(negedge clock)
        for(int i = 0; i < `N; i++) begin
            for(int j = 0; j < 2; j++) begin
                $display("Regfile{physical_address: %d, data: %d}\n", read_request_in[j][i].reg_ref, read_response_out[j][i].data);
            end
        end

        @(negedge clock)
        $display("@@@Passed!");
        $finish;
    end

endmodule
