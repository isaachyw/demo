`include "verilog/sys_defs.svh"

import "DPI-C" function void print_single_rob_entry_input(int inst, int newT, int oldT, int valid);
import "DPI-C" function void print_single_rob_entry_stored(int head, int tail, int inst, int newT, int oldT, int valid, 
                                                           int idx, int has_excuted, int has_completed);


module testbench;
    logic clock, reset, enable;

    logic [$clog2(`ROB_SZ):0] available_entry_reg;  // output, verify
    ROB_PACKET [`N-1:0] new_inst_supplies;  // input - need generation
    ROB_REF_T [`N-1:0] new_inst_refs;  // output, verify

    ROB_REF_T [`N-1:0] executed_inst_rob_index;  // input - need artifitial generation, calls to retire
    ROB_PACKET [`N-1:0] retired_inst;

    ROB_REF_T [`N-1:0] hazard_ref;
    // Cancel(/rollback) one instruction at a time if hazard
    ROB_REF_T cancellation_broadcast;
    // Map table access for rollback
    MAP_TABLE_WRITE_COMMAND rollback_mt_write;
    // free physical reg when rollback
    PHYS_REG_T phy_reg_to_free

    `ifdef DEBUG
    ;DEBUG_ROB dbg;
    // output, verify
    ROB_ENTRY [`ROB_SZ-1:0] ROB_content_out;
    logic [$clog2(`ROB_SZ):0] head_out;
    logic [$clog2(`ROB_SZ):0] tail_out;
    logic is_uninitialized_out;
    ROB_ENTRY [`N-1:0] next_rob_entry_out;
    logic [$clog2(`ROB_SZ):0] next_head_out;
    logic [$clog2(`ROB_SZ):0] next_tail_out;
    logic [$clog2(`N):0] tail_offset_out;
    `endif

    rob dut (
        .clock(clock),
        .reset(reset),
        .enable(enable),

        .available_entry_reg(available_entry_reg),
        .new_inst_supplies(new_inst_supplies),
        .new_inst_refs(new_inst_refs),

        .executed_inst_rob_index(executed_inst_rob_index),

        .retired_inst(retired_inst),

        .hazard_ref(hazard_ref),
        .cancellation_broadcast(cancellation_broadcast),
        .rollback_mt_write(rollback_mt_write),
        .phy_reg_to_free(phy_reg_to_free)

        `ifdef DEBUG
        ,.dbg(dbg)
        `endif
    );

    assign head_out = dbg.head_out;
    assign tail_out = dbg.tail_out;
    assign is_uninitialized_out = dbg.is_uninitialized_out; 
    assign next_rob_entry_out = dbg.next_rob_entry_out;
    assign next_head_out = dbg.next_head_out;
    assign next_tail_out = dbg.next_tail_out;
    assign tail_offset_out = dbg.tail_offset_out;
    assign ROB_content_out = dbg.ROB_content_out;

    task exit_on_error;
        begin
            $display("@@@Failed at time %d", $time);
            $finish;
        end
    endtask

    task clear_new_inst_input;
        for (int i = 0; i < 3; i++) new_inst_supplies[i] = '0;
    endtask

    task clear_executed_rob_index;
        for (int i = 0; i < 3; i++) begin
            executed_inst_rob_index[i].valid = 0;
            executed_inst_rob_index[i].rob_index = `N-1;
        end
    endtask

    task print_head_tail;
        $display("time: %d\thead: %d\ttail: %d\t|\tnext_head: %d\tnext_tail: %d\ttail_offset:%d", $time, head_out, tail_out, next_head_out, next_tail_out, tail_offset_out);
    endtask

    task print_next_rob_entry_array;
        $display("TIME: %d\t NEXT_ROB_ENTRY:", $time);
        for (int i = 0; i < `N; i ++)
            print_single_rob_entry_stored(head_out, tail_out, next_rob_entry_out[i].inst, next_rob_entry_out[i].newT, next_rob_entry_out[i].oldT, 
                                next_rob_entry_out[i].valid, next_rob_entry_out[i].rob_index, next_rob_entry_out[i].has_executed, next_rob_entry_out[i].has_completed);
    endtask

    task print_ALL_ROB_Entries;
        $display("TIME: %d\t ROB_CONTENT:\thead: %d\ttail: %d", $time, head_out, tail_out);
        for (int i = 0; i < `ROB_SZ; i ++)
            print_single_rob_entry_stored(head_out, tail_out, ROB_content_out[i].inst, ROB_content_out[i].newT, ROB_content_out[i].oldT, 
                                ROB_content_out[i].valid, ROB_content_out[i].rob_index, ROB_content_out[i].has_executed, ROB_content_out[i].has_completed);
    endtask
    // global inst counter
    int inst_counter;
    // generate rob entry and pass to rob
    function ROB_PACKET gen_single_rob_entry(
                       PHYS_REG_T newT,
                       PHYS_REG_T oldT,
                       logic valid,
                       int inst_counter);
        ROB_PACKET rob_entry_out = '0;
        if (valid==0) rob_entry_out = '0;
        else begin
            rob_entry_out.inst =  inst_counter;
            rob_entry_out.newT =  newT;
            rob_entry_out.oldT =  oldT;
            rob_entry_out.valid = valid;
        end
        return rob_entry_out;
    endfunction

    function ROB_REF_T gen_single_executed_inst_entry(logic [$clog2(`ROB_SZ)-1:0] rob_index, logic valid);
        ROB_REF_T ret = '0;
        ret.rob_index = rob_index;
        ret.valid = valid;
        return ret;
    endfunction

    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end



    initial begin
        $display("Start testing, ROB size: %d.", `ROB_SZ);
        $monitor("time: %d | available_entry_reg: %d | head: %d | tail: %d | un_initialized: %d", $time, available_entry_reg, head_out,  tail_out, is_uninitialized_out);
        // init
        clock = 0;
        reset = 1;
        enable = 0;
        clear_new_inst_input();
        clear_executed_rob_index();


        @(negedge clock)
        reset = 0;
        enable = 1;
        

        // test when ROB empty, number of available slots should be correct
        @(negedge clock)

        assert(available_entry_reg==`ROB_SZ) else begin
            $display("ROB actual output: %d", available_entry_reg);
            exit_on_error();
        end



        @(negedge clock)
        // insert 3 inst to RO, no tag forwarding
        for (int i = 0; i < 3; i++) begin
            new_inst_supplies[i] = gen_single_rob_entry(i+5,i,1,inst_counter);
            inst_counter = inst_counter + 1;
        end



        @(posedge clock)
        // check rob output index, 0, 1, 2
        // clear_new_inst_input();
        for (int i = 0; i < 3; i++) begin
            print_single_rob_entry_input(new_inst_supplies[i].inst, new_inst_supplies[i].newT, new_inst_supplies[i].oldT, new_inst_supplies[i].valid);
            assert(new_inst_refs[i].rob_index == i) else begin
                $display("i: %d\tnew_inst_ref.rob_index: %d\tnew_inst_ref.valid: %d", i, new_inst_refs[i].rob_index, new_inst_refs[i].valid);
                exit_on_error();
            end
        end
        
        // print_next_rob_entry_array();
        @(negedge clock)
        clear_new_inst_input();
        print_next_rob_entry_array();
        print_ALL_ROB_Entries();
        

        assert(available_entry_reg ==`ROB_SZ - 3) else begin
            $display("ROB actual output: %d", available_entry_reg);
            exit_on_error();
        end

        // fill another 1 instruction
        @(negedge clock)
        print_ALL_ROB_Entries();
        // fill ROB with 4 inst 
        for (int i = 0; i < 3; i++) begin
            if (i == 2 || i == 1) new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i+3,0,inst_counter);
            else begin
                new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i+3,1,inst_counter);
                inst_counter = inst_counter + 1;
            end
        end

        @(posedge clock)
        clear_new_inst_input();
        

        @(negedge clock)


        
        // first three insts executed in ROB
        for (int i = 0; i < 3 ; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i, 1);
            $display("test exe idx: %d \tvalid: %d", executed_inst_rob_index[i].rob_index, executed_inst_rob_index[i].valid);
        end
        
        @(negedge clock)
        // check first three entry of rob, should be in complete stage
        print_ALL_ROB_Entries();
        clear_executed_rob_index();

        @(negedge clock)
        // the first three inst should be in retire state
        print_head_tail();
        print_ALL_ROB_Entries();

  
        @(negedge clock)
        // the first three inst should be in invalid state
        print_head_tail();
        print_ALL_ROB_Entries();      
        
        @(negedge clock)
        // complete the last instruction in the rob
        $display("Clearing 4-th instruction in the ROB");
        for (int i = 3; i < 6 ; i++) begin
            if (i==3) begin
                executed_inst_rob_index[0] = gen_single_executed_inst_entry(i, 1);
            end
            $display("exe idx: %d \tvalid: %d", executed_inst_rob_index[i-3].rob_index, executed_inst_rob_index[i-3].valid);

        end

        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        $display("ROB should be empty!");
        print_ALL_ROB_Entries(); 
        assert( available_entry_reg == `ROB_SZ) else exit_on_error();
        print_head_tail();

        @(negedge clock)
        // insert 3 inst to RO, no tag forwarding
        for (int i = 0; i < 3; i++) begin
            new_inst_supplies[i] = gen_single_rob_entry(i+5,i,1,inst_counter);
            inst_counter = inst_counter + 1;
        end

        @(negedge clock)
        print_ALL_ROB_Entries();
        clear_new_inst_input();


        executed_inst_rob_index[0] = gen_single_executed_inst_entry(0, 1);
        executed_inst_rob_index[1] = gen_single_executed_inst_entry(3, 1);
        executed_inst_rob_index[2] = gen_single_executed_inst_entry(4, 1);
        for (int i = 0; i < 3; i++)
            $display("exe idx: %d \tvalid: %d", executed_inst_rob_index[i].rob_index, executed_inst_rob_index[i].valid);

    

        @(negedge clock)
                print_ALL_ROB_Entries(); 
        @(negedge clock)
        print_ALL_ROB_Entries(); 
        @(negedge clock)
        $display("ROB should be empty!");
        print_ALL_ROB_Entries(); 
        assert( available_entry_reg == `ROB_SZ) else exit_on_error();
        print_head_tail();

        @(negedge clock)
        
        // insert 3 inst to RO, no tag forwarding
        for (int i = 0; i < 3; i++) begin
            new_inst_supplies[i] = gen_single_rob_entry(i+5,i,1,inst_counter);
            inst_counter = inst_counter + 1;
        end

        @(negedge clock)
        print_ALL_ROB_Entries();
        assert( available_entry_reg == `ROB_SZ - 3) else exit_on_error();

        $display("@@@Passed!");
        $finish;
    end

endmodule