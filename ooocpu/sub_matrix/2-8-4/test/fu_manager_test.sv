`include "verilog/sys_defs.svh"
`include "verilog/regfile.sv"

module testbench;
    // input
    logic clock, reset;
    // RS input
    RS_ENTRY [`RS_SZ-1:0] rs_entries_in_wire_in;
    logic [`RS_SZ-1:0] rs_ready_in_wire_in;
    DEBUG_FU_MANAGER dbg_out;

    REG_READ_RESPONSE[1:0][`N-1:0] read_reg_response_in;
    // ROB_REF_T cancel_ref_in;

    //output
    REG_READ_REQ[1:0][`N-1:0] read_reg_req_out;
    RS_REF_T [`N-1:0] fu_feedback_out;
    ROB_REF_T[`N-1:0] completed_inst_out;
    REG_WRITE_REQ[`CDB_W-1:0] write_reg_req_out;
    BRANCH_OUTCOME[`N-1:0] branch_outcomes_out;
    CDB_ENTRY [`CDB_W-1:0] cdb_entries_wire_out;

    REG_WRITE_REQ[`CDB_W-1:0] regfile_write_req_in;
    logic [`PHYS_REG_SZ-1:1][`XLEN-1:0] regfile_debug;

    MULT_OUTPUT [`NUM_FU_MULT-1:0] debug_mult_result;
    logic [`N-1:0][$clog2(`NUM_FU_MULT)-1:0] mult_av_idxs;
    logic [$clog2(`N)-1:0] mult_av_cnt;
    MULT_INPUT [`NUM_FU_MULT-1:0] mult_input_debug;

    regfile regfile0(
        .clock(clock),
        .reset(reset),
        .read_request(read_reg_req_out),
        .read_response(read_reg_response_in),
        .write_request(write_reg_req_out)
        `ifdef DEBUG
        // Debug:
            , .registers_out(regfile_debug)
        `endif
    );

    fu_manager fu(
        .clock(clock),
        .reset(reset),
        .rs_entries_in_wire(rs_entries_in_wire_in),
        .rs_ready_in_wire(rs_ready_in_wire_in),
        .fu_feedback(fu_feedback_out),

        .read_reg_req(read_reg_req_out),
        .read_reg_response(read_reg_response_in),

        .completed_inst(completed_inst_out),
        .write_reg_req(write_reg_req_out),

        .branch_outcomes(branch_outcomes_out),  // NOTE: change BR_FEEDBACK to BR_OUTCOME --ziangli

        .cdb_entries_wire(cdb_entries_wire_out)
        `ifdef DEBUG
        , .dbg_out(dbg_out)
        , .mult_results(debug_mult_result)
        , .mul_idx_debug(mult_av_idxs)
        , .mul_cnt_debug(mult_av_cnt)
        , .mult_input_debug(mult_input_debug)
        `endif
    );

    task assign_to_fu;
        for(int i = 0; i < 3; i++) begin
            rs_entries_in_wire_in[i] = {
                inst: {
                    pc: i,
                    npc: i+4,
                    inst: 32'b00101000000000000000000000001111,
                    fu: ALU,
                    opa_select: OPA_IS_RS1,
                    opb_select: OPB_IS_I_IMM,
                    func: ALU_ADD,
                    has_dest: 1,
                    illegal: 0,
                    branch_prediction: '0
                },
                operands_status: '0,
                dest_reg: i+1,
                rob_ref: i+1,
                valid: 1,
                ready: 1

            };
            rs_entries_in_wire_in[i].operands_status[0] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[1] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 0
            };

            rs_ready_in_wire_in[i] = 1;
        end
    endtask

    task secondALU;
        for(int i = 0; i < 3; i++) begin
            rs_entries_in_wire_in[i] = {
                inst: {
                    pc: i,
                    npc: i+4,
                    inst: 32'b00101010111000000000000000001111,
                    fu: ALU,
                    opa_select: OPA_IS_RS1,
                    opb_select: OPB_IS_I_IMM,
                    func: ALU_ADD,
                    has_dest: 1,
                    illegal: 0,
                    branch_prediction: '0
                },
                operands_status: '0,
                dest_reg: i+1,
                rob_ref: i+2,
                valid: 1,
                ready: 1

            };
            rs_entries_in_wire_in[i].operands_status[0] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[1] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 0
            };

            rs_ready_in_wire_in[i] = 1;
        end
    endtask

    task begin_mult;
        for(int i = 0; i < 2; i++) begin
            rs_entries_in_wire_in[i] = {
                inst: {
                    pc: i,
                    npc: i+4,
                    inst: 32'b0,
                    fu: MULT,
                    opa_select: OPA_IS_RS1,
                    opb_select: OPB_IS_RS2,
                    func: MULT_MUL,
                    has_dest: 1,
                    illegal: 0,
                    branch_prediction: '0
                },
                operands_status: '0,
                dest_reg: i+5,
                rob_ref: i,
                valid: 1,
                ready: 1

            };
            rs_entries_in_wire_in[i].operands_status[0] = {
                phy_reg_idx: 1,
                ready: 1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[1] = {
                phy_reg_idx: 1,
                ready: 1,
                valid: 1
            };

            rs_ready_in_wire_in[i] = 1;
        end
    endtask

    task one_alu_two_mult;
        for(int i = 2; i < 3; i++) begin
            rs_entries_in_wire_in[i] = {
                inst: {
                    pc: i,
                    npc: i+4,
                    inst: 32'b00101000000000000000000000001111,
                    fu: ALU,
                    opa_select: OPA_IS_RS1,
                    opb_select: OPB_IS_I_IMM,
                    func: ALU_ADD,
                    has_dest: 1,
                    illegal: 0,
                    branch_prediction: '0
                },
                operands_status: '0,
                dest_reg: i+1,
                rob_ref: i+1,
                valid: 1,
                ready: 1

            };
            rs_entries_in_wire_in[i].operands_status[0] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[1] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 0
            };

            rs_ready_in_wire_in[i] = 1;
        end
    endtask

    task clear_assign;
        rs_entries_in_wire_in = '0;
        rs_ready_in_wire_in = '0;
    endtask

    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    initial begin
        $monitor("{RS_ENTRIES_ROB: %d %d %d} {Completed inst: %d %d %d}\n{CDB: %d %d %d}\n{mult_input: %d %d}\n{mult_result: %d; done: %b}\nfu_output0: %d %d, fu_output1: %d %d, fu_output2: %d %d\nreg[1]: %d , reg[2]: %d , reg[3]: %d\nreg[5]: %d, reg[6]: %d, reg[7]: %d",
                rs_entries_in_wire_in[0].rob_ref, rs_entries_in_wire_in[1].rob_ref, rs_entries_in_wire_in[2].rob_ref,
                completed_inst_out[0].rob_index, completed_inst_out[1].rob_index, completed_inst_out[2].rob_index,
                cdb_entries_wire_out[0].phy_reg_idx, cdb_entries_wire_out[1].phy_reg_idx, cdb_entries_wire_out[2].phy_reg_idx,
                mult_input_debug[0].opa, mult_input_debug[0].opa,
                debug_mult_result[0].result, debug_mult_result[0].done,
                write_reg_req_out[0].reg_ref, write_reg_req_out[0].data, write_reg_req_out[1].reg_ref, write_reg_req_out[1].data, write_reg_req_out[2].reg_ref, write_reg_req_out[2].data,
                regfile_debug[1], regfile_debug[2], regfile_debug[3],
                regfile_debug[5], regfile_debug[6], regfile_debug[7]
                );
        $display("Start testing...");
        clock = 0;
        reset = 1;

        @(negedge clock)
        reset = 0;

        @(negedge clock)
        assign_to_fu();
        @(negedge clock)
        clear_assign();
        @(negedge clock)
        secondALU();
        @(negedge clock)
        clear_assign();
        @(negedge clock)
        begin_mult();
        @(negedge clock)
        begin_mult();
        one_alu_two_mult();
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)
        @(negedge clock)



        $display("@@@Passed!");
        $finish;
    end

endmodule