`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"



import "DPI-C" function void print_single_rob_entry_input(int inst, int newT, int oldT, int valid);
import "DPI-C" function void print_single_rob_entry_stored(int head, int tail, int inst, int newT, int oldT, int valid, 
                                                           int idx, int has_excuted, int has_completed);

`define Itype(op, funct3)         {{12{1'b1}},{5{1'b0}},``funct3``,{5{1'b1}},``op``}
`define Stype(op, funct3)          {{7{1'b0}},{5{1'b1}},{5{1'b0}},``funct3``,{5{1'b1}},``op``}

module testbench;
    logic clock, reset;

    logic [$clog2(`ROB_SZ):0] available_entry_sig;  // output, verify
    ROB_PACKET [`N-1:0] new_inst_supplies;  // input - need generation
    ROB_REF_T [`N-1:0] new_inst_refs;  // output, verify

    ROB_REF_T [`N-1:0] executed_inst_rob_index;  // input - need artifitial generation, calls to retire
    ROB_PACKET [`N-1:0] retired_inst;

    ROB_REF_T [`N-1:0] hazard_ref;
    // Cancel(/rollback) one instruction at a time if hazard
    ROB_REF_T cancellation_broadcast;
    // Map table access for rollback
    MAP_TABLE_WRITE_COMMAND rollback_mt_write;
    // free physical reg when rollback
    PHYS_REG_T phy_reg_to_free;
    logic rollback_load_store_inst;
    NUM_INST_T num_retire_load_store_inst;

    `ifdef DEBUG
    DEBUG_ROB dbg;
    // output, verify
    ROB_ENTRY [`ROB_SZ-1:0] ROB_content_out;
    logic [$clog2(`ROB_SZ):0] head_out;
    logic [$clog2(`ROB_SZ):0] tail_out;
    logic is_uninitialized_out;
    ROB_ENTRY [`ROB_SZ-1:0] next_rob_entry_out;
    logic [$clog2(`ROB_SZ):0] next_head_out;
    logic [$clog2(`ROB_SZ):0] next_tail_out;
    logic signed [$clog2(`N)+1:0] tail_offset_out;
    logic [$clog2(`ROB_SZ):0] rollback_branch_index_reg_out;
    logic [$clog2(`ROB_SZ):0] available_entry_reg_out;
    logic rollback_enabled_reg_out;
    logic next_rollback_enabled_wire_out;
    logic stall_store_retire;
    BRANCH_OUTCOME [`N-1:0] branch_outcomes_fu_man;
    `endif

    rob dut (
        .clock(clock),
        .reset(reset),

        .stall_store_retire(stall_store_retire),
        .available_entry_sig(available_entry_sig),
        .new_inst_supplies(new_inst_supplies),
        .new_inst_refs(new_inst_refs),

        .executed_inst_rob_index(executed_inst_rob_index),
        .branch_outcomes_fu_man(branch_outcomes_fu_man),

        .retired_inst(retired_inst),
        .num_retire_load_store_inst(num_retire_load_store_inst),

        .hazard_ref(hazard_ref),
        .cancellation_broadcast(cancellation_broadcast),
        .rollback_mt_write(rollback_mt_write),
        .phy_reg_to_free(phy_reg_to_free),
        .rollback_load_store_inst(rollback_load_store_inst)

        `ifdef DEBUG
        ,.dbg(dbg)
        `endif
    );

    assign head_out = dbg.head_out;
    assign tail_out = dbg.tail_out;
    assign next_rob_entry_out = dbg.next_rob_entry_out;
    assign next_head_out = dbg.next_head_out;
    assign next_tail_out = dbg.next_tail_out;
    assign tail_offset_out = dbg.tail_offset_out;
    assign ROB_content_out = dbg.ROB_content_out;
    assign rollback_branch_index_reg_out = dbg.rollback_branch_index_reg_out;
    assign available_entry_reg_out = dbg.available_entry_reg_out;
    assign rollback_enabled_reg_out = dbg.rollback_enabled_reg_out;
    assign next_rollback_enabled_wire_out = dbg.next_rollback_enabled_wire_out;

    task exit_on_error;
        begin
            $display("@@@Failed at time %d", $time);
            $finish;
        end
    endtask

    task clear_new_inst_input;
        for (int i = 0; i < 3; i++) new_inst_supplies[i] = '0;
    endtask

    task clear_executed_rob_index;
        for (int i = 0; i < 3; i++) begin
            executed_inst_rob_index[i].valid = 0;
            executed_inst_rob_index[i].rob_index = `N-1;
        end
    endtask

    task print_head_tail;
        $display("time: %d\thead: %d\ttail: %d\t|\tnext_head: %d\tnext_tail: %d\ttail_offset:%d", $time, head_out, tail_out, next_head_out, next_tail_out, tail_offset_out);
    endtask

    task print_next_rob_entry_array;
        $display("TIME: %d\t NEXT_ROB_ENTRY:", $time);
        for (int i = 0; i < `N; i ++)
            print_single_rob_entry_stored(head_out, tail_out, next_rob_entry_out[i].inst, next_rob_entry_out[i].newT, next_rob_entry_out[i].oldT, 
                                next_rob_entry_out[i].valid, next_rob_entry_out[i].rob_index, next_rob_entry_out[i].has_executed, next_rob_entry_out[i].has_completed);
    endtask

    task print_ALL_ROB_Entries;
        $display("TIME: %d\t ROB_CONTENT:\thead: %d\ttail: %d\trollback_enabled_reg: %d", $time, head_out, tail_out, dbg.rollback_enabled_reg_out);
        for (int i = 0; i < `ROB_SZ; i ++)
            print_single_rob_entry_stored(head_out, tail_out, next_rob_entry_out[i].inst.inst, next_rob_entry_out[i].newT, next_rob_entry_out[i].oldT, 
                                next_rob_entry_out[i].valid, next_rob_entry_out[i].rob_index, next_rob_entry_out[i].has_executed, next_rob_entry_out[i].has_completed);
    endtask
    // global inst counter
    int inst_counter;
    // generate rob entry and pass to rob
    function ROB_PACKET gen_single_rob_entry(
                       PHYS_REG_T newT,
                       PHYS_REG_T oldT,
                       logic valid,
                       int inst_counter);
        ROB_PACKET rob_entry_out = '0;
        if (valid==0) rob_entry_out = '0;
        else begin
            rob_entry_out.inst =  inst_counter;
            rob_entry_out.newT =  newT;
            rob_entry_out.oldT =  oldT;
            rob_entry_out.valid = valid;
        end
        return rob_entry_out;
    endfunction

    function ROB_REF_T gen_single_executed_inst_entry(logic [$clog2(`ROB_SZ)-1:0] rob_index, logic valid);
        ROB_REF_T ret = '0;
        ret.rob_index = rob_index;
        ret.valid = valid;
        return ret;
    endfunction

    function ROB_REF_T gen_hazard_ref_entry(logic [$clog2(`ROB_SZ)-1:0] rob_index, logic valid);
        ROB_REF_T ret = '0;
        ret.rob_index = rob_index;
        ret.valid = valid;
        return ret;
    endfunction

    function ROB_PACKET gen_wfi_rob_entry(
                       PHYS_REG_T newT,
                       PHYS_REG_T oldT,
                       logic valid,
                       int inst_counter);
        ROB_PACKET rob_entry_out = '0;
        if (valid==0) rob_entry_out = '0;
        else begin
            rob_entry_out.inst =  0;
            rob_entry_out.inst.pc = inst_counter;
            rob_entry_out.newT =  newT;
            rob_entry_out.oldT =  oldT;
            rob_entry_out.valid = valid;
            rob_entry_out.inst.inst = `WFI;
        end
        inst_counter += 1;
        return rob_entry_out;
    endfunction

    function ROB_PACKET gen_load_rob_entry(
                       PHYS_REG_T newT,
                       PHYS_REG_T oldT,
                       logic valid,
                       int inst_counter);
    

        ROB_PACKET rob_entry_out = '0;
        if (valid==0) rob_entry_out = '0;
        else begin
            rob_entry_out.inst =  0;
            rob_entry_out.inst.pc = inst_counter;
            rob_entry_out.newT =  newT;
            rob_entry_out.oldT =  oldT;
            rob_entry_out.valid = valid;
            case (inst_counter >> 3)
                0: rob_entry_out.inst.inst = `Itype(`RV32_LOAD, 3'b000);
                1: rob_entry_out.inst.inst = `Itype(`RV32_LOAD, 3'b001);
                2: rob_entry_out.inst.inst = `Itype(`RV32_LOAD, 3'b010);
                3: rob_entry_out.inst.inst = `Itype(`RV32_LOAD, 3'b100);
                4: rob_entry_out.inst.inst = `Itype(`RV32_LOAD, 3'b101);
                default: rob_entry_out.inst.inst = `Itype(`RV32_LOAD, 3'b000);
            endcase
        end
        $display("LW: %b", rob_entry_out.inst.inst);
        inst_counter += 1;
        return rob_entry_out;
    endfunction


    function ROB_PACKET gen_store_rob_entry(
                       PHYS_REG_T newT,
                       PHYS_REG_T oldT,
                       logic valid,
                       int inst_counter);
        ROB_PACKET rob_entry_out = '0;
        if (valid==0) rob_entry_out = '0;
        else begin
            rob_entry_out.inst =  0;
            rob_entry_out.inst.pc = inst_counter;
            rob_entry_out.newT =  newT;
            rob_entry_out.oldT =  oldT;
            rob_entry_out.valid = valid;
            case (inst_counter >> 2)
                0: rob_entry_out.inst.inst = `Stype(`RV32_STORE, 3'b000);
                1: rob_entry_out.inst.inst = `Stype(`RV32_STORE, 3'b001);
                2: rob_entry_out.inst.inst = `Stype(`RV32_STORE, 3'b010);
                default: rob_entry_out.inst.inst = `Stype(`RV32_STORE, 3'b000);
            endcase
        end
        $display("ST: %b", rob_entry_out.inst.inst);
        inst_counter += 1;
        return rob_entry_out;
    endfunction

    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end



    initial begin
        $display("Start testing, ROB size: %d.", `ROB_SZ);
        
        $monitor("time: %d | available_entry_sig: %d | head: %d | tail: %d | avaible_entry_reg: %d | tail_offset: %d | rollback_reg: %d | next_head_real: %d | next_tail_real: %d | rollback_branch_index_reg_out: %d | rollback_enable_reg: %d | next_rollback_enabled_wire: %d | [rollback]\tcancellation_broadcast.valid: %d | cancellation_broadcast.rob_index: %d | num_retire_load_store_inst: %d | rollback_load_store_inst: %d", 
        $time, available_entry_sig, head_out,  tail_out, available_entry_reg_out, tail_offset_out, rollback_enabled_reg_out, next_head_out, next_tail_out,
         rollback_branch_index_reg_out, rollback_enabled_reg_out, next_rollback_enabled_wire_out, cancellation_broadcast.valid, cancellation_broadcast.rob_index,
         num_retire_load_store_inst, rollback_load_store_inst);
        // init
        clock = 0;
        reset = 1;
        clear_new_inst_input();
        clear_executed_rob_index();
        branch_outcomes_fu_man = '0;
        hazard_ref = '0;
        stall_store_retire = 0;
        @(posedge clock)

        @(posedge clock)
        reset = 0;


        // test when ROB empty, number of available slots should be correct
        @(posedge clock)


        assert(available_entry_sig==`ROB_SZ) else begin
            $display("ROB actual output: %d", available_entry_sig);
            exit_on_error();
        end

        // @(negedge clock)


        @(posedge clock)
        // insert 3 inst to RO, no tag forwarding
        new_inst_supplies[0] = gen_load_rob_entry(0,0,1,inst_counter);
        new_inst_supplies[1] = gen_store_rob_entry(1,1,1,inst_counter);
        new_inst_supplies[2] = gen_load_rob_entry(2,2,1,inst_counter);

        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)
        clear_new_inst_input();
        for (int i = 0 ;i < 3; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i,1);
        end
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)
        executed_inst_rob_index = '0;
        print_ALL_ROB_Entries();

        @(posedge clock)
        // insert 3 inst to RO, no tag forwarding
        new_inst_supplies[0] = gen_load_rob_entry(3,0,1,inst_counter);
        new_inst_supplies[1] = gen_store_rob_entry(4,1,1,inst_counter);
        new_inst_supplies[2] = gen_store_rob_entry(5,2,1,inst_counter);

        @(posedge clock)
        stall_store_retire = 1;
        for (int i = 0 ;i < 3; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i+2,1);
        end
        // hazard_ref[0] = gen_hazard_ref_entry(2,1);
        clear_new_inst_input();
        print_ALL_ROB_Entries();
        @(posedge clock)
        
        hazard_ref = '0;
        clear_new_inst_input();
        print_ALL_ROB_Entries();
        @(posedge clock)
        stall_store_retire = 0;
        print_ALL_ROB_Entries();
        @(posedge clock)
        print_ALL_ROB_Entries();
        @(posedge clock)
        print_ALL_ROB_Entries();
            @(posedge clock)
        print_ALL_ROB_Entries();
        @(posedge clock)
        print_ALL_ROB_Entries();
        $finish;


        // insert 2 inst to ROB
        // for (int i = 0; i < 3; i++) begin
        //     if (i == 2) new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,0,inst_counter);
        //     else new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,1,inst_counter);
        //     inst_counter = inst_counter + 1;
        // end

        print_ALL_ROB_Entries();

        @(posedge clock)
        clear_new_inst_input();
        // clear_new_inst_input();
        print_ALL_ROB_Entries();
        // // insert 2 inst to ROB
        // for (int i = 0; i < 3; i++) begin
        //     if (i == 2) new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,0,inst_counter);
        //     else new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,1,inst_counter);
        //     inst_counter = inst_counter + 1;
        // end
        /// retire three
        for (int i = 0 ;i < 3; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i,1);
        end

        @(posedge clock)
        for (int i = 0 ;i < 3; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i+3,1);
            if (i==2) executed_inst_rob_index[i] = '0;
        end
        print_ALL_ROB_Entries();


        @(posedge clock)

        print_ALL_ROB_Entries();


        @(posedge clock)
        clear_executed_rob_index();
        print_ALL_ROB_Entries();

        inst_counter = 0;
        @(posedge clock)
        for (int i = 0; i < 3; i++) begin
            if (i == 2) begin 
                new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,0,inst_counter);
                inst_counter = inst_counter + 1;
            end
            else new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,1,inst_counter);
        end
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)

        for (int i = 0; i < 3; i++) begin
            new_inst_supplies[i] = gen_single_rob_entry(i+5,i,1,inst_counter);
            inst_counter = inst_counter + 1;
        end
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)
        clear_new_inst_input();
        @(posedge clock)
        print_ALL_ROB_Entries();

        // executed_inst_rob_index[1] = gen_single_executed_inst_entry(0,1)
        // executed_inst_rob_index[0] = gen_single_executed_inst_entry(4,1)

        @(posedge clock)
        $display("rollback to head warp");
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)
        // executed_inst_rob_index[1] = gen_single_executed_inst_entry(1,1);


        hazard_ref[0] = gen_hazard_ref_entry(1,1);
        hazard_ref[1] = gen_hazard_ref_entry(2,1);
        hazard_ref[2] = gen_hazard_ref_entry(4,1);
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)
        hazard_ref = '0;
        
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)
        
        @(negedge clock)
        print_ALL_ROB_Entries();

        @(posedge clock)

        @(negedge clock)
        print_ALL_ROB_Entries();
        
        @(posedge clock)

        @(negedge clock)
        print_ALL_ROB_Entries();
        
        @(posedge clock)

        @(negedge clock)
        print_ALL_ROB_Entries();


        @(posedge clock)
        // insert 3 inst to RO, no tag forwarding
        for (int i = 0; i < 3; i++) begin
            new_inst_supplies[i] = gen_single_rob_entry(i+5,i,1,inst_counter);
            inst_counter = inst_counter + 1;
        end

        @(posedge clock)
        // clear_new_inst_input();
        // insert 2 inst to ROB
        for (int i = 0; i < 3; i++) begin
            if (i == 2) new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,0,inst_counter);
            else new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,1,inst_counter);
            inst_counter = inst_counter + 1;
        end

        print_ALL_ROB_Entries();

        @(posedge clock)
        clear_new_inst_input();
        // clear_new_inst_input();
        print_ALL_ROB_Entries();
        // // insert 2 inst to ROB
        // for (int i = 0; i < 3; i++) begin
        //     if (i == 2) new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,0,inst_counter);
        //     else new_inst_supplies[i] = gen_single_rob_entry(i+5+3,i,1,inst_counter);
        //     inst_counter = inst_counter + 1;
        // end
        /// retire three
        for (int i = 0 ;i < 3; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i,1);
        end

        @(posedge clock)
        for (int i = 0 ;i < 3; i++) begin
            executed_inst_rob_index[i] = gen_single_executed_inst_entry(i+3,1);
            if (i==2) executed_inst_rob_index[i] = '0;
        end
        print_ALL_ROB_Entries();


        // @(negedge clock)
        // clear_new_inst_input();
        // print_ALL_ROB_Entries();
        // // generate rollback condition
        // hazard_ref[0] = gen_hazard_ref_entry(1, 1);
        // hazard_ref[1] = gen_hazard_ref_entry(3, 0);
        // hazard_ref[2] = gen_hazard_ref_entry(4, 1);

        // @(negedge clock)
        // $display("rollback_branch_index_reg_out: %d", rollback_branch_index_reg_out);
        // assert(rollback_branch_index_reg_out == 1) else exit_on_error();
        // hazard_ref[0] = gen_hazard_ref_entry(0, 0);
        // hazard_ref[1] = gen_hazard_ref_entry(1, 0);
        // hazard_ref[2] = gen_hazard_ref_entry(4, 0);

        // @(negedge clock)
        //  print_ALL_ROB_Entries();
        //  assert(available_entry_sig == 0) else exit_on_error();

        // @(negedge clock)
        //  print_ALL_ROB_Entries();
        //  assert(available_entry_sig == 0) else exit_on_error();


        // @(negedge clock)
        //  print_ALL_ROB_Entries();
         


        // @(negedge clock)
        //  print_ALL_ROB_Entries();



        // @(negedge clock)
        //  print_ALL_ROB_Entries();

        $finish;

    end

endmodule