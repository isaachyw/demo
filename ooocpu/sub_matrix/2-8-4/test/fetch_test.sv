`include "verilog/sys_defs.svh"
// `define CACHE_MODE
`define DEBUG
`ifndef CLOCK_PERIOD
`define CLOCK_PERIOD 20 // make it bigger for memory to late in cycle
`endif

task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;

  logic clock;
  logic reset;

  // from i cache to mem
  logic [`XLEN-1:0] icache2mem_addr_wire;
  logic [1:0] icache2mem_command_wire;
  logic [`N-1:0]icache_valids;

  FETCH_RES [`N-1:0] final_fetch_res,to_inst_buffer,next_fetch_out,fetch_no_pred_out;
  logic jump_unresolved, next_jump_unresolved;
  // from mem to fetch
  logic [3:0] mem2icache_response_wire;
  logic [63:0] mem2icache_data_wire;
  logic [3:0] mem2icache_tag_wire;
  MEM_SIZE proc2mem_size;
  logic d_request;
  PC_T rollback_pc;

  /****** From Dispatch to Fetch ******/
  logic [$clog2(`INST_BUFFER_SZ):0] dispatch_count;
  /****** From Backend to Fetch ******/
  BRANCH_OUTCOME rollback;
  BP_UPDATE [`N-1:0] bp_update;
  //   // debug output:
  logic [63:0] unified_memory[`MEM_64BIT_LINES - 1:0];

  // CLOCK_PERIOD is defined on the commandline by the makefile
  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end

  mem mem (
      .clk(clock),
      .proc2mem_addr(icache2mem_addr_wire),
      .proc2mem_data(),
`ifndef CACHE_MODE
      .proc2mem_size(proc2mem_size),
`endif

      .proc2mem_command(icache2mem_command_wire),
      .mem2proc_response(mem2icache_response_wire),
      .mem2proc_data(mem2icache_data_wire),
      .mem2proc_tag(mem2icache_tag_wire)
      // DEBUG:
`ifdef DEBUG,
      .memory_out(unified_memory)
`endif
  );

  fetch fetch (
      .clock(clock),
      .reset(reset),
      .fetch_res_out(final_fetch_res),
      .fetch2Imem_command(icache2mem_command_wire),
      .fetch2Imem_addr(icache2mem_addr_wire),
      .Imem2fetch_response(mem2icache_response_wire),
      .Imem2fetch_data(mem2icache_data_wire),
      .Imem2fetch_tag(mem2icache_tag_wire),
      .d_request(d_request),
      .rollback_pc(rollback_pc),
      .dispatch_count(dispatch_count),
      .rollback(rollback),
      .branch_update(bp_update)
      `ifdef DEBUG
      , .to_inst_buffer(to_inst_buffer)
      , .next_fetch_out(next_fetch_out)
      , .jump_unresolved_out(jump_unresolved)
      , .next_jump_unresolved_out(next_jump_unresolved)
      , .fetch_no_pred_out(fetch_no_pred_out)
      , .Icache_valid_out_debug(icache_valids)
      `endif
  );

  /***************** tasks *****************/
    task reset_all_input;
      dispatch_count = 0;
      d_request = 0;
      rollback = '0;
      bp_update = '0;
      rollback_pc = 0;
    endtask  //reset_all_input
    task wait_until_done;
      forever begin : wait_loop
        @(negedge clock);
        if (mem2icache_tag_wire) begin
          disable wait_until_done;
        end
      end
    endtask

    // task display_fetch_res;
    //   $display("CACHE PRINT: (time %d)", $time);
    //   for (int i = 0; i < `CACHE_LINES_DEBUG; i++) begin
    //     $display("idx: %d, DATA: %h, valid: %d, tag: %b", i, cache_lines[i].data,
    //              cache_lines[i].valid, cache_lines[i].tags);
    //   end
    // endtask

  //   task display_mem;
  //     $display("MEM PRINT: (time %d)", $time);
  //     for (int i = 0; i < `MEM_64BIT_LINES; i++) begin
  //       if (unified_memory[i] != 0) begin
  //         $display("index: %d\tdata:%h", i, unified_memory[i]);
  //       end
  //     end
  //   endtask

  //   task display_valids;
  //     $display("icache valids out: %3b", icache_valids);
  //   endtask

  //   task assert_reset;
  //     assert (icache_valids == '0)
  //     else exit_on_error();
  //     assert (icache_insts == '0)
  //     else exit_on_error();
  //     assert (cache_lines == '0)
  //     else exit_on_error();
  //   endtask

  /************functions************/

  /*  Test Methodology:
  1.test reset is clean
  2.test valid bit for all miss
    2.1 test mem successfully return the data and first valid bit is set

  */

  initial begin
    // monitor the final fetch res
    clock = 0;
    if(`N==3) begin
      $monitor("time: %4d | result:{resV:%b%b%b|icache:%b%b%b |pc: %3d%3d%3d |next:%3d%3d%3d|no_pred:%3d%3d%3d|r: %b%b}",
       $time, to_inst_buffer[0].valid,to_inst_buffer[1].valid,
      to_inst_buffer[2].valid,
      icache_valids[0],icache_valids[1],icache_valids[2],
      
       to_inst_buffer[0].pc,to_inst_buffer[1].pc,to_inst_buffer[2].pc
      ,next_fetch_out[0].pc,next_fetch_out[1].pc,next_fetch_out[2].pc,
      fetch_no_pred_out[0].pc,fetch_no_pred_out[1].pc,fetch_no_pred_out[2].pc,
      jump_unresolved,next_jump_unresolved);
    end
    reset_all_input();
    reset = 1;
    @(posedge clock);
    @(posedge clock);
    reset = 0;
    #250;

    /************ Test rollback ************/
    @(posedge clock);
    rollback.valid = 1;
    rollback.direction = NOT_TAKEN;
    rollback.target = 400;
    rollback_pc = 800;
    @(posedge clock);
    reset_all_input();
    #500;
    $display("Again..");
    @(posedge clock);
    rollback.valid = 1;
    rollback.target = 4;
    rollback.direction = TAKEN;
    @(posedge clock);
    reset_all_input();
    #300;
    $display("@@@Passed!");
    $finish;
  end

endmodule