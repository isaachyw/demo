`include "verilog/sys_defs.svh"
`define ignore_assert_inst_buffer
task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;
    logic clock, reset, rollback;
    FETCH_RES [`N-1:0] fetch_res;
    logic [$clog2(`INST_BUFFER_SZ):0] dispatch_count;  // use for free up inst_buffer
    logic [$clog2(`INST_BUFFER_SZ):0] available_slot_num;
    FETCH_RES [`N-1:0] fetch_res_out;

    logic     [        31:0] cycle_count;

    always_ff @(posedge clock) begin
        if (reset) cycle_count <= 0;
        else cycle_count <= cycle_count + 1;
    end
  `ifdef DEBUG
    logic [$clog2(`INST_BUFFER_SZ) + 1:0] debug_head, debug_size;
    FETCH_RES [`INST_BUFFER_SZ-1:0] debug_instruction_buffer;
  `endif

  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end
  inst_buffer dut (
    .clock(clock),
    .reset(reset),
    .rollback(rollback),
    .fetch_res(fetch_res),
    .dispatch_count(dispatch_count),
    .available_slot_num(available_slot_num),
    .fetch_res_out(fetch_res_out)
  `ifdef DEBUG
    ,.debug_head(debug_head)
    ,.debug_size(debug_size)
    ,.debug_instruction_buffer(debug_instruction_buffer)
  `endif
  );


  task make_addi;
    input ARCH_REG_T in;
    output INST out;
    
    out.i.imm = 0;
    out.i.rs1 = 5'd1;
    out.i.funct3 = 0;
    out.i.opcode = 7'b0010011;
    out.i.rd = in;
  endtask


  task send3inst;
    input PC_T pc_base;
    for (int i = 0; i < `N; i++) begin
        fetch_res[i].pc = pc_base + i;
        make_addi(i + 4, fetch_res[i].inst);
        fetch_res[i].valid  = 1;
    end
  endtask


  task checkinst;
    input PC_T pc_base;
    input integer count;
    for (int i = 0; i < count; i++) begin
      $display("fetch_res_out: %p", fetch_res_out);
      $display("i: %p, pc: %p, pc_base + i: %p", i, fetch_res_out[i].pc, pc_base + i);
      assert(fetch_res_out[i].pc === pc_base + i) else exit_on_error();
      assert(fetch_res_out[i].valid) else exit_on_error();
      assert(fetch_res_out[i].inst.i.opcode === 7'b0010011) else exit_on_error();
    end
  endtask

  task print_debug;
  FETCH_RES tmp;
`ifdef DEBUG
    $display("head: %p size: %p available_slot_num: %p", debug_head, debug_size, available_slot_num);
    for (int i = 0; i < `INST_BUFFER_SZ; i++) begin
      tmp = debug_instruction_buffer[i];
      $display("i: %p, inst buf: %p", i, tmp);
    end
    for (int i = 0; i < `N; i++) begin
      tmp = fetch_res_out[i];
      $display("i: %p, fetch_res_out[i]: %p", i, tmp);
    end
`endif
  endtask

  

  initial begin
    $display("inst buffer test");
`ifdef DEBUG
    $display("!!! running in DEBUG");
`else
    $display("!!! running in prod");
`endif

    reset = 0;
    clock = 0;
    fetch_res = 0;
    dispatch_count = 0;
    rollback = 0;
    /* Test 0: Check reset success */
    @(posedge clock);
    reset = 1;
    @(posedge clock);

    send3inst(0);
    @(negedge clock);
    assert (available_slot_num == `INST_BUFFER_SZ - `N * 2) else exit_on_error(); // available_slot_num is in-cycle forwarded
    fetch_res = 0;

    @(posedge clock);

    assert (available_slot_num == `INST_BUFFER_SZ - `N) else exit_on_error(); // because of reset = 1
    reset = 0;
    dispatch_count = `N;
    send3inst(5);
    @(negedge clock);
    for (int i = 0; i < `N; i++) begin
      assert(fetch_res_out[i].valid == 0);
    end
    // checkinst(5, 3);
    $display("available_slot_num@101 %p", available_slot_num);
    assert (available_slot_num == `INST_BUFFER_SZ - `N) else exit_on_error();

    // for (int options = 0; options < 2; options++) begin
      for (int i = 0; i < (`INST_BUFFER_SZ - `N - 1) / 2; i++) begin
          @(posedge clock);
          dispatch_count = 3;
          send3inst(i * 3 + 10);
          $display("fetched inst: %p", fetch_res_out);
          @(negedge clock);
          $display("available_slot_num@109 %p", available_slot_num);
          $display("fetched inst: %p", fetch_res_out);
          print_debug();
          $display("i: %p, available_slot_num: %p", i, available_slot_num);
          // assert (available_slot_num == `INST_BUFFER_SZ - (i + 1) * 2) else exit_on_error();
          for (int j = 0; j < `N; j++) begin
              assert(fetch_res_out[i].valid == 0);
              $display("@ i = %p: %p", i, fetch_res_out);
          end
      end

      for (int i = 0; i < 1; i++) begin
          @(posedge clock);
          dispatch_count = 1;
          send3inst(i * 3 + 10);
          $display("fetched inst: %p", fetch_res_out);
          @(negedge clock);
          $display("available_slot_num@109 %p", available_slot_num);
          $display("fetched inst: %p", fetch_res_out);
          print_debug();
          for (int j = 0; j <= 1; j++) begin
              assert(fetch_res_out[j].valid);
          end
          // checkinst(10 + i, 2);
          $display("i: %p, available_slot_num: %p", i, available_slot_num);
          // assert (available_slot_num == `INST_BUFFER_SZ - (i + 1) * 2) else exit_on_error();
          for (int j = 0; j < `N; j++) begin
              $display("@ i = %p: %p", i, fetch_res_out);
          end
      end

      // for (int i = `INST_BUFFER_SZ / 2; i < `INST_BUFFER_SZ + `INST_BUFFER_SZ / 2; i++) begin
      //     @(posedge clock);
      //     dispatch_count = 1;
      //     if (options == 0) 
      //       fetch_res = 0;
      //     else 
      //       send3inst(i * 3 + 10);
      //     $display("fetched inst: %p", fetch_res_out);
      //     @(negedge clock);
      //     $display("available_slot_num@109 %p", available_slot_num);
      //     $display("fetched inst: %p", fetch_res_out);
      //     print_debug();
      //     checkinst(10 + i, 1);
      //     for (int j = 0; j < `N; j++) begin
      //         $display("@ i = %p: %p", i, fetch_res_out);
      //     end
      // end
      // $display("available_slot_num finally %p", available_slot_num == (options ? 0 : 16));
    // end

    // // swap all inst out
    // for (int i = 0; i < 8; i++) begin
    //   assert (available_slot_num == 0) else exit_on_error(); 
    //   @(posedge clock);
    //   dispatch_count = 2;
    //   send3inst(i * 2 + 128);
    //   @(negedge clock);
    //   $display("swapping out i: %p, fetch_res_out: %p", i, fetch_res_out);
    //   assert (available_slot_num == 0) else exit_on_error(); 
    // end

    // for (int i = 0; i < 8; i++) begin
    //   assert (available_slot_num == 0) else exit_on_error(); 
    //   @(posedge clock);
    //   dispatch_count = 2;
    //   send3inst(i * 2 + 128);
    //   @(negedge clock);
    //   $display("checking out i: %p, fetch_res_out: %p", i, fetch_res_out);
    //   assert (available_slot_num == 0) else exit_on_error(); 
    //   checkinst(i * 2 + 128, 2);
    // end

    // reset = 1;
    // fetch_res = 0;
    // dispatch_count = 0;
    // @(posedge clock);
    // @(negedge clock);
    // assert (available_slot_num == `INST_BUFFER_SZ) else exit_on_error(); 


    // fetch_res = 0;
    // dispatch_count = 0;
    // $display("available_slot_num %p", available_slot_num);
    // assert (available_slot_num == `INST_BUFFER_SZ - `N) else exit_on_error();
    $display("@@@Passed");
    $finish;
  end

endmodule
