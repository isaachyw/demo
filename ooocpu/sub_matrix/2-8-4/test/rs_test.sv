// Warning: This should test with rs sz
`include "verilog/sys_defs.svh"
task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;

  logic clock, reset;

  // Dispatch:
  // For populating new instructions
  // output NUM_INST_T_RS num_entry_available,
  RS_INPUT [`N-1:0] new_inst_supplies;
  // output NUM_INST_T num_entry_available,
  RS_INPUT [`N_WIDE] new_inst_supplies;

  // Listening to CDB forwarding in the subsequent trials
  CDB_ENTRY [`CDB_W-1:0] cdb_entries;
  NUM_INST_T_RS num_entry_available_out;
  // Issue:
  // The availability of FU next cycle
  // input FU_STATUS fu_status,
  // The instructions that's ready to execute in the next cycle
  RS_ENTRY [`RS_SZ-1:0] rs_entries_out;
  RS_REF_T [`N_WIDE] fu_feedback;

  // Cancellation:
  ROB_REF_T cancel_ref;
  logic [2:0][$clog2(`RS_SZ)-1:0] next_three_idx_out;
  logic [`RS_SZ-1:0] ready_issue_out;
  logic [$clog2(`RS_SZ)-1:0] num_rs_available_out;
  rs dut (
      clock,
      reset,
      new_inst_supplies,
      cdb_entries,
      num_entry_available_out,
      rs_entries_out,
      ready_issue_out,
      fu_feedback,
      cancel_ref
  );

  //   task assert_get_n;
  //     input integer num;
  //     for (integer i = 0; i < num; i = i + 1) begin
  //       assert (i <= num ? free_reg_out[i].valid == 1 : free_reg_out[i].valid == 0)
  //       else begin
  //         $display("Should get %d reg, violate at index: %d, got: %p", num, i, free_reg_out);
  //         exit_on_error();
  //       end
  //     end
  //   endtask

  task display_rs_out;
    $display("---------------data_out:----------------------");
    for (integer i = 0; i < `RS_SZ; i = i + 1) begin
      $display("rs_out.valid[%d]:v: %d rob: %d ,op1:[%d] %d,op2:[%d] %d :::issue%d", i,
               rs_entries_out[i].valid, rs_entries_out[i].rob_ref.rob_index,
               rs_entries_out[i].operands_status[0].phy_reg_idx,
               rs_entries_out[i].operands_status[0].ready,
               rs_entries_out[i].operands_status[1].phy_reg_idx,
               rs_entries_out[i].operands_status[1].ready, ready_issue_out[i]);
    end
    $display("-------------------------------");
    $display("");
  endtask
  function RS_ENTRY gen_single_rs_entry_input(PHYS_REG_T op1, PHYS_REG_T op2, logic valid = 1,
                                              logic [$clog2(`ROB_SZ)-1:0] rob_index = 1);
    RS_ENTRY rs_entry_in = '0;
    rs_entry_in.valid = 1;
    if (valid) begin
      rs_entry_in.operands_status[0].ready = 1;
      rs_entry_in.operands_status[1].ready = 1;
      rs_entry_in.operands_status[0].valid = 1;
      rs_entry_in.operands_status[1].valid = 1;
      rs_entry_in.operands_status[0].phy_reg_idx = op1;
      rs_entry_in.operands_status[1].phy_reg_idx = op2;
      rs_entry_in.rob_ref.rob_index = rob_index;
      rs_entry_in.rob_ref.valid = 1;
    end
    return rs_entry_in;
  endfunction

  function RS_ENTRY gen_single_rs_entry_Nready(PHYS_REG_T op1, PHYS_REG_T op2 = 0, logic valid = 1);
    RS_ENTRY rs_entry_in = '0;
    rs_entry_in.valid = 1;
    if (valid) begin
      rs_entry_in.operands_status[0].ready = 0;
      rs_entry_in.operands_status[0].valid = 1;
      rs_entry_in.operands_status[1].ready = 0;
      rs_entry_in.operands_status[1].valid = 0;
      rs_entry_in.operands_status[0].phy_reg_idx = op1;
      rs_entry_in.operands_status[1].phy_reg_idx = op2;
    end
    return rs_entry_in;
  endfunction

  function void set_cdb(PHYS_REG_T phy0, phy1, phy2, logic [2:0] valids);
    cdb_entries[0].valid = valids[0];
    cdb_entries[0].phy_reg_idx = phy0;
    cdb_entries[1].valid = valids[1];
    cdb_entries[1].phy_reg_idx = phy1;
    cdb_entries[2].valid = valids[2];
    cdb_entries[2].phy_reg_idx = phy2;
  endfunction

  function void set_fu_feedback(logic [$clog2(`RS_SZ)-1:0] rs0, rs1, rs2, logic [2:0] valids);
    fu_feedback[0].valid  = valids[0];
    fu_feedback[0].rs_idx = rs0;
    fu_feedback[1].valid  = valids[1];
    fu_feedback[1].rs_idx = rs1;
    fu_feedback[2].valid  = valids[2];
    fu_feedback[2].rs_idx = rs2;
  endfunction

  task reset_all;
    fu_feedback = '0;
    cancel_ref = '0;
    new_inst_supplies = '0;
    cdb_entries = '0;
  endtask

  task assert_avai_n;
    input integer num;
    assert (num_entry_available_out == num)
    else begin
      $display("Should get %d available, got: %p", num, num_entry_available_out);
      exit_on_error();
    end
  endtask

  task assert_ready_n;
    input integer num;
    integer counter = 0;
    for (integer i = 0; i < `RS_SZ; i = i + 1) begin
      if (i == 0) counter = 0;
      if (ready_issue_out[i] == 1'b1) begin
        counter++;
      end
    end
    assert (counter == num)
    else begin
      $display("Should get %d ready, got: %p", num, counter);
      exit_on_error();
    end
  endtask

  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end

  initial begin
    $display("Test 0: Test reset");
    clock = 0;
    reset = 1;
    reset_all();
    @(posedge clock);
    @(posedge clock);
    assert (rs_entries_out == '0 && ready_issue_out == '0)
    else begin
      display_rs_out();
      exit_on_error();
    end
    reset = 0;
    $display("Test 1: Test new inst supply only with ready");
    assert_avai_n(`RS_SZ);
    new_inst_supplies = '{
        '0,
        gen_single_rs_entry_input(8, 9, 1),
        gen_single_rs_entry_input(2, 0, 1, 2)
    };
    @(posedge clock);
    assert_ready_n(2);
    assert_avai_n(`RS_SZ - 2);
    $display("Test 2: Test new inst supply with not ready");
    new_inst_supplies = '{'0, gen_single_rs_entry_Nready(10), gen_single_rs_entry_Nready(4)};
    @(posedge clock);
    display_rs_out();
    assert_ready_n(2);
    assert_avai_n(`RS_SZ - 4);
    reset_all();
    new_inst_supplies = '0;
    $display("Test 3: Test cbd ready");
    set_cdb(8, 9, 10, '1);
    @(posedge clock);

    assert_avai_n(`RS_SZ - 4);
    display_rs_out();
    assert_ready_n(3);
    $display("Test 4: Test cancle ref");
    reset_all();
    cancel_ref.rob_index = 1;
    cancel_ref.valid = 1;
    @(posedge clock);
    assert_avai_n(`RS_SZ - 3);
    display_rs_out();
    $display("Test 5: Test fu feedback");
    set_fu_feedback(2, 3, 5, '1);
    cancel_ref = '0;
    @(posedge clock);
    assert_avai_n(`RS_SZ);
    assert (rs_entries_out == '0)
    else begin
      display_rs_out();
      exit_on_error();
    end
    $display("@@@Passed simple test, start complicated test...");
    reset = 1;
    reset_all();
    @(posedge clock);
    @(posedge clock);
    reset = 0;
    assert (rs_entries_out == '0);
    assert_avai_n(`RS_SZ);
    $display("Complicated Test 1: Test new inst supply with not ready while set by cdb later");
    new_inst_supplies = '{
        '0,
        gen_single_rs_entry_Nready(8, 9, 1),
        gen_single_rs_entry_Nready(2, 0, 1)
    };
    set_cdb(8, 9, 10, 3'b111);
    @(posedge clock);
    assert_avai_n(`RS_SZ - 2);
    assert_ready_n(1);
    display_rs_out();
    for (integer i = 0; i < `RS_SZ; i++) begin
      if (rs_entries_out[i].operands_status[1].valid) begin
        assert (rs_entries_out[i].operands_status[1].ready == 1);
      end
    end

    $display("Complicated Test 2: Test clear by fu feebback and then have new inst");
    reset_all();
    @(posedge clock);
    new_inst_supplies = '{
        '0,
        gen_single_rs_entry_Nready(3, 20, 1),
        gen_single_rs_entry_Nready(4, 21, 1)

    };
    set_fu_feedback(4, 5, 0, 3'b011);
    @(posedge clock);
    assert_avai_n(`RS_SZ - 2);
    display_rs_out();

    $display("@@@Passed all tests");
    $finish;
  end

endmodule