`include "verilog/sys_defs.svh"
task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;
  logic clock, reset;
  logic [1:0] reg_needed;
  FREE_LIST_ENTRY [2:0] retired_reg_in, free_reg_out;

`ifdef DEBUG
  DEBUG_FREE_LIST dbg;

  PHYS_REG_T [(`FREE_LIST_SIZE-1):0] data_out;
  logic [$clog2(`FREE_LIST_SIZE)-1:0] next_stack_ptr_out;

  assign data_out = dbg.data_out;
  assign next_stack_ptr_out = dbg.stack_ptr_out;
`endif

  free_list dut (
      clock,
      reset,
      reg_needed,
      retired_reg_in,
      free_reg_out
`ifdef DEBUG,
      dbg
`endif
  );

  task assert_get_n;
    input integer num;
    for (integer i = 0; i < `N; i = i + 1) begin
      assert (i < num ? free_reg_out[i].valid == 1 : free_reg_out[i].valid == 0)
      else begin
        $display("Should get %d reg, violate at index: %d, got: %p", num, i, free_reg_out);
        exit_on_error();
      end
    end
  endtask


  task assert_reg_at_n;
    input integer num;
    input PHYS_REG_T expected;

    begin
      assert (free_reg_out[num].valid)
      else begin
        $display("Entry %d should be free, got %p", num, free_reg_out);
        exit_on_error();
      end

      assert (free_reg_out[num].phy_reg_idx == expected)
      else begin
        $display("Entry %d produce result (%d) that is different than expected (%d)", num,
                 free_reg_out[num].phy_reg_idx, expected);
        $display("Got: %p", free_reg_out);
        exit_on_error();
      end
    end
  endtask

  // Intrusive tests
  task display_data_out;
`ifdef DEBUG
    $display("data_out:----------------------");
    for (integer i = 0; i < `FREE_LIST_SIZE; i = i + 1) begin
      $display("data_out[%d]: %d", i, data_out[i]);
    end
    $display("next_stack_ptr_out: %d", next_stack_ptr_out);
    $display("-------------------------------");
`else
    $display("Cannot display data_out without DEBUG flag");
`endif
  endtask

  task assert_stack_ptr;
    input integer num;
`ifdef DEBUG
    assert (next_stack_ptr_out == num)
    else begin
      $display("Should get stack_ptr: %d, got: %d", num, next_stack_ptr_out);
      exit_on_error();
    end
`endif
  endtask

  task assert_data_out_at_n;
    input integer num;
    input PHYS_REG_T result;
`ifdef DEBUG
    assert (data_out[num] == result)
    else begin
      $display("data_out violation, should be %d at %d, is %d", result, num, data_out[num]);
    end
`endif
  endtask

  // Helper function: schedule retire to retired_reg_in
  function void schedule_retire(PHYS_REG_T phy_reg_idx_1, PHYS_REG_T phy_reg_idx_2,
                                PHYS_REG_T phy_reg_idx_3);
    retired_reg_in[0].phy_reg_idx = phy_reg_idx_1;
    retired_reg_in[1].phy_reg_idx = phy_reg_idx_2;
    retired_reg_in[2].phy_reg_idx = phy_reg_idx_3;
    retired_reg_in[0].valid = phy_reg_idx_1 == 0 ? 0 : 1;
    retired_reg_in[1].valid = phy_reg_idx_2 == 0 ? 0 : 1;
    retired_reg_in[2].valid = phy_reg_idx_3 == 0 ? 0 : 1;
  endfunction

  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end

  initial begin
`ifdef DEBUG
    $display("!!! running in DEBUG");
`else
    $display("!!! running in prod");
`endif

    reset = 0;
    clock = 0;
    reg_needed = 0;
    retired_reg_in = 0;
    $display("Free list size: %d", `FREE_LIST_SIZE);
    /* Test 1: Check initialization with no retirement */
    @(posedge clock) reset = 1;

    $display("Test 1: Check initialization with no retirement");
    @(posedge clock) assert_get_n(0);
    display_data_out();
    reset = 0;
    assert_stack_ptr(0);
    // Initial pull
    reg_needed = 2'd1;
    @(posedge clock) assert_get_n(1);
    assert_stack_ptr(1);
    // The head should be at the 33rd physical register.
    assert_reg_at_n(0, 6'd32);
    reg_needed = 2'd2;
    @(posedge clock) assert_get_n(2);
    assert_stack_ptr(3);
    assert_reg_at_n(0, 6'd33);
    assert_reg_at_n(1, 6'd34);
    reg_needed = 2'd2;
    @(posedge clock) assert_get_n(2);
    assert_reg_at_n(0, 6'd35);
    assert_reg_at_n(1, 6'd36);
    assert_stack_ptr(5);

    /* Test2: Retire more than dispatch*/
    reg_needed = 2'd0;
    reset = 1;
    @(posedge clock) $display("Test2: Retire more than dispatch init");
    @(posedge clock) display_data_out();
    reset = 0;
    reg_needed = 2'd2;
    @(posedge clock) assert_stack_ptr(2);
    assert_reg_at_n(0, 6'd32);
    assert_reg_at_n(1, 6'd33);
    reg_needed = 2'd0;
    @(posedge clock) assert_stack_ptr(2);
    $display("schedule_retire(1, 2, 0)");
    schedule_retire(1, 2, 0);
    reg_needed = 2'd1;
    @(posedge clock) assert_get_n(1);
    // Prioritize retired registers
    assert_reg_at_n(0, 6'd1);
    assert_stack_ptr(1);
    reg_needed = 2'd0;
    schedule_retire(3, 0, 0);
    @(posedge clock) assert_get_n(0);
    schedule_retire(0, 0, 0);
    @(posedge clock) display_data_out();
    // check the value has been successfully retired
    assert_data_out_at_n(0, 3);
    assert_data_out_at_n(1, 2);


    /* Test3: Retire less than dispatch*/
    @(posedge clock) reg_needed = 2'd0;
    reset = 1;
    schedule_retire(0, 0, 0);
    @(posedge clock);
    $display("Test3: Retire less than dispatch init");
    @(posedge clock) reset = 0;
    reg_needed = 2'd2;
    schedule_retire(1, 0, 0);
    @(posedge clock) assert_get_n(2);
    assert_reg_at_n(0, 6'd1);
    assert_reg_at_n(1, 6'd32);
    assert_stack_ptr(1);

    /* Test 4: Stable after reset/full */
    $display("Test 4: Stable after reset/full");
    @(posedge clock) reg_needed = 0;
    schedule_retire(0, 0, 0);  // clear the retire
    reset = 1;
    @(posedge clock) reset = 0;
    assert_stack_ptr(0);
    @(posedge clock) assert_get_n(0);
    assert_stack_ptr(0);
    @(posedge clock) assert_get_n(0);
    assert_stack_ptr(0);
    @(posedge clock) assert_get_n(0);
    assert_stack_ptr(0);
    @(posedge clock) assert_get_n(0);
    assert_stack_ptr(0);

    /* Test 5: Confirm behavior on empty free-list */
    $display("Test 5: Test behavior when empty");
    reset = 1;
    reg_needed = 0;
    @(posedge clock) reset = 0;
    display_data_out();
    @(posedge clock) reg_needed = 2'b11;
    @(posedge clock) assert_get_n(3);
    reg_needed = 2'd2;
    @(posedge clock) assert_get_n(2);
    reg_needed = 0;
    @(posedge clock) assert_get_n(0);

    // Should be stable
    @(posedge clock) assert_stack_ptr(5);
    @(posedge clock) assert_stack_ptr(5);
    @(posedge clock) assert_stack_ptr(5);
    @(posedge clock) assert_stack_ptr(5);
    @(posedge clock) assert_stack_ptr(5);

    // Should be able to stay at 0 size with in/out
    schedule_retire(5, 0, 0);
    reg_needed = 1;
    @(posedge clock) assert_get_n(1);
    assert_reg_at_n(0, 6'd5);
    assert_stack_ptr(5);
    @(posedge clock) assert_get_n(1);
    assert_reg_at_n(0, 6'd5);
    assert_stack_ptr(5);
    @(posedge clock) assert_get_n(1);
    assert_reg_at_n(0, 6'd5);
    assert_stack_ptr(5);
    
    /* Test 6: Noncompact return reg */
    $display("Test 6: Noncompact return reg");
    reset = 1;
    reg_needed = 0;
    @(posedge clock) reset = 0;
    reg_needed = 3;
    @(posedge clock) assert_get_n(3);
    reg_needed = 0;
    @(posedge clock) schedule_retire(0, 0, 5);
    reg_needed = 1;
    @(posedge clock) assert_get_n(1);
    assert_reg_at_n(0, 6'd5);
    reg_needed = 0;
    @(posedge clock) schedule_retire(0, 5, 0);
    reg_needed = 1;
    @(posedge clock) assert_get_n(1);
    assert_reg_at_n(0, 6'd5);
    reg_needed = 0;
    @(posedge clock) schedule_retire(0, 5, 6);
    reg_needed = 2;
    @(posedge clock) assert_get_n(2);
    assert_reg_at_n(0, 6'd5);
    assert_reg_at_n(1, 6'd6);


    $display("@@@Passed");
    $finish;
  end

endmodule
