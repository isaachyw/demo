.section .text
.globl main

main:
  li x5, 3           # Outer loop counter (initialize to 3)
  li x6, 6        # Inner loop counter (initialize to 5)
  li x4, 12
  li x8, 2
  li x3, 10

inner_loop:
  beqz x6, end_program  # Exit the inner loop when the inner loo counter reaches 0
  # Your inner loop code here (without memory operations)
  mul x3, x4, x3
  mul x3, x4, x8
  # Decrement the inner loop counter
  addi x6, x6, -1     # Decrement x6 by 1
  j inner_loop



end_program:
  # End of the program
  wfi                 # No operation (placeholder)