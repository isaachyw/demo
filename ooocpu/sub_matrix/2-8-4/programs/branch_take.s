.section .text
    .globl _start

_start:
    li x1, 1
    li x2, 2
    li x4, 5
    bne x1, x4, start
    li x5, 6
start:    add x1, x2, x1
    wfi