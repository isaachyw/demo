.section .text
    .globl _start

_start:
    li x1, 1
    li x2, 2
    li x4, 5
start:    add x1, x2, x1
    beq x1, x4, start
    wfi