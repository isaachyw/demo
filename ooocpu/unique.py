def extract_unique_lines(filename, search_string):
    unique_lines = set()
    with open(filename, 'r') as file:
        for line in file:
            if search_string in line:
                unique_lines.add(line.strip())  # Remove newline characters and surrounding whitespace
    return unique_lines

def main():
    input_filename = 'result.log'  # Replace with your input filename
    output_filename = 'output_file.log'  # Replace with your desired output filename
    search_string = 'ROB RETIRE INSIDE: PC:'
    
    unique_lines = extract_unique_lines(input_filename, search_string)

    with open(output_filename, 'w') as output_file:
        for line in unique_lines:
            output_file.write(line + '\n')

    print(f"Unique lines written to {output_filename}")

if __name__ == "__main__":
    main()
