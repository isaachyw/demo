
`include "verilog/sys_defs.svh"

module testbench;
    logic clock, reset;
    MULT_INPUT mult_input;
    MULT_OUTPUT mult_output;
    logic [2*`XLEN-1:0] cres;
    logic [2*`XLEN-1:0] signed_ex_mcand;
    logic [2*`XLEN-1:0] signed_ex_mplier;
    logic [2*`XLEN-1:0] dbg_res;
    logic [2*`XLEN-1:0] dbg_ex_mcand;
    logic [2*`XLEN-1:0] dbg_ex_mplier;
    integer i;
    FUNC func;

    mult dut(
        .clock(clock),
        .reset(reset),
        .mult_input(mult_input),
        .mult_output(mult_output),
        .debug_product(dbg_res),
        .debug_extended_mcand(dbg_ex_mcand),
        .debug_extended_mplier(dbg_ex_mplier)
    );


    // CLOCK_PERIOD is defined on the commandline by the makefile
    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    assign signed_ex_mcand = $signed(mult_input.opa);
    assign signed_ex_mplier = $signed(mult_input.opb);

    always_comb begin
        if (func == MULT_MUL || func == MULT_MULH) begin
            cres = signed_ex_mcand * signed_ex_mplier;
        end else if (func == MULT_MULHSU) begin
            cres = signed_ex_mcand * mult_input.opb;
        end else begin
            cres = mult_input.opa * mult_input.opb;
        end
    end

    assign func = MULT_MUL;
    assign correct = mult_output.done && (cres === mult_output.result);


    // always @(posedge clock) begin
    //     #(`CLOCK_PERIOD*0.2); // a short wait to let signals stabilize
    //     if (!correct) begin
    //         $display("@@@ Incorrect at time %4.0f", $time);
    //         $display("@@@ done:%b a:%h b:%h result:%h", mult_output.done, mult_input.opa, mult_input.opb, mult_output.result);
    //         $display("@@@ Expected result:%h", cres);
    //         $finish;
    //     end
    // end


    // Some students have had problems just using "@(posedge done)" because their
    // "done" signals glitch (even though they are the output of a register). This
    // prevents that by making sure "done" is high at the clock edge.
    task wait_until_done;
        forever begin : wait_loop
            @(posedge mult_output.done);
            @(negedge clock);
            if (mult_output.done) begin
                disable wait_until_done;
            end
        end
    endtask

    task display_vals;
        $display("Time:%4.0f done:%b a:%h b:%h result:%h correct:%h product:%h ex_mcand:%h ex_mplier:%h",
                     $time, mult_output.done, mult_input.opa, mult_input.opb, mult_output.result, cres, dbg_res, dbg_ex_mcand, dbg_ex_mplier);
    endtask


    initial begin
        // NOTE: monitor starts using 5-digit decimal values for printing
        // $monitor("Time:%4.0f done:%b a:%5d b:%5d result:%5d correct:%5d product:%h ex_mcand:%h ex_mplier:%h",
        //          $time, mult_output.done, mult_input.opa, mult_input.opb, mult_output.result, cres, dbg_res, dbg_ex_mcand, dbg_ex_mplier);

        $display("\nBeginning edge-case testing:");

        reset = 1;
        clock = 0;
        mult_input.opa = 2;
        mult_input.opb = 3;
        mult_input.start = 1;
        mult_input.func = func;
        mult_input.cdb_stall = 0;
        @(negedge clock);
        reset = 0;
        @(negedge clock);
        mult_input.start = 0;
        wait_until_done();
        display_vals();

        mult_input.start = 1;
        mult_input.opa = 5;
        mult_input.opb = 50;
        @(negedge clock);
        mult_input.start = 0;
        wait_until_done();
        display_vals();

        mult_input.start = 1;
        mult_input.opa = 0;
        mult_input.opb = 257;
        @(negedge clock);
        mult_input.start = 0;
        wait_until_done();

        // change the monitor to hex for these values
        display_vals();
        // $monitor("Time:%4.0f done:%b a:%h b:%h result:%h correct:%h product:%h",
        //          $time, mult_output.done, mult_input.opa, mult_input.opb, mult_output.result, cres, dbg_res);

        mult_input.start = 1;
        mult_input.opa = 32'hFFFF_FFFF;
        mult_input.opb = 32'hFFFF_FFFF;
        @(negedge clock);
        mult_input.start = 0;
        wait_until_done();
        display_vals();

        $display("\nInput values every cycle");
        @(negedge clock);
        mult_input.start = 1;
        mult_input.opa = 2;
        mult_input.opb = 3;
        @(negedge clock);
        mult_input.opa = 3;
        mult_input.opb = 4;
        @(negedge clock);
        mult_input.opa = 5;
        mult_input.opb = 6;
        @(negedge clock);
        mult_input.start = 0;
        display_vals();
        @(negedge clock);
        display_vals();
        @(negedge clock);
        display_vals();
        @(negedge clock);
        display_vals();
        @(negedge clock);
        display_vals();
        @(negedge clock);

        // $monitor(); // turn off monitor for the for-loop
        $display("\nBeginning random testing:");
        for (i = 0; i <= 15; i = i+1) begin
            mult_input.start = 1;
            mult_input.opa = {$random, $random}; // multiply random 64-bit numbers
            mult_input.opb = {$random, $random};
            @(negedge clock);
            mult_input.start = 0;
            wait_until_done();
            display_vals();
            // $display("Time:%4.0f done:%b a:%h b:%h result:%h correct:%h product:%h",
            //          $time, mult_output.done, mult_input.opa, mult_input.opb, mult_output.result, cres, dbg_res);
        end

        $display("@@@ Passed\n");
        $finish;
    end

endmodule
