`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"

task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;
  logic clock, reset;
  
  FETCH_RES[`N-1:0] instructions_to_decode;

  MAP_TABLE_READ_COMMAND[`N-1:0] map_table_read_req;
  MAP_TABLE_ENTRY[`N-1:0] map_table_read_result;
  MAP_TABLE_WRITE_COMMAND[`N-1:0] map_table_updates;
  ROB_PACKET[`N-1:0] decoded_instructions;
  PHYS_REG_T[`N-1:0] return_reg;

`ifdef DEBUG
    // Debug:
  DEBUG_DECODER dbg;
`endif

  task reset_io;
    instructions_to_decode = 0;
    map_table_read_result = 0;
    return_reg = 0;
    #1;
  endtask

  task dump_fl;
`ifdef DEBUG
  PHYS_REG_T [(`FREE_LIST_SIZE-1):0] data_out;
  logic [$clog2(`FREE_LIST_SIZE)-1:0] next_stack_ptr_out;
  
  assign data_out = dbg.fl_dbg.data_out;
  assign next_stack_ptr_out = dbg.fl_dbg.stack_ptr_out;

  $display("data_out:----------------------");
  for (integer i = 0; i < `FREE_LIST_SIZE; i = i + 1) begin
    $display("data_out[%d]: %d", i, data_out[i]);
  end
  $display("next_stack_ptr_out: %d", next_stack_ptr_out);
  $display("-------------------------------");
`else
  $display("Cannot display data_out without DEBUG flag");
`endif
  endtask

  
  // task assert_map_table_req;
  //   for (int i = 0; i < `N; i++) begin
  //     assert(map_table_read_req[i].valid 
  //       && map_table_read_req[i].arch_reg_idx == req) else begin
  //         $display("Incorrect map table read request %p", map_table_read_req[i]);
  //         exit_on_error();
  //       end
  //   end
  // endtask

  task map_table_res;
    input PHYS_REG_T a, b, c;
    
    map_table_read_result[0] = { a, `TRUE, a != 'd0 };
    map_table_read_result[1] = { b, `TRUE, b != 'd0 };
    map_table_read_result[2] = { c, `TRUE, c != 'd0 };
    
    #1;
  endtask
  
  decoder dut (
    clock, reset, 
    instructions_to_decode, 
    map_table_read_req, 
    map_table_read_result,
    map_table_updates,
    decoded_instructions,
    return_reg
`ifdef DEBUG
    , dbg
`endif
  );

  task make_addi;
    input ARCH_REG_T in;
    output INST out;
    
    out.i.imm = 0;
    out.i.rs1 = 5'd1;
    out.i.funct3 = 0;
    out.i.opcode = 7'b0010011;
    out.i.rd = in;
  endtask
  
  task addi_dst;
    input ARCH_REG_T a, b, c;
    
    INST ins_a, ins_b, ins_c;
    PC_T pc;
    BRANCH_OUTCOME bo;

    make_addi(a, ins_a);
    make_addi(b, ins_b);
    make_addi(c, ins_c);
    
    pc = 0;
    bo = 0;
    
    instructions_to_decode[0] = { pc, ins_a, bo, a != 0 };
    instructions_to_decode[1] = { pc, ins_b, bo, b != 0 };
    instructions_to_decode[2] = { pc, ins_c, bo, c != 0 };
    
    #1;
  endtask
  
  task step_clock;
    #10
    clock = 1;
    #10
    clock = 0;
  endtask
  
  task __intern_assert_newT;
    input integer i;
    input PHYS_REG_T expected;
    
    ROB_PACKET pck;
    pck = decoded_instructions[i];
    if (expected != 0) begin
      assert (pck.valid && pck.newT == expected) else begin
        $display("offending: %p, expected %p", pck.newT, expected);
        $display("i: %d, obj: %p", i, pck);
        exit_on_error();
      end
    end else begin
      assert (!pck.valid) else begin
        $display("Should be invalid, i: %d, obj: %p", i, pck);
        exit_on_error();
      end
    end
  endtask

  task __intern_assert_oldT;
    input integer i;
    input PHYS_REG_T expected;
    
    ROB_PACKET pck;
    pck = decoded_instructions[i];
    if (expected != 0) begin
      assert (pck.valid && pck.oldT == expected) else begin
        $display("offending: %p, expected %p", pck.oldT, expected);
        $display("i: %d, obj: %p", i, pck);
        exit_on_error();
      end
    end else begin
      assert (!pck.valid) else begin
        $display("Should be invalid, i: %d, obj: %p", i, pck);
        exit_on_error();
      end
    end
  endtask
  
  task assert_newT;
    input PHYS_REG_T a, b, c;
    __intern_assert_newT(0, a);
    __intern_assert_newT(1, b);
    __intern_assert_newT(2, c);
  endtask
  
  task assert_oldT;
    input PHYS_REG_T a, b, c;
    __intern_assert_oldT(0, a);
    __intern_assert_oldT(1, b);
    __intern_assert_oldT(2, c);
  endtask
  
  task assert_stack_ptr;
    input logic [$clog2(`FREE_LIST_SIZE)-1:0] expected;
`ifdef DEBUG
    assert(dbg.fl_dbg.stack_ptr_out == expected) else begin
      $display("unexpeted fl stack ptr: got: %p, expected: %p", 
        dbg.fl_dbg.stack_ptr_out, expected);
      $display("reg-needed: %p", dbg.fl_reg_needed);
      exit_on_error();
    end
`endif
  endtask
  
  // For debug printing
  // `define __DC_TRACE

  initial begin
`ifdef DEBUG
    $display("!!! running in DEBUG");
`else
    $display("!!! running in prod");
`endif

    reset = 1;
    clock = 0;
    reset_io();
    step_clock();
    reset = 0;

    reset_io();
    // No register needed at first
    assert_stack_ptr(0);
    $display("Test simply allocation, no forwarding");
    map_table_res(5, 6, 7);

    addi_dst(1, 2, 3);
    assert_oldT(5, 6, 7);
    assert_newT(32, 33, 34);

    addi_dst(1, 0, 0);
    assert_oldT(5, 0, 0);
    assert_newT(32, 0, 0);
    
    addi_dst(0, 2, 3);
    assert_oldT(0, 6, 7);
    assert_newT(0, 32, 33);

    addi_dst(0, 0, 0);
    assert_oldT(0, 0, 0);
    assert_newT(0, 0, 0);
    
    $display("Test forwarding");
    map_table_res(5, 5, 5);
    addi_dst(3, 3, 3);
    assert_oldT(5, 32, 33);
    assert_newT(32, 33, 34);

    $display("Test all non-returning");
    instructions_to_decode[0].valid = 1;
    instructions_to_decode[1].valid = 1;
    instructions_to_decode[2].valid = 1;

    instructions_to_decode[0].inst.i.rd = 0;
    instructions_to_decode[1].inst.i.rd = 0;
    instructions_to_decode[2].inst.i.rd = 0;
    map_table_res(0, 0, 0);
    assert (decoded_instructions[0].valid && 
      decoded_instructions[1].valid && 
      decoded_instructions[2].valid) else begin
        exit_on_error();
      end
    assert (decoded_instructions[0].newT == 0&& 
      decoded_instructions[1].newT == 0&& 
      decoded_instructions[2].newT == 0) else begin
        exit_on_error();
      end
    assert (decoded_instructions[0].oldT == 0&& 
      decoded_instructions[1].oldT == 0&& 
      decoded_instructions[2].oldT == 0) else begin
        exit_on_error();
      end

`ifdef DEBUG
    $display("Test freelist consumption");
    addi_dst(3, 3, 3);
    map_table_res(5, 6, 7);
    // before
    assert_stack_ptr(3);
    step_clock();
    reset_io();
    assert_stack_ptr(3);
    addi_dst(2, 3, 0);
    map_table_res(5, 6, 0);
    assert_stack_ptr(5);
    step_clock();
    reset_io();

    $display("Test freelist return");
    assert_stack_ptr(5);

    return_reg[0] = {
      5'd5,
      `TRUE
    };

    step_clock();
    reset_io();
    assert_stack_ptr(4);
    step_clock();
`else
    $display("Cannot test freelist without DEBUG flag");
`endif

    $display("@@@Passed");
    $finish;
  end

endmodule
