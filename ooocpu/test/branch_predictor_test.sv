`include "verilog/sys_defs.svh"
task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;
  logic clock, reset;
  BP_QUERY  [      `N-1:0] bp_query;
  BP_OUTPUT [      `N-1:0] bp_output;

  //update, come from execute
  BP_UPDATE [      `N-1:0] bp_update;
  BP_STATE  [ `BHT_SZ-1:0] bh_table;
  BTB_ENTRY [`BTB_SET-1:0] btb_table;

  logic     [        31:0] cycle_count;

  always_ff @(posedge clock) begin
    if (reset) cycle_count <= 0;
    else cycle_count <= cycle_count + 1;
  end


  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end
  branch_predictor dut (
      .clock(clock),
      .reset(reset),
      .bp_query(bp_query),
      .bp_output(bp_output),
      .bp_update(bp_update)
`ifdef DEBUG,
      .bh_table_out(bh_table),
      .btb_table_out(btb_table)
`endif
  );

  task reset_input();
    bp_query  = '0;
    bp_update = '0;
  endtask

  task show_btb;
`ifdef DEBUG
    begin
      $display("BTB:----------------------");
      for (integer i = 0; i < `BTB_SET; i = i + 1) begin
        if (btb_table[i].target != 0) begin
          $display("btb_table[%2d]: %d \t tag: %4d", i, btb_table[i].target, btb_table[i].tag);
        end
      end
      $display("-------------------------------");
    end
`endif
  endtask

  task show_bht;
`ifdef DEBUG
    begin
      $display("BHT:----------------------");
      for (integer i = 0; i < `BHT_SZ; i = i + 1) begin
        if (bh_table[i] != WEAK_NT) begin
          $display("bh_table[%2d]: %2d", i, bh_table[i]);
        end
      end
      $display("-------------------------------");
    end
`endif
  endtask

  task show_bp_output;
    $display("BP_OUTPUT:#############################");
    for (integer i = 0; i < `N; i = i + 1) begin
      if (bp_output[i].valid) begin
        $display("bp_output[%d]: direction:%1d \t target:%d", i, bp_output[i].direction,
                 bp_output[i].target);
      end
    end
    $display("######################################");
  endtask

  // Helper function to make a fake pc
  function BP_QUERY make_pc_bp(logic [`BHT_W-1:0] idx);
    PC_T pc;
    pc = '0;
    pc[`BHT_W+1:2] = idx;
    return {1'b1, pc};
  endfunction

  function BP_QUERY make_pc_btb(logic [`BTB_IDX_W-1:0] idx, logic [`BTB_TAG_W-1:0] tag);
    PC_T pc;
    pc = '0;
    pc[`BTB_IDX_W+1:2] = idx;
    pc[`BTB_TAG_W+`BTB_IDX_W+1:`BTB_IDX_W+2] = tag;
    return {1'b1,pc};
  endfunction

  function BP_UPDATE make_bp_update(PC_T pc, PC_T target, logic direction);
    BP_UPDATE update;
    update = '0;
    update.valid = 1;
    update.branch_outcome.valid = 1;
    update.pc = pc;
    update.branch_outcome.direction = direction;
    update.branch_outcome.target = target;
    return update;
  endfunction

  initial begin
`ifdef DEBUG
    $display("!!! running in DEBUG");
`else
    $display("!!! running in prod");
`endif

    reset = 0;
    clock = 0;
    /* Test 0: Check reset success */
    @(posedge clock);
    reset = 1;
    reset_input();
    @(posedge clock);
    @(posedge clock);
    foreach (bh_table[i]) begin
      if (bh_table[i] != WEAK_NT) begin
        $display("bh_table[%0d] = %0d", i, bh_table[i]);
        exit_on_error();
      end
    end
    assert (btb_table == '0)
    else begin
      $display("btb_table is not zero");
      exit_on_error();
    end
    $display("Test 0: Check reset success passed");
    reset = 0;
    /* Test 1: Check random query with no target in btb */
    bp_query[0] = make_pc_bp(8);


    @(posedge clock);
    assert (bp_output[0].valid == 1 && bp_output[0].direction == 0 && bp_output[0].target != 0)
    else begin
      $display("%d,%d,%d", bp_output[0].valid, bp_output[0].direction, bp_output[0].target);
      exit_on_error();
    end

    bp_query[0]  = make_pc_bp(8);
    bp_query[1]  = make_pc_bp(9);
    bp_update[0] = make_bp_update(bp_query[0].pc, bp_query[0].pc + 200, 1);


    @(posedge clock);

    $display("\nTest 1: Update the btb and direction and let it make taken passed\n");
    assert(bp_output[0].valid == 1 && bp_output[0].direction == 1 && bp_output[0].target == bp_query[0].pc + 200);
    // let the bp_output[0] be updated to the btb and update to be taken, thus in this cycle should be predicted taken
    @(posedge clock);
    reset_input();
    bp_query[0]  = make_pc_bp(8);
    bp_query[1]  = make_pc_btb(8,10);
    @(posedge clock);
    assert(bp_output[0].valid == 1 && bp_output[0].direction == 1 && bp_output[0].target == bp_query[0].pc + 200);
    assert(bp_output[1].valid == 1 && bp_output[1].direction == 0 && bp_output[1].target == bp_query[1].pc + 4);
    show_bp_output();
    $display("\nTest 2: test one of the query is different tag in btb Passed\n");

    $display("@@@Passed");
    $finish;
  end

endmodule
