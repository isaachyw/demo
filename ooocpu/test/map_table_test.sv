`include "verilog/sys_defs.svh"


module testbench;
   logic clock; logic reset;


   // Coming from decode@dispatch
    MAP_TABLE_READ_COMMAND[`N-1:0] decode_dest_read_request;
   MAP_TABLE_ENTRY[`N-1:0] decode_dest_read_response;
   MAP_TABLE_WRITE_COMMAND[`N-1:0] decode_map_write_command;


   MAP_TABLE_READ_COMMAND[`N-1:0][1:0] decode_operands_read_request;
   MAP_TABLE_ENTRY[`N-1:0][1:0] decode_operands_read_response;
  
   // CDB request coming from fu_manager@complete
   CDB_ENTRY[`CDB_W-1:0] cdb_entries;
  
   // Write command from rob@rollback
   MAP_TABLE_WRITE_COMMAND rollback_mt_write


   `ifdef DEBUG ;DEBUG_MAP_TABLE dbg; `endif
  
   map_table dut(
       .clock(clock),
       .reset(reset),
       .decode_dest_read_request(decode_dest_read_request),
       .decode_dest_read_response(decode_dest_read_response),
       .decode_map_write_command(decode_map_write_command),
       .decode_operands_read_request(decode_operands_read_request),
       .decode_operands_read_response(decode_operands_read_response),


       .cdb_entries(cdb_entries),
       .rollback_mt_write(rollback_mt_write)
       `ifdef DEBUG, .dbg(dbg) `endif
   );


   always begin
       #(`CLOCK_PERIOD/2.0);
       clock = ~clock;
   end


   task dump_table;
`ifdef DEBUG
       $display("Map Entry:");
       $display("| idx|Reg|R|V|");
       for (integer i = 0; i < `ARCH_REG_SZ; i = i + 1) begin
           $display("|arch: %d | phy: %d| ready: %b| valid: %b|",
               i,
               dbg.map_table[i].phy_reg_idx,
               dbg.map_table[i].ready,
               dbg.map_table[i].valid
           );
       end
`endif
   endtask


   task exit_on_error;
       dump_table();
       $display("@@@Failed at time %d", $time);
       $finish;
   endtask
  
  


   initial begin
       clock = 0;
       reset = 0;


       @(posedge clock)
       reset = 1;
       @(posedge clock)
       reset = 0;
       @(negedge clock)
       dump_table();
       // Test case 1:
       $display("Test initialization");


    //    @(posedge clock)
    //    cdb_entries = '0;
    //    // r1 = r2 + r3  1 -> 31    2 -> 2   3 -> 3
    //    // r4 = r1 + r2  4 -> 34    1 -> 31  2 -> 2
    //    // r3 = r1 + r4  3 -> 33    1 -> 31  4 -> 34
    //    decode_dest_read_request[0] = {arch_reg_idx: 1, valid: 1};
    //    decode_dest_read_request[1] = {arch_reg_idx: 4, valid: 1};
    //    decode_dest_read_request[2] = {arch_reg_idx: 3, valid: 1};


    //    decode_map_write_command[0] = {phy_reg_idx: 31, arch_reg_idx: 1, ready :0, valid :1};
    //    decode_map_write_command[1] = {phy_reg_idx: 34, arch_reg_idx: 4, ready :0, valid :1};
    //    decode_map_write_command[2] = {phy_reg_idx: 33, arch_reg_idx: 3, ready :0, valid :1};


    //    decode_operands_read_request[0][0] = {arch_reg_idx: 2, valid: 1};
    //    decode_operands_read_request[0][1] = {arch_reg_idx: 3, valid: 1};


    //    decode_operands_read_request[1][0] = {arch_reg_idx: 1, valid: 1};
    //    decode_operands_read_request[1][1] = {arch_reg_idx: 2, valid: 1};


    //    decode_operands_read_request[2][0] = {arch_reg_idx: 1, valid: 1};
    //    decode_operands_read_request[2][1] = {arch_reg_idx: 4, valid: 1};




    //     @(negedge clock)

    //     $display("dest: %p", decode_dest_read_response);

       @(posedge clock)
       
       @(negedge clock)
       dump_table();
    //    Test case 1:
       


       @(posedge clock)
       cdb_entries = '0;
       // r1 = r2 + r3  1 -> 31    2 -> 2   3 -> 3
       // r1 = r1 + r2  4 -> 34    w: 1 -> 36   r:1 -> 31  2 -> 2
       // r1 = r1 + r4  3 -> 33    w: 1 -> 49   r:1 -> 36  4 -> 4
       decode_dest_read_request[0] = {arch_reg_idx: 1, valid: 1};
       decode_dest_read_request[1] = {arch_reg_idx: 1, valid: 1};
       decode_dest_read_request[2] = {arch_reg_idx: 1, valid: 1};


       decode_map_write_command[0] = {phy_reg_idx: 31, arch_reg_idx: 1, ready :0, valid :1};
       decode_map_write_command[1] = {phy_reg_idx: 36, arch_reg_idx: 1, ready :0, valid :1};
       decode_map_write_command[2] = {phy_reg_idx: 39, arch_reg_idx: 1, ready :0, valid :1};


       decode_operands_read_request[0][0] = {arch_reg_idx: 2, valid: 1};
       decode_operands_read_request[0][1] = {arch_reg_idx: 3, valid: 1};


       decode_operands_read_request[1][0] = {arch_reg_idx: 1, valid: 1};
       decode_operands_read_request[1][1] = {arch_reg_idx: 2, valid: 1};


       decode_operands_read_request[2][0] = {arch_reg_idx: 1, valid: 1};
       decode_operands_read_request[2][1] = {arch_reg_idx: 4, valid: 1};




        @(negedge clock)
        for (int i = 0; i <3;i++) $display("NEW [%d] dest: %d", i, decode_dest_read_response[i].phy_reg_idx);
        
        @(posedge clock)
        // $display("dest: %p", decode_dest_read_response);
        dump_table();
       // // Read first three elements
       // reset_commands();
       // make_read_request(1, 2, 3);
       // @(posedge clock)
       // assert_read_response(0, 1, `TRUE);
       // assert_read_response(1, 2, `TRUE);
       // assert_read_response(2, 3, `TRUE);
      
       // // Read last one element
       // reset_commands();
       // make_read_request(0, 0, 31);
       // @(posedge clock)
       // assert_read_response(2, 31, `TRUE);
      
       // // Test case 2:
       // $display("Test 0 register");


       // // Read 0 register
       // reset_commands();
       // decode_map_read_request[0].valid = 1;
       // @(posedge clock)
       // assert_read_align();
       // assert(decode_map_read_response[0].phy_reg_idx == 0
       //     && decode_map_read_response[0].valid) else begin
       //     $display("0 reg incorrect");
       //     exit_on_error();
       // end
      
       // // Write 0 register
       // reset_commands();
       // decode_map_write_command[0] = { 6'd33, 5'd0, `FALSE, `TRUE };
       // @(posedge clock)
       // reset_commands();
       // decode_map_read_request[0]= { 5'd0, `TRUE };
       // @(posedge clock)
       // assert(decode_map_read_response[0].phy_reg_idx == 0
       //     && decode_map_read_response[0].valid) else begin
       //     $display("Should not be able to overwrite phy_reg 0");
       //     exit_on_error();
       // end




       // // Test case 3:
       // $display("Test write");
       // reset_commands();
       // make_write_request(0, 1, 34, `TRUE);
       // @(posedge clock)
       // reset_commands();
       // make_read_request(1, 0, 0);
       // @(posedge clock)
       // assert_read_response(0, 34, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 34, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 34, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 34, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 34, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 34, `TRUE);


       // // Overwrite
       // reset_commands();
       // make_write_request(0, 1, 36, `TRUE);
       // @(posedge clock)
       // reset_commands();
       // make_read_request(1, 0, 0);
       // @(posedge clock)
       // assert_read_response(0, 36, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 36, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 36, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 36, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 36, `TRUE);
       // @(posedge clock)
       // assert_read_response(0, 36, `TRUE);
      
       // reset_commands();
       // make_write_request(0, 1, 37, `FALSE);
       // make_write_request(1, 2, 38, `FALSE);
       // make_write_request(2, 3, 39, `FALSE);
       // // Should get overwritten with 0, TRUE
       // make_write_request(3, 4, 6'd0, `FALSE);
      
       // @(posedge clock)
       // reset_commands();
       // make_read_request(1, 2, 4);
       // @(posedge clock)
       // assert_read_response(0, 37, `FALSE);
       // assert_read_response(1, 38, `FALSE);
       // assert_read_response(2, 0, `TRUE);
      
       // // Test case 4:
       // $display("Test CDB write");
       // reset_commands();
       // make_cdb_request(37, 38, 1);
       // @(posedge clock)
       // @(posedge clock)
       // reset_commands();
       // make_read_request(1, 2, 3);
       // @(posedge clock)
       // assert_read_response(0, 37, `TRUE);
       // assert_read_response(1, 38, `TRUE);
       // assert_read_response(2, 39, `FALSE);




       // // Test case 5:
       // $display("Test no internal forwarding");
       // reset_commands();
       // make_read_request(1, 0, 0);
       // @(posedge clock)
       // // Write cycle
       // // Here the map table does read and write in the same cycle
       // assert_read_align();
       // assert_read_response(0, 37, `TRUE);
      
       // reset_commands();
       // make_write_request(0, 1, 32, `TRUE);
       // make_read_request(0, 1, 0);
       // @(posedge clock)
       // // Post-Write cycle 1
       // assert_read_response(1, 37, `TRUE);


       // @(posedge clock)
       // // Post-Write cycle 2
       // assert_read_response(1, 32, `TRUE);
      
       // $display("@@@Passed");
       $finish;
   end


endmodule

