`include "verilog/sys_defs.svh"

module testbench;
MEM_INPUT mem_input;
LSQ_ISSUE_REQUEST lsq_issue_in;

mem_issue mem0(
    .mem_input(mem_input),
    .lsq_issue_in(lsq_issue_in)
);

endmodule