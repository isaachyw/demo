`include "verilog/sys_defs.svh"
`include "verilog/regfile.sv"

module testbench;
    // input
    logic clock, reset;
    // RS input
    RS_ENTRY [`RS_SZ-1:0] rs_entries_in_wire_in;
    logic [`RS_SZ-1:0] rs_ready_in_wire_in;
    ROB_REF_T rob_rollback_idx;

    REG_READ_RESPONSE[`REG_PORT_WIDTH-1:0][1:0] read_reg_response_in;

    //output
    REG_READ_REQ[`REG_PORT_WIDTH-1:0][1:0] read_reg_req_out;
    RS_REF_T [`N-1:0] fu_feedback_out;
    ROB_REF_T[`N-1:0] completed_inst_out;
    REG_WRITE_REQ[`CDB_W-1:0] write_reg_req_out;
    BRANCH_OUTCOME[`N-1:0] branch_outcomes_out;
    CDB_ENTRY [`CDB_W-1:0] cdb_entries_wire_out;

    REG_WRITE_REQ[`CDB_W-1:0] regfile_write_req_in;
    logic [`PHYS_REG_SZ-1:1][`XLEN-1:0] regfile_debug;

    MULT_OUTPUT [`NUM_FU_MULT-1:0] debug_mult_result;
    logic [`N-1:0][$clog2(`NUM_FU_MULT)-1:0] mult_av_idxs;
    logic [$clog2(`N)-1:0] mult_av_cnt;
    MULT_INPUT [`NUM_FU_MULT-1:0] mult_input_debug;

    LSQ_ENTRY [`ROB_SZ - 1 : 0] lsq_content;
    LSQ_ISSUE_REQUEST [`N - 1 : 0] lsq_issue_request;
    logic [`N - 1 : 0] issue_approved;
    logic [`N-1:0][$clog2(`ROB_SZ)+1:0] completed_entry;
    logic [`N-1:0] completed_entry_valid;

    regfile regfile0(
        .clock(clock),
        .reset(reset),
        .read_request(read_reg_req_out),
        .read_response(read_reg_response_in),
        .write_request(write_reg_req_out)
        `ifdef DEBUG
        // Debug:
            , .registers_out(regfile_debug)
        `endif
    );

    fu_manager fu(
        .clock(clock),
        .reset(reset),
        .rs_entries_in_wire(rs_entries_in_wire_in),
        .rs_ready_in_wire(rs_ready_in_wire_in),
        .fu_feedback(fu_feedback_out),

        .read_reg_req(read_reg_req_out),
        .read_reg_response(read_reg_response_in),

        .completed_inst(completed_inst_out),
        .write_reg_req(write_reg_req_out),

        .branch_outcomes(branch_outcomes_out),  // NOTE: change BR_FEEDBACK to BR_OUTCOME --ziangli

        .cdb_entries_wire(cdb_entries_wire_out),

        .lsq_content(lsq_content),
        .lsq_issue_request(lsq_issue_request),
        .issue_approved(issue_approved),
        .completed_entry(completed_entry),
        .completed_entry_valid(completed_entry_valid)

        `ifdef DEBUG
        , .mult_results(debug_mult_result)
        , .mul_idx_debug(mult_av_idxs)
        , .mul_cnt_debug(mult_av_cnt)
        , .mult_input_debug(mult_input_debug)
        `endif
    );

    task assign_to_fu;
        for(int i = 0; i < 3; i++) begin
            rs_entries_in_wire_in[i] = {
                inst: {
                    pc: i,
                    npc: i+4,
                    inst: 32'b00101000000000000000000000001111,
                    fu: ALU,
                    opa_select: OPA_IS_RS1,
                    opb_select: OPB_IS_I_IMM,
                    func: ALU_ADD,
                    has_dest: 1,
                    illegal: 0,
                    branch_prediction: '0,
                    has_reg1: 1,
                    has_reg2: 0
                },
                operands_status: '0,
                dest_reg: i+1,
                rob_ref: i+1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[0] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[1] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 0
            };

            rs_ready_in_wire_in[i] = 1;
        end
    endtask

    // task secondALU;
    //     for(int i = 0; i < 3; i++) begin
    //         rs_entries_in_wire_in[i] = {
    //             inst: {
    //                 pc: i,
    //                 npc: i+4,
    //                 inst: 32'b00101010111000000000000000001111,
    //                 fu: ALU,
    //                 opa_select: OPA_IS_RS1,
    //                 opb_select: OPB_IS_I_IMM,
    //                 func: ALU_ADD,
    //                 has_dest: 1,
    //                 illegal: 0,
    //                 branch_prediction: '0
    //             },
    //             operands_status: '0,
    //             dest_reg: i+1,
    //             rob_ref: i+2,
    //             valid: 1,
    //             ready: 1

    //         };
    //         rs_entries_in_wire_in[i].operands_status[0] = {
    //             phy_reg_idx: 0,
    //             ready: 1,
    //             valid: 1
    //         };
    //         rs_entries_in_wire_in[i].operands_status[1] = {
    //             phy_reg_idx: 0,
    //             ready: 1,
    //             valid: 0
    //         };

    //         rs_ready_in_wire_in[i] = 1;
    //     end
    // endtask

    // task begin_mult;
    //     for(int i = 0; i < 2; i++) begin
    //         rs_entries_in_wire_in[i] = {
    //             inst: {
    //                 pc: i,
    //                 npc: i+4,
    //                 inst: 32'b0,
    //                 fu: MULT,
    //                 opa_select: OPA_IS_RS1,
    //                 opb_select: OPB_IS_RS2,
    //                 func: MULT_MUL,
    //                 has_dest: 1,
    //                 illegal: 0,
    //                 branch_prediction: '0
    //             },
    //             operands_status: '0,
    //             dest_reg: i+5,
    //             rob_ref: i,
    //             valid: 1,
    //             ready: 1

    //         };
    //         rs_entries_in_wire_in[i].operands_status[0] = {
    //             phy_reg_idx: 1,
    //             ready: 1,
    //             valid: 1
    //         };
    //         rs_entries_in_wire_in[i].operands_status[1] = {
    //             phy_reg_idx: 1,
    //             ready: 1,
    //             valid: 1
    //         };

    //         rs_ready_in_wire_in[i] = 1;
    //     end
    // endtask

    // task one_alu_two_mult;
    //     for(int i = 2; i < 3; i++) begin
    //         rs_entries_in_wire_in[i] = {
    //             inst: {
    //                 pc: i,
    //                 npc: i+4,
    //                 inst: 32'b00101000000000000000000000001111,
    //                 fu: ALU,
    //                 opa_select: OPA_IS_RS1,
    //                 opb_select: OPB_IS_I_IMM,
    //                 func: ALU_ADD,
    //                 has_dest: 1,
    //                 illegal: 0,
    //                 branch_prediction: '0
    //             },
    //             operands_status: '0,
    //             dest_reg: i+1,
    //             rob_ref: i+1,
    //             valid: 1,
    //             ready: 1

    //         };
    //         rs_entries_in_wire_in[i].operands_status[0] = {
    //             phy_reg_idx: 0,
    //             ready: 1,
    //             valid: 1
    //         };
    //         rs_entries_in_wire_in[i].operands_status[1] = {
    //             phy_reg_idx: 0,
    //             ready: 1,
    //             valid: 0
    //         };

    //         rs_ready_in_wire_in[i] = 1;
    //     end
    // endtask

    task load;
        for(int i = 0; i < 3; i++) begin
            rs_entries_in_wire_in[i] = {
                inst: {
                    pc: i,
                    npc: i+4,
                    inst: 32'b0000_0000_0000_0001_0010_0010_0000_0011,
                    fu: MEM,
                    opa_select: OPA_IS_RS1,
                    opb_select: OPB_IS_I_IMM,
                    func: MEM_RD,
                    has_dest: 1,
                    illegal: 0,
                    branch_prediction: '0,
                    has_reg1: 1,
                    has_reg2: 0
                },
                operands_status: '0,
                dest_reg: 4+i,
                rob_ref: {
                    rob_index: 3+i,
                    valid: 1
                },
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[0] = {
                phy_reg_idx: 2,
                ready: 1,
                valid: 1
            };
            rs_entries_in_wire_in[i].operands_status[1] = {
                phy_reg_idx: 0,
                ready: 1,
                valid: 0
            };

            rs_ready_in_wire_in[i] = 1;
        end
    endtask

    task approved_sim;
        issue_approved[0] = 1;
        issue_approved[1] = 1;
        issue_approved[2] = 1;
    endtask

    task return_lsq_out;
        for(int i = 0; i < `N; i++) begin
            lsq_content[i].request.basic_info.rob_idx = 3+i;
            lsq_content[i].request.basic_info.valid = 1;
            lsq_content[i].read_ready = 1;
            lsq_content[i].completed = 0;
            lsq_content[i].read_data = 666;
            lsq_content[i].request.basic_info.mem_op = MEM_READ;
            lsq_content[i].request.dst_reg_idx = 4 + i;
        end
    endtask

    task clear_assign;
        rs_entries_in_wire_in = '0;
        rs_ready_in_wire_in = '0;
    endtask

    task print_issue_request;
    for(int i = 0; i < `N; i++) begin
        $display("time: [%d] Issue request to LSQ:\nrob_idx: %d\tvalid:%d\tmem_op:%p\tsize:%p\nmemory_addr:%h\tdata:%d\tdst_reg_idx:%d", $time,
                  lsq_issue_request[i].basic_info.rob_idx,
                  lsq_issue_request[i].basic_info.valid, lsq_issue_request[i].basic_info.mem_op, lsq_issue_request[i].basic_info.size,
                  lsq_issue_request[i].memory_addr, lsq_issue_request[i].data, lsq_issue_request[i].dst_reg_idx
        );
    end
    endtask

    task print_regfile;
    $display("REGFILE: time[%d]", $time);
    for(int i = 0; i < 32; i++) begin
        $display("phy[%d]:\t%d", i, regfile_debug[i]);
    end
    endtask

    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    initial begin
        // $monitor("{RS_ENTRIES_ROB: %d %d %d} {Completed inst: %d %d %d}\n{CDB: %d %d %d}\n{mult_input: %d %d}\n{mult_result: %d; done: %b}\nfu_output0: %d %d, fu_output1: %d %d, fu_output2: %d %d\nreg[1]: %d , reg[2]: %d , reg[3]: %d\nreg[5]: %d, reg[6]: %d, reg[7]: %d",
        //         rs_entries_in_wire_in[0].rob_ref, rs_entries_in_wire_in[1].rob_ref, rs_entries_in_wire_in[2].rob_ref,
        //         completed_inst_out[0].rob_index, completed_inst_out[1].rob_index, completed_inst_out[2].rob_index,
        //         cdb_entries_wire_out[0].phy_reg_idx, cdb_entries_wire_out[1].phy_reg_idx, cdb_entries_wire_out[2].phy_reg_idx,
        //         mult_input_debug[0].opa, mult_input_debug[0].opa,
        //         debug_mult_result[0].result, debug_mult_result[0].done,
        //         write_reg_req_out[0].reg_ref, write_reg_req_out[0].data, write_reg_req_out[1].reg_ref, write_reg_req_out[1].data, write_reg_req_out[2].reg_ref, write_reg_req_out[2].data,
        //         regfile_debug[1], regfile_debug[2], regfile_debug[3],
        //         regfile_debug[5], regfile_debug[6], regfile_debug[7]
        //         );

        $monitor("CDB: %d %d %d", cdb_entries_wire_out[0].phy_reg_idx, cdb_entries_wire_out[1].phy_reg_idx, cdb_entries_wire_out[2].phy_reg_idx);
        $display("Start testing...");
        clock = 0;
        reset = 1;
        @(negedge clock)
        reset = 0;
        @(negedge clock)
        assign_to_fu();
        @(negedge clock)
        clear_assign();
        @(negedge clock)
        load();
        approved_sim();
        return_lsq_out();
        print_issue_request();
        @(negedge clock)
        print_issue_request();
        // @(negedge clock)
        // begin_mult();
        // @(negedge clock)
        // begin_mult();
        // one_alu_two_mult();
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        // @(negedge clock)
        @(negedge clock)
        print_issue_request();
        @(negedge clock)
        print_regfile();



        $display("@@@Passed!");
        $finish;
    end

endmodule