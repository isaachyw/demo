`include "verilog/sys_defs.svh"
// `define CACHE_MODE
`define DEBUG
`ifndef CLOCK_PERIOD
`define CLOCK_PERIOD 10
`endif
`define CACHE_LINES_DEBUG 32
`define CACHE_LINE_BITS_DEBUG $clog2(`CACHE_LINES_DEBUG)

task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

typedef struct packed {
  logic [63:0]                        data;
  // (13 bits) since only need 16 bits to access all memory and 3 are the offset
  logic [12-`CACHE_LINE_BITS_DEBUG:0] tags;
  logic                               valid;
} ICACHE_ENTRY_DEBUG;
module testbench;

  logic clock;
  logic reset;

  // from i cache to mem
  logic [`XLEN-1:0] icache2mem_addr_wire;
  logic [1:0] icache2mem_command_wire;

  // from icache to fetch
  logic [`N-1:0] icache_valids;
  INST [`N-1:0] icache_insts;

  // from fetch to icache
  FETCH_RES [`N-1:0] proc2Icache_addr;

  // from mem to d cache
  logic [3:0] mem2icache_response_wire;
  logic [63:0] mem2icache_data_wire;
  logic [3:0] mem2icache_tag_wire;

  MEM_SIZE proc2mem_size;

  // Clean
  logic d_request;

  // debug output:
  ICACHE_ENTRY_DEBUG [`CACHE_LINES_DEBUG-1:0] cache_lines;
  DEBUG_DCACHE dbg;
  logic [63:0] unified_memory[`MEM_64BIT_LINES - 1:0];

  // CLOCK_PERIOD is defined on the commandline by the makefile
  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end

  mem mem (
      .clk(clock),
      .proc2mem_addr(icache2mem_addr_wire),
      .proc2mem_data(),
`ifndef CACHE_MODE
      .proc2mem_size(proc2mem_size),
`endif

      .proc2mem_command(icache2mem_command_wire),
      .mem2proc_response(mem2icache_response_wire),
      .mem2proc_data(mem2icache_data_wire),
      .mem2proc_tag(mem2icache_tag_wire)
      // DEBUG:
`ifdef DEBUG,
      .memory_out(unified_memory)
`endif
  );

  icache dut (
      .clock(clock),
      .reset(reset),
      .d_request(d_request),
      .Imem2proc_response(mem2icache_response_wire),
      .Imem2proc_data(mem2icache_data_wire),
      .Imem2proc_tag(mem2icache_tag_wire),
      .proc2Imem_command(icache2mem_command_wire),
      .proc2Imem_addr(icache2mem_addr_wire),
      .Icache_valid_out(icache_valids),
      .Icache_inst_out(icache_insts),
      .proc2Icache_addr(proc2Icache_addr)
`ifdef DEBUG,
      .icache_data_out(cache_lines)
`endif
  );

  /***************** tasks *****************/
  task reset_all_input;
    proc2Icache_addr = '0;
    d_request = 0;
  endtask  //reset_all_input
  task wait_until_done;
    forever begin : wait_loop
      @(negedge clock);
      if (mem2icache_tag_wire) begin
        disable wait_until_done;
      end
    end
  endtask

  task display_cache;
    $display("CACHE PRINT: (time %d)", $time);
    for (int i = 0; i < `CACHE_LINES_DEBUG; i++) begin
      $display("idx: %d, DATA: %h, valid: %d, tag: %b", i, cache_lines[i].data,
               cache_lines[i].valid, cache_lines[i].tags);
    end
  endtask

  task display_mem;
    $display("MEM PRINT: (time %d)", $time);
    for (int i = 0; i < `MEM_64BIT_LINES; i++) begin
      if (unified_memory[i] != 0) begin
        $display("index: %d\tdata:%h", i, unified_memory[i]);
      end
    end
  endtask

  task display_valids;
    $display("icache valids out: %3b", icache_valids);
  endtask

  task assert_reset;
    assert (icache_valids == '0)
    else exit_on_error();
    assert (icache_insts == '0)
    else exit_on_error();
    assert (cache_lines == '0)
    else exit_on_error();
  endtask

  /************functions************/
  function FETCH_RES make_fetch_input(logic[`CACHE_LINE_BITS_DEBUG-1:0] idx
  ,logic [12-`CACHE_LINE_BITS_DEBUG:0]tag, logic valid = 1'b1);
    FETCH_RES res;
    res = '0;
    res.pc[15:3] = {tag, idx};
    res.valid = valid;
    return res;
  endfunction

  /*  Test Methodology:
  1.test reset is clean
  2.test valid bit for all miss
    2.1 test mem successfully return the data and first valid bit is set

  */

  initial begin
    $monitor("time: %4d | response:{tag:%d|res:%d |data: %d}", $time, mem2icache_tag_wire
             , mem2icache_response_wire, mem2icache_data_wire);
    // $monitor("time: %4d | request:{command:%d |addr: %d}", $time, icache2mem_command_wire,
    //          icache2mem_addr_wire);
    $display("Start Testing!");
    reset = 1;
    clock = 0;
    reset_all_input();

    @(posedge clock);
    @(posedge clock);
    assert_reset();
    $display("@@@Passed reset test");
    reset = 0;
    proc2Icache_addr[0] = make_fetch_input(0,0, 0);
    @(posedge clock);
    assert_reset();  // Test invalid addr in should not change anything
    proc2Icache_addr[0] = make_fetch_input(1,21, 1);
    @(posedge clock);
    @(posedge clock);
    assert (icache_valids == '0)
    else exit_on_error();
    assert (icache2mem_command_wire == BUS_LOAD)
    else exit_on_error();
    assert (icache2mem_addr_wire != 0)
    else exit_on_error();

    wait_until_done();
    display_valids();
    assert (icache_valids == 1)
    else exit_on_error();
    /**************** Test d_request should block the command ***************/
    reset_all_input();
    make_fetch_input(4,21, 1);
    d_request = 1;
    @(posedge clock);
    assert (icache2mem_command_wire == BUS_NONE)
    else exit_on_error();
    /*************** Test icache will retive all data in order ***************/
    $display("\n*****Start Test retrive order*****");
    reset_all_input();
    for(int i = 0; i<`N;i++)begin
        proc2Icache_addr[i]=make_fetch_input(i+1, i+2);
    end
    @(posedge clock);
    for(int i = 0;i<`N;i++)begin
        wait_until_done();
    end
    assert(icache_valids == {`N{1'b1}})else exit_on_error();

    $display("@@@Passed!");
    $finish;
  end

endmodule
