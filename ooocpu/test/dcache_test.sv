`include "verilog/sys_defs.svh"
`include "test/mem.sv"

`define CACHE_MODE
`define DEBUG

// import "DPI-C" function void print_membus(int proc2mem_command, int mem2proc_response,
//                                           int proc2mem_addr_hi, int proc2mem_addr_lo,
//                                           int proc2mem_data_hi, int proc2mem_data_lo);

module testbench;

    logic clock;
    logic reset;

    // from d cache to mem
    logic [`XLEN-1:0] proc2mem_addr;
    logic [63:0] proc2mem_data;
    logic [1:0]  proc2mem_command;

    // from mem to d cache
    logic [3:0]  mem2proc_response;
    logic [63:0] mem2proc_data;
    logic [3:0]  mem2proc_tag;

    MEM_SIZE proc2mem_size;

    PC_T pc;
    logic stall;

    DCACHE_REQUEST dc_req;
    DCACHE_RESPONSE dc_res;

    // Clean
    logic clean_signal;
    logic clean_out;

    // debug output:
    CACHE_LINE [`DC_N_BLKS-1:0] cache_lines;
    DEBUG_DCACHE dbg;
    logic [63:0] unified_memory [`MEM_64BIT_LINES - 1:0];

    // CLOCK_PERIOD is defined on the commandline by the makefile
    always begin
        #(`CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    mem mem (
        .clk(clock),
        .proc2mem_addr(proc2mem_addr),
        .proc2mem_data(proc2mem_data),
        `ifndef CACHE_MODE
        .proc2mem_size(proc2mem_size),
        `endif

        .proc2mem_command(proc2mem_command),
        .mem2proc_response(mem2proc_response),
        .mem2proc_data(mem2proc_data),
        .mem2proc_tag(mem2proc_tag)
        // DEBUG:
        `ifdef DEBUG
        , .memory_out(unified_memory)
    `   endif
    );

    dcache dut(
        .clock(clock),
        .reset(reset),
        .pc(pc),
        .stall(stall),
        .dcache_requests(dc_req),
        .dcache_responses(dc_res),
        .Dmem2proc_response(mem2proc_response),
        .Dmem2proc_data(mem2proc_data),
        .Dmem2proc_tag(mem2proc_tag),
        .proc2Dmem_command(proc2mem_command),
        .proc2Dmem_addr(proc2mem_addr),
        .proc2Dmem_data(proc2mem_data),
        .start_cleaning_up(clean_signal),
        .dcache_clean(clean_out)
        `ifdef DEBUG
        , .dbg(dbg)
        , .cache_lines_debug(cache_lines)
        `endif
    );

    task wait_until_done;
        forever begin : wait_loop
            @(posedge dc_res.valid);
            @(negedge clock);
            if (dc_res.valid) begin
                disable wait_until_done;
            end
        end
    endtask

    task wait_for_clean;
        forever begin : wait_loop
            @(posedge clean_out);
            @(negedge clock);
            if (clean_out) begin
                disable wait_for_clean;
            end
        end
    endtask

    task display_cache;
        $display("CACHE PRINT: (time %d)", $time);
        for(int i = 0; i < `DC_N_BLKS; i++) begin
            $display("idx: %d, BLOCK: %h, valid: %d, dirty: %d, tag: %b", i,
            cache_lines[i].block, cache_lines[i].valid, cache_lines[i].dirty, cache_lines[i].tag
            );
        end
    endtask

    task display_mem;
        $display("MEM PRINT: (time %d)", $time);
        for(int i = 0; i < `MEM_64BIT_LINES; i++) begin
            if(unified_memory[i] != 0) begin
                $display("index: %d\tdata:%h", i, unified_memory[i]);
            end
        end
    endtask

    // always @(negedge clock) begin
    //     // From p3
    //     print_membus({30'b0,proc2mem_command}, {28'b0,mem2proc_response},
    //         32'b0, proc2mem_addr[31:0],
    //         proc2mem_data[63:32], proc2mem_data[31:0]);
    // end


    initial begin
        $monitor("time: %d | response: {%d | %d}", $time, dc_res.reg_data, dc_res.valid);
        $display("Start Testing!");
        reset = 1;
        clock = 0;
        pc = 0;
        clean_signal = 0;

        @(negedge clock)
        $display("Store info to unify memory...");
        reset = 0;
        dc_req = {
            mem_op: MEM_WRITE,
            addr: 32'h0000_0001,
            size: WORD,
            write_content: 10,
            valid: 1
        };

        wait_until_done();
        $display("First store, response: {%d | %d}", dc_res.reg_data, dc_res.valid);
        display_cache();
        dc_req.valid = 0;

        @(negedge clock)
        dc_req = {
            mem_op: MEM_READ,
            addr: 32'h0000_0001,
            size: WORD,
            write_content: 0,
            valid: 1
        };
        wait_until_done();
        $display("First read, response: {%d | %d}", dc_res.reg_data, dc_res.valid);
        display_cache();
        dc_req.valid = 0;

        @(negedge clock)
        $display("Stress test -> Massive store:");
        for(int i = 0; i < 33; i++) begin
            // The first write will overwrite the first cache line, the last write will evict one cache line
            dc_req = {
            mem_op: MEM_WRITE,
            addr: 32'h0000_0001 + i * 8,
            size: WORD,
            write_content: 1 + i,
            valid: 1
            };

            wait_until_done();
            if(i == 0 || i == 32) begin
                display_cache();
            end
        end

        @(negedge clock)
        $display("Clean the cache!");
        dc_req.valid = 0;
        clean_signal = 1;
        wait_for_clean();
        display_cache();

        #1000
        display_mem();

        $finish;
    end

endmodule
