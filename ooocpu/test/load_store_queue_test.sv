`include "verilog/sys_defs.svh"
`include "test/mem.sv"

`define CACHE_MODE
`define DEBUG
`define check_subtraction(a, size) ((a) >= (size) ? (a) - (size) : (a))
`define head_offset_i `check_subtraction(lsq_head + i, `ROB_SZ)
task exit_on_error;
  begin
    $display("@@@Failed at time %d", $time);
    $finish;
  end
endtask

module testbench;

  logic clock;
  logic reset;


  // BEGIN ONLY USED BY DCACHE
  // from d cache to mem
  logic [`XLEN-1:0] dcache2mem_addr_wire;
  logic [63:0] dcache2mem_data_wire;
  logic [1:0]  dcache2mem_command_wire;

  // from mem to d cache
  logic [3:0]  mem2dcache_response_wire;
  logic [63:0] mem2dcache_data_wire;
  logic [3:0]  mem2dcache_tag_wire;

  MEM_SIZE proc2mem_size;
  logic [63:0] unified_memory [`MEM_64BIT_LINES - 1:0];
  PC_T pc;
  // END ONLY USED BY DCACHE


  // Dispatch: Reserve Spots for load and store
  LSQ_REQUEST_BASE [`N - 1 : 0] dispatch_request;

  // Issue: Issue load (possible reject: reflect in as not issue_success) and store (guarenteed by LSQ to success)
  LSQ_ISSUE_REQUEST [`N - 1 : 0] issue_request;
  logic [`N - 1 : 0] issue_approved;

  // Complete
  logic [`N - 1 : 0][$clog2(`ROB_SZ) + 1 : 0] complete_request; // LSQ INDEX (NOT COUNT FROM HEAD, NOT ROB INDEX)
  logic [`N - 1 : 0] complete_request_valid;

  // Retire: only contain the count of load/store inst of the total inst retiring!!!!! 
  // must contain at most 1 store per cycle to retire
  NUM_INST_T num_retire_load_store_inst; 

  // Rollback
  logic rollback_load_store_inst; // at most one

  // of the whole Load Store Queue Structure
  LSQ_ENTRY [`ROB_SZ - 1 : 0] LSQ_content;
  logic [$clog2(`ROB_SZ) + 1 : 0] lsq_head, lsq_content_size;
  logic stall_store_retire;

  logic [31 : 0] inst_to_retire;

`ifdef DEBUG
  logic [$clog2(`ROB_SZ) + 1 : 0] debug_next_lsq_head;
  logic [$clog2(`ROB_SZ) + 1 : 0] debug_next_lsq_content_size;
  LSQ_ENTRY [`ROB_SZ - 1 : 0] debug_next_lsq_content;
`endif

  mem mem(
    .clk(clock),
    .proc2mem_addr(dcache2mem_addr_wire),
    .proc2mem_data(dcache2mem_data_wire),
    `ifndef CACHE_MODE
    .proc2mem_size(proc2mem_size),
    `endif

    .proc2mem_command(dcache2mem_command_wire),
    .mem2proc_response(mem2dcache_response_wire),
    .mem2proc_data(mem2dcache_data_wire),
    .mem2proc_tag(mem2dcache_tag_wire)
    // DEBUG:
    `ifdef DEBUG
    , .memory_out(unified_memory)
`   endif
  );

  load_store_queue dut (
    .clock(clock),
    .reset(reset),
    // .pc(pc), 
    .Dmem2proc_response(mem2dcache_response_wire), 
    .Dmem2proc_data(mem2dcache_data_wire),
    .Dmem2proc_tag(mem2dcache_tag_wire),
    .proc2Dmem_command(dcache2mem_command_wire),
    .proc2Dmem_addr(dcache2mem_addr_wire),
    .proc2Dmem_data(dcache2mem_data_wire),
    .dispatch_request(dispatch_request),
    .issue_request(issue_request),
    .issue_approved(issue_approved),
    .complete_request(complete_request),
    .complete_request_valid(complete_request_valid),
    .num_retire_load_store_inst(num_retire_load_store_inst),
    .rollback_load_store_inst(rollback_load_store_inst),
    .LSQ_content(LSQ_content),
    .lsq_head(lsq_head), 
    .lsq_content_size(lsq_content_size),
    .stall_store_retire(stall_store_retire)
    `ifdef DEBUG
    , .debug_next_lsq_head(debug_next_lsq_head),
    .debug_next_lsq_content_size(debug_next_lsq_content_size),
    .debug_next_lsq_content(debug_next_lsq_content)
    `endif
  );

  task reset_all;
    reset = 1;
    pc = 0;
    dispatch_request = '0;
    issue_request = '0;
    num_retire_load_store_inst = 0;
    rollback_load_store_inst = 0;
    complete_request = 0;
    complete_request_valid = 0;
  endtask

  task send3dispatch_inst;
    input [$clog2(`ROB_SZ):0] rob_idx_base;
    for (int i = 0; i < `N; i++) begin
      dispatch_request[i].rob_idx = rob_idx_base + i;
      dispatch_request[i].valid = 1;
      dispatch_request[i].mem_op = i < 2 ? MEM_WRITE : MEM_READ;
      dispatch_request[i].size = i < 2 ? BYTE : HALF;
    end
  endtask

  task send3dispatch_inst_word;
    input [$clog2(`ROB_SZ):0] rob_idx_base;
    for (int i = 0; i < `N; i++) begin
      dispatch_request[i].rob_idx = rob_idx_base + i;
      dispatch_request[i].valid = 1;
      dispatch_request[i].mem_op = i < 2 ? MEM_WRITE : MEM_READ;
      dispatch_request[i].size = i < 2 ? HALF : WORD;
    end
  endtask

  task send3issue_inst;
    input [$clog2(`ROB_SZ):0] rob_idx_base;
    input [`XLEN:0] memory_addr_base;
    input REG_DATA_T data_base;
    for (int i = 0; i < `N; i++) begin
      issue_request[i].basic_info.rob_idx = rob_idx_base + i;
      issue_request[i].basic_info.valid = 1;
      issue_request[i].basic_info.mem_op = i < 2 ? MEM_WRITE : MEM_READ;
      issue_request[i].basic_info.size = i < 2 ? BYTE : HALF;
      issue_request[i].memory_addr = i < 2 ? memory_addr_base + i : memory_addr_base;
      issue_request[i].data = i + data_base;
    end
  endtask

  task send3issue_inst_wwr;
    input [$clog2(`ROB_SZ):0] rob_idx_base3;
    input [$clog2(`ROB_SZ):0] rob_idx_base12; 
    input [`XLEN:0] memory_addr_base3;  // 100
    input [`XLEN:0] memory_addr_base12; // 1000
    input REG_DATA_T data_base3; // 200
    input REG_DATA_T data_base12; // 233
    for (int i = 0; i < `N; i++) begin
      issue_request[i].basic_info.rob_idx = i < 2 ? rob_idx_base12 + i : rob_idx_base3 + i;
      issue_request[i].basic_info.valid = 1;
      issue_request[i].basic_info.mem_op = i < 2 ? MEM_WRITE : MEM_READ;
      issue_request[i].basic_info.size = HALF;
      issue_request[i].memory_addr = i < 2 ? memory_addr_base12 + i * 2 : memory_addr_base3;
      issue_request[i].data = i + (i < 2 ? data_base12 : data_base3);
    end
  endtask

    task send3issue_inst_word;
    input [$clog2(`ROB_SZ):0] rob_idx_base;
    input [`XLEN:0] memory_addr_base;
    input REG_DATA_T data_base;
    for (int i = 0; i < `N; i++) begin
      issue_request[i].basic_info.rob_idx = rob_idx_base + i;
      issue_request[i].basic_info.valid = 1;
      issue_request[i].basic_info.mem_op = i < 2 ? MEM_WRITE : MEM_READ;
      issue_request[i].basic_info.size = i < 2 ? HALF : WORD;
      issue_request[i].memory_addr = i < 2 ? memory_addr_base + i * 2 : memory_addr_base;
      issue_request[i].data = i + data_base;
    end
  endtask

  task send3complete_inst;
    input [`N - 1 : 0] complete_request_base;
    for (int i = 0; i < `N; i++) begin
      complete_request[i] = complete_request_base + i >= `ROB_SZ ? complete_request_base + i - `ROB_SZ : complete_request_base + i;
      complete_request_valid[i] = 1;
    end
  endtask

  always begin
    #(`CLOCK_PERIOD / 2.0);
    clock = ~clock;
  end

  initial begin
    clock = 0;
    reset_all();
    $display("LSQ_content: %p lsq_head: %p lsq_content_size1: %p", LSQ_content, lsq_head, lsq_content_size);

    // cycle 1
    @(posedge clock);
    @(negedge clock);
    $display("LSQ_content: %p lsq_head: %p lsq_content_size2: %p", LSQ_content, lsq_head, lsq_content_size);

    // cycle 2
    @(posedge clock);
    reset = 0;
    send3dispatch_inst(0); // w w r
    @(negedge clock);
    assert (issue_approved == 0) else $finish;
    assert (LSQ_content == 0) else $finish;
    assert (lsq_head == 0) else $finish;
    assert (lsq_content_size == 0) else $finish;
    $display("LSQ_content: %p lsq_head: %p lsq_content_size4: %p ", LSQ_content, lsq_head, lsq_content_size);
    `ifdef DEBUG
      $display("debug_next_lsq_head: %p debug_next_lsq_content_size1: %p", debug_next_lsq_head, debug_next_lsq_content_size);
      $display("debug_next_lsq_content1: %p", debug_next_lsq_content);
    `endif

    // cycle 3
    @(posedge clock);
    send3dispatch_inst_word(4); // w w r + w w r
    send3issue_inst(0, 100, 200); // w w r
    send3complete_inst(lsq_head);
    @(negedge clock);
    $display("LSQ_content: %p lsq_head: %p lsq_content_size5: %p", LSQ_content, lsq_head, lsq_content_size);

    `ifdef DEBUG
      $display("debug_next_lsq_head: %p debug_next_lsq_content_size2: %p", debug_next_lsq_head, debug_next_lsq_content_size);
      $display("debug_next_lsq_content2: %p", debug_next_lsq_content);
    `endif
    $display("LSQ_content: %p lsq_head: %p lsq_content_size6: %p issue_request: %p issue_approved: %p", LSQ_content, lsq_head, lsq_content_size, issue_request, issue_approved);
    assert(lsq_head == 0) else $finish;
    assert(lsq_content_size == `N) else $finish;
    for (int i = 0; i < `N; i++) begin
      // assert(issue_approved[i] == (i < 2 ? 1 : 0)) else $finish;
      assert(issue_approved[i] == 1) else $finish;
      assert(LSQ_content[`head_offset_i].completed == 0) else $finish;
      assert(LSQ_content[`head_offset_i].forwarded_bytes == 0) else $finish;
      assert(LSQ_content[`head_offset_i].read_ready == 0) else $finish;
      assert(LSQ_content[`head_offset_i].issue_success == 0) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i) else $finish;
    end

    // cycle 4
    @(posedge clock);
    dispatch_request = 0;
    send3complete_inst(lsq_head + `N);
    // send3issue_inst_wwr(0, 4, 100, 1000, 200, 233); //wwRWWr
    // input [$clog2(`ROB_SZ):0] rob_idx_base3;
    // input [$clog2(`ROB_SZ):0] rob_idx_base12;
    // input [`XLEN:0] memory_addr_base3;
    // input [`XLEN:0] memory_addr_base12;
    // input REG_DATA_T data_base3;
    // input REG_DATA_T data_base12;
    send3issue_inst_word(4, 1000, 233); // w w r
    @(negedge clock);
    $display("LSQ_content: %p lsq_head: %p lsq_content_size7: %p", LSQ_content, lsq_head, lsq_content_size);
    assert(lsq_head == 0) else $finish;
    assert(lsq_content_size == `N * 2) else $finish;
    for (int i = 0; i < `N; i++) begin
      assert(issue_approved[i] == 1) else $finish;
      assert(LSQ_content[`head_offset_i].completed == 1) else $finish;
      // assert(LSQ_content[`head_offset_i].issue_success == (i < 2 ? 1 : 0)) else $finish;
      assert(LSQ_content[`head_offset_i].issue_success == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i) else $finish;
      // if (i < 2) begin
        assert(LSQ_content[`head_offset_i].request.memory_addr == ((i < 2) ? (100 + i) : 100)) else $finish;
        // assert(LSQ_content[`head_offset_i].request.data == ((i < 2) ? (32'd200 + i) : (32'd200 + (32'd201 << 8)))) else $finish;
      // end
    end
    for (int i = `N; i < `N * 2; i++) begin
      assert(LSQ_content[`head_offset_i].completed == 0) else $finish;
      assert(LSQ_content[`head_offset_i].forwarded_bytes == 0) else $finish;
      assert(LSQ_content[`head_offset_i].read_ready == 0) else $finish;
      assert(LSQ_content[`head_offset_i].issue_success == 0) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i + 1) else $finish;
    end

    // cycle 5
    $display("             cycle 5");
    @(posedge clock);
    dispatch_request = 0;
    complete_request_valid = 0;
    issue_request = 0;
    // for (int i = 2; i <= 2; i++) begin // wwrwwR
    //   issue_request[i].basic_info.rob_idx = 4 + i;
    //   issue_request[i].basic_info.valid = 1;
    //   issue_request[i].basic_info.mem_op = MEM_READ;
    //   issue_request[i].basic_info.size = WORD;
    //   issue_request[i].memory_addr = 1000;
      // issue_request[i].data = i + data_base;
    // end
    
    @(negedge clock);
    $display("LSQ_content: %p lsq_head: %p lsq_content_size@cycle5: %p", LSQ_content, lsq_head, lsq_content_size);
    for (int i = 0; i < `N; i++) begin // R is issued but not forwarded
      // assert(issue_approved[i] == i < 2 ? 0 : 1) else $finish;
      assert(LSQ_content[`head_offset_i].completed == 1) else $finish;
      assert(LSQ_content[`head_offset_i].issue_success == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i) else $finish;
      assert(LSQ_content[`head_offset_i].request.memory_addr == 100 + (i < 2 ? i : 0)) else $finish;
      if (i < 2) begin
        assert(LSQ_content[`head_offset_i].request.data == 200 + i) else $finish;
      end
      else begin
        `ifdef ENABLE_LSQ_FORWARD
        assert(LSQ_content[`head_offset_i].read_ready == 1) else $finish;
        assert(LSQ_content[`head_offset_i].read_data == 200 + (32'd201 << 8)) else $finish;
        `endif
      end
    end
    for (int i = `N; i < `N * 2; i++) begin
      assert(LSQ_content[`head_offset_i].completed == 1) else $finish;
      assert(LSQ_content[`head_offset_i].forwarded_bytes == 0) else $finish;
      assert(LSQ_content[`head_offset_i].read_ready == 0) else $finish;
      assert(LSQ_content[`head_offset_i].issue_success == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i + 1) else $finish;
    end

    // cycle 6
    $display("             cycle 5");
    @(posedge clock);
    dispatch_request = 0;
    complete_request_valid = 0;
    issue_request = 0;
    @(negedge clock);
    $display("LSQ_content: %p lsq_head: %p lsq_content_size@cycle6: %p", LSQ_content, lsq_head, lsq_content_size);
    assert(issue_approved == 0) else $finish;
    for (int i = 0; i < `N; i++) begin // R is issued and not forwarded
      assert(LSQ_content[`head_offset_i].completed == 1) else $finish;
      // if (i == 2) begin
      //   assert(LSQ_content[`head_offset_i].read_ready == 1) else $finish;
      //   assert(LSQ_content[`head_offset_i].forwarded_bytes == 2'b11) else $finish;
      //   assert(LSQ_content[`head_offset_i].read_data == ((32'd201 << 8) + 32'd200)) else $finish;
      // end
      assert(LSQ_content[`head_offset_i].issue_success == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i) else $finish;
      assert(LSQ_content[`head_offset_i].request.memory_addr == 100 + (i < 2 ? i : 0)) else $finish;
      if (i < 2) begin
        assert(LSQ_content[`head_offset_i].request.data == 200 + i) else $finish;
      end
      else begin
        `ifdef ENABLE_LSQ_FORWARD
        assert(LSQ_content[`head_offset_i].read_ready == 1) else $finish;
        assert(LSQ_content[`head_offset_i].read_data == 200 + (32'd201 << 8)) else $finish;
        `endif
      end
    end
    for (int i = `N; i < `N * 2; i++) begin
      assert(LSQ_content[`head_offset_i].completed == 1) else $finish;
      assert(LSQ_content[`head_offset_i].issue_success == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i + 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.memory_addr == 1000 + ((i - `N) < 2 ? 2 * (i - `N) : 0)) else $finish;
      if (i < `N + 2) begin
        assert(LSQ_content[`head_offset_i].request.data == 233 + i - `N) else $finish;
      end
      else begin
        `ifdef ENABLE_LSQ_FORWARD
        assert(LSQ_content[`head_offset_i].read_ready == 1) else $finish;
        assert(LSQ_content[`head_offset_i].read_data == 32'd233 + (32'd234 << 16)) else $finish;
        `endif
      end
    end

    // cycle 7
    @(posedge clock);
    @(negedge clock);
    for (int i = `N; i < `N * 2; i++) begin
      assert(LSQ_content[`head_offset_i].completed == 1) else $finish;
      // if (i == 2 + `N) begin
      //   assert(LSQ_content[`head_offset_i].read_ready == 1) else $finish;
      //   assert(LSQ_content[`head_offset_i].forwarded_bytes == 4'b1111) else $finish;
      //   assert(LSQ_content[`head_offset_i].read_data == ((32'd234 << 16) + 32'd233)) else $finish;
      // end
      assert(LSQ_content[`head_offset_i].issue_success == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.valid == 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.basic_info.rob_idx == i + 1) else $finish;
      assert(LSQ_content[`head_offset_i].request.memory_addr == 1000 + ((i - `N) < 2 ? 2 * (i - `N) : 0)) else $finish;
      if (i < 2 + `N) begin
        assert(LSQ_content[`head_offset_i].request.data == 233 + i - `N) else $finish;
      end
      else begin
        `ifdef ENABLE_LSQ_FORWARD
        assert(LSQ_content[`head_offset_i].read_ready == 1) else $finish;
        assert(LSQ_content[`head_offset_i].read_data == 233 + (32'd234 << 16)) else $finish;
        `endif
      end
    end
    
    $display("BOSS: LSQ_content: %p lsq_head: %p lsq_content_size: %p", LSQ_content, lsq_head, lsq_content_size);

    // wait for outstanding mem operations to complete
    for (int i = 0; i < 100; i++) begin
      @(posedge clock);
    end
    `ifdef ENABLE_LSQ_FORWARD
    assert(LSQ_content[5].read_data == 233 + (32'd234 << 16)) else $finish;
    assert(LSQ_content[2].read_data == 200 + (32'd201 << 8)) else $finish;
    `endif
    $display("FINISHED WAITING: LSQ_content: %p lsq_head: %p lsq_content_size: %p stall_store_retire: %p", LSQ_content, lsq_head, lsq_content_size, stall_store_retire);

    inst_to_retire = 5;
    for (int i = 0; i < 600; i++) begin
      @(posedge clock);
      rollback_load_store_inst = i == 0; // rollback the last load
      if (inst_to_retire > 0 && i % 100 == 0) begin
        inst_to_retire -= 1;
        num_retire_load_store_inst = 1;
      end
      else begin
        num_retire_load_store_inst = 0;
      end
      $display("                       BEGIN clearing cycle %p: num_retire_load_store_inst: %p stall_store_retire: %p rollback_load_store_inst: %p", i, num_retire_load_store_inst, stall_store_retire, rollback_load_store_inst);
      @(negedge clock);
      if (i == 1) begin
        $display("LSQ_content@ i == 1: %p lsq_head: %p lsq_content_size@cycle7: %p", LSQ_content, lsq_head, lsq_content_size);
        assert (issue_approved == 0) else $finish;
        assert (LSQ_content[`check_subtraction(lsq_head + 5, `ROB_SZ)] == 0) else $finish; // just rollback
        assert (lsq_head == 1) else $finish;
        assert (lsq_content_size == 4) else $finish; // rollback 1 retire 1
      end
      $display("                       ING   clearing cycle %p: stall_store_retire: %p, num_retire_load_store_inst: %p", i, stall_store_retire, num_retire_load_store_inst);
    end

    assert (issue_approved == 0) else $finish;
    assert (LSQ_content == 0) else $finish;
    assert (lsq_content_size == 0) else $finish;

    $display("------AFTER RETIRE ALL: LSQ_content: %p lsq_head: %p lsq_content_size: %p", LSQ_content, lsq_head, lsq_content_size);

    // NEW
    @(posedge clock);
    num_retire_load_store_inst = 0;
    complete_request_valid = 0;
    issue_request = 0;
    for (int i = 0; i < `N; i++) begin
      dispatch_request[i].rob_idx = 2 + i;
      dispatch_request[i].valid = 1;
      dispatch_request[i].mem_op = MEM_READ;
      dispatch_request[i].size = i < 2 ? HALF : WORD;
    end
    @(negedge clock);
    assert (issue_approved == 0) else $finish;
    assert (LSQ_content == 0) else $finish;
    assert (lsq_content_size == 0) else $finish;
    $display("LSQ_content@400 : %p lsq_head: %p lsq_content_size9: %p ", LSQ_content, lsq_head, lsq_content_size);
    `ifdef DEBUG
      $display("debug_next_lsq_head: %p debug_next_lsq_content_size1: %p", debug_next_lsq_head, debug_next_lsq_content_size);
      $display("debug_next_lsq_content1: %p", debug_next_lsq_content);
    `endif

    // cycle 3
    @(posedge clock);
    complete_request_valid = 0;
    dispatch_request = 0;
    issue_request = 0;
    for (int i = 0; i < `N; i++) begin
      issue_request[i].basic_info.rob_idx = 2 + i;
      issue_request[i].basic_info.valid = 1;
      issue_request[i].basic_info.mem_op = MEM_READ;
      issue_request[i].basic_info.size = i < 2 ? HALF : WORD;
      issue_request[i].memory_addr = i < 2 ? 1000 + i * 2 : 1000;
      issue_request[i].data = 32 + i;
    end
    @(negedge clock);
    $display("LSQ_content@420: %p lsq_head: %p lsq_content_size10: %p", LSQ_content, lsq_head, lsq_content_size);

    // cycle 4
    @(posedge clock);
    complete_request_valid = 0;
    dispatch_request = 0;
    issue_request = 0;
    @(negedge clock);
    $display("-----------------------------");
    $display("LSQ_content@429: %p lsq_head: %p lsq_content_size11: %p", LSQ_content, lsq_head, lsq_content_size);
    for (int j = 1; j <= 3; j++) begin
      for (int i = 1; i <= 100; i++) begin
        @(posedge clock);
        if (i == 75)
          num_retire_load_store_inst = 1;
        else 
          num_retire_load_store_inst = 0;
        @(negedge clock);
        $display("-----------------j: %p cycle %p LSQ_content: %p lsq_head: %p lsq_content_size: %p", j, i, LSQ_content, lsq_head, lsq_content_size);
        if (i == 25) begin
          if (j == 1)
            assert(LSQ_content[lsq_head].read_data == 233) else $finish;
          else if (j == 2)
            assert(LSQ_content[lsq_head].read_data == 234) else $finish;
          else 
            assert(LSQ_content[lsq_head].read_data == ((234 << 16) | 233)) else $finish;
        end
      end

    end
    `ifdef DEBUG
      $display("debug_next_lsq_head: %p debug_next_lsq_content_size9: %p", debug_next_lsq_head, debug_next_lsq_content_size);
      $display("debug_next_lsq_content2: %p", debug_next_lsq_content);
    `endif
    $display("LSQ_content@440: %p lsq_head: %p lsq_content_size12: %p issue_approved: %p", LSQ_content, lsq_head, lsq_content_size, issue_approved);
    // assert(lsq_head == 0) else $finish;
    assert(lsq_content_size == 0) else $finish;
    // assert(LSQ_content[(lsq_head + 0 >= `ROB_SZ ? lsq_head + 0 - `ROB_SZ : lsq_head + 0)].read_data == 233) else $finish;
    // assert(LSQ_content[(lsq_head + 1 >= `ROB_SZ ? lsq_head + 1 - `ROB_SZ : lsq_head + 1)].read_data == 234) else $finish;
    // assert(LSQ_content[(lsq_head + 2 >= `ROB_SZ ? lsq_head + 2 - `ROB_SZ : lsq_head + 2)].read_data == ((234 << 16) | 233)) else $finish;


    $display("@@@Passed all tests");
    $finish;
  end

endmodule