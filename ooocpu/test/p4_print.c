

#include <stdio.h>

#define NOOP_INST 0x00000013

static int cycle_count = 0;
static FILE* ppfile = NULL;


print_single_rob_entry_input(int inst, int newT, int oldT, int valid){
    printf("ROB_ETY_INPUT{inst: %d, newT: %d, oldT: %d, valid: %d}\n", inst, newT, oldT, valid);
}


void print_single_rob_entry_stored(int head, int tail, int inst, int newT, int oldT, int valid, int ROBidx, int has_excuted, int has_completed){
    const char* ch_ex = (has_excuted == 1) ? "+" : "-";
    const char* ch_cp = (has_completed == 1) ? "+" : "-";
    char* tag;
    if (ROBidx == head && ROBidx == tail) tag = "H/T";
    else if(ROBidx == head) tag = "H";
    else if (ROBidx == tail) tag = "T";
    else if ( head <= tail && ROBidx > head && ROBidx < tail) tag = "v";
    else if (head > tail && (ROBidx > head || ROBidx < tail) ) tag = "v";
    else tag = "0";
    if (valid == 0) tag = "0";
    printf("[ROB_ENTRY| %s] rob_index: %d|\tinst: %d|\t newT: %d|\t oldT: %d|\t valid: %d|\t |\thas_executed: %d|\thas_completed: %d\n", tag, ROBidx, inst, newT, oldT, valid, has_excuted, has_completed);
    // printf("[ROB_ENTRY|] rob_index: %d|\tinst: %d|\t newT: %d|\t oldT: %d|\t valid: %d|\t |\thas_executed: %s|\thas_completed: %s\n", ROBidx, inst, newT, oldT, valid, ch_ex, ch_cp);
}

void print_membus(int proc2mem_command, int mem2proc_response,
                  int proc2mem_addr_hi, int proc2mem_addr_lo,
                  int proc2mem_data_hi, int proc2mem_data_lo)
{
    if (ppfile == NULL)
        return;

    switch(proc2mem_command)
    {
        case 1: fprintf(ppfile, "BUS_LOAD  MEM["); break;
        case 2: fprintf(ppfile, "BUS_STORE MEM["); break;
        default: return; break;
    }

    if ( (proc2mem_addr_hi == 0) ||
        ((proc2mem_addr_hi == -1) && (proc2mem_addr_lo < 0)))
        fprintf(ppfile, "%d", proc2mem_addr_lo);
    else
        fprintf(ppfile, "0x%x%x", proc2mem_addr_hi, proc2mem_addr_lo);
    if (proc2mem_command == 1)
    {
        fprintf(ppfile, "]");
    } else {
        fprintf(ppfile, "] = ");
        if ( (proc2mem_data_hi == 0)||
            ((proc2mem_data_hi == -1) && (proc2mem_data_lo < 0)))
            fprintf(ppfile, "%d", proc2mem_data_lo);
        else
            fprintf(ppfile, "0x%x%x", proc2mem_data_hi, proc2mem_data_lo);
    }
    if(mem2proc_response) {
        fprintf(ppfile, " accepted %d", mem2proc_response);
    } else {
        fprintf(ppfile, " rejected");
    }
}