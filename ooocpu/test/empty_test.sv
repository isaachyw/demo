`include "verilog/sys_defs.svh"

/// This file is used to pass `make simv` or `make syn_simv` without 
/// specifying a specific component to test of

module dummy;
initial begin
    $display("This is an empty test case");

    $display("@@@Passed");
    $finish;
end
endmodule