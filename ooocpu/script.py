import re

def extract_and_sort_lines(filename, search_string):
    lines_with_time = []
    time_pattern = re.compile(r"time:\s+(\d+)")  # Regular expression to extract the timestamp

    with open(filename, 'r') as file:
        for line in file:
            if search_string in line:
                match = time_pattern.search(line)
                if match:
                    timestamp = int(match.group(1))
                    lines_with_time.append((timestamp, line.strip()))

    # Sort the list based on the timestamp
    lines_with_time.sort(key=lambda x: x[0])

    # Return only the lines, without timestamps
    return [line for _, line in lines_with_time]

def main():
    input_filename = 'output_file.log'  # Replace with your input filename
    output_filename = 'mergesort_out_withvalue_80.log'  # Replace with your desired output filename
    search_string = 'ROB RETIRE INSIDE: PC:'

    sorted_lines = extract_and_sort_lines(input_filename, search_string)

    with open(output_filename, 'w') as output_file:
        for line in sorted_lines:
            output_file.write(line + '\n')

    print(f"Sorted unique lines written to {output_filename}")

if __name__ == "__main__":
    main()
