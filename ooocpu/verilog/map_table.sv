`include "verilog/sys_defs.svh"

module map_table(
    input logic clock, reset,

    // Coming from decode@dispatch
    input MAP_TABLE_READ_COMMAND[`N-1:0] decode_dest_read_request,
    output MAP_TABLE_ENTRY[`N-1:0] decode_dest_read_response,
    input MAP_TABLE_WRITE_COMMAND[`N-1:0] decode_map_write_command,

    input MAP_TABLE_READ_COMMAND[`N-1:0][1:0] decode_operands_read_request,
    output MAP_TABLE_ENTRY[`N-1:0][1:0] decode_operands_read_response,
    
    // CDB request coming from fu_manager@complete
    input CDB_ENTRY[`CDB_W-1:0] cdb_entries,
    
    // Write command from rob@rollback
    input MAP_TABLE_WRITE_COMMAND rollback_mt_write

`ifdef DEBUG
    // Debug:
    , output DEBUG_MAP_TABLE dbg_out
`endif
);
  // Note on internal forwarding and conflicting writes:
  // There is no internal forwarding between the write and read command,
  // that is handled through CDB.
  //
  // Write conflicts from decode stage should be resolved by decode,
  // which value gets written is undefined.
  // 
  // Conflicts between decode stage write and rollback stage write is also
  // undefined, as the decode stage should be inactive during rollback.
  MAP_TABLE_ENTRY[`ARCH_REG_SZ-1:0] map_table, next_map_table,cdb_map_table;
  // set CDB
  always_comb begin : set_cdb_ready
    cdb_map_table = map_table;
     for (int i = 0; i < `N; i++) begin
      if (cdb_entries[i].valid) begin
        for (int j = 0; j < `ARCH_REG_SZ; j++) begin
          if (map_table[j].phy_reg_idx == cdb_entries[i].phy_reg_idx) begin
            cdb_map_table[j].ready = `TRUE;
          end
        end
      end
    end
  end


  // Reads
  always_comb begin
    decode_dest_read_response = 0;
    for(int i = 0; i < `N; i++)begin
      if (decode_dest_read_request[i].valid) begin
        decode_dest_read_response[i] =
          cdb_map_table[decode_dest_read_request[i].arch_reg_idx];
        for (integer j=i-1;j>=0;j--)begin
          // $display("decode_dest_read_request[%d].arch_reg_idx = %d",i, decode_map_write_command[j]);
          if ((decode_dest_read_request[i].arch_reg_idx == decode_map_write_command[j].arch_reg_idx) & decode_map_write_command[j].valid & decode_map_write_command[j].phy_reg_idx != 0) begin
            decode_dest_read_response[i].phy_reg_idx = decode_map_write_command[j].phy_reg_idx;
            decode_dest_read_response[i].ready = decode_map_write_command[j].ready;
            break;
          end
        end
      end

    // for (int i = 0; i < `N; i++) begin
    //     $display("[%d] dest: %d", i, decode_dest_read_response[i].phy_reg_idx);
    // end
  end
    decode_operands_read_response = 0;
    for (int i = 0; i < `N; i++) begin
      for (int j = 0; j < 2; j++) begin
        if (decode_operands_read_request[i][j].valid) begin
          decode_operands_read_response[i][j] = cdb_map_table[decode_operands_read_request[i][j].arch_reg_idx];

          for (integer k=i-1;k>=0;k--)begin
            if ((decode_operands_read_request[i][j].arch_reg_idx == decode_map_write_command[k].arch_reg_idx) & decode_map_write_command[k].valid & decode_map_write_command[k].phy_reg_idx != 0) begin
              decode_operands_read_response[i][j].phy_reg_idx = decode_map_write_command[k].phy_reg_idx;
              decode_operands_read_response[i][j].ready = decode_map_write_command[k].ready;
              break;
            end
          end
        end
      end
    end

    // for (int i = 0; i < `N; i++) begin
    //     $display("[%d] opa: %d opb: %d", i, decode_operands_read_response[i][0].phy_reg_idx, decode_operands_read_response[i][1].phy_reg_idx);
    // end
  end
  
  // Writes
  always_comb begin
    next_map_table = cdb_map_table;
    // Write requests
    for (int i = 0; i < `N; i++) begin
      if (decode_map_write_command[i].valid) begin
        // when rolling back, there cannot be instructions in the dispatch stage
        assert(rollback_mt_write.valid == 0);
        assert(decode_map_write_command[i].phy_reg_idx != 0);
        assert(decode_map_write_command[i].ready == 0);
        `ifdef DEBUG
        if (decode_map_write_command[i].phy_reg_idx == 0) begin
          $display("time: %d [MAP TABLE] WARNING: writing to 0 register", $time);
        end
        `endif
        next_map_table[decode_map_write_command[i].arch_reg_idx] = {
          phy_reg_idx: decode_map_write_command[i].phy_reg_idx,
          ready: decode_map_write_command[i].ready,
          valid: `TRUE
        };
      end
    end
    
    if (rollback_mt_write.valid) begin
      `ifdef DEBUG
      $display("time: %d rollback reg arch: %d", $time,rollback_mt_write.arch_reg_idx );
      `endif
      next_map_table[rollback_mt_write.arch_reg_idx] = {
        phy_reg_idx: rollback_mt_write.phy_reg_idx,
        ready: rollback_mt_write.phy_reg_idx == 0 ? `TRUE : rollback_mt_write.ready,
        valid:`TRUE
      };
    end
    // 0 reg is always 0 and available
  end

 `ifdef DEBUG
 always_comb begin
    for (int i = 0; i < `N; i++) $display("[Map Write]\ttime:%d| arch_reg: %d | phy_reg: %d | valid: %d", $time, decode_map_write_command[i].arch_reg_idx, decode_map_write_command[i].phy_reg_idx, decode_map_write_command[i].valid);
 end
 `endif

  always_ff @(posedge clock) begin
    if (reset) begin
      // Initialization
      for(int i = 0; i < `ARCH_REG_SZ; i++) begin
        map_table[i] <= {
          i,
          `TRUE,
          `TRUE
        };
      end
    end else begin
      map_table <= next_map_table;
    end

    `ifdef DEBUG
    $display("time: %d [MAP TABLE]", $time);
    for (int i = 0; i <`ARCH_REG_SZ; i++) begin
      $display("arch #%d\tphys #%d\tready: %d",i, next_map_table[i].phy_reg_idx, next_map_table[i].ready);
    end
    `endif
  end

  `ifdef DEBUG
  assign dbg_out.map_table = map_table;
  `endif
endmodule