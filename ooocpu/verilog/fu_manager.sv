`include "verilog/sys_defs.svh"

module fu_manager(
    input logic clock,
    input logic reset,

    input ROB_REF_T rob_rollback_idx,

    // Issue:
    input RS_ENTRY [`RS_SZ-1:0] rs_entries_in_wire,
    input [`RS_SZ-1:0] rs_ready_in_wire,
    output [`RS_SZ-1:0] fu_feedback_mask,
    output NUM_INST_T_RS fu_feedback_num,

    // Regfile port is RS_SZ
    output REG_READ_REQ[`RS_SZ-1:0][1:0] read_reg_req,
    input REG_READ_RESPONSE[`RS_SZ-1:0][1:0] read_reg_response,

    // Complete:
    output ROB_REF_T[`N-1:0] completed_inst,
    output REG_WRITE_REQ[`CDB_W-1:0] write_reg_req,
    output BRANCH_FEEDBACK[`N-1:0] branch_feedbacks,

    output CDB_ENTRY [`CDB_W-1:0] cdb_entries_wire,

    // LSQ:
    input LSQ_ENTRY [`ROB_SZ - 1 : 0] lsq_content,
    output LSQ_ISSUE_REQUEST [`N - 1 : 0] lsq_issue_request,
    // input logic [`N - 1 : 0] issue_approved,
    output logic [`N-1:0][$clog2(`ROB_SZ)+1:0] completed_entry,
    output logic [`N-1:0] completed_entry_valid

`ifdef DEBUG
    , output MULT_OUTPUT [`NUM_FU_MULT-1:0] mult_results
    , output [`N-1:0][$clog2(`NUM_FU_MULT):0] mul_idx_debug
    , output [$clog2(`N):0] mul_cnt_debug
    , output MULT_INPUT [`NUM_FU_MULT-1:0] mult_input_debug
`endif
);

    logic [$clog2(`N):0] issue_cnt;
    logic [$clog2(`N):0] cdb_cnt;
    logic next_internal_rollback_not_issued;
    logic internal_rollback_not_issued;
    logic [`RS_SZ-1:0] internal_fu_feedback_mask;
    NUM_INST_T_RS internal_fu_feedback_num;


    // ALU input output
    ALU_INPUT [`NUM_FU_ALU-1:0] alus_input;
    ALU_INPUT [`NUM_FU_ALU-1:0] next_alus_input;
    ALU_OUTPUT [`NUM_FU_ALU-1:0] alus_output;

    // MULT input output
    MULT_INPUT [`NUM_FU_MULT-1:0] mults_input;
    MULT_INPUT [`NUM_FU_MULT-1:0] next_mults_input;
    MULT_OUTPUT [`NUM_FU_MULT-1:0] mults_output;

    // MEM input output
    MEM_INPUT [`N-1:0] mems_issue_input;

    // ALU side comb logics
    logic [`NUM_FU_ALU-1:0] alu_cdb_selected_mask_wire;
    logic [`NUM_FU_ALU-1:0] alu_cdb_stall_mask_wire;
    logic [$clog2(`N):0] alu_av_cnt;
    logic [$clog2(`N):0] alu_issue_cnt;
    logic [`N-1:0][$clog2(`NUM_FU_ALU):0] alu_av_idxs;

    // MULT side comb logics
    logic [`NUM_FU_MULT-1:0] mult_cdb_selected_mask_wire;
    logic [`NUM_FU_MULT-1:0] mult_cdb_stall_mask_wire;
    logic [$clog2(`N):0] mult_av_cnt;
    logic [$clog2(`N):0] mult_issue_cnt;
    logic [`N-1:0][$clog2(`NUM_FU_MULT):0] mult_av_idxs;

    // MEM side comb logics
    logic [$clog2(`N):0] mem_attempt_cnt;
    logic [$clog2(`N):0] mem_cdb_cnt;
    logic [`XLEN-1:0] sign_extended_read_data;

    assign fu_feedback_mask = internal_fu_feedback_mask;
    assign fu_feedback_num = internal_fu_feedback_num;

    `ifdef DEBUG
    assign mult_results = mults_output;
    assign mul_idx_debug = mult_av_idxs;
    assign mul_cnt_debug = mult_av_cnt;
    assign mult_input_debug = mults_input;
    `endif

    `ifdef DEBUG
    always_comb begin
        $display("[FU ROLLBACK] time: %d rob_rollback_idx: %d",$time, rob_rollback_idx);
    end
    `endif

    alu alus [`NUM_FU_ALU-1:0] (
        .clock(clock),
        .reset(reset),
        .alu_input(alus_input),
        .alu_output(alus_output),
        .rob_rollback_idx(rob_rollback_idx),
        .rollback_not_issued(internal_rollback_not_issued),
        .curr_cdb_stall(alu_cdb_stall_mask_wire)
    );

    mult mults [`NUM_FU_MULT-1:0] (
        .clock(clock),
        .reset(reset),
        .mult_input(mults_input),
        .mult_output(mults_output),
        .rob_rollback_idx(rob_rollback_idx),
        .rollback_not_issued(internal_rollback_not_issued),
        .curr_cdb_stall(mult_cdb_stall_mask_wire)
    );

    mem_issue mems_issue [`N-1:0] (
        .mem_input(mems_issue_input),
        .lsq_issue_in(lsq_issue_request)
    );
    `ifdef DEBUG
    always_comb begin : debug
        $display("Time: [%d]\tHERE IS THE LSQ CONTENT I GOT:", $time);
        for(int i = 0; i < `ROB_SZ; i++) begin
            $display("index: %d\trob_idx: %d\tread_data: %d\tread_ready: %d\tcompleted: %d\tissue: %d", i, lsq_content[i].request.basic_info.rob_idx, lsq_content[i].read_data, lsq_content[i].read_ready, lsq_content[i].completed, lsq_content[i].issue_success);
        end

        $display("Time: [%d]\tLSQ ISSUE TO LSQ:", $time);
        for(int i = 0; i < `N; i++) begin
            $display("index: %d\tmemory_addr: %d\tdst_reg_idx: %d", i, lsq_issue_request[i].memory_addr, lsq_issue_request[i].dst_reg_idx);
        end
    end
    `endif

    always_comb begin: drive_cdb_stall_mask
        for (int i = 0; i < `NUM_FU_ALU; i++) begin
            if (alu_cdb_selected_mask_wire[i]) begin
                alu_cdb_stall_mask_wire[i] = 0;
            end else begin
                if (alus_output[i].valid) begin
                    alu_cdb_stall_mask_wire[i] = 1;
                end else begin
                    alu_cdb_stall_mask_wire[i] = 0;
                end
            end
        end
        for (int i = 0; i < `NUM_FU_MULT; i++) begin
            if (mult_cdb_selected_mask_wire[i]) begin
                mult_cdb_stall_mask_wire[i] = 0;
            end else begin
                if (mults_output[i].done) begin
                    mult_cdb_stall_mask_wire[i] = 1;
                end else begin
                    mult_cdb_stall_mask_wire[i] = 0;
                end
            end
        end

    end

    always_comb begin: drive_next_av_wire
        alu_av_cnt = 0;
        alu_av_idxs = 0;
        mult_av_cnt = 0;
        mult_av_idxs = 0;
        // ALU
        for (int i = 0; i < `NUM_FU_ALU; i++) begin
            if (alu_av_cnt >= `N) break;
            if (~alus_output[i].valid | ~alu_cdb_stall_mask_wire[i]) begin
                alu_av_idxs[alu_av_cnt] = i;
                alu_av_cnt = alu_av_cnt + 1;
            end
        end
        // MULT
        for (int i = 0; i < `NUM_FU_MULT; i++) begin
            if (mult_av_cnt >= `N) break;
            if (~mults_output[i].done | ~mult_cdb_stall_mask_wire[i]) begin
                mult_av_idxs[mult_av_cnt] = i;
                mult_av_cnt = mult_av_cnt + 1;
            end
        end
    end

    always_comb begin : RS_FU
        mem_attempt_cnt = 0;
        alu_issue_cnt = 0;
        mult_issue_cnt = 0;
        issue_cnt = 0;
        internal_fu_feedback_mask = '0;
        internal_fu_feedback_num = '0;
        next_alus_input = 0;
        next_mults_input = 0;   // By default, cdb stall is 0
        read_reg_req = '0;
        mems_issue_input = '0;
        next_internal_rollback_not_issued = 0;
        if (rob_rollback_idx.valid) begin
            next_internal_rollback_not_issued = 1;
        end else begin
            `ifdef DEBUG
            $display("time: %d mult cdb stall mask: %b", $time, mult_cdb_stall_mask_wire);
            `endif
            for (int i = 0; i < `RS_SZ; i++) begin
                if (rs_entries_in_wire[i].valid && rs_ready_in_wire[i]) begin
                    case (rs_entries_in_wire[i].inst.fu)
                        ALU: begin
                            if (alu_issue_cnt < alu_av_cnt) begin
                                internal_fu_feedback_mask[i] = 1;
                                internal_fu_feedback_num = internal_fu_feedback_num + 1;
                                `ifdef  DEBUG
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].rs_entry = rs_entries_in_wire[i];
                                `endif
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].has_input = 1;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].opa_select = rs_entries_in_wire[i].inst.opa_select;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].opb_select = rs_entries_in_wire[i].inst.opb_select;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].inst = rs_entries_in_wire[i].inst.inst;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].pc = rs_entries_in_wire[i].inst.pc;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].npc = rs_entries_in_wire[i].inst.npc;
                                // use to store the predict target pc
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].func = rs_entries_in_wire[i].inst.func;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].next_has_dest = rs_entries_in_wire[i].inst.has_dest;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].next_rob_idx = rs_entries_in_wire[i].rob_ref;
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].next_dst_reg_idx = rs_entries_in_wire[i].dest_reg;
                                // TODO: Maybe more in the future
                                next_alus_input[alu_av_idxs[alu_issue_cnt]].predict = rs_entries_in_wire[i].inst.branch_prediction.direction;
                                `ifdef DEBUG
                                $display("next alus input rob index here: %d", next_alus_input[alu_av_idxs[alu_issue_cnt]].next_rob_idx);
                                `endif

                                // Assign read_request to the reg file
                                read_reg_req[i][0].reg_ref = rs_entries_in_wire[i].operands_status[0].phy_reg_idx;
                                read_reg_req[i][0].valid = rs_entries_in_wire[i].operands_status[0].valid;
                                read_reg_req[i][1].reg_ref = rs_entries_in_wire[i].operands_status[1].phy_reg_idx;
                                read_reg_req[i][1].valid = rs_entries_in_wire[i].operands_status[1].valid;

                                // Assign n_alu_inputs from the reg file output
                                // Assume the read_response is in the sequence of RS
                                if(read_reg_response[i][0].valid) begin
                                    next_alus_input[alu_av_idxs[alu_issue_cnt]].phy_reg_val_a = read_reg_response[i][0].data;
                                end
                                if(read_reg_response[i][1].valid) begin
                                    next_alus_input[alu_av_idxs[alu_issue_cnt]].phy_reg_val_b = read_reg_response[i][1].data;
                                end

                                issue_cnt = issue_cnt + 1;
                                alu_issue_cnt = alu_issue_cnt + 1;
                            end
                        end
                        MULT: begin
                            // TODO: tricky issue logic with mult cdb stall
                            if (mult_issue_cnt < mult_av_cnt) begin
                                // If a mult is stalled due to cbd
                                internal_fu_feedback_mask[i] = 1;
                                internal_fu_feedback_num = internal_fu_feedback_num + 1;
                                `ifdef  DEBUG
                                next_mults_input[mult_av_idxs[mult_issue_cnt]].rs_entry = rs_entries_in_wire[i];
                                // $display("time: [%d]\tHere is next_mults_input[%d].rs_entry = %p", $time, mult_av_idxs[mult_issue_cnt], rs_entries_in_wire[i]);
                                `endif 

                                next_mults_input[mult_av_idxs[mult_issue_cnt]].start = 1;
                                next_mults_input[mult_av_idxs[mult_issue_cnt]].func = rs_entries_in_wire[i].inst.func;
                                next_mults_input[mult_av_idxs[mult_issue_cnt]].next_rob_idx = rs_entries_in_wire[i].rob_ref;
                                next_mults_input[mult_av_idxs[mult_issue_cnt]].next_dst_reg_idx = rs_entries_in_wire[i].dest_reg;

                                // Assign read_request to the reg file
                                read_reg_req[i][0].reg_ref = rs_entries_in_wire[i].operands_status[0].phy_reg_idx;
                                read_reg_req[i][0].valid = 1;
                                read_reg_req[i][1].reg_ref = rs_entries_in_wire[i].operands_status[1].phy_reg_idx;
                                read_reg_req[i][1].valid = 1;

                                // DEBUG
                                `ifdef DEBUG
                                if(rs_entries_in_wire[i].inst.pc == 40) begin
                                    $display("I'm the buggy mult, here is my two operands: %d %d", read_reg_req[i][0].reg_ref, read_reg_req[i][1].reg_ref);
                                end
                                `endif

                                // Assume the read_response is in the sequence of RS
                                if(read_reg_response[i][0].valid) begin
                                    next_mults_input[mult_av_idxs[mult_issue_cnt]].opa = read_reg_response[i][0].data;
                                end
                                if(read_reg_response[i][1].valid) begin
                                    next_mults_input[mult_av_idxs[mult_issue_cnt]].opb = read_reg_response[i][1].data;
                                end

                                issue_cnt = issue_cnt + 1;
                                mult_issue_cnt = mult_issue_cnt + 1;
                            end
                        end
                        MEM: begin
                            if (mem_attempt_cnt < `N) begin
                                internal_fu_feedback_mask[i] = 1;
                                internal_fu_feedback_num = internal_fu_feedback_num + 1;
                                read_reg_req[i][0].reg_ref = rs_entries_in_wire[i].operands_status[0].phy_reg_idx;

                                //DEBUG
                                // $display("read request: phy[%d]", read_reg_req[i][0].reg_ref);

                                read_reg_req[i][0].valid = 1;
                                read_reg_req[i][1].reg_ref = rs_entries_in_wire[i].operands_status[1].phy_reg_idx;
                                read_reg_req[i][1].valid = 1;

                                mems_issue_input[mem_attempt_cnt] = {
                                    valid: 1,
                                    phy_reg_val_a: (read_reg_response[i][0].valid) ? read_reg_response[i][0].data : 0,
                                    phy_reg_val_b: (read_reg_response[i][1].valid) ? read_reg_response[i][1].data : 0,
                                    opb_select: rs_entries_in_wire[i].inst.opb_select,
                                    inst: rs_entries_in_wire[i].inst.inst,
                                    pc: rs_entries_in_wire[i].inst.pc,
                                    func: rs_entries_in_wire[i].inst.func,
                                    has_dest: rs_entries_in_wire[i].inst.has_dest,
                                    rob_idx: rs_entries_in_wire[i].rob_ref,
                                    dst_reg_idx: rs_entries_in_wire[i].dest_reg,
                                    rs_entry: rs_entries_in_wire[i]
                                };

                                issue_cnt = issue_cnt + 1;
                                mem_attempt_cnt = mem_attempt_cnt + 1;
                            end
                        end
                        default: issue_cnt = issue_cnt + 0;
                    endcase
                end
                if (issue_cnt >= `N) break;
            end
        end
    end


    // CDB ~ RS complete
    always_comb begin
        mem_cdb_cnt = 0;
        cdb_cnt = 0;
        cdb_entries_wire = 0;
        completed_inst = 0;
        branch_feedbacks = 0;
        alu_cdb_selected_mask_wire = 0;
        mult_cdb_selected_mask_wire = 0;
        write_reg_req = '0;
        completed_entry = '0;
        completed_entry_valid = '0;
        sign_extended_read_data = '0;
        // one big loop for synthesis
        for (int i = 0; i < `NUM_FU_ALU + `NUM_FU_MULT + `ROB_SZ; i++) begin
            `define IDX_MULT (i)
            `define IDX_ALU (i - `NUM_FU_MULT)
            `define IDX_MEM (i - `NUM_FU_ALU - `NUM_FU_MULT)

            // MULT
            if (i < `NUM_FU_MULT) begin
                if (rob_rollback_idx.valid & (rob_rollback_idx == mults_output[`IDX_MULT].rob_idx)) begin
                end else begin
                    if (mults_output[`IDX_MULT].done) begin     // check correctness for using done
                        mult_cdb_selected_mask_wire[`IDX_MULT] = 1;
                        // connect ports
                        completed_inst[cdb_cnt] = mults_output[`IDX_MULT].rob_idx;
                        cdb_entries_wire[cdb_cnt] = {
                            phy_reg_idx: mults_output[`IDX_MULT].dst_reg_idx,
                            valid: 1    // MULT instructions must have destination
                            // check correctness
                        };
                        // write mult result to phys reg file
                        write_reg_req[cdb_cnt] = {
                            reg_ref: mults_output[`IDX_MULT].dst_reg_idx,
                            data: mults_output[`IDX_MULT].result,
                            valid: 1
                        };
                        cdb_cnt = cdb_cnt + 1;

                        `ifdef DEBUG
                        $display("[time:%d]\tFU_MULT\tPC:%h\twrite arch reg #%d, phys reg #%d result: %h \t mult cdb mults_output[`IDX_MULT].rob_idx: %d valid: %d", 
                        $time, 
                        mults_output[`IDX_MULT].rs_entry.inst.pc, 
                        mults_output[`IDX_MULT].rs_entry.inst.inst.r.rd, 
                        mults_output[`IDX_MULT].rs_entry.dest_reg,
                        mults_output[`IDX_MULT].result,
                        mults_output[`IDX_MULT].rob_idx.rob_index, 
                        mults_output[`IDX_MULT].rob_idx.valid);
                        `endif
                    end
                end
            end
            // ALU
            else if (i < `NUM_FU_ALU + `NUM_FU_MULT) begin
                if (rob_rollback_idx.valid & (rob_rollback_idx == alus_output[`IDX_ALU].rob_idx)) begin
                end else begin
                    if (alus_output[`IDX_ALU].valid) begin
                        `ifdef DEBUG
                        $display("alu slected: %d", cdb_cnt);
                        `endif
                        alu_cdb_selected_mask_wire[`IDX_ALU] = 1;
                        // get rob idx from alus_rob_idx, put it into rob idx output
                        completed_inst[cdb_cnt] = alus_output[`IDX_ALU].rob_idx;
                        // put dst reg idx to cdb slot
                        cdb_entries_wire[cdb_cnt] = {
                            phy_reg_idx: alus_output[`IDX_ALU].dst_reg_idx,
                            valid:   alus_output[`IDX_ALU].has_dest
                        };
                        // put branching result
                        branch_feedbacks[cdb_cnt] = alus_output[`IDX_ALU].br_feedback;
                        `ifdef DEBUG
                            $display("display branch_feedback %b, %b,reality%b",branch_feedbacks[cdb_cnt].valid,branch_feedbacks[cdb_cnt].predict,branch_feedbacks[cdb_cnt].reality.direction);
                        `endif 
                        // write alu result to phys reg file
                        if (alus_output[`IDX_ALU].has_dest) begin
                            write_reg_req[cdb_cnt] = {
                                reg_ref: alus_output[`IDX_ALU].dst_reg_idx,
                                data: alus_output[`IDX_ALU].result,
                                valid: 1
                            };

                            `ifdef DEBUG
                            $display("[time:%d]\tFU_ALU\tPC:%h\twrite arch reg #%d, phys reg #%d: result: %h", 
                            $time, alus_output[`IDX_ALU].rs_entry.inst.pc, alus_output[`IDX_ALU].rs_entry.inst.inst.r.rd, alus_output[`IDX_ALU].rs_entry.dest_reg, alus_output[`IDX_ALU].result);
                            `endif 
                        end
                        cdb_cnt = cdb_cnt + 1;
                    end
                end
            end
            // MEM
            else begin
                `ifdef DEBUG
                // $display("time: [%d]\tALL mem_issue_input arrives here, its rob_idx: %d, it's issue_apporved: %d", $time, mems_issue_input[`IDX_MEM].rob_idx.rob_index, issue_approved[`IDX_MEM]);
                `endif
                if (rob_rollback_idx.valid & (lsq_content[`IDX_MEM].request.basic_info.rob_idx == rob_rollback_idx.rob_index)) begin
                    // Want to complete a to-rollback lsq entry, do nothing
                end else begin
                    if(~lsq_content[`IDX_MEM].completed && lsq_content[`IDX_MEM].issue_success) begin
                        if(lsq_content[`IDX_MEM].request.basic_info.mem_op == MEM_WRITE) begin
                            completed_inst[cdb_cnt].rob_index = lsq_content[`IDX_MEM].request.basic_info.rob_idx;
                            completed_inst[cdb_cnt].valid = 1;
                            completed_entry[mem_cdb_cnt] = `IDX_MEM;
                            completed_entry_valid[mem_cdb_cnt] = 1;
                            mem_cdb_cnt = mem_cdb_cnt + 1;
                            cdb_cnt = cdb_cnt + 1;

                            // DEBUG
                            if(lsq_content[`IDX_MEM].request.memory_addr == 65004) begin
                                $display("Time: %d\tThis is a store operation(pc%h) to addr[%h], the write data is: %h", $time, lsq_content[`IDX_MEM].request.pc, lsq_content[`IDX_MEM].request.memory_addr, lsq_content[`IDX_MEM].request.data);
                            end
                        // LOAD
                        end else if(lsq_content[`IDX_MEM].read_ready) begin
                            sign_extended_read_data = lsq_content[`IDX_MEM].read_data;
                            // unsigned with all 0:
                            if (~lsq_content[`IDX_MEM].request.basic_info.is_ld_signed) begin
                                if (lsq_content[`IDX_MEM].request.basic_info.size == BYTE) begin
                                    sign_extended_read_data[`XLEN-1:8] = 0;
                                end else if (lsq_content[`IDX_MEM].request.basic_info.size == HALF) begin
                                    sign_extended_read_data[`XLEN-1:16] = 0;
                                end
                            end else begin
                                if (lsq_content[`IDX_MEM].request.basic_info.size == BYTE) begin
                                    sign_extended_read_data[`XLEN-1:8] = {(`XLEN-8){lsq_content[`IDX_MEM].read_data[7]}};
                                end else if (lsq_content[`IDX_MEM].request.basic_info.size == HALF) begin
                                    sign_extended_read_data[`XLEN-1:16] = {(`XLEN-16){lsq_content[`IDX_MEM].read_data[15]}};
                                end
                            end
                            write_reg_req[cdb_cnt] = {
                                reg_ref: lsq_content[`IDX_MEM].request.dst_reg_idx,
                                // data: lsq_content[`IDX_MEM].read_data,
                                data: sign_extended_read_data,
                                valid: 1
                            };





                            // DEBUG
                            if(lsq_content[`IDX_MEM].request.pc == 680) begin
                            $display("Time: %d\tHere is pc%h load to x14, the read data is %h at address[%h]", $time, lsq_content[`IDX_MEM].request.pc, sign_extended_read_data, lsq_content[`IDX_MEM].request.memory_addr);
                            end







                            cdb_entries_wire[cdb_cnt] = {
                                phy_reg_idx: lsq_content[`IDX_MEM].request.dst_reg_idx,    // Only read will broadcast
                                valid: 1
                            };

                            completed_inst[cdb_cnt].rob_index = lsq_content[`IDX_MEM].request.basic_info.rob_idx;
                            completed_inst[cdb_cnt].valid = 1;
                            `ifdef DEBUG
                            $display("Time: [%d]\tThe completed MEM READ rob index is: %d", $time, completed_inst[cdb_cnt].rob_index);
                            `endif
                            completed_entry[mem_cdb_cnt] = `IDX_MEM;
                            completed_entry_valid[mem_cdb_cnt] = 1;
                            mem_cdb_cnt = mem_cdb_cnt + 1;
                            cdb_cnt = cdb_cnt + 1;
                        end
                    end
                end
            end
            if (cdb_cnt >= `N) break;
            // TODO: If change order, remember to modify macro
            `undef IDX_ALU
            `undef IDX_MULT
            `undef IDX_MEM
        end
    end

// always_comb begin
//     for (int i = 0; i < `N; i++) $display("[rs_entries_in_wire]\ttime: %d\t%p", rs_entries_in_wire[i]);
// end
    always_ff @(posedge clock) begin
        if(reset) begin
            internal_rollback_not_issued <= 0;
            alus_input <= '0;
            mults_input <= '0;
        end else begin
            internal_rollback_not_issued <= next_internal_rollback_not_issued;
            // ALU
            alus_input <= next_alus_input;
            // MULT
            mults_input <= next_mults_input;
            // MEM
        end
    end
endmodule