`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"

// Debug tracing
// `define __DC_TRACE

// Decodes and resolves the new register for the given instruction,
// Note: decoder contains the freelist structure
// All in the same cycle (decode) - pure combinational
module dispatch_stage(
    input logic clock, reset,

    input FETCH_RES[`N-1:0] instructions_to_decode, // come from instruction buffer

    /// ROB Packet
    // resolves the T (destination) and Told from MapEntry for ROB_ETY
    output MAP_TABLE_READ_COMMAND[`N-1:0] dest_read_req,
    input MAP_TABLE_ENTRY[`N-1:0] dest_read_res,
    // update T to MapEntry
    output MAP_TABLE_WRITE_COMMAND[`N-1:0] map_table_updates,

    output ROB_PACKET[`N-1:0] rob_result,

    /// RS Input
    input ROB_REF_T[`N-1:0] rob_refs,
    output MAP_TABLE_READ_COMMAND[`N-1:0] [1:0] operands_read_req,
    input MAP_TABLE_ENTRY[`N-1:0] [1:0] operands_read_res,

    output RS_INPUT [`N-1:0] rs_result,

    // Freed register (resolve in the same cycle)
    input PHYS_REG_T[`N:0] return_reg// come from rob, bypass to freelist

`ifdef DEBUG
    // Debug:
    , output DEBUG_DECODER dbg_out
`endif
);

    /*          Internal decoding:        */

    DECODED_INST [`N-1:0] inst_pack_wire;
    
    /// ROB
    
    // Get new tag
    _intern_decoder intern_decoder0 [`N-1:0](
        .inst_in(instructions_to_decode),
        .result(inst_pack_wire)
    );
    `ifdef DEBUG
    always_comb begin
        for(int i = 0; i < `N; i++) begin
            $display("time: %d instructions_to_decode[%d].pc: %h valid: %d inst: %h", $time, i, instructions_to_decode[i].pc, instructions_to_decode[i].valid, instructions_to_decode[i].inst);
        end
    end
    `endif
    /*     Dest Reg:                         */

    ARCH_REG_T [`N-1:0] arch_dest_reg;
    always_comb begin : MAP_ARCH_REG
        // $display("inst_pack_wire: %p", inst_pack_wire);

        for (int i = 0; i < `N; i++) begin
            arch_dest_reg[i] = (inst_pack_wire[i].has_dest & ( inst_pack_wire[i].illegal == 0 ) ) ? inst_pack_wire[i].inst.r.rd : 0;
        end
        `ifdef DEBUG
        $display("[time: %d]\tdecoder arch_dest_reg", $time);
        for (int i = 0; i < `N; i++) begin
            $display("arch_dest_reg[%d]: %d", i, arch_dest_reg[i]);
        end
        `endif
    end
    
    /*      New Dest Register Allocation:    */

    // Free-list interface:
    logic [$clog2(`N):0] fl_reg_needed;
    FREE_LIST_ENTRY [`N-1:0] fl_free_reg;
    FREE_LIST_ENTRY [`N:0] fl_retired_reg;
    
    always_comb begin : FL_COUNT_REG_NEED
        fl_reg_needed = 0;
        if (!reset) begin
            for (int i = 0; i < `N; i++) begin
                if (arch_dest_reg[i] != 0) begin
                    fl_reg_needed += 1;
                end
            end
        end
        `ifdef DEBUG
        $display("[time: %d]\tfl_reg_needed: %d",$time, fl_reg_needed);
        `endif
        `ifdef __DC_TRACE $display("reg_needed: %p", fl_reg_needed); `endif
    end

    always_comb begin : FL_RETIREMENT_FORWARD
        fl_retired_reg = '0;
        for (int i = 0; i < `N+1; i++) begin
            if (return_reg[i] != 0) begin
                fl_retired_reg[i] = {
                    return_reg[i],
                    `TRUE
                };
            end
        end
        `ifdef __DC_TRACE $display("Retired: %p", fl_retired_reg); `endif
    end
    
    `ifdef DEBUG DEBUG_FREE_LIST fl_dbg; `endif
    
    free_list _m_fl(
        .clock(clock), .reset(reset),
        .reg_needed(fl_reg_needed),
        .retired_reg_in(fl_retired_reg),
        .free_reg_out(fl_free_reg)
        `ifdef DEBUG, .dbg(fl_dbg) `endif
    );
    `ifdef DEBUG
    always_comb begin 
        $display("time: %d num_free_needed: %d fl_free_reg: %p", $time, fl_reg_needed, fl_free_reg);
        for (int i =0; i < `N; i++)begin
            $display("time: %d fl_free_reg[%d]: %d valid: %d", $time, i, fl_free_reg[i].phy_reg_idx, fl_free_reg[i].valid);
        end
        $display("time: %d fl_retired_reg: %p", $time, fl_retired_reg);
    end
    `endif
    // Map between instruction and its new destination register.
    PHYS_REG_T new_preg[`N-1:0];
    logic [$clog2(`N):0] dest_reg_fl_i;
    logic [$clog2(`N):0] flst_reg_fl_i;
    always_comb begin : CAL_MAP_DEST_REG
        dest_reg_fl_i = 0;
        for (int i = 0; i < `N; i++) begin
            if (arch_dest_reg[i] != 0) begin
                // `ifdef DEBUG assert (fl_free_reg[dest_reg_fl_i].valid); `endif
                `ifdef DEBUG assert (fl_free_reg[i].valid); `endif
                // new_preg[i] = fl_free_reg[dest_reg_fl_i].phy_reg_idx;
                new_preg[i] = fl_free_reg[dest_reg_fl_i].phy_reg_idx;
                dest_reg_fl_i += 1;
            end else begin
                new_preg[i] = 0;
            end
        end
    end

    /*      Old Dest Register:               */
    
    // Map Entry reads
    always_comb begin : MAP_ENTRY_READ
        for (int i = 0; i < `N; i++) begin
            // This is safe for instructions with no dest, as they have 0 reg
            if(arch_dest_reg[i] == 0) begin
                dest_read_req[i] = {'0, `FALSE};
            end else begin
                dest_read_req[i] = { arch_dest_reg[i], `TRUE }; // sb
            end
        end
    end

    // Resole Told
    PHYS_REG_T old_preg[`N-1:0];
    always_comb begin : RESOLVE_OLD_REG
        for (int i = 0; i < `N; i++) begin
            old_preg[i] = dest_read_res[i].phy_reg_idx;
            /******************** no need, solved in map table ********************/
            // Resolve forwarding
            // for (int j = i - 1; j >= 0; j--) begin
            //     // We use the new destination register immediatly before this
            //     if (arch_dest_reg[j] == arch_dest_reg[i]) begin
            //         old_preg[i] = new_preg[j];
            //         break;
            //     end
            // end
        end
    end

    /*      Collect output:                  */
    always_comb begin : ASSIGN_ROB_OUT
    rob_result = '0;
        for (int i = 0; i < `N; i++) begin
            rob_result[i] = {
                inst: inst_pack_wire[i],
                newT: new_preg[i],
                oldT: old_preg[i],
                arch_reg_idx: (inst_pack_wire[i].has_dest == 1) ? inst_pack_wire[i].inst.r.rd : 0,
                // Not llegal instructions don't go into the ROB or RS
                valid: (inst_pack_wire[i].illegal == 0)
            };

        end

    end
    
    always_comb begin : ASSIGN_MAP_UPDATE
        map_table_updates = '0;
        for (int i = 0; i < `N; i++) begin
            if(~inst_pack_wire[i].illegal & inst_pack_wire[i].has_dest)begin
                map_table_updates[i] = {
                phy_reg_idx: new_preg[i],
                arch_reg_idx: arch_dest_reg[i],
                ready: 0,
                valid: (new_preg[i] != 0)
            };
            end
            `ifdef DEBUG
            $display("time: %d pc:%h write map table arch: %d\tphy reg: %d valid: %d", $time, inst_pack_wire[i].pc, map_table_updates[i].arch_reg_idx, map_table_updates[i].phy_reg_idx, map_table_updates[i].valid);
            `endif
        end
    end

    /// RS
    always_comb begin : OPERAND_MAP_READ
        for (int i = 0; i < `N; i++) begin
            operands_read_req[i][0] = {
                arch_reg_idx: inst_pack_wire[i].inst.r.rs1,
                valid: inst_pack_wire[i].has_reg1 & (inst_pack_wire[i].illegal == 0) 
            };
            operands_read_req[i][1] = {
                arch_reg_idx: inst_pack_wire[i].inst.r.rs2,
                valid: inst_pack_wire[i].has_reg2 & (inst_pack_wire[i].illegal == 0)
            };
        end
    end
    always_comb begin : ASSIGN_RS_RES
        for (int i = 0; i < `N; i++) begin
            rs_result[i] = {
                inst: inst_pack_wire[i],
                operands_status: operands_read_res[i],
                dest_reg: new_preg[i],
                rob_ref: rob_refs[i],
                valid: (inst_pack_wire[i].illegal == 0)
            };
        end
        `ifdef DEBUG
        $display("[time: %d]\tdecoder rs_input", $time);
        for (int i = 0; i < `N; i++) begin
            $display("rs_input: pc: %h\tvalid: %d\tinst: %p\thas_dest: %d\tfunc: %p\tdest: [arch:%d, phys:%d]\tregA: [arch:%d, phys:%d]\tregB: [arch:%d, phys:%d]\t orig: %d", 
            rs_result[i].inst.pc, 
            rs_result[i].valid, 
            rs_result[i].inst.inst,
            inst_pack_wire[i].has_dest,
            rs_result[i].inst.func,
            rs_result[i].inst.inst.r.rd, rs_result[i].dest_reg,
            rs_result[i].inst.inst.r.rs1, rs_result[i].operands_status[0].phy_reg_idx,
            rs_result[i].inst.inst.r.rs2, rs_result[i].operands_status[1].phy_reg_idx,
            inst_pack_wire[i].inst.r.rd
            );
        end
        `endif
    end
`ifdef DEBUG
    /* Debug */
    always_comb begin
        dbg_out.fl_free_reg_out = fl_free_reg;
        dbg_out.fl_reg_needed = fl_reg_needed;
        dbg_out.intern_decoded_inst = inst_pack_wire;
        dbg_out.fl_dbg = fl_dbg;
    end
`endif

endmodule

// Coming from stage_id provided in p3
// 1. Removed csr_op as it does nothing
// 2. Improved the interface
//
// Decode an instruction: generate useful datapath control signals by matching the RISC-V ISA
// This module is purely combinational
module _intern_decoder (
    input FETCH_RES inst_in,
    output DECODED_INST result
);
    FU_T fu;
    ALU_OPA_SELECT opa_select;
    ALU_OPB_SELECT opb_select;
    FUNC func;
    logic has_dest;
    logic illegal;
    logic has_reg1;
    logic has_reg2;
    logic is_ld_signed;

    always_comb begin
        // $display("instruction in decoder: %p",inst_in);
        // if (!inst_in.valid || illegal) begin
        //     result = NOOP_DECODED_INST;
        //     `ifdef __DC_TRACE $display("invalid/ illegal, %p", inst_in.inst); `endif
        // end else begin
            result = { 
                pc: inst_in.pc,
                npc: inst_in.branch_prediction.target,// change to the predicted target
                inst: inst_in.inst,
                fu: fu,
                opa_select: opa_select,
                opb_select: opb_select,
                func: func,
                has_dest: has_dest,
                illegal: (inst_in.valid == 0) | illegal,
                branch_prediction: inst_in.branch_prediction,
                has_reg1: has_reg1,
                has_reg2: has_reg2,
                is_ld_signed: is_ld_signed
            };
        // end
        `ifdef DEBUG
        $display("timeL: %d\tpc: %d\thas_dest: %d",$time, inst_in.pc, has_dest);
        `endif
    end
    
    always_comb begin
        casez (inst_in.inst) 
             `RV32_LB, `RV32_LH, `RV32_LW: begin
                is_ld_signed = 1;
             end
             default: begin
                is_ld_signed = 0;
             end
        endcase
    end

    // Note: I recommend using an IDE's code folding feature on this block
    always_comb begin
        // Default control values (looks like a NOP)
        fu = ALU;
        opa_select    = OPA_IS_RS1;
        opb_select    = OPB_IS_RS2;
        func          = NOOP;
        has_dest      = `FALSE;
        illegal       = `FALSE;

        has_reg1      = `FALSE;
        has_reg2      = `FALSE;

        fu = ALU;
        casez (inst_in.inst)
            `RV32_LUI: begin
                has_dest   = `TRUE;
                opa_select = OPA_IS_ZERO;
                opb_select = OPB_IS_U_IMM;
                func = ALU_ADD;
            end
            `RV32_AUIPC: begin
                has_dest   = `TRUE;
                opa_select = OPA_IS_PC;
                opb_select = OPB_IS_U_IMM;
                func = ALU_ADD;
            end
            `RV32_JAL: begin
                has_dest      = `TRUE;
                opa_select    = OPA_IS_PC;
                opb_select    = OPB_IS_J_IMM;
                func          = BRANCH_UNCOND;
            end
            `RV32_JALR: begin
                has_dest      = `TRUE;
                opa_select    = OPA_IS_RS1;
                opb_select    = OPB_IS_I_IMM;
                func          = BRANCH_UNCOND;

                has_reg1      = `TRUE;
            end
            `RV32_BEQ, `RV32_BNE, `RV32_BLT, `RV32_BGE,
            `RV32_BLTU, `RV32_BGEU: begin
                opa_select  = OPA_IS_PC;
                opb_select  = OPB_IS_B_IMM;
                func        = BRANCH_COND;
                has_dest      = `FALSE;
                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_LB, `RV32_LH, `RV32_LW,
            `RV32_LBU, `RV32_LHU: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = MEM_RD;
                fu = MEM;

                has_reg1      = `TRUE;
            end
            `RV32_SB, `RV32_SH, `RV32_SW: begin
                opb_select = OPB_IS_S_IMM;
                func = MEM_WR;
                fu = MEM;
                has_dest      = `FALSE;
                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_ADDI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = ALU_ADD;

                has_reg1      = `TRUE;
            end
            `RV32_SLTI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = ALU_SLT;

                has_reg1      = `TRUE;
            end
            `RV32_SLTIU: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = ALU_SLTU;

                has_reg1      = `TRUE;
            end
            `RV32_ANDI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = ALU_AND;

                has_reg1      = `TRUE;
            end
            `RV32_ORI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = ALU_OR;

                has_reg1      = `TRUE;
            end
            `RV32_XORI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func = ALU_XOR;

                has_reg1      = `TRUE;
            end
            `RV32_SLLI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func   = ALU_SLL;

                has_reg1      = `TRUE;
            end
            `RV32_SRLI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func   = ALU_SRL;

                has_reg1      = `TRUE;
            end
            `RV32_SRAI: begin
                has_dest   = `TRUE;
                opb_select = OPB_IS_I_IMM;
                func   = ALU_SRA;

                has_reg1      = `TRUE;
            end
            `RV32_ADD: begin
                has_dest   = `TRUE;
                func = ALU_ADD;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_SUB: begin
                has_dest   = `TRUE;
                func   = ALU_SUB;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_SLT: begin
                has_dest   = `TRUE;
                func   = ALU_SLT;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_SLTU: begin
                has_dest   = `TRUE;
                func   = ALU_SLTU;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_AND: begin
                has_dest   = `TRUE;
                func   = ALU_AND;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_OR: begin
                has_dest   = `TRUE;
                func   = ALU_OR;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_XOR: begin
                has_dest   = `TRUE;
                func   = ALU_XOR;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_SLL: begin
                has_dest   = `TRUE;
                func   = ALU_SLL;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_SRL: begin
                has_dest   = `TRUE;
                func   = ALU_SRL;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_SRA: begin
                has_dest   = `TRUE;
                func   = ALU_SRA;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_MUL: begin
                has_dest   = `TRUE;
                fu = MULT;
                func   = MULT_MUL;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_MULH: begin
                has_dest   = `TRUE;
                fu = MULT;
                func   = MULT_MULH;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_MULHSU: begin
                has_dest   = `TRUE;
                fu = MULT;
                func   = MULT_MULHSU;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            `RV32_MULHU: begin
                has_dest   = `TRUE;
                fu = MULT;
                func   = MULT_MULHU;

                has_reg1      = `TRUE;
                has_reg2      = `TRUE;
            end
            // `RV32_CSRRW, `RV32_CSRRS, `RV32_CSRRC: begin
            //     csr_op = `TRUE;
            // end
            `WFI: begin
                fu = ALU;
                func = HALT;
            end
            default: begin
                illegal = `TRUE;
            end
        endcase // casez (inst)
    end // always

endmodule // decoder
