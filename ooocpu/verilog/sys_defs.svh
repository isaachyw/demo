/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  sys_defs.svh                                        //
//                                                                     //
//  Description :  This file has the macro-defines for macros used in  //
//                 the pipeline design.                                //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`ifndef __SYS_DEFS_SVH__
`define __SYS_DEFS_SVH__

// `define DEBUG
// all files should `include "sys_defs.svh" to at least define the timescale
// `define SYN_DEBUG
`timescale 1ns / 100ps

///////////////////////////////////
// ---- Starting Parameters ---- //
///////////////////////////////////

// some starting parameters that you should set
// this is *your* processor, you decide these values (try analyzing which is best!)

// debug
// NOTE: TO DISABLE DEBUG, COMMENT OUT THIS LINE INSTEAD OF SETTING IT TO 0
// `define DEBUG

// superscalar width, 
// Note: we are only doing 3-way scalar for this project.
`define N 2

// sizes
`define ROB_SZ 8
`define REG_PORT_WIDTH `RS_SZ

`define ARCH_REG_SZ 32
`define CDB_W `N
`define RS_SZ 4 // make sure they are same, num inst t not resolved yet.
`define PHYS_REG_SZ `ARCH_REG_SZ+`ROB_SZ

// worry about these later
// `define BRANCH_PRED_SZ xx
// `define LSQ_SZ xx

// functional units (you should decide if you want more or fewer types of FUs)
`define NUM_FU_ALU `N
`define NUM_FU_MULT 1
// `define NUM_FU_LOAD xx
// `define NUM_FU_STORE xx

// number of mult stages (2, 4, or 8)
`define MULT_STAGES 4

///////////////////////////////
// ---- Basic Constants ---- //
///////////////////////////////

// NOTE: the global CLOCK_PERIOD is defined in the Makefile

// useful boolean single-bit definitions
`define FALSE 1'h0
`define TRUE 1'h1
`define TRUE 1'h1

// data length
`define XLEN 32

// the zero register
// In RISC-V, any read of this register returns zero and any writes are thrown away
`define ZERO_REG 5'd0

// Basic NOP instruction. Allows pipline registers to clearly be reset with
// an instruction that does nothing instead of Zero which is really an ADDI x0, x0, 0
`define NOP 32'h00000013

//////////////////////////////////
// ---- Memory Definitions ---- //
//////////////////////////////////

// Cache mode removes the byte-level interface from memory, so it always returns
// a double word. The original processor won't work with this defined. Your new
// processor will have to account for this effect on mem.
// Notably, you can no longer write data without first reading.
`define CACHE_MODE

// you are not allowed to change this definition for your final processor
// the project 3 processor has a massive boost in performance just from having no mem latency
// see if you can beat it's CPI in project 4 even with a 100ns latency!
// `define MEM_LATENCY_IN_CYCLES  0
`define MEM_LATENCY_IN_CYCLES (100.0/`CLOCK_PERIOD+0.49999)
// the 0.49999 is to force ceiling(100/period). The default behavior for
// float to integer conversion is rounding to nearest

// How many memory requests can be waiting at once
`define NUM_MEM_TAGS 15

`define MEM_SIZE_IN_BYTES (64*1024)
`define MEM_64BIT_LINES (`MEM_SIZE_IN_BYTES/8)




//------------------------------self-defined---------------------------------//



typedef logic [$clog2(`ARCH_REG_SZ):0] ARCH_REG_T;
typedef logic [$clog2(`PHYS_REG_SZ):0] PHYS_REG_T;
typedef logic [`XLEN-1:0] REG_DATA_T;

`define DC_SZ 256
`define DC_BLK_SZ 8

`define DC_N_BLKS 32
`define DC_TAG_LEN 29
`define DC_IDX_LEN 5

// Uniform memory request/ response structs
typedef enum logic { MEM_READ, MEM_WRITE } MEM_OP_T;
typedef logic [`XLEN-1:0] MEM_ADDR_T;

typedef union packed {
  logic [7:0][7:0]  byte_level;
  logic [3:0][15:0] half_level;
  logic [1:0][31:0] word_level;
} EXAMPLE_CACHE_BLOCK;

// Stable integer assignment is needed
typedef enum logic [1:0] {
  BYTE   = 2'h0,
  HALF   = 2'h1,
  WORD   = 2'h2
} MEM_SIZE;

// Memory bus commands
typedef enum logic [1:0] {
  BUS_NONE  = 2'h0,
  BUS_LOAD  = 2'h1,
  BUS_STORE = 2'h2
} BUS_COMMAND;

typedef struct packed {
    EXAMPLE_CACHE_BLOCK block;
    logic valid;
    logic dirty;
    logic [`DC_TAG_LEN-1:0] tag;
} CACHE_LINE;

typedef enum logic [2:0] {
    DC_READY,
    DC_WAITING_WB_RES,
    DC_WAITING_RD_ACK,
    DC_WAITING_RD_RES,
    DC_WAITING_CLEANING_RES
} DCACHE_WAITING_STAGE;

typedef enum logic [1:0] {
    DC_OUTPUT_NONE,
    DC_OUTPUT_WB,
    DC_OUTPUT_LD_NEW,
    DC_OUTPUT_CLEANING
} DCACHE_OUTPUT_TYPE;

///////////////////////////////
// ---- Exception Codes ---- //
///////////////////////////////

/**
 * Exception codes for when something goes wrong in the processor.
 * Note that we use HALTED_ON_WFI to signify the end of computation.
 * It's original meaning is to 'Wait For an Interrupt', but we generally
 * ignore interrupts in 470
 *
 * This mostly follows the RISC-V Privileged spec
 * except a few add-ons for our infrastructure
 * The majority of them won't be used, but it's good to know what they are
 */

typedef enum logic [3:0] {
  INST_ADDR_MISALIGN  = 4'h0,
  INST_ACCESS_FAULT   = 4'h1,
  ILLEGAL_INST        = 4'h2,
  BREAKPOINT          = 4'h3,
  LOAD_ADDR_MISALIGN  = 4'h4,
  LOAD_ACCESS_FAULT   = 4'h5,
  STORE_ADDR_MISALIGN = 4'h6,
  STORE_ACCESS_FAULT  = 4'h7,
  ECALL_U_MODE        = 4'h8,
  ECALL_S_MODE        = 4'h9,
  NO_ERROR            = 4'ha,  // a reserved code that we use to signal no errors
  ECALL_M_MODE        = 4'hb,
  INST_PAGE_FAULT     = 4'hc,
  LOAD_PAGE_FAULT     = 4'hd,
  HALTED_ON_WFI       = 4'he,  // 'Wait For Interrupt'. In 470, signifies the end of computation
  STORE_PAGE_FAULT    = 4'hf
} EXCEPTION_CODE;

///////////////////////////////////
// ---- Instruction Typedef ---- //
///////////////////////////////////

// from the RISC-V ISA spec
typedef union packed {
  logic [31:0] inst;
  struct packed {
    logic [6:0] funct7;
    logic [4:0] rs2;  // source register 2
    logic [4:0] rs1;  // source register 1
    logic [2:0] funct3;
    logic [4:0] rd;  // destination register
    logic [6:0] opcode;
  } r;  // register-to-register instructions
  struct packed {
    logic [11:0] imm;  // immediate value for calculating address
    logic [4:0] rs1;  // source register 1 (used as address base)
    logic [2:0] funct3;
    logic [4:0] rd;  // destination register
    logic [6:0] opcode;
  } i;  // immediate or load instructions
  struct packed {
    logic [6:0] off;  // offset[11:5] for calculating address
    logic [4:0] rs2;  // source register 2
    logic [4:0] rs1;  // source register 1 (used as address base)
    logic [2:0] funct3;
    logic [4:0] offset;  // offset[4:0] for calculating address
    logic [6:0] opcode;
  } s;  // store instructions
  struct packed {
    logic       of;      // offset[12]
    logic [5:0] s;       // offset[10:5]
    logic [4:0] rs2;     // source register 2
    logic [4:0] rs1;     // source register 1
    logic [2:0] funct3;
    logic [3:0] et;      // offset[4:1]
    logic       f;       // offset[11]
    logic [6:0] opcode;
  } b;  // branch instructions
  struct packed {
    logic [19:0] imm;  // immediate value
    logic [4:0] rd;  // destination register
    logic [6:0] opcode;
  } u;  // upper-immediate instructions
  struct packed {
    logic       of;      // offset[20]
    logic [9:0] et;      // offset[10:1]
    logic       s;       // offset[11]
    logic [7:0] f;       // offset[19:12]
    logic [4:0] rd;      // destination register
    logic [6:0] opcode;
  } j;  // jump instructions

  // extensions for other instruction types
`ifdef ATOMIC_EXT
  struct packed {
    logic [4:0] funct5;
    logic       aq;
    logic       rl;
    logic [4:0] rs2;
    logic [4:0] rs1;
    logic [2:0] funct3;
    logic [4:0] rd;
    logic [6:0] opcode;
  } a;  // atomic instructions
`endif
`ifdef SYSTEM_EXT
  struct packed {
    logic [11:0] csr;
    logic [4:0]  rs1;
    logic [2:0]  funct3;
    logic [4:0]  rd;
    logic [6:0]  opcode;
  } sys;  // system call instructions
`endif

} INST;  // instruction typedef, this should cover all types of instructions

////////////////////////////////////////
// ---- Datapath Control Signals ---- //
////////////////////////////////////////

// The common representation of number of instruction in the pipeline
//
// ROB_SZ is used here as thats the most amount of the instruction could be in the pipeline,
// one more bit is used for overflow.
typedef logic [$clog2(`ROB_SZ):0] NUM_INST_T;
typedef logic [$clog2(`RS_SZ):0] NUM_INST_T_RS;
// fetch unit

typedef logic [`XLEN-1:0] PC_T;


typedef enum logic { TAKEN = 1'd1, NOT_TAKEN = 1'd0 } BRANCH_DIRECTION_T;


typedef struct packed {
  BRANCH_DIRECTION_T direction;
  PC_T target;
  logic valid;
} BRANCH_OUTCOME;               // TODO: pipeline todo, write pc to pipline

typedef struct packed {
  // if prediction is correct
  // Where the instruction is located
  PC_T  location;
  PC_T  npc;//predicted target
  // Branch outcomes:
  BRANCH_OUTCOME reality;
  logic predict;

  logic valid;
} BRANCH_FEEDBACK;

typedef struct packed {
  PC_T pc;
  INST inst;
  BRANCH_OUTCOME branch_prediction;
  logic valid;
} FETCH_RES;

// Instruction Buffer
`define INST_BUFFER_SZ `ROB_SZ

// Decoder:

// ALU opA input mux selects
typedef enum logic [1:0] {
  OPA_IS_RS1  = 2'h0,
  OPA_IS_NPC  = 2'h1,
  OPA_IS_PC   = 2'h2,
  OPA_IS_ZERO = 2'h3
} ALU_OPA_SELECT;

// ALU opB input mux selects
typedef enum logic [3:0] {
  OPB_IS_RS2   = 4'h0,
  OPB_IS_I_IMM = 4'h1,
  OPB_IS_S_IMM = 4'h2,
  OPB_IS_B_IMM = 4'h3,
  OPB_IS_U_IMM = 4'h4,
  OPB_IS_J_IMM = 4'h5
} ALU_OPB_SELECT;

// ALU function code input
typedef enum {
  ALU_ADD,
  ALU_SUB,
  ALU_SLT,
  ALU_SLTU,
  ALU_AND,
  ALU_OR,
  ALU_XOR,
  ALU_SLL,
  ALU_SRL,
  ALU_SRA,
  MULT_MUL,  // Mult FU
  MULT_MULH,  // Mult FU
  MULT_MULHSU,  // Mult FU
  MULT_MULHU,  // Mult FU
  MEM_RD,
  MEM_WR,
  BRANCH_COND,
  BRANCH_UNCOND,
  HALT,
  NOOP
} FUNC;

// Function unit appropriate to execute this instruction
// NOOP/ Pass through is processed via ALU
typedef enum {
  ALU,
  MULT,
  MEM
} FU_T;

typedef struct packed {
  PC_T pc;
  PC_T npc;
  INST inst;
  FU_T fu;
  ALU_OPA_SELECT opa_select;
  ALU_OPB_SELECT opb_select;
  FUNC func;
  logic has_dest, illegal;    // TODO: figure out what's for
  BRANCH_OUTCOME branch_prediction;
  logic has_reg1;
  logic has_reg2;
  logic is_ld_signed;
} DECODED_INST;


parameter DECODED_INST NOOP_DECODED_INST = {
  `XLEN'd0, `NOP, ALU, OPA_IS_RS1, OPB_IS_RS2, NOOP, `TRUE, `FALSE
};

// Map Table:
typedef struct packed {
  PHYS_REG_T  phy_reg_idx;  // read: input architectural reg idx to find; write: physical reg idx to modify
  ARCH_REG_T arch_reg_idx;
  logic ready;
  logic valid;
} MAP_TABLE_WRITE_COMMAND;


typedef struct packed {
  ARCH_REG_T arch_reg_idx;
  logic      valid;
} MAP_TABLE_READ_COMMAND;

typedef struct packed {
  PHYS_REG_T phy_reg_idx;
  logic      ready;
  logic      valid;
} MAP_TABLE_ENTRY;


typedef struct packed {
  PHYS_REG_T phy_reg_idx;
  logic      valid;
} CDB_ENTRY;

// ROB:

typedef struct packed {
  logic [$clog2(`ROB_SZ):0] rob_index;
  logic                       valid;
} ROB_REF_T;

// RS

typedef struct packed {
  DECODED_INST inst;
  MAP_TABLE_ENTRY [1:0] operands_status;
  PHYS_REG_T dest_reg;
  ROB_REF_T rob_ref;
  logic valid;
} RS_INPUT;

typedef struct packed {
  logic valid;
  logic [$clog2(`RS_SZ):0] rs_idx;
} RS_REF_T;

typedef RS_INPUT RS_ENTRY;
typedef struct packed {
  logic has_input;
  logic [`XLEN -1:0] phy_reg_val_a;
  logic [`XLEN -1:0] phy_reg_val_b;
  ALU_OPA_SELECT opa_select;
  ALU_OPB_SELECT opb_select;
  INST inst;
  PC_T pc;
  PC_T npc;
  FUNC func;
  logic next_has_dest;
  ROB_REF_T next_rob_idx;
  PHYS_REG_T next_dst_reg_idx;
  logic predict;
  `ifdef  DEBUG
  RS_ENTRY rs_entry;
  `endif 
} ALU_INPUT;

// MEM
typedef struct packed {
  logic valid;
  logic [`XLEN -1:0] phy_reg_val_a;
  logic [`XLEN -1:0] phy_reg_val_b;
  // ALU_OPA_SELECT opa_select;
  ALU_OPB_SELECT opb_select;
  INST inst;
  PC_T pc;
  // PC_T npc;
  FUNC func;
  logic has_dest;
  ROB_REF_T rob_idx;
  PHYS_REG_T dst_reg_idx;
  RS_ENTRY rs_entry;
} MEM_INPUT;

typedef struct packed {
  logic [`XLEN-1:0] result;
  logic             valid;
  logic             has_dest;
  BRANCH_FEEDBACK    br_feedback;
  ROB_REF_T         rob_idx;
  PHYS_REG_T        dst_reg_idx;
  `ifdef DEBUG
  RS_ENTRY rs_entry;
  `endif
} ALU_OUTPUT;

typedef struct packed {
  logic [`XLEN-1:0] opa;
  logic [`XLEN-1:0] opb;
  logic start;  // i.e. "valid" input
  FUNC func;
  ROB_REF_T next_rob_idx;
  PHYS_REG_T next_dst_reg_idx;

  `ifdef DEBUG
  RS_ENTRY rs_entry;
  `endif
} MULT_INPUT;

typedef struct packed {
  logic [`XLEN-1:0] result;
  logic done;
  ROB_REF_T rob_idx;
  PHYS_REG_T dst_reg_idx;
  `ifdef DEBUG
  RS_ENTRY rs_entry;
  `endif 
  
} MULT_OUTPUT;

typedef struct packed {
  ROB_REF_T rob_idx;
  PHYS_REG_T dst_reg_idx;
  FUNC func;
  `ifdef DEBUG
  RS_ENTRY rs_entry;
  `endif
} MULT_STAGE_PASS;


// ROB:
typedef struct packed {
  DECODED_INST inst;
  PHYS_REG_T newT;
  PHYS_REG_T oldT;
  ARCH_REG_T arch_reg_idx;  // for rollback
  logic valid;
} ROB_PACKET;

typedef struct packed {
    logic valid;
    logic [$clog2(`ROB_SZ):0] rob_index;
} RETIRED_INST_T; // used inside ROB





// FREE LIST:

`define FREE_LIST_SIZE `ROB_SZ
typedef CDB_ENTRY FREE_LIST_ENTRY;



// Reg file:

typedef struct packed {
  PHYS_REG_T reg_ref;
  logic valid;
} REG_READ_REQ;

typedef struct packed {
  REG_DATA_T data;
  logic valid;
} REG_READ_RESPONSE;

typedef struct packed {
  PHYS_REG_T reg_ref;
  REG_DATA_T data;
  logic valid;
} REG_WRITE_REQ;

// Internal structures, please keep private to the invidual components:

// ROB
typedef struct packed {
  DECODED_INST inst;
  BRANCH_OUTCOME branch_outcome;
  PC_T hazard_target;
  PHYS_REG_T   newT;
  PHYS_REG_T   oldT;
  ARCH_REG_T arch_reg_idx;  // for rollback
  logic [$clog2(`ROB_SZ):0] rob_index; // original is ROB_REF_T, we don't need valid in that struct (also backward compatibility)
  logic        valid;
  logic        has_executed;
  logic        has_completed;  // can retire
} ROB_ENTRY;

`ifdef DEBUG
typedef struct packed {
  ROB_ENTRY [`ROB_SZ-1:0] ROB_content_out;
  logic [$clog2(`ROB_SZ):0] head_out;
  logic [$clog2(`ROB_SZ):0] tail_out;
  ROB_ENTRY [`ROB_SZ-1:0] next_rob_entry_out;
  logic [$clog2(`ROB_SZ):0] available_entry_reg_out;
  logic [$clog2(`ROB_SZ):0] next_head_out;
  logic [$clog2(`ROB_SZ):0] next_tail_out;
  logic signed [$clog2(`N)+1:0] tail_offset_out;
  logic [$clog2(`ROB_SZ):0] rollback_branch_index_reg_out;
  logic                     rollback_enabled_reg_out;
  logic                     next_rollback_enabled_wire_out;
  logic [$clog2(`ROB_SZ):0] rollback_branch_index_reg;
  logic hazard_detected;
  logic signed [$clog2(`ROB_SZ)+1:0] tail_offset_rollback;
} DEBUG_ROB;
`endif



// Load Store Queue

typedef struct packed {
  logic [$clog2(`ROB_SZ):0] rob_idx;
  logic valid;
  MEM_OP_T mem_op;
  MEM_SIZE size;
  logic is_ld_signed;
} LSQ_REQUEST_BASE;

typedef struct packed {
  LSQ_REQUEST_BASE basic_info;
  logic [`XLEN:0] memory_addr; // intentionally added by 1 to prevent overflow in lsq forwarding
  REG_DATA_T data; // only valid when it's a write request
  PHYS_REG_T dst_reg_idx; // TODO for LSQ
  PC_T pc;
} LSQ_ISSUE_REQUEST;

typedef struct packed {
  LSQ_ISSUE_REQUEST request;
  logic issue_success;
  REG_DATA_T read_data;
  logic read_ready;
  logic [3:0] forwarded_bytes;
  logic completed;  // TODO for LSQ
} LSQ_ENTRY;


// Note: 9 is chosen arbitrarily, it should be larger or equals to
// $clog(`LSQ_SZ)-1 and the number of rows in the cache
typedef logic [9:0] MEM_REF_T;

typedef struct packed {
  MEM_OP_T mem_op;
  MEM_ADDR_T addr;
  MEM_SIZE size;
  REG_DATA_T write_content;
  logic valid;
  PC_T pc;
} DCACHE_REQUEST;

typedef struct packed {
    REG_DATA_T reg_data;
    logic valid;
} DCACHE_RESPONSE;


typedef struct packed {
  MEM_OP_T mem_op;
  // Addr for operation
  MEM_ADDR_T addr;
  // Data to write, undefined on read
  REG_DATA_T write_content;
  logic valid;
} MEM_REQ;

typedef struct packed {
  MEM_REF_T mem_ref;
  logic valid;
} MEM_REQ_ACK;

typedef struct packed {
  // Reference for which instruction is completed
  MEM_REF_T mem_ref;
  // Undefined for write operations
  REG_DATA_T read_result;
  logic valid;
} MEM_RES;

// DCache
// `define DCACHE_W 2

// Branch Predictor


`define BHT_SZ 16
`define BHT_W $clog2(`BHT_SZ)
`define BHT_TAG_W `XLEN-`BHT_W-2

`define BTB_SET 16
`define BTB_TAG_W 8 // offset and tag offset, btb don't need exact tag match, be aware idx+tagw
// should not exceed xlen
// `define BTB_TARGET_W 16
`define BTB_IDX_W $clog2(`BTB_SET)

typedef struct packed {
  // logic valid; don't need valid bit ? benchmark to prove
  logic [`BTB_TAG_W-1:0] tag;
  PC_T target;
} BTB_ENTRY;

typedef struct packed {
  logic valid;
  PC_T pc;
} BP_QUERY;


typedef BRANCH_OUTCOME BP_OUTPUT;

typedef struct packed {
  BRANCH_OUTCOME branch_outcome;
  PC_T pc;
  logic valid;
} BP_UPDATE;

typedef enum logic[1:0]{
  STRONG_NT = 2'h0,
  WEAK_NT = 2'h1,
  WEAK_T = 2'h2,
  STRONG_T = 2'h3
} BP_STATE;

`ifdef DEBUG
// Debug structs:

typedef struct packed {
  // To be determined
  logic placeholder;
} DEBUG_FETCH;

typedef struct packed {
  // To be determined
  logic placeholder;
    PHYS_REG_T [(`FREE_LIST_SIZE-1):0] data_out;
    logic [$clog2(`FREE_LIST_SIZE):0] stack_ptr_out;
} DEBUG_FREE_LIST;

typedef struct packed {
    FREE_LIST_ENTRY [`N-1:0] fl_free_reg_out;
    logic [$clog2(`N):0] fl_reg_needed;
    DEBUG_FREE_LIST fl_dbg;

    DECODED_INST [`N-1:0] intern_decoded_inst;
} DEBUG_DECODER;

typedef struct packed {
  // To be determined
  logic placeholder;
} DEBUG_RS;

typedef struct packed {
  // To be determined
  logic placeholder;
} DEBUG_FU_MANAGER;

typedef struct packed {
    MAP_TABLE_ENTRY[`ARCH_REG_SZ-1:0] map_table;
} DEBUG_MAP_TABLE;

typedef struct packed {
  // To be determined
  logic placeholder;
} DEBUG_REG_FILE;

typedef struct packed {
  // To be determined
  logic placeholder;
} DEBUG_DCACHE;

`endif



`endif // __SYS_DEFS_SVH__
