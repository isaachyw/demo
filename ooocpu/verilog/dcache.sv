`include "verilog/sys_defs.svh"

// Key assumptions to user:

// The user should keep sending the request until receiving
// a dcache_responses with the valid bit high

// In the new design, on a hit, the result would no longer
// be forwarded in the current cycle

// Key assumption to using cleaning up:

// The start_cleaning_up input must be maintained high,
// untill the dcache_clean output turns high

// Understanding stall bit:

// On a hit, the stall would never be high

// On a miss, the stall bit would remain low in the current
// cycle, remain high starting the next cycle, and change
// to low the next cycle that a valid response is output

// Understanding the internal forwarding:

// Once a miss is detected, the req to mem starts at the
// current cycle immediatley

// Once we receive the data from mem, we read / write the
// received data and output a valid response at the current
// cycle immediatley

// Cache policies:

// One bank, one W/R ports
// Fully associative
// Write back
// Allocate on write
// evict: empty line, then XOR PC
// Looks like we never use a double

// In bytes: (eight bits, two hex digits)

module dcache (
    input        clock,
    input        reset,
    input  PC_T  pc,
    output logic stall,

    output CACHE_LINE [`DC_N_BLKS-1:0] dcache_contents,

    input  DCACHE_REQUEST  dcache_requests,
    output DCACHE_RESPONSE dcache_responses,

    // From memory
    input [3:0]  Dmem2proc_response, // Should be zero unless there is a response
    input [63:0] Dmem2proc_data,
    input [3:0]  Dmem2proc_tag,

    // To memory
    output logic [1:0]  proc2Dmem_command,
    output logic [`XLEN-1:0] proc2Dmem_addr,
    output logic [63:0] proc2Dmem_data,

    input  start_cleaning_up,
    output dcache_clean

    `ifdef DEBUG
    , DEBUG_DCACHE dbg
    , CACHE_LINE [`DC_N_BLKS-1:0] cache_lines_debug
    `endif
);

    // Registers:
    CACHE_LINE [`DC_N_BLKS-1:0] cache_lines;
    DCACHE_WAITING_STAGE internal_stage;
    logic [3:0] current_ld_new_mem_tag;

    // Register peripherals:
    CACHE_LINE [`DC_N_BLKS-1:0] next_cache_lines;
    DCACHE_WAITING_STAGE next_internal_stage;
    logic [3:0] next_current_ld_new_mem_tag;
    DCACHE_RESPONSE next_dcache_responses;

    // Combinationals:
    logic hit;
    logic need_ld_new;
    logic empty_line_exist;
    logic dirty_line_exist;
    logic new_data_ready;
    logic need_wb_dirty;
    logic this_cycle_write;
    logic this_cycle_read;
    logic this_cycle_clean_dirty_tag;
    logic [`XLEN-1:0] aligned_wb_addr;
    logic [`XLEN-1:0] aligned_ld_new_addr;
    logic [`DC_TAG_LEN-1:0] input_tag;
    logic [$clog2(`DC_N_BLKS)-1:0] hit_idx;
    logic [$clog2(`DC_N_BLKS)-1:0] empty_line_idx;
    logic [$clog2(`DC_N_BLKS)-1:0] first_dirty_idx;
    logic [$clog2(`DC_N_BLKS)-1:0] random_evict_idx;
    logic [$clog2(`DC_N_BLKS)-1:0] dc_comb_to_operate_idx;
    logic [$clog2(`DC_N_BLKS)-1:0] dc_comb_to_update_idx;
    DCACHE_OUTPUT_TYPE output_selector;

    // DEBUG:
    assign cache_lines_debug = cache_lines;

    `ifdef DEBUG
    always_comb begin : debug_block
        $display("time: %d | dc_comb_to_update_idx: %d | evict_idx: %d", $time, dc_comb_to_update_idx, random_evict_idx);
    end
    `endif

    assign dcache_contents        = cache_lines;

    assign stall                  = (internal_stage != DC_READY);
    assign dcache_clean           = ~dirty_line_exist;
    assign input_tag              = dcache_requests.addr[31:3];
    assign need_ld_new            = (dcache_requests.valid) & (~hit);
    assign need_wb_dirty          = need_ld_new & (cache_lines[dc_comb_to_update_idx].valid & cache_lines[dc_comb_to_update_idx].dirty);
    assign this_cycle_read        = (hit | new_data_ready) & (dcache_requests.mem_op == MEM_READ);
    assign this_cycle_write       = (hit | new_data_ready) & (dcache_requests.mem_op == MEM_WRITE);
    assign aligned_wb_addr        = {cache_lines[dc_comb_to_update_idx].tag, 3'b000};
    assign aligned_ld_new_addr    = {dcache_requests.addr[31:3], 3'b000};
    assign random_evict_idx       = dcache_requests.addr[7:4] ^ pc[6:3];
    assign dc_comb_to_update_idx  = empty_line_exist ? empty_line_idx : random_evict_idx;
    assign dc_comb_to_operate_idx = (internal_stage == DC_READY) ? hit_idx : dc_comb_to_update_idx;

    always_comb begin
        case (output_selector)
            DC_OUTPUT_NONE: begin
                proc2Dmem_command = BUS_NONE;
                proc2Dmem_addr    = 0;
                proc2Dmem_data    = 0;
            end
            DC_OUTPUT_WB: begin
                proc2Dmem_command = BUS_STORE;
                proc2Dmem_addr    = aligned_wb_addr;
                proc2Dmem_data    = cache_lines[dc_comb_to_update_idx].block;
            end
            DC_OUTPUT_LD_NEW: begin
                proc2Dmem_command = BUS_LOAD;
                proc2Dmem_addr    = aligned_ld_new_addr;
                proc2Dmem_data    = 0;
            end
            DC_OUTPUT_CLEANING: begin
                proc2Dmem_command = BUS_STORE;
                // TODO: Use next_* for forwarding, check understanding
                proc2Dmem_addr    = {cache_lines[first_dirty_idx].tag, 3'b000};
                proc2Dmem_data    = cache_lines[first_dirty_idx].block;
            end
            default: begin
                proc2Dmem_command = BUS_NONE;
                proc2Dmem_addr    = 0;
                proc2Dmem_data    = 0;
            end
        endcase
    end

    always_comb begin
        // TODO: Do we want to avoid glitch? maybe no?
        next_current_ld_new_mem_tag = current_ld_new_mem_tag;
        new_data_ready = 0;
        this_cycle_clean_dirty_tag = 0;
        case (internal_stage)
            DC_READY: begin
                next_internal_stage = DC_READY;
                output_selector = DC_OUTPUT_NONE;
                if (start_cleaning_up) begin
                    next_internal_stage = DC_WAITING_CLEANING_RES;
                    output_selector = DC_OUTPUT_CLEANING;
                end else begin
                    if (need_wb_dirty) begin
                        next_internal_stage = DC_WAITING_WB_RES;
                        output_selector = DC_OUTPUT_WB;
                    end else if (need_ld_new) begin
                        next_internal_stage = DC_WAITING_RD_ACK;
                        output_selector = DC_OUTPUT_LD_NEW;
                    end
                end
            end
            DC_WAITING_WB_RES: begin
                next_internal_stage = DC_WAITING_WB_RES;
                output_selector = DC_OUTPUT_WB;
                // On a non zero response, output rd new
                if (Dmem2proc_response != 0) begin
                    next_internal_stage = DC_WAITING_RD_ACK;
                    output_selector = DC_OUTPUT_LD_NEW;
                end
            end
            DC_WAITING_RD_ACK: begin
                // output rd new
                next_internal_stage = DC_WAITING_RD_ACK;
                output_selector = DC_OUTPUT_LD_NEW;
                if (Dmem2proc_response != 0) begin
                    next_internal_stage = DC_WAITING_RD_RES;
                    // output_selector = DC_OUTPUT_NONE;
                    next_current_ld_new_mem_tag = Dmem2proc_response;
                end
            end
            DC_WAITING_RD_RES: begin
                assert(current_ld_new_mem_tag != 0) else $display("Error: waiting on an empty mem tag");
                // no need to output rd new
                next_internal_stage = DC_WAITING_RD_RES;
                output_selector = DC_OUTPUT_NONE;
                // On a feed, change next to ready
                if (current_ld_new_mem_tag == Dmem2proc_tag) begin
                    next_internal_stage = DC_READY;
                    output_selector = DC_OUTPUT_NONE;
                    next_current_ld_new_mem_tag = 0;    // Empty the holding tag upon receiving
                    new_data_ready = 1;
                end
            end
            DC_WAITING_CLEANING_RES: begin
                // TODO: check understanding for this stage
                next_internal_stage = DC_WAITING_CLEANING_RES;
                output_selector = DC_OUTPUT_CLEANING;
                // On a non zero response, clean the dirty tag for the current one
                if (Dmem2proc_response != 0) begin
                    next_internal_stage = DC_READY;
                    output_selector = DC_OUTPUT_NONE;
                    this_cycle_clean_dirty_tag = 1;
                end
            end
            default: begin
                // TODO: how to get rid of this?
                next_internal_stage = DC_READY;
                output_selector = DC_OUTPUT_NONE;
            end
        endcase
    end

    always_comb begin
        hit = '0;
        hit_idx = '0;
        if (~stall & dcache_requests.valid) begin
            for (int j = 0; j < `DC_N_BLKS; j++) begin
                if (cache_lines[j].valid & input_tag == cache_lines[j].tag) begin
                    hit = 1;
                    hit_idx = j;
                    break;
                end
            end
        end
    end

    always_comb begin
        empty_line_exist = '0;
        empty_line_idx = '0;
        dirty_line_exist = '0;
        first_dirty_idx = '0;
        for (int j = `DC_N_BLKS - 1; j >= 0; j--) begin
            if (!cache_lines[j].valid) begin
                empty_line_exist = 1;
                empty_line_idx = j;
            end
            if (cache_lines[j].dirty) begin
                dirty_line_exist = 1;
                first_dirty_idx = j;
            end
        end
    end

    CACHE_LINE [`DC_N_BLKS-1:0] new_cache_lines;
    logic has_new_lines;

    always_comb begin
        has_new_lines = 0;
        new_cache_lines = cache_lines;
        if (~start_cleaning_up & new_data_ready) begin
            has_new_lines = 1;
            new_cache_lines[dc_comb_to_update_idx].valid = 1;
            new_cache_lines[dc_comb_to_update_idx].dirty = 0;
            new_cache_lines[dc_comb_to_update_idx].block = Dmem2proc_data;
            new_cache_lines[dc_comb_to_update_idx].tag = dcache_requests.addr[31:3];
        end
    end

    always_comb begin
        next_dcache_responses = '0;
        next_cache_lines = has_new_lines ? new_cache_lines : cache_lines;
        if (this_cycle_read) begin
            next_dcache_responses.valid = 1;
            case (dcache_requests.size)
                BYTE: next_dcache_responses.reg_data[7:0]  = next_cache_lines[dc_comb_to_operate_idx].block.byte_level[dcache_requests.addr[2:0]];
                HALF: next_dcache_responses.reg_data[15:0] = next_cache_lines[dc_comb_to_operate_idx].block.half_level[dcache_requests.addr[2:1]];
                WORD: next_dcache_responses.reg_data[31:0] = next_cache_lines[dc_comb_to_operate_idx].block.word_level[dcache_requests.addr[2:2]];
                default: assert(0) $display("Invalid data size!");
            endcase
        end else if (this_cycle_write) begin
            next_dcache_responses.valid = 1;
            next_cache_lines[dc_comb_to_operate_idx].dirty = 1;
            case (dcache_requests.size)
                BYTE: next_cache_lines[dc_comb_to_operate_idx].block.byte_level[dcache_requests.addr[2:0]] = dcache_requests.write_content[7:0];
                HALF: next_cache_lines[dc_comb_to_operate_idx].block.half_level[dcache_requests.addr[2:1]] = dcache_requests.write_content[15:0];
                WORD: next_cache_lines[dc_comb_to_operate_idx].block.word_level[dcache_requests.addr[2:2]] = dcache_requests.write_content[31:0];
                default: assert(0) $display("Invalid data size!");
            endcase
        end else if (this_cycle_clean_dirty_tag) begin
            // TODO: check understanding
            next_cache_lines[first_dirty_idx].dirty = 0;
        end
    end

    `ifdef DEBUG
    logic signed [`XLEN-1:0] signed_data;
    always_comb begin
        signed_data = dcache_requests.write_content;
    if (dcache_requests.mem_op == MEM_WRITE & dcache_requests.valid) begin
      $display("time: %d ppc: %h write cache addr: %d val: %d dcache_requests: %p", $time, dcache_requests.pc, dcache_requests.addr, signed_data, dcache_requests );
    end

    if (dcache_requests.mem_op == MEM_READ & dcache_requests.valid) begin
      $display("time: %d ppc: %h load cache addr: %d val: %d dcache_requests: %p", $time, dcache_requests.pc, dcache_requests.addr, dcache_requests.write_content, dcache_requests );
    end
    end
    `endif

    always_ff @(posedge clock) begin
        if (reset) begin
            cache_lines             <= '0;
            internal_stage          <= DC_READY;
            current_ld_new_mem_tag  <= '0;
            dcache_responses        <= '0;
        end else begin
            cache_lines             <= next_cache_lines;
            internal_stage          <= next_internal_stage;
            current_ld_new_mem_tag  <= next_current_ld_new_mem_tag;
            dcache_responses        <= next_dcache_responses;
        end
    end

endmodule