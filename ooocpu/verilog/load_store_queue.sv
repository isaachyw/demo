`include "verilog/sys_defs.svh"
`include "verilog/dcache.sv"
// `define ENABLE_LSQ_FORWARD
`define LSQ_FORWARD_SZ `ROB_SZ

module load_store_queue(
    input clock,
    input reset,

    // only used by dcache
    // input PC_T pc, 

    output CACHE_LINE [`DC_N_BLKS-1:0] dcache_contents,

    // From memory (only used by DCache)
    input [3:0]  Dmem2proc_response, // Should be zero unless there is a response
    input [63:0] Dmem2proc_data,
    input [3:0]  Dmem2proc_tag,

    // To memory  (only used by DCache)
    output logic [1:0]  proc2Dmem_command,
    output logic [`XLEN-1:0] proc2Dmem_addr,
    output logic [63:0] proc2Dmem_data,

    // Dispatch: Reserve Spots for load and store
    input LSQ_REQUEST_BASE [`N - 1 : 0] dispatch_request,

    // Issue / Execute: Issue load (possible reject: reflect in output as not issue_success) and store (guarenteed by LSQ to success)
    input LSQ_ISSUE_REQUEST [`N - 1 : 0] issue_request,
    // output logic [`N - 1 : 0] issue_approved, // return in-cycle issue success (combinational logic)

    // Complete
    input logic [`N - 1 : 0][$clog2(`ROB_SZ) + 1 : 0] complete_request, // LSQ INDEX (NOT COUNT FROM HEAD, NOT ROB INDEX)
    input logic [`N - 1 : 0] complete_request_valid,

    // Retire: PLEASE CONSIDER stall_store_retire BEFORE SETTING num_retire_load_store_inst
    // only contain the count of load/store inst of the total inst retiring!!!!! 
    // must contain at most 1 store per cycle (if stall_store_retire == 0) to retire: 
    //      stall_store_retire == 0 && Store Store Load -> can only retire first Store
    //      stall_store_retire == 0 && Store Load Load -> can retire all three
    //      stall_store_retire == 1 && Store Store Load -> can NOT retire ANYTHING!!!
    //      stall_store_retire == 1 && Load Load Store -> can retire first two loads
    input NUM_INST_T num_retire_load_store_inst,

    // Rollback
    // PLEASE ONLY CONTAIN LOAD / STORE
    input logic rollback_load_store_inst, // at most one

    // Output of the whole Load Store Queue Structure
    output LSQ_ENTRY [`ROB_SZ - 1 : 0] LSQ_content,
    output logic [$clog2(`ROB_SZ) + 1 : 0] lsq_head, // maximum lsq_head possible is `ROB_SZ - 1, 
    output logic [$clog2(`ROB_SZ) + 1 : 0] lsq_content_size, // maximum lsq_content_size possible is `ROB_SZ, 
    output logic stall_store_retire, // a wire directly from DCache without changes

    // clean cache when WFI
    input logic start_cleaning_up,
    output logic dcache_clean

    `ifdef DEBUG
        ,
        output logic [$clog2(`ROB_SZ) + 1 : 0] debug_next_lsq_head, 
        output logic [$clog2(`ROB_SZ) + 1 : 0] debug_next_lsq_content_size,
        output LSQ_ENTRY [`ROB_SZ - 1 : 0] debug_next_lsq_content
    `endif
);

    PC_T pc;

    // ALL INTERNAL STATES:
    logic [$clog2(`ROB_SZ) + 1 : 0] lsq_entry_idx_reading_dcache, last_lsq_entry_idx_reading_dcache; // set whenever request is sent
    // if the value is exactly `ROB_SZ, then it means it's invalid

    // logic dcache_stall; 
    DCACHE_REQUEST  dcache_requests, last_dcache_requests;
    DCACHE_RESPONSE dcache_responses;
    // logic last_stall_store_retire;

    // logic [$clog2(`ROB_SZ):0] lsq_head_out, lsq_content_size_out; 
    // LSQ_ENTRY [`ROB_SZ-1:0] LSQ_content_out;

    logic [$clog2(`ROB_SZ) + 1 : 0] next_lsq_head, next_lsq_content_size; 
    LSQ_ENTRY [`ROB_SZ - 1 : 0] next_LSQ_content;

    // Temporary Variables
    // logic [$clog2(`ROB_SZ) + 1 : 0] /*temp_rollback_tail_index, */temp_dispatch_index, temp_issue_index, temp_forward_index;

    logic [`N - 1 : 0][$clog2(`ROB_SZ) + 1 : 0] internal_dispatch_counter;
    // logic [`ROB_SZ - 1 : 0][$clog2(`ROB_SZ) + 1 : 0] internal_mem_read_counter;
    logic [`ROB_SZ - 1 : 0] readable_mem_read_index;
    logic readable_mem_read_index_valid;
    logic [$clog2(`ROB_SZ) + 1 : 0] valid_dispatch_request;
    logic [$clog2(`ROB_SZ) + 1 : 0] store_retire_request_counter;
    logic [`N - 1 : 0][$clog2(`ROB_SZ) + 1 : 0] issue_cam_index;
    logic [`N - 1 : 0] issue_cam_index_valid;

    `ifdef ENABLE_LSQ_FORWARD
    logic [`ROB_SZ - 1 : 0][3 : 0][$clog2(`ROB_SZ) + 1 : 0] forward_source;
    logic [`ROB_SZ - 1 : 0][3 : 0] valid_forward_source;
    `endif

    // assign LSQ_content = LSQ_content_out;
    // assign lsq_head = lsq_head_out;
    // assign lsq_content_size = lsq_content_size_out;

    `ifdef DEBUG
        assign debug_next_lsq_head = next_lsq_head;
        assign debug_next_lsq_content_size = next_lsq_content_size;
        assign debug_next_lsq_content = next_LSQ_content;
    `endif

    dcache cache(
        .clock(clock),
        .reset(reset),
        .pc(pc),
        .stall(stall_store_retire),

        .dcache_contents(dcache_contents),

        .dcache_requests(dcache_requests),
        .dcache_responses(dcache_responses),

        .Dmem2proc_response(Dmem2proc_response),
        .Dmem2proc_data(Dmem2proc_data),
        .Dmem2proc_tag(Dmem2proc_tag),
        .proc2Dmem_command(proc2Dmem_command),
        .proc2Dmem_addr(proc2Dmem_addr),
        .proc2Dmem_data(proc2Dmem_data),

        .start_cleaning_up(start_cleaning_up),
        .dcache_clean(dcache_clean)
    );

    `define check_subtraction(a, size) ((a) >= (size) ? (a) - (size) : (a))
    `define def_next_lsq_head `check_subtraction(lsq_head + num_retire_load_store_inst, `ROB_SZ)
    `define def_next_lsq_content_size_before_dispatch (lsq_content_size - rollback_load_store_inst - num_retire_load_store_inst)
    `define def_next_lsq_content_size_after_dispatch (`def_next_lsq_content_size_before_dispatch + valid_dispatch_request)
    `define def_lsq_retire_index `check_subtraction(lsq_head + i, `ROB_SZ)
    `define def_lsq_dispatch_start_index `check_subtraction(`def_next_lsq_head + `def_next_lsq_content_size_before_dispatch, `ROB_SZ)
    `define def_lsq_dispatch_index `check_subtraction(`def_lsq_dispatch_start_index + internal_dispatch_counter[i], `ROB_SZ)
    `define def_lsq_mem_request_cur_index `check_subtraction(`def_next_lsq_head + i, `ROB_SZ)

    always_comb begin
        issue_cam_index_valid = 0;
        issue_cam_index = 0;
        for (integer i = 0; i < `N; i++) begin
            if (issue_request[i].basic_info.valid) begin
                // CAM by rob_idx
                for (integer j = 0; j < `ROB_SZ; j++) begin
                    if (LSQ_content[j].request.basic_info.valid && 
                        LSQ_content[j].request.basic_info.rob_idx == issue_request[i].basic_info.rob_idx) begin
                        issue_cam_index[i] = j;
                        issue_cam_index_valid[i] = 1;
                    end
                end
            end
        end
    end

    `ifdef ENABLE_LSQ_FORWARD
    always_comb begin
        forward_source = 0;
        valid_forward_source = 0;
        for (integer i = 1; i < `LSQ_FORWARD_SZ; i++) begin // offset: most forwarding occurs within 2 * N entries from head
            `define def_lsq_forward_index `check_subtraction(`def_next_lsq_head + i, `ROB_SZ)
            // temp_forward_index = next_lsq_head + i;
            // temp_forward_index = temp_forward_index >= `ROB_SZ ? temp_forward_index - `ROB_SZ : temp_forward_index;
            if (i < `def_next_lsq_content_size_before_dispatch &&
                LSQ_content[`def_lsq_forward_index].request.basic_info.valid &&
                LSQ_content[`def_lsq_forward_index].request.basic_info.mem_op == MEM_READ && 
                LSQ_content[`def_lsq_forward_index].issue_success == 1) begin  
                // Found issued read request, trying to find a source to forward
                for (integer j = 0; j < i; j++) begin
                    `define def_lsq_forward_source_index `check_subtraction(`def_next_lsq_head + j, `ROB_SZ)
                    // integer source_index = next_lsq_head + j; // data source
                    // source_index = source_index >= `ROB_SZ ? source_index - `ROB_SZ : source_index;
                    if (LSQ_content[`def_lsq_forward_source_index].request.basic_info.valid &&
                        LSQ_content[`def_lsq_forward_source_index].issue_success == 1) begin
                        for (integer offset_source = 0; offset_source < 4; offset_source++) begin
                            `define def_lsq_forward_offset_dest (LSQ_content[`def_lsq_forward_source_index].request.memory_addr + offset_source - LSQ_content[`def_lsq_forward_index].request.memory_addr)
                            `define def_lsq_8bit_mask (`XLEN'hFF)   
                            if (offset_source < (`XLEN'b1 << LSQ_content[`def_lsq_forward_source_index].request.basic_info.size) && 
                                LSQ_content[`def_lsq_forward_index].request.memory_addr <= LSQ_content[`def_lsq_forward_source_index].request.memory_addr + offset_source && 
                                LSQ_content[`def_lsq_forward_source_index].request.memory_addr + offset_source < LSQ_content[`def_lsq_forward_index].request.memory_addr + (4'b0001 << LSQ_content[`def_lsq_forward_index].request.basic_info.size)) begin
                                // source_index is Read
                                if ((LSQ_content[`def_lsq_forward_source_index].request.basic_info.mem_op == MEM_READ && LSQ_content[`def_lsq_forward_source_index].read_ready == 1) || 
                                    LSQ_content[`def_lsq_forward_source_index].request.basic_info.mem_op == MEM_WRITE) begin
                                    // Forward from previous read
                                    forward_source[i][`def_lsq_forward_offset_dest] = `def_lsq_forward_source_index;
                                    valid_forward_source[i][`def_lsq_forward_offset_dest] = 1;
                                end
                            end
                        end
                        // if (LSQ_content[`def_lsq_forward_index].forwarded_bytes == (`XLEN'b1 << (`XLEN'b1 << LSQ_content[`def_lsq_forward_index].request.basic_info.size)) - 1) begin
                        //     next_LSQ_content[`def_lsq_forward_index].read_ready = 1;
                        // end
                    end
                end
            end
        end
    end
    `endif

    always_comb begin // valid_dispatch_request
        valid_dispatch_request = 0;
        for (int i = 0; i < `N; i++) begin
            valid_dispatch_request += dispatch_request[i].valid ? 1 : 0;
        end
    end

    always_comb begin // store_retire_request_counter
        store_retire_request_counter = 0;
        for (int i = 0; i < `N; i++) begin
            store_retire_request_counter += 
                ((i < num_retire_load_store_inst) && 
                LSQ_content[`def_lsq_retire_index].request.basic_info.valid && 
                (LSQ_content[`def_lsq_retire_index].request.basic_info.mem_op == MEM_WRITE)) ? 1 : 0;
        end
    end

    always_comb begin // internal_dispatch_counter
        internal_dispatch_counter = 0;
        for (integer i = 0; i < `N; i++) begin
            for (int j = 0; j < i; j++) begin
                internal_dispatch_counter[i] += dispatch_request[j].valid ? 1 : 0;
            end
        end
    end

    always_comb begin // internal_mem_read_counter
        // internal_mem_read_counter = 0;
        readable_mem_read_index = 0;
        readable_mem_read_index_valid = 0;
        `ifdef ENABLE_LSQ_FORWARD
        for (integer i = 0; i < `ROB_SZ; i++) begin // offset
            if (LSQ_content[`check_subtraction(`def_next_lsq_head + i, `ROB_SZ)].request.basic_info.valid &&
                LSQ_content[`check_subtraction(`def_next_lsq_head + i, `ROB_SZ)].request.basic_info.mem_op == MEM_WRITE && 
                LSQ_content[`check_subtraction(`def_next_lsq_head + i, `ROB_SZ)].issue_success == 0) begin  
                break;
            end
            if ((LSQ_content[`check_subtraction(`def_next_lsq_head + i, `ROB_SZ)].request.basic_info.mem_op == MEM_READ) &&
                (LSQ_content[`check_subtraction(`def_next_lsq_head + i, `ROB_SZ)].issue_success) &&
                (LSQ_content[`check_subtraction(`def_next_lsq_head + i, `ROB_SZ)].read_ready == 0)) begin
                readable_mem_read_index = `check_subtraction(`def_next_lsq_head + i, `ROB_SZ);
                readable_mem_read_index_valid = 1;
                break;
            end
        end
        `endif 
        `ifndef ENABLE_LSQ_FORWARD
        if ((LSQ_content[`def_next_lsq_head].request.basic_info.mem_op == MEM_READ) &&
            (LSQ_content[`def_next_lsq_head].issue_success) &&
            (LSQ_content[`def_next_lsq_head].read_ready == 0)) begin
            readable_mem_read_index = `def_next_lsq_head;
            readable_mem_read_index_valid = 1;
        end
        `endif 
    end

    assign pc = dcache_requests.pc;

    // Pipeline Stage Logic
    always_comb begin
        lsq_entry_idx_reading_dcache = last_lsq_entry_idx_reading_dcache;
        dcache_requests = last_dcache_requests;
        next_LSQ_content = LSQ_content;
        next_lsq_head = `def_next_lsq_head;
        next_lsq_content_size = `def_next_lsq_content_size_after_dispatch;
        // issue_approved = 0; 
        // dcache_available = last_stall_store_retire ? 0 : 1; // TODO

        `ifdef DEBUG
        $display("qwq    ----> dcache_responses: %p last_dcache_requests: %p",dcache_responses, last_dcache_requests);
        `endif

        // Received Memory Response For Read
        if (dcache_responses.valid) begin
            if (last_dcache_requests.mem_op == MEM_READ && 
                last_lsq_entry_idx_reading_dcache != `ROB_SZ && // is valid
                LSQ_content[last_lsq_entry_idx_reading_dcache].read_ready == 0) begin
                // prevent the forwarded result from being overwritten by memory response
                next_LSQ_content[last_lsq_entry_idx_reading_dcache].read_data = dcache_responses.reg_data;
                next_LSQ_content[last_lsq_entry_idx_reading_dcache].read_ready = 1;
            end
            dcache_requests = 0;
        end

        // Rollback
        if (rollback_load_store_inst) begin
            // assert (lsq_content_size > 0) else begin $display("ASSERTION ERROR: Trying to rollback at size 0"); $finish; end
            // temp_rollback_tail_index = next_lsq_head + next_lsq_content_size;
            // temp_rollback_tail_index = temp_rollback_tail_index >= `ROB_SZ ? temp_rollback_tail_index - `ROB_SZ : temp_rollback_tail_index;
            if (`check_subtraction(`check_subtraction(lsq_head + lsq_content_size - 1 + `ROB_SZ, `ROB_SZ), `ROB_SZ) == last_lsq_entry_idx_reading_dcache) begin
                lsq_entry_idx_reading_dcache = `ROB_SZ;
            end
            next_LSQ_content[`check_subtraction(`check_subtraction(lsq_head + lsq_content_size - 1 + `ROB_SZ, `ROB_SZ), `ROB_SZ)] = 0;
            // next_lsq_content_size -= 1;
        end
        `ifdef DEBUG
        $display("-------------- before retire stall_store_retire: %p dcache_requests: %p next_lsq_head: %p next_lsq_content_size: %p", stall_store_retire, dcache_requests, next_lsq_head, next_lsq_content_size);
        `endif
        // Retire
        for (integer i = 0; i < `N; i++) begin
            // next_lsq_head = lsq_head + i >= `ROB_SZ ? lsq_head + i - `ROB_SZ : lsq_head + i;
            if (i < num_retire_load_store_inst) begin
                // assert (i < lsq_content_size - rollback_load_store_inst) else $finish;
                // assert (LSQ_content[`lsq_retire_index].issue_success) else begin $display("ASSERTION ERROR: Trying to retire not ready (issue_success) inst. idx: %p, inst: %p", next_lsq_head, next_LSQ_content); $finish; end
                // assert (LSQ_content[`lsq_retire_index].request.basic_info.valid) else begin $display("ASSERTION ERROR: Trying to retire an invalid inst idx: %p, inst: %p", next_lsq_head, next_LSQ_content); $finish; end
                assert (LSQ_content[`def_lsq_retire_index].request.basic_info.valid && LSQ_content[`def_lsq_retire_index].issue_success) else begin 
                    $display("----------------lsq_head %p def_lsq_retire_index %p lsq_content_size %p LSQ_content: %p lsq_head: %p lsq_content_size: %p", lsq_head, `def_lsq_retire_index, lsq_content_size, LSQ_content, lsq_head, lsq_content_size);
                    $finish; 
                end
                if (LSQ_content[`def_lsq_retire_index].request.basic_info.valid && LSQ_content[`def_lsq_retire_index].request.basic_info.mem_op == MEM_WRITE && LSQ_content[`def_lsq_retire_index].issue_success) begin
                    assert (~stall_store_retire) else $finish; 
                    // $display("retire store: dcache_available: %p num_retire_load_store_inst: %p i: %p lsq_head: %p ", dcache_available, num_retire_load_store_inst, i, lsq_head);
                    // assert ((dcache_available && num_retire_load_store_inst) || !num_retire_load_store_inst) else begin $display("ASSERTION ERROR: Trying to retire an inst while dcache is busy @ idx %p", next_lsq_head); $finish; end
                    dcache_requests.mem_op = MEM_WRITE;
                    dcache_requests.addr = LSQ_content[`def_lsq_retire_index].request.memory_addr;
                    dcache_requests.size = LSQ_content[`def_lsq_retire_index].request.basic_info.size;
                    dcache_requests.write_content = LSQ_content[`def_lsq_retire_index].request.data;
                    dcache_requests.valid = 1;
                    dcache_requests.pc = LSQ_content[`def_lsq_retire_index].request.pc;
                end
                next_LSQ_content[`def_lsq_retire_index] = '0;
            end
        end
        // assert (next_lsq_content_size >= num_retire_load_store_inst) else begin 
        //     $display("ASSERTION ERROR: Trying to retire but next_lsq_content_size is too small next_lsq_content_size: %p num_retire_load_store_inst: %p", 
        //         next_lsq_content_size, 
        //         num_retire_load_store_inst); 
        //     $finish; 
        // end
        // next_lsq_content_size -= num_retire_load_store_inst;
        // next_lsq_head += num_retire_load_store_inst;
        // next_lsq_head = next_lsq_head >= `ROB_SZ ? next_lsq_head - `ROB_SZ : next_lsq_head;
        `ifdef DEBUG
        $display("----------- after retire: next_lsq_content_size: %p next_lsq_head: %p dcache_requests: %p next_LSQ_content: %p", next_lsq_content_size, next_lsq_head, dcache_requests, next_LSQ_content);
        `endif

        // Dispatch
        for (integer i = 0; i < `N; i++) begin
            if (dispatch_request[i].valid) begin
                // assert (!rollback_load_store_inst) else begin $display("ASSERTION ERROR: while rollback new inst coming in"); $finish; end
                // assert (next_lsq_content_size < `ROB_SZ) else begin $display("ASSERTION ERROR: Trying to dispatch but next_lsq_content_size is too large"); $finish; end
                // temp_dispatch_index = next_lsq_head + next_lsq_content_size;
                // temp_dispatch_index = temp_dispatch_index >= `ROB_SZ ? temp_dispatch_index - `ROB_SZ : temp_dispatch_index;
                next_LSQ_content[`def_lsq_dispatch_index] = '0;
                next_LSQ_content[`def_lsq_dispatch_index].request.basic_info = dispatch_request[i];
                // ++next_lsq_content_size;
                
                // next_LSQ_content[temp_dispatch_index].issue_success = 0;
                // next_LSQ_content[temp_dispatch_index].read_ready = 0;
            end
        end

        // Issue
        for (integer i = 0; i < `N; i++) begin
            if (issue_cam_index_valid[i]) begin
                // issue_approved[i] = 1; // will be set to zero later in the for loop because of un-issued store before it
                next_LSQ_content[issue_cam_index[i]].issue_success = 1;
                next_LSQ_content[issue_cam_index[i]].request.memory_addr = issue_request[i].memory_addr;
                next_LSQ_content[issue_cam_index[i]].request.data = issue_request[i].data;
                next_LSQ_content[issue_cam_index[i]].request.dst_reg_idx = issue_request[i].dst_reg_idx;
                next_LSQ_content[issue_cam_index[i]].request.pc = issue_request[i].pc;
                /*
                if (issue_request[i].basic_info.mem_op == MEM_READ) begin 
                    `define def_lsq_issue_index_relative_to_head (issue_cam_index[i] < `def_next_lsq_head ? issue_cam_index[i] + `ROB_SZ - `def_next_lsq_head : issue_cam_index[i] - `def_next_lsq_head)
                    // integer index_relative_to_head = j < next_lsq_head ? j + `ROB_SZ - next_lsq_head : j - next_lsq_head;
                    for (integer k = 1; k <= `ROB_SZ; k++) begin // k: delta before j
                        `define def_lsq_issue_cur_index (issue_cam_index[i] >= k ? issue_cam_index[i] - k : issue_cam_index[i] + `ROB_SZ - k)
                        // integer cur_index = j >= k ? j - k : j + `ROB_SZ - k;
                        if (k <= `def_lsq_issue_index_relative_to_head &&
                            LSQ_content[`def_lsq_issue_cur_index].request.basic_info.valid &&
                            LSQ_content[`def_lsq_issue_cur_index].request.basic_info.mem_op == MEM_WRITE && 
                            LSQ_content[`def_lsq_issue_cur_index].issue_success == 0) begin  
                            // there is a store inst has no address ready
                            issue_approved[i] = 0;
                            next_LSQ_content[issue_cam_index[i]].issue_success = 0;
                        end
                    end
                end
                else if (issue_request[i].basic_info.mem_op == MEM_WRITE) begin
                    next_LSQ_content[issue_cam_index[i]].request.data = issue_request[i].data;
                end*/
            end
        end
        /*
        for (integer i = 0; i < `N; i++) begin
            if (issue_request[i].basic_info.valid) begin
                // CAM by rob_idx
                for (integer j = 0; j < `ROB_SZ; j++) begin
                    if (LSQ_content[j].request.basic_info.valid && 
                        LSQ_content[j].request.basic_info.rob_idx == issue_request[i].basic_info.rob_idx) begin
                        // Found the entry match the request
                        issue_approved[i] = 1; // will be set to zero later in the for loop because of un-issued store before it
                        next_LSQ_content[j].issue_success = 1;
                        next_LSQ_content[j].request.dst_reg_idx = issue_request[i].dst_reg_idx;
                        next_LSQ_content[j].request.memory_addr = issue_request[i].memory_addr;
                        if (issue_request[i].basic_info.mem_op == MEM_READ) begin 
                            `define def_lsq_issue_index_relative_to_head (j < `def_next_lsq_head ? j + `ROB_SZ - `def_next_lsq_head : j - `def_next_lsq_head)
                            // integer index_relative_to_head = j < next_lsq_head ? j + `ROB_SZ - next_lsq_head : j - next_lsq_head;
                            for (integer k = 1; k <= `ROB_SZ; k++) begin // k: delta before j
                                `define def_lsq_issue_cur_index (j >= k ? j - k : j + `ROB_SZ - k)
                                // integer cur_index = j >= k ? j - k : j + `ROB_SZ - k;
                                if (k <= `def_lsq_issue_index_relative_to_head &&
                                    LSQ_content[`def_lsq_issue_cur_index].request.basic_info.valid &&
                                    LSQ_content[`def_lsq_issue_cur_index].request.basic_info.mem_op == MEM_WRITE && 
                                    LSQ_content[`def_lsq_issue_cur_index].issue_success == 0) begin  
                                    // there is a store inst has no address ready
                                    issue_approved[i] = 0;
                                    next_LSQ_content[j].issue_success = 0;
                                end
                            end
                        end
                        else if (issue_request[i].basic_info.mem_op == MEM_WRITE) begin
                            next_LSQ_content[j].request.data = issue_request[i].data;
                        end
                    end
                end
            end
        end*/

        // Complete (only used by external)
        for (integer i = 0; i < `N; i++) begin
            if (complete_request_valid[i]) begin
                next_LSQ_content[complete_request[i]].completed = 1;
            end
        end 

        // Forward (head is impossible to forward, therefore start at offset 1)
        `ifdef ENABLE_LSQ_FORWARD
        for (integer i = 1; i < `LSQ_FORWARD_SZ; i++) begin // offset Can be anything from 1 to `ROB_SZ, to `N is efficient enough since forwarding is rare
            for (integer offset_dest = 0; offset_dest < 4; offset_dest++) begin
                if (valid_forward_source[i][offset_dest]) begin
                    `define cam_offset_soure (LSQ_content[`def_lsq_forward_index].request.memory_addr + offset_dest - LSQ_content[forward_source[i][offset_dest]].request.memory_addr)
                    if (LSQ_content[forward_source[i][offset_dest]].request.basic_info.mem_op == MEM_READ && 
                        LSQ_content[forward_source[i][offset_dest]].read_ready == 1) begin
                        // Forward from previous read
                        next_LSQ_content[`def_lsq_forward_index].forwarded_bytes[offset_dest] = 1;
                        next_LSQ_content[`def_lsq_forward_index].read_data &= ~(`def_lsq_8bit_mask << (8 * offset_dest)); // clear out the 8bits to write
                        next_LSQ_content[`def_lsq_forward_index].read_data |= ((LSQ_content[forward_source[i][offset_dest]].read_data >> (8 * `cam_offset_soure)) & `def_lsq_8bit_mask) << (8 * offset_dest); // 
                    end
                    // source_index is Write
                    if (LSQ_content[forward_source[i][offset_dest]].request.basic_info.mem_op == MEM_WRITE) begin
                        next_LSQ_content[`def_lsq_forward_index].forwarded_bytes[offset_dest] = 1;
                        next_LSQ_content[`def_lsq_forward_index].read_data &= ~(`def_lsq_8bit_mask << (8 * offset_dest));
                        next_LSQ_content[`def_lsq_forward_index].read_data |= ((LSQ_content[forward_source[i][offset_dest]].request.data >> (8 * `cam_offset_soure)) & `def_lsq_8bit_mask) << (8 * offset_dest);
                    end
                end
                    
            end
        end
        `endif
        /*
        for (integer i = 1; i < `ROB_SZ; i++) begin // offset
            `define def_lsq_forward_index `check_subtraction(`def_next_lsq_head + i, `ROB_SZ)
            // temp_forward_index = next_lsq_head + i;
            // temp_forward_index = temp_forward_index >= `ROB_SZ ? temp_forward_index - `ROB_SZ : temp_forward_index;
            if (i < `def_next_lsq_content_size_before_dispatch &&
                LSQ_content[`def_lsq_forward_index].request.basic_info.valid &&
                LSQ_content[`def_lsq_forward_index].request.basic_info.mem_op == MEM_READ && 
                LSQ_content[`def_lsq_forward_index].issue_success == 1) begin  
                // Found issued read request, trying to find a source to forward
                for (integer j = 0; j < i; j++) begin
                    `define def_lsq_forward_source_index `check_subtraction(`def_next_lsq_head + j, `ROB_SZ)
                    // integer source_index = next_lsq_head + j; // data source
                    // source_index = source_index >= `ROB_SZ ? source_index - `ROB_SZ : source_index;
                    if (LSQ_content[`def_lsq_forward_source_index].request.basic_info.valid &&
                        LSQ_content[`def_lsq_forward_source_index].issue_success == 1) begin
                        for (integer offset_source = 0; offset_source < 4; offset_source++) begin
                            `define def_lsq_forward_offset_dest (LSQ_content[`def_lsq_forward_source_index].request.memory_addr + offset_source - LSQ_content[`def_lsq_forward_index].request.memory_addr)
                            `define def_lsq_8bit_mask (`XLEN'hFF)   
                            if (offset_source < (`XLEN'b1 << LSQ_content[`def_lsq_forward_source_index].request.basic_info.size) && 
                                LSQ_content[`def_lsq_forward_index].request.memory_addr <= LSQ_content[`def_lsq_forward_source_index].request.memory_addr + offset_source && 
                                LSQ_content[`def_lsq_forward_source_index].request.memory_addr + offset_source < LSQ_content[`def_lsq_forward_index].request.memory_addr + (4'b0001 << LSQ_content[`def_lsq_forward_index].request.basic_info.size)) begin
                                // source_index is Read
                                if (LSQ_content[`def_lsq_forward_source_index].request.basic_info.mem_op == MEM_READ && 
                                    LSQ_content[`def_lsq_forward_source_index].read_ready == 1) begin
                                    // Forward from previous read
                                    next_LSQ_content[`def_lsq_forward_index].forwarded_bytes[`def_lsq_forward_offset_dest] = 1;
                                    next_LSQ_content[`def_lsq_forward_index].read_data &= ~(`def_lsq_8bit_mask << (8 * `def_lsq_forward_offset_dest)); // clear out the 8bits to write
                                    next_LSQ_content[`def_lsq_forward_index].read_data |= ((LSQ_content[`def_lsq_forward_source_index].read_data >> (8 * offset_source)) & `def_lsq_8bit_mask) << (8 * `def_lsq_forward_offset_dest); // 
                                end
                                // source_index is Write
                                if (LSQ_content[`def_lsq_forward_source_index].request.basic_info.mem_op == MEM_WRITE) begin
                                    // Forward from previous write
                                    next_LSQ_content[`def_lsq_forward_index].forwarded_bytes[`def_lsq_forward_offset_dest] = 1;
                                    next_LSQ_content[`def_lsq_forward_index].read_data &= ~(`def_lsq_8bit_mask << (8 * `def_lsq_forward_offset_dest));
                                    next_LSQ_content[`def_lsq_forward_index].read_data |= ((LSQ_content[`def_lsq_forward_source_index].request.data >> (8 * offset_source)) & `def_lsq_8bit_mask) << (8 * `def_lsq_forward_offset_dest);
                                end
                            end
                        end
                        // if (LSQ_content[`def_lsq_forward_index].forwarded_bytes == (`XLEN'b1 << (`XLEN'b1 << LSQ_content[`def_lsq_forward_index].request.basic_info.size)) - 1) begin
                        //     next_LSQ_content[`def_lsq_forward_index].read_ready = 1;
                        // end
                    end
                end
            end
        end*/

        // Send Memory Request To DCache After forward
        if (readable_mem_read_index_valid &&
            (~stall_store_retire) &&
            store_retire_request_counter == 0) begin
            lsq_entry_idx_reading_dcache = readable_mem_read_index;
            dcache_requests.mem_op = MEM_READ;
            dcache_requests.addr = LSQ_content[readable_mem_read_index].request.memory_addr;
            dcache_requests.size = LSQ_content[readable_mem_read_index].request.basic_info.size;
            dcache_requests.write_content = 0;
            dcache_requests.valid = 1;
            dcache_requests.pc = LSQ_content[readable_mem_read_index].request.pc;
        end
        /*
        for (integer i = 0; i < `ROB_SZ; i++) begin // offset
            // integer cur_index = next_lsq_head + i;
            // cur_index = cur_index >= `ROB_SZ ? cur_index - `ROB_SZ : cur_index;
            if (internal_mem_read_counter[`def_lsq_mem_request_cur_index] == 0 &&
                (~stall_store_retire) && 
                store_retire_request_counter == 0 &&
                LSQ_content[`def_lsq_mem_request_cur_index].request.basic_info.mem_op == MEM_READ &&
                LSQ_content[`def_lsq_mem_request_cur_index].issue_success &&
                LSQ_content[`def_lsq_mem_request_cur_index].read_ready == 0) begin
                // Found one entry to send DCache read request for
                // dcache_available = 0;
                lsq_entry_idx_reading_dcache = `def_lsq_mem_request_cur_index;
                dcache_requests.mem_op = MEM_READ;
                dcache_requests.addr = LSQ_content[`def_lsq_mem_request_cur_index].request.memory_addr;
                dcache_requests.size = LSQ_content[`def_lsq_mem_request_cur_index].request.basic_info.size;
                dcache_requests.write_content = 0;
                dcache_requests.valid = 1;
            end
        end
        */
    end

    // reset
    always_ff @(posedge clock) begin
        if (reset) begin
            lsq_head <= 0;
            lsq_content_size <= 0;
            LSQ_content <= 0;
            // last_stall_store_retire <= 0;
            last_dcache_requests <= 0;
            last_lsq_entry_idx_reading_dcache <= 0;
        end else begin
            lsq_head <= next_lsq_head;
            lsq_content_size <= next_lsq_content_size;
            
            for (int i = 0; i < `ROB_SZ; i++) begin
                LSQ_content[i].request <= next_LSQ_content[i].request;
                LSQ_content[i].issue_success <= next_LSQ_content[i].issue_success;
                LSQ_content[i].read_data <= next_LSQ_content[i].read_data;
                if (next_LSQ_content[i].request.basic_info.valid && 
                    next_LSQ_content[i].request.basic_info.mem_op == MEM_READ &&
                    next_LSQ_content[i].forwarded_bytes == (`XLEN'b1 << (`XLEN'b1 << next_LSQ_content[i].request.basic_info.size)) - 1) begin
                    LSQ_content[i].read_ready <= 1;
                end
                else 
                begin
                    LSQ_content[i].read_ready <= next_LSQ_content[i].read_ready;
                end
                LSQ_content[i].forwarded_bytes <= next_LSQ_content[i].forwarded_bytes;
                LSQ_content[i].completed <= next_LSQ_content[i].completed;
            end
            // last_stall_store_retire <= stall_store_retire;
            last_dcache_requests <= dcache_requests;
            last_lsq_entry_idx_reading_dcache <= lsq_entry_idx_reading_dcache;
        end
    end

endmodule