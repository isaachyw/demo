`include "verilog/sys_defs.svh"

// Reservation station:
module rs (
    input logic clock,
    reset,

    // Dispatch:
    // For populating new instructions
    // output NUM_INST_T_RS num_entry_available,
    input RS_INPUT [`N-1:0] new_inst_supplies,

    // Listening to CDB forwarding in the subsequent trials
    input CDB_ENTRY [`CDB_W-1:0] cdb_entries,

    output NUM_INST_T_RS num_entry_available,// invalid entry number

    // Issue:
    // The availability of FU next cycle
    // input FU_STATUS fu_status,
    // The instructions that's ready to execute in the next cycle
    output RS_ENTRY [`RS_SZ-1:0] rs_entries_out,
    output logic [`RS_SZ-1:0] ready_issue,
    // input RS_REF_T [`N-1:0] fu_feedback,
    input [`RS_SZ-1:0] fu_feedback_mask,
    input NUM_INST_T_RS fu_feedback_num,

    // Cancellation:
    input ROB_REF_T cancel_ref
);

  NUM_INST_T_RS num_rs_available_reg,next_num_rs_available;
  NUM_INST_T_RS cancel_num;
  RS_ENTRY [`RS_SZ-1:0] rs_entries, cleared_rs_entries,supplied_rs_entries, next_rs_entries;
  logic [$clog2(`N):0] supply_inst_num;
  logic [`N-1:0] [`RS_SZ-1:0] valids;// valid bits extract from cleared_rs_entries, used for req for psl_gen
  logic [`N-1:0] [`RS_SZ-1:0] gnt;
  logic [`RS_SZ-1:0] op0_not,op1_not;
  /*general combitional structure
  rs_entries------->cleared_rs      
                                  --------->gnts(one hot coding)------>supplied_rs_entries->next_rs_entries(cdb_accessed)      
  rs_supply-----> supply_inst_num
  */


  // DEBUG
  `ifdef DEBUG
  always_comb begin
    for(int i = 0; i < `RS_SZ; i++) begin
      if(new_inst_supplies[i].inst.pc == 48) begin
        $display("Time: %d\tNow I'm inside new_inst_supplies\tI'm PC30\tvalid: %d\trob_index: %d", $time, new_inst_supplies[i].valid, new_inst_supplies[i].rob_ref.rob_index);
      end
    end
    for(int i = 0; i < `RS_SZ; i++) begin
      if(rs_entries[i].inst.pc == 48) begin
        $display("Time: %d\tNow I'm insde rs_entries\tI'm PC30\tvalid: %d\trob_index: %d", $time, rs_entries[i].valid, rs_entries[i].rob_ref.rob_index);
      end
    end
  end
  `endif

  always_comb begin : calculate_supply_inst_num
  // force the order of input, invalid valid valid is not acceptable
    supply_inst_num = '0;
    for (integer i = 0; i < `N; i++) begin
      if (new_inst_supplies[i].valid) begin
        // $display("operand ------------------------ %d, %d",new_inst_supplies[i].operands_status[0].ready,new_inst_supplies[i].operands_status[1].ready);
        supply_inst_num = i + 1;
      end
    end
  end

  always_comb begin : clear_entry
    cleared_rs_entries = rs_entries;
    cancel_num = 0;
    for (int i = 0; i < `RS_SZ; i++) begin
      if (fu_feedback_mask[i]) begin
        cleared_rs_entries[i] = '0;
      end
    end
    if (cancel_ref.valid) begin
      for (integer i = 0; i < `RS_SZ; i++) begin
        if (rs_entries[i].rob_ref.rob_index == cancel_ref.rob_index&& rs_entries[i].valid) begin
          cleared_rs_entries[i] = '0;
          cancel_num = 1;
          break;
        end
      end
    end



  end

  always_comb begin
    valids[0] = '0;
    for(integer i = 0; i < `RS_SZ; i++)begin
      valids[0][i] = ~cleared_rs_entries[i].valid;
    end
  end
  genvar i;
  generate
    for (i = 0 ;i<`N;i++) begin
      psel_gen #(.WIDTH(`RS_SZ),.REQS(1)) psel_gen_loop (
        .req(valids[i]),
        .gnt(gnt[i])
      );
      if (i < `N-1) begin
        assign valids[i+1] = valids[i] & ~gnt[i];
      end
    end
  endgenerate

  always_comb begin : insert_entry
    supplied_rs_entries = cleared_rs_entries;
    for (integer i = 0; i < `N; i++) begin
      if (new_inst_supplies[i].valid) begin
        for (integer j = 0; j < `RS_SZ; j++) begin
          if (gnt[i][j]) begin
            supplied_rs_entries[j] = new_inst_supplies[i];

            //DEBUG
            // if(supplied_rs_entries[j].inst.pc == 48) begin
            //   $display("Am I ever inside this supplied_rs_entries?");
            // end

            break;
          end
        end
      end
    end
  end

  always_comb begin :resolve_cdb
  next_rs_entries = supplied_rs_entries;

  // DEBUG
  `ifdef DEBUG
  for(int i = 0; i < `RS_SZ; i++) begin
    if(next_rs_entries[i].inst.pc == 48) begin
      $display("Time: %d\tNow I'm insde next_rs_entries\tI'm PC30\tvalid: %d\trob_index: %d", $time, next_rs_entries[i].valid, next_rs_entries[i].rob_ref.rob_index);
    end
  end
  `endif

    for (integer i = 0; i < `N; i++) begin
      if (cdb_entries[i].valid) begin
        `ifdef DEBUG
        $display("[RS CDB]\ttime:%d\tphy_reg:%d is ready", $time, cdb_entries[i].phy_reg_idx);
        `endif
        for (integer j = 0; j < `RS_SZ; j++) begin
          for (integer operand = 0; operand < 2; operand++) begin
            if(supplied_rs_entries[j].operands_status[operand].valid&&
                        supplied_rs_entries[j].operands_status[operand].phy_reg_idx==cdb_entries[i].phy_reg_idx)begin
              next_rs_entries[j].operands_status[operand].ready = 1;
            end
          end
        end
      end
    end
  end

  always_comb begin : set_ready
  ready_issue = '0;
  op0_not = '0;
  op1_not = '0;
    for (integer i=0;i<`RS_SZ;i++)begin
      if(rs_entries[i].valid)begin
        op0_not[i] = rs_entries[i].operands_status[0].valid && ~rs_entries[i].operands_status[0].ready;
        op1_not[i] = rs_entries[i].operands_status[1].valid && ~rs_entries[i].operands_status[1].ready;
        ready_issue[i] = ~op0_not[i] & ~op1_not[i];
        `ifdef DEBUG
        $display("Time: [%d]\tRS_ENTRY: PC: %h\t ready: %d", $time, rs_entries[i].inst.pc, ready_issue[i]);
        `endif
      end
    end

  end

  assign next_num_rs_available = num_rs_available_reg-supply_inst_num+fu_feedback_num+cancel_num;
  assign num_entry_available = num_rs_available_reg;
  assign rs_entries_out = rs_entries;

`ifdef DEBUG
always_comb begin
  $display("[RS NEXT NUM READY]\ttime: %d\tnext_num_rs_available: %d\tnum_rs_available_reg: %d\tsupply_inst_num: %d\tfeedback_num: %d\tcancel_num: %d", $time, next_num_rs_available,num_rs_available_reg, supply_inst_num, fu_feedback_num, cancel_num);
end
`endif

  always_ff @(posedge clock) begin
    if (reset) begin
      num_rs_available_reg <= `RS_SZ;
      rs_entries <= '0;
    end else begin
      num_rs_available_reg <= next_num_rs_available;
      rs_entries <= next_rs_entries;
    end

    `ifdef DEBUG
    $display("****************%d, %d, %d, %d", num_rs_available_reg, supply_inst_num, fu_feedback_num, cancel_num);
    if (cancel_num==1)begin
      $display("time:%d\t[RS CANCEL]\trob:%d", $time, cancel_ref.rob_index);
    end
    `endif

  end

`ifdef DEBUG
always_comb begin
      $display("time:%d\t[RS CONTENT]", $time);
    for (int i = 0; i < `RS_SZ; i++) begin
        if (rs_entries[i].valid) $display("decodede_inst.pc: %h\tinst: %d\tfunc: %p\tfu: %p\trob: %d regA: %d ready: %d valid: %d regB: %d ready: %d valid: %d", 
        rs_entries[i].inst.pc, rs_entries[i].inst.inst, rs_entries[i].inst.func, rs_entries[i].inst.fu, rs_entries[i].rob_ref.rob_index,
        rs_entries[i].operands_status[0].phy_reg_idx, rs_entries[i].operands_status[0].ready, rs_entries[i].operands_status[0].valid,
        rs_entries[i].operands_status[1].phy_reg_idx, rs_entries[i].operands_status[1].ready, rs_entries[i].operands_status[1].valid);
    end
end
`endif

endmodule