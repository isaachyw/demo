/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  regfile.sv                                          //
//                                                                     //
//  Description :  This module creates the Regfile used by the ID and  //
//                 WB Stages of the Pipeline.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`include "verilog/sys_defs.svh"

// Register file

module regfile(
    input clock,
    input reset,

    // coming from RS @issue
    input REG_READ_REQ[`REG_PORT_WIDTH-1:0][1:0] read_request,
    output REG_READ_RESPONSE[`REG_PORT_WIDTH-1:0][1:0] read_response,

    // coming from FU @complete
    input REG_WRITE_REQ[`CDB_W-1:0] write_request

`ifdef DEBUG
    // Debug:
    , output [`PHYS_REG_SZ-1:1][`XLEN-1:0] registers_out
`endif
);

    logic [`PHYS_REG_SZ-1:1][`XLEN-1:0] registers; // 31 XLEN-length Regiters (0 is known)
    `ifdef DEBUG
    // Debug:
    assign registers_out = registers;
    `endif

    always_comb begin : Read_logic
        // $display("read request phy reg index: %p", read_request);
        // for(integer i = 0; i < `REG_PORT_WIDTH; i++) begin
        //     $display("time: [%d]\tWrite request: index[%d] %d",$time, write_request[i].reg_ref, write_request[i].data);
        // end

        read_response = '0;
        for(integer n = 0; n < `REG_PORT_WIDTH; n++) begin
            for(integer i = 0; i < 2; i++) begin
                if(read_request[n][i].valid) begin
                    // reg 0
                    if(read_request[n][i].reg_ref == `ZERO_REG) begin
                        read_response[n][i] = {
                            data: 0,
                            valid: 1
                        };
                    end else begin
                        read_response[n][i] = {
                            data: registers[read_request[n][i].reg_ref],
                            valid: 1
                        };
                        // Internal Fowarding for wr
                        for(integer j = 0; j < `REG_PORT_WIDTH; j++) begin
                            if(write_request[j].valid && read_request[n][i].reg_ref == write_request[j].reg_ref) begin
                                read_response[n][i] = {
                                    data: write_request[j].data,
                                    valid: 1
                                };
                            end
                        end
                    end
                end else begin
                    read_response[n][i] = {
                        data: 0,
                        valid: 0
                    };
                end
            end
        end
    end

    // Write port
    always_ff @(posedge clock) begin
        if(reset) begin
            registers <= '0;
        end else begin
            for(integer i = 0; i < `REG_PORT_WIDTH; i++) begin
                if(write_request[i].valid && write_request[i].reg_ref != `ZERO_REG) begin
                    registers[write_request[i].reg_ref] <= write_request[i].data;
                end

            end
        end
        `ifdef DEBUG
        $display("time: %d REGFILE", $time );
        for(int i = 0; i < `PHYS_REG_SZ; i++) $display("phy#%d val: %h", i, registers[i]);
        `endif
    end

endmodule