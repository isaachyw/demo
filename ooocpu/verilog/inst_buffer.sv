`timescale 1ns / 100ps

`include "verilog/sys_defs.svh"
`define check_subtraction(a, size) ((a) >= (size) ? (a) - (size) : (a))

module inst_buffer (
  input clock,
  input reset,
  input rollback, // clear the buffer when rollback
  input FETCH_RES [`N-1:0] fetch_res,
  input logic [$clog2(`INST_BUFFER_SZ):0] dispatch_count,  // use for free up inst_buffer
  output logic [$clog2(`INST_BUFFER_SZ):0] available_slot_num,
  output FETCH_RES [`N-1:0] fetch_res_out
`ifdef DEBUG
  , output logic [$clog2(`INST_BUFFER_SZ) + 1:0] debug_head, debug_size
  , output FETCH_RES [`INST_BUFFER_SZ-1:0] debug_instruction_buffer
`endif
);
  logic [$clog2(`INST_BUFFER_SZ) + 2:0] head, next_head;
  logic [$clog2(`INST_BUFFER_SZ) + 2:0] size, next_size;
  FETCH_RES [`INST_BUFFER_SZ-1:0] instruction_buffer, next_instruction_buffer;

  // logic [$clog2(`INST_BUFFER_SZ) + 2:0] temp_index;
  logic [$clog2(`INST_BUFFER_SZ) + 2:0] valid_count;

  assign available_slot_num = `INST_BUFFER_SZ - `N - `N - size;
`ifdef DEBUG
  assign debug_head = head;
  assign debug_size = size;
  assign debug_instruction_buffer = instruction_buffer;
`endif

  // update fetch_res_out
  always_comb begin
    for (integer i = 0; i < `N; i++) begin
      if (i < size) begin
        fetch_res_out[i] = instruction_buffer[`check_subtraction(head + i, `INST_BUFFER_SZ)];
      end
      else begin
        fetch_res_out[i] = fetch_res[i - size];
      end
    end
  end
  
  // calculate valid_count
  always_comb begin
    valid_count = 0;
    for (int i = 0; i < `N; i++) begin
      valid_count += fetch_res[i].valid;
    end
  end


  // update next info
  always_comb begin
    `ifdef DEBUG
        $display("[INST BURRER dispatch_count] %d fetch_res: %p", $time, fetch_res);
    for (int i = 0; i < `N; i++) begin
      $display("[ICACHE TO INSTBUFFER]time: %d\tfetch_res[%d].pc: %h\t valid: %d\tinst: %h", $time, i, fetch_res[i].pc, fetch_res[i].valid, fetch_res[i].inst);
    end
    `endif
    next_instruction_buffer = instruction_buffer;
    next_head = `check_subtraction(head + dispatch_count, `INST_BUFFER_SZ);
    next_size = size + valid_count - dispatch_count;
    for (integer i = 0; i < `N; i++) begin
      if (fetch_res[i].valid && (size + i) < `INST_BUFFER_SZ) begin
        next_instruction_buffer[`check_subtraction(head + size + i, `INST_BUFFER_SZ)] = fetch_res[i];
      end
    end
    for (integer i = 0; i < `N; i++) begin
      if (i < dispatch_count) begin
        next_instruction_buffer[`check_subtraction(head + i, `INST_BUFFER_SZ)].valid = 0;
      end
    end
  end

  always_ff @(posedge clock) begin
    // $display("dispatch_count: %p available_slot_num: %p size: %d",dispatch_count,available_slot_num,size);
    
    if (reset || rollback) begin
      instruction_buffer <= 0;
      head <= 0;
      size <= 0;
    end else begin
      instruction_buffer <= next_instruction_buffer;
      head <= next_head;
      size <= next_size;
    end
    `ifdef DEBUG
    for (int i = 0; i < `INST_BUFFER_SZ; i++) $display("time: %d\ti: %d\tpc: %d\tvalid: %d\tinst: %h", $time, i, instruction_buffer[i].pc,instruction_buffer[i].valid, instruction_buffer[i].inst);
    `endif
  end

endmodule