`include "verilog/sys_defs.svh"
`include "verilog/ISA.svh"

/* 
Use this in pipeline:
mem_input: RS_INPUT generate from dispatch
mem_dispatch_lsq: input for the lsq
*/

module mem_dispatch(
    input RS_INPUT mem_input,
    output LSQ_REQUEST_BASE mem_dispatch_lsq
);

    MEM_OP_T mem_op;
    MEM_SIZE mem_size;
    LSQ_REQUEST_BASE next_lsq_req_base;

    always_comb begin : size_sel
        mem_op = (mem_input.inst.func == MEM_RD) ? MEM_READ : MEM_WRITE;
        mem_size = '0;

        `ifdef DEBUG
        if(mem_input.valid) begin
          $display("time: [%d]\tNow I suspect the inst in is broken:%b", $time, mem_input.inst.inst);
        end
        `endif


        if(mem_input.inst.func == MEM_RD) begin
          `ifdef DEBUG
          $display("time: [%d]\tcurrent inst is:%h, funt3 is: %b", $time, mem_input.inst.inst, mem_input.inst.inst.i.funct3);
          `endif
          case(mem_input.inst.inst.i.funct3)
          // LB
          3'b000:begin             mem_size = BYTE;
          // $display("Do you know I'm LB?");
          end

          // LH
          3'b001:             mem_size = HALF;
          // LW
          3'b010:begin             mem_size = WORD;
          // $display("Do you know I'm LW?");
          end
          // LBU
          3'b100:             mem_size = BYTE;
          // LHU
          3'b101:             mem_size = HALF;
          endcase
        end else if(mem_input.inst.func == MEM_WR) begin
          case(mem_input.inst.inst.s.funct3)
          // SB
          3'b000:             mem_size = BYTE;
          // SH
          3'b001:             mem_size = HALF;
          // SW
          3'b010:             mem_size = WORD;
          endcase
        end
    end


    always_comb begin
      if(mem_input.inst.fu == MEM && mem_input.valid) begin
        mem_dispatch_lsq = {
          rob_idx: mem_input.rob_ref.rob_index,
          valid: 1,
          mem_op: mem_op,
          size: mem_size,
          is_ld_signed: mem_input.inst.is_ld_signed
        };
      end else begin
        mem_dispatch_lsq = '0;
      end
    end

endmodule

module mem_issue(
  input MEM_INPUT mem_input,
  output LSQ_ISSUE_REQUEST lsq_issue_in
);

  LSQ_REQUEST_BASE lsq_base;

  mem_dispatch mem0 (
    .mem_input(mem_input.rs_entry),
    .mem_dispatch_lsq(lsq_base)
  );

// Store data (RS2_value) gives to LSQ should be ready (issued)

  logic [`XLEN-1:0] opa_mux_out;
  logic [`XLEN-1:0] opb_mux_out;
  REG_DATA_T mem_data;
  MEM_ADDR_T mem_addr;

  always_comb begin: drive_mux_out
    `ifdef DEBUG
    $display("Here is mem_issue, maybe I got the wrong thing: %p", mem_input.rs_entry.inst.inst);
    `endif


    opa_mux_out = `XLEN'hdeadbeef;
    opb_mux_out = `XLEN'hdeadbeef;

    // ST and LD all have RS1
    opa_mux_out = mem_input.phy_reg_val_a;
    case (mem_input.opb_select)
      OPB_IS_I_IMM: opb_mux_out = `RV32_signext_Iimm(mem_input.inst); // LD have I_IMM
      OPB_IS_S_IMM: opb_mux_out = `RV32_signext_Simm(mem_input.inst); // ST have RS2 and Simm
    endcase
    `ifdef DEBUG
    $display("time: [%d]\tHere is mem_addr_calculation, opa_mux_out: %d, opb_mux_out: %d", $time, opa_mux_out, opb_mux_out);
    `endif
    mem_addr = opa_mux_out + opb_mux_out; // Calculate the memeory address for LSQ
    mem_data = mem_input.phy_reg_val_b;
    `ifdef DEBUG
    $display("time: [%d]\tHere is mem issue, the mem_addr is: %h", $time, mem_addr);
    `endif
  end

  always_comb begin : Assign
    lsq_issue_in = '0;
    if(mem_input.valid) begin
      lsq_issue_in = {
        basic_info: lsq_base,
        memory_addr: mem_addr,
        data: mem_data,
        dst_reg_idx: mem_input.dst_reg_idx,
        pc: mem_input.pc
      };
    end
  end

endmodule