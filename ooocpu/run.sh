#! /bin/bash
# if [ -z $1 ]; then
#     echo 1 > /dev/null;
# else
#     make nuke;
# fi
make simv;
mem="mergesort.mem";
echo "./simv +MEMORY=programs/${mem}";
./simv +MEMORY=programs/alexnet.mem > result.log +PIPELINE=result.ppln &
command_pid=$!;
sleep 30;
kill -9 $command_pid;