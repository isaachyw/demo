import os
import sys

def extract_p3_wb_pc(file:str):
    with open(file,'r') as fd:
        with open('retire_pc.log','w') as wf:
            for l in fd:
                content = l.split('|')
                if(len(content)>4):
                    result=content[4].split(":")[0]
                    if len(content[4].split(":"))>1:
                        if not content[4].split(":")[1].startswith('-'):
                            wf.write(content[4].split(":")[0])
                            wf.write('\n')
                # print(content[1])
                # wf.write(content)

def parse_mem():
    ourfile = "result.log"
    p3correctfile = sys.argv[1]
    with open("ourMem.log",'w') as wf:
        with open(ourfile, 'r') as rf:
            for line in rf:
                if line.find("write cache addr") != -1:
                    addr = line.split("addr:")[1].split("val")[0].strip()
                    pc = line.split("ppc:")[1].split("write cache")[0].strip()
                    idx = 0
                    for s in pc:
                        if s == '0': idx+=1
                        else: break
                    pc  = pc[idx:]
                    val = line.split("val:")[1].split("dcache")[0].strip()
                    wf.write(f"addr:{addr} pc:{pc} val:{val}\n")

    with open("p3Mem.log",'w') as wf:
        with open(p3correctfile, 'r') as rf:
            for line in rf:
                if line.find("BUS_STORE") != -1:
                    tmp = line.split("BUS_STORE")[1].split("accepted")[0].strip()
                    addr = tmp.split("[")[1].split("]")[0].strip()
                    val = tmp.split("=")[1].strip()
                    pc = hex(int(line.split("|")[-2].split(":")[0]) - 4)[2:].strip()
                    wf.write(f"addr:{addr} pc:{pc} val:{val}\n")

                    

def main():
    extract_p3_wb_pc(sys.argv[1])

with open ("retireInst.log",'w') as wf:
    with open("result.log", 'r') as file:
        for line in file:
            if line.find("retired inst") != -1:

                tmp = int(line.split("|")[1].split(":")[1])
                wf.write(f'{tmp }\n')


main()
# parse_mem()