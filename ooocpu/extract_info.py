import os

def extract_reg5():
    reg5ValList = []
    with open("result.log", 'r') as reslog:
        for line in reslog:
            if line.find("REG[          5]") != -1:
                tmp = line.split("=")[1].split(" ")[0]
                if tmp.find("x") == -1:
                    reg5ValList.append(tmp)

    with open("reg5.log", 'w') as reslog:
        prev = reg5ValList[0]
        reslog.write(f'{reg5ValList[0]}\n')
        for i in range(1, len(reg5ValList)):
            if prev != reg5ValList[i]:
                reslog.write(f'{reg5ValList[i]}\n')
            prev = reg5ValList[i]

def extract_reg11(x):
    reg5ValList = []
    with open("result.log", 'r') as reslog:
        for line in reslog:
            if line.find(f"REG[         11]") != -1:
                tmp = line.split("=")[1].split(" ")[0]
                if tmp.find("x") == -1:
                    print(tmp)
                    reg5ValList.append(tmp)

    with open(f"reg{x}.log", 'w') as reslog:
        prev = reg5ValList[0]
        reslog.write(f'{reg5ValList[0]}\n')
        for i in range(1, len(reg5ValList)):
            if prev != reg5ValList[i]:
                reslog.write(f'{reg5ValList[i]}\n')
            prev = reg5ValList[i]
            
def extract_reg11_correct():
    regValList = []
    with open("alll_reg_mult.log", 'r') as reslog:
        for line in reslog:
            if line.find("REG[11]") != -1:
                tmp = line.split("REG[11]=")[1]
                if tmp.find("x") == -1:
                    print(tmp)
                    regValList.append(tmp)

    with open("reg11.log", 'w') as reslog:
        prev = regValList[0]
        reslog.write(f'{regValList[0]}\n')
        for i in range(1, len(regValList)):
            if prev != regValList[i]:
                reslog.write(f'{regValList[i]}\n')
            prev = regValList[i]
        
def extract_reg2():
    reg5ValList = []
    with open("result.log", 'r') as reslog:
        for line in reslog:
            if line.find(f"REG[          2]") != -1:
                tmp = line.split("=")[1].split(" ")[0]
                if tmp.find("x") == -1:
                    print(tmp)
                    reg5ValList.append(tmp)

    with open(f"reg2.log", 'w') as reslog:
        prev = reg5ValList[0]
        reslog.write(f'{reg5ValList[0]}\n')
        for i in range(1, len(reg5ValList)):
            if prev != reg5ValList[i]:
                reslog.write(f'{reg5ValList[i]}\n')
            prev = reg5ValList[i]

def main():
    extract_reg5()
    
if __name__ == "__main__":
    main()