import asyncio
import argparse
import csv
import subprocess
import json
from dataclasses import dataclass, asdict, fields

@dataclass
class ParameterizedExecution:
    n: int
    rob_sz: int
    rs_sz: int
    target: str
    cpi: str

csv_f = open('matrix_report.csv', 'w')
csv_w = csv.DictWriter(csv_f, [fld.name for fld in fields(ParameterizedExecution)])
csv_w.writeheader()

async def run_cmd(cmd, checkRet=True):
    print(f'[Dispatch] Execute shell {cmd}')
    proc = await asyncio.create_subprocess_shell(cmd, stderr=subprocess.STDOUT)
    try:
        await asyncio.wait_for(proc.wait(), timeout=360.0)
    except asyncio.exceptions.TimeoutError:
        print(f'[ERROR] Timeout for: {cmd}')
        raise Exception()
    if checkRet and proc.returncode != 0:
        print(f'[ERROR] Non-zero return code for: {cmd} (ret: {proc.returncode})')
        raise Exception()

def grab_cpi(target):
    with open(f'output/{target}.out') as f:
        for line in f:
            if '@@' in line and 'CPI' in line:
                cpi = line.split(' ')[-2]
                try:
                    if float(cpi) > 100.00:
                        return 'TIMEOUT'
                    else:
                        return cpi
                except Exception:
                    print(f'[ERROR] error parsing cpi line: {line}')
                    return cpi
        return 'ERROR/TIMEOUT'

async def run_config(n, rob, rs, file, dir):
    try:
        await run_cmd(f'cd {dir} && ./simv +MEMORY=programs/{file}.mem +WRITEBACK=output/{file}.wb +PIPELINE=output/{file}.ppln > output/{file}.out')
        cpi = grab_cpi(file)
    except Exception as exc:
        cpi = 'ERROR/TIMEOUT'
    return ParameterizedExecution(n, rob, rs, file, cpi)

async def setup_dir(name):
    await run_cmd(f'mkdir -p {name}')
    await run_cmd(f'cp -r ./programs {name}/programs')
    await run_cmd(f'cp -r ./output {name}/output')
    await run_cmd(f'cp -r ./test {name}/test')
    await run_cmd(f'cp -r ./verilog {name}/verilog')
    await run_cmd(f'cp ./Makefile {name}/Makefile')


async def execute_test_cases(n, rob, rs, cases):
    futures=[]
    local_dir = f'sub_matrix/{n}-{rob}-{rs}'
    await setup_dir(local_dir)
    await run_cmd(f'cd {local_dir} && make simv --always-make VCS_MATRIX_CMD="+define+N={n} +define+ROB_SZ={rob} +define+RS_SZ={rs}"')
    for target in cases:
        fut = run_config(n, rob, rs, target, local_dir)
        futures.append(fut)
    return await asyncio.gather(*futures)

async def compile_test_cases(cases):
    futures=[]
    for target in cases:
        futures.append(run_cmd(f'make programs/{target}.mem'))
    await asyncio.gather(*futures)

@dataclass
class MatrixCommand:
    n: list
    rob_sz: list
    rs_sz: list
    targets: list

async def execute_matrix_unit(n, rob, rs):
    print(f'[MATRIX] Execute matrix for N={n}, ROB_SZ={rob} RS_SZ={rs}')
    results = await execute_test_cases(n, rob, rs, TEST_CASE)
    csv_w.writerows([asdict(entry) for entry in results])
    csv_f.flush()
    print(f'[MATRIX] Finished matrix for N={n}, ROB_SZ={rob} RS_SZ={rs}')

async def execute_matrix(mtx):
    await run_cmd('rm -rf sub_matrix', checkRet=False)
    await compile_test_cases(mtx.targets)
    futures=[]
    for n in mtx.n:
        for rob in mtx.rob_sz:
            for rs in mtx.rs_sz:
                futures.append(execute_matrix_unit(n, rob, rs))
    await asyncio.gather(*futures)

def write_config(path, mtx):
    with open(path, 'w') as f:
        f.write(json.dumps(asdict(mtx)))

async def spread_matrix(mtx):
    await run_cmd('rm -rf sub_matrix')
    for n in mtx.n:
        await run_cmd(f'mkdir -p sub_matrix/mtx_{n}')
        await run_cmd(f'cp -R ./programs sub_matrix/mtx_{n}/programs')
        await run_cmd(f'cp -R ./verilog sub_matrix/mtx_{n}/verilog')
        await run_cmd(f'cp -R ./test sub_matrix/mtx_{n}/test')
        await run_cmd(f'cp ./Makefile sub_matrix/mtx_{n}/Makefile')
        await run_cmd(f'cp ./profile.py sub_matrix/mtx_{n}/profile.py')
        sub_mtx = MatrixCommand(n, mtx.rob_sz, mtx.rs_sz, mtx.targets)
        write_config(f'sub_matrix/mtx_{n}/profile_cmd.json', sub_mtx)
        print(f'[SPREAD] finish matrix setup for {n}')
    
    futures=[]
    for n in mtx.n:
        futures.append(run_cmd(f'cd sub_matrix/mtx_{n} && python profile.py --use_config'))
    await asyncio.gather(**futures)


normal_prog = ['backtrack',
'basic_malloc',
'bfs',
'branch_take']
# 'branch',
# 'dft',
# 'fc_forward',
# 'forward',
# 'graph',
# 'haha',
# 'insertionsort',
# 'matrix_mult_rec',
# 'mergesort']

rv32_prog = ['rv32_copy',
'rv32_copy_long',
'rv32_evens',
'rv32_evens_long',
'rv32_fib',
'rv32_fib_rec',
'rv32_fib_long',
'rv32_btest1',
'rv32_btest2',
'rv32_insertion',
'rv32_mult',
'rv32_parallel',
'rv32_saxpy']

N=[2]
ROB_SZ=[ 8]
RS_SZ=[4]
TEST_CASE=rv32_prog

def arg_setup():
    argP = argparse.ArgumentParser()
    argP.add_argument('--use_config', action='store_true')
    # argP.add_argument('--spread_n', action='store_true')
    return argP.parse_args()

async def main():
    config = arg_setup()
    print(f'launch with config: {config}')

    if config.use_config:
        with open(f'profile_cmd.json') as f:
            mtx = MatrixCommand(**json.load(f))
    else:
        mtx = MatrixCommand(N, ROB_SZ, RS_SZ, normal_prog)
    
    # if config.spread_n:
    if False:
        await spread_matrix(mtx)
    else:
        await execute_matrix(mtx)

asyncio.run(main())

