import time
import threading

def main():
    print("main starting...")
    threads = []
    thread = threading.Thread(target = worker)
    threads.append(thread)
    thread.start()
    print("main do work")
    print("wait for exit")
    
    print("main down")
    
def worker():
    print("worker starting...")
    time.sleep(1)
    print("worker down")
    
    
if __name__ == "__main__":
    main()
