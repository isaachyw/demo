"""Utils package.
This package is for code shared by the Manager and the Worker.
"""
from mapreduce.utils.__main__ import (send_tcp_message)
# Configure logging
LOGGER = logging.getLogger(__name__)


def send_tcp_message(host, port, message):
    """Send a JSON message over TCP."""
    message_json = json.dumps(message)
    message_data = message_json.encode()

    # Send the message
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((host, port))
        sock.sendall(message_data)


# def recv_tcp_message(sock, worker):
#     """Receive a JSON message over TCP."""
#     try:
#         message_data = b""
#         while True:
#             try:
#                 data = sock.recv(4096)
#             except sock.timeout:
#                 continue
#             if not data:
#                 break
#             message_data += data
#
#         if not message_data:
#             return None
#
#         # Decode and return the message
#         message_json = message_data.decode()
#         message = json.loads(message_json)
#         return message
#     except (ConnectionError, ValueError, struct.error) as e:
#         LOGGER.error("Error in recv_tcp_message: %s", e)
#         return None
