"""Utils package.
This package is for code shared by the Manager and the Worker.
"""
from mapreduce.utils.__main__ import (send_tcp_message)
