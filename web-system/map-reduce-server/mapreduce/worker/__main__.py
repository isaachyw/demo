"""MapReduce framework Worker node."""
import glob
import os
import logging
import json
import tempfile
import time
import click
import hashlib
import mapreduce.utils
import shutil
from pathlib import Path
import subprocess
import threading
import socket
import heapq

# Configure logging
LOGGER = logging.getLogger(__name__)


class Worker:
    """A class representing a Worker node in a MapReduce cluster."""

    def __init__(self, host, port, manager_host, manager_port):
        """Construct a Worker instance and start listening for messages."""

        # Variable Initialization
        self.host = host
        self.port = port
        self.manager_host = manager_host
        self.manager_port = manager_port
        self.registered = False
        self.working = True
        self.heartbeat_thread = threading.Thread(target=self.send_heartbeat, daemon=True)

        LOGGER.info(
            "Starting worker host=%s port=%s pwd=%s",
            host, port, os.getcwd(),
        )
        LOGGER.info(
            "manager_host=%s manager_port=%s",
            manager_host, manager_port,
        )

        # Send the register message to the manager
        register_message = {
            "message_type": "register",
            "worker_host": self.host,
            "worker_port": self.port,
        }

        mapreduce.utils.send_tcp_message(self.manager_host, self.manager_port, register_message)

        # Create a new TCP socket and start listening for messages
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as listener_socket:
            listener_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            listener_socket.bind((self.host, self.port))
            listener_socket.listen()

            listener_socket.settimeout(1)

            while self.working:
                try:
                    clientsocket, address = listener_socket.accept()
                except socket.timeout:
                    continue

                clientsocket.settimeout(1)

                with clientsocket:
                    message_chunks = []
                    while True:
                        try:
                            data = clientsocket.recv(4096)
                        except socket.timeout:
                            continue
                        if not data:
                            break
                        message_chunks.append(data)

                # Decode list-of-byte-strings to UTF8 and parse JSON data
                message_data = b"".join(message_chunks)
                message_str = message_data.decode("utf-8")

                try:
                    message_dict = json.loads(message_str)
                except json.JSONDecodeError:
                    continue

                self.tcp_handler(message_dict)

            if self.registered:
                self.heartbeat_thread.join()

    def tcp_handler(self, message_dict):
        """Handle a message received over TCP."""
        message_type = message_dict["message_type"]
        if message_type == "shutdown":
            self.working = False
            LOGGER.info("Shutting down")
        elif message_type == "register_ack":
            self.registered = True
            self.heartbeat_thread.start()
            LOGGER.info("Registered with manager")
        elif message_type == "new_map_task":
            self.map_handler(message_dict)
        elif message_type == "new_reduce_task":
            self.reduce_handler(message_dict)

    def send_heartbeat(self):
        """Send an udp heartbeat message to the manager."""
        heartbeat_message = {
            "message_type": "heartbeat",
            "worker_host": self.host,
            "worker_port": self.port,
        }

        while self.working:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                sock.connect((self.manager_host, self.manager_port))
                message = json.dumps(heartbeat_message)
                sock.sendall(message.encode('utf-8'))
            time.sleep(2)

    def map_handler(self, message):
        """Handle a map task."""
        LOGGER.info("Starting map task")
        task_id = message["task_id"]
        num_partitions = message["num_partitions"]
        with tempfile.TemporaryDirectory(prefix=f"mapreduce-local-task{task_id:05d}-") as tmpdir:
            LOGGER.info("Created temporary directory %s", tmpdir)
            tmpdir_path = Path(tmpdir)
            output_dir_path = Path(message["output_directory"])

            tmp_paths = []
            output_paths = []

            for partition in range(num_partitions):
                tmp_file_name = f"maptask{task_id:05d}-part{partition:05d}"
                output_file_name = f"maptask{task_id:05d}-part{partition:05d}"

                tmp_file_path = tmpdir_path / tmp_file_name
                output_file_path = output_dir_path / output_file_name

                tmp_paths.append(tmp_file_path)
                output_paths.append(output_file_path)

            # Open the temporary files for writing
            tmp_files = [file.open('a') for file in tmp_paths]

            # Process each input file
            for i in range(len(message["input_paths"])):
                input_path = Path(message["input_paths"][i])
                LOGGER.info("Processing input file %s", input_path)

                with input_path.open('r') as input_file:
                    with subprocess.Popen(
                            [message["executable"]],
                            stdin=input_file,
                            stdout=subprocess.PIPE,
                            text=True,
                    ) as map_process:
                        for line in map_process.stdout:
                            # Extract the key from the line
                            key = line.partition('\t')[0]

                            # Compute the partition number using key hashing
                            hexdigest = hashlib.md5(key.encode("utf-8")).hexdigest()
                            keyhash = int(hexdigest, base=16)
                            partition_number = keyhash % num_partitions

                            # Write the line to the corresponding partition file
                            tmp_files[partition_number].write(line)

            # Close the temporary files
            for file in tmp_files:
                file.close()

            # Sort each partition file and move them to the output directory
            for i in range(num_partitions):
                tmp_file_path = tmp_paths[i]
                output_file_path = output_paths[i]

                # Open the partition file for reading and the output file for writing
                with tmp_file_path.open('r') as tmp_file, output_file_path.open('w') as output_file:
                    LOGGER.info("Sorting partition %d", i)

                    # Sort the lines in the partition file and write them to the output file
                    for line in sorted(tmp_file):
                        output_file.write(line)

                # Remove the partition file
                tmp_file_path.unlink()

            LOGGER.info("Finished map task")
            finish_message = {
                "message_type": "finished",
                "task_id": task_id,
                "worker_host": self.host,
                "worker_port": self.port,
            }

            mapreduce.utils.send_tcp_message(self.manager_host, self.manager_port, finish_message)

    def reduce_handler(self, message):
        """Handle a reduce task."""
        LOGGER.info("Starting reduce task")
        task_id = message["task_id"]
        with tempfile.TemporaryDirectory(prefix=f"mapreduce-local-task{task_id:05d}-") as tmpdir:
            LOGGER.info("Created temporary directory %s", tmpdir)
            tmpdir_path = Path(tmpdir)

            tmp_file_name = f"part-{task_id:05d}"
            output_file_name = f"part-{task_id:05d}"

            tmp_file_path = tmpdir_path / tmp_file_name
            output_file_path = tmpdir_path / output_file_name

            # Merge sorted input files and write to temporary file
            with tmp_file_path.open('w') as tmp_file:
                input_paths = list(Path(path) for path in message["input_paths"])
                input_files = [path.open('r') for path in input_paths]

                # Merge the input files into instream
                instream = heapq.merge(*input_files)

            # Open the temporary file for reading and the output file for writing
            with tmp_file_path.open('r') as tmp_file, output_file_path.open('a+') as output_file:
                LOGGER.info("Running reduce executable")

                # Run the reduce executable on the merged input and write the output to the output file
                with subprocess.Popen(
                        [message["executable"]],
                        stdin=subprocess.PIPE,
                        stdout=output_file,
                        text=True,
                ) as reduce_process:
                    # Pipe the merged input to the reduce executable
                    for line in instream:
                        reduce_process.stdin.write(line)

            # After finishing writing, copy the files from the temporary directory to the output_directory
            for path in glob.glob(f"{tmpdir}/*"):
                LOGGER.info("Copying file %s", path)
                shutil.copy(path, message["output_directory"])

            LOGGER.info("Finished reduce task")
            finish_message = {
                "message_type": "finished",
                "task_id": task_id,
                "worker_host": self.host,
                "worker_port": self.port,
            }

            mapreduce.utils.send_tcp_message(self.manager_host, self.manager_port, finish_message)


@click.command()
@click.option("--host", "host", default="localhost")
@click.option("--port", "port", default=6001)
@click.option("--manager-host", "manager_host", default="localhost")
@click.option("--manager-port", "manager_port", default=6000)
@click.option("--logfile", "logfile", default=None)
@click.option("--loglevel", "loglevel", default="info")
def main(host, port, manager_host, manager_port, logfile, loglevel):
    """Run Worker."""
    if logfile:
        handler = logging.FileHandler(logfile)
    else:
        handler = logging.StreamHandler()
    formatter = logging.Formatter(f"Worker:{port} [%(levelname)s] %(message)s")
    handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    root_logger.setLevel(loglevel.upper())
    Worker(host, port, manager_host, manager_port)


if __name__ == "__main__":
    main()
