import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import InfiniteScroll from "react-infinite-scroll-component";
import Post from "./post";

function Scroll(props) {
  const [allPosts, setAllPosts] = useState([]);
  const [next, setNext] = useState("");
  const [allLength, setAllLength] = useState(0);
  const [hasMore, setHasMore] = useState(true);

  useEffect(() => {
    const { url } = props;
    fetch(url, { credentials: "same-origin" })
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then((data) => {
        const posts = data.results.map((post) => (
          <Post url={post.url} key={post.postid} />
        ));
        setAllPosts(posts);
        setAllLength(data.results.length);
        setHasMore(true);
        setNext(data.next);
      })
      .catch((error) => console.log(error));
  }, [props]);

  const handleScroll = () => {
    if (next !== "") {
      fetch(next, { credentials: "same-origin" })
        .then((response) => {
          if (!response.ok) throw Error(response.statusText);
          return response.json();
        })
        .then((data) => {
          const posts = data.results.map((post) => (
            <Post url={post.url} key={post.postid} />
          ));
          setAllPosts([...allPosts, ...posts]);
          setAllLength(allLength + data.results.length);
          setHasMore(true);
          setNext(data.next);
        })
        .catch((error) => console.log(error));
    } else {
      setHasMore(false);
    }
  };

  return (
    <InfiniteScroll
      dataLength={allLength}
      next={handleScroll}
      hasMore={hasMore}
      loader={<h4>Loading...</h4>}
    >
      <div className="posts">{allPosts}</div>
    </InfiniteScroll>
  );
}

Scroll.propTypes = {
  url: PropTypes.string.isRequired,
};

export default Scroll;
