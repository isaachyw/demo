import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import moment from "moment";

function PostHeader({ owner, ownerLink, ownerImgUrl, postLink, time }) {
  return (
    <div className="post_header">
      <div className="post_user">
        <a href={ownerLink}>
          <div style={{ float: "left" }}>
            <img src={ownerImgUrl} className="post_icon" alt="post_icon" />
          </div>
          <div style={{ float: "left", padding_left: "10px" }}>
            <p>{owner}</p>
          </div>
        </a>
      </div>
      <div className="post_time">
        <a href={postLink}>
          <p>{time}</p>
        </a>
      </div>
    </div>
  );
}

function LikeWord({ likeNum }) {
  if (likeNum === 1) {
    return <div className="post_likes">1 like</div>;
  }
  return <div className="post_likes">{likeNum} likes</div>;
}

// The parameter of this function is an object with a string called url inside it.
// url is a prop for the Post component.
export default function Post({ url }) {
  /* Display image and post owner of a single post */

  const [imgUrl, setImgUrl] = useState("");
  const [comments, setComments] = useState([]);
  const [isLike, setIsLike] = useState(false);
  const [numLike, setNumLike] = useState(0);
  const [likeUrl, setLikeUrl] = useState("");
  const [text, setText] = useState("");
  const [owner, setOwner] = useState("");
  const [ownerUrl, setOwnerUrl] = useState("");
  const [ownerImg, setOwnerImg] = useState("");
  const [postUrl, setPostUrl] = useState("");
  const [postTime, setPostTime] = useState("");
  const [postid, setPostid] = useState(0);

  useEffect(() => {
    // Declare a boolean flag that we can use to cancel the API request.
    let ignoreStaleRequest = false;

    // Call REST API to get the post's information
    fetch(url, { credentials: "same-origin" })
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then((data) => {
        // If ignoreStaleRequest was set to true, we want to ignore the results of the
        // the request. Otherwise, update the state to trigger a new render.
        if (!ignoreStaleRequest) {
          setImgUrl(data.imgUrl);
          setComments(data.comments);
          setIsLike(data.likes.lognameLikesThis);
          setNumLike(data.likes.numLikes);
          setLikeUrl(data.likes.url);
          setOwner(data.owner);
          setOwnerUrl(data.ownerShowUrl);
          setOwnerImg(data.ownerImgUrl);
          setPostUrl(data.postShowUrl);
          setPostTime(moment(data.created).fromNow());
          setPostid(data.postid);
        }
      })
      .catch((error) => console.log(error));

    return () => {
      // This is a cleanup function that runs whenever the Post component
      // unmounts or re-renders. If a Post is about to unmount or re-render, we
      // should avoid updating state.
      ignoreStaleRequest = true;
    };
  }, [url]);

  function handleLikeClick() {
    if (isLike) {
      // Change to false
      setNumLike((n) => n - 1);
    } else {
      setNumLike((n) => n + 1);
    }
  }

  function handleLikeSubmit(e) {
    e.preventDefault();
    if (isLike) {
      // Change to false
      const deleteRequestOpt = {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      };

      fetch(likeUrl, deleteRequestOpt)
        .then((response) => {
          if (!response.ok) throw Error(response.statusText);
          setIsLike(false);
          setLikeUrl("");
        })
        .catch((error) => console.log(error));
    } else {
      const postRequestOpt = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const postLikeUrl = "/api/v1/likes/?postid=".concat(postid);
      fetch(postLikeUrl, postRequestOpt)
        .then((response) => {
          if (!response.ok) throw Error(response.statusText);
          setIsLike(true);
          return response.json();
        })
        .then((data) => {
          setLikeUrl(data.url);
        })
        .catch((error) => console.log(error));
    }
  }

  function handleDoubleClick() {
    if (!isLike) {
      // Change to true
      setNumLike((n) => n + 1);
      const postRequestOpt = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const postLikeUrl = "/api/v1/likes/?postid=".concat(postid);
      fetch(postLikeUrl, postRequestOpt)
        .then((response) => {
          if (!response.ok) throw Error(response.statusText);
          setIsLike(true);
          return response.json();
        })
        .then((data) => {
          setLikeUrl(data.url);
        })
        .catch((error) => console.log(error));
    }
  }

  const handleDeleteClick = (commentUrl, id) => {
    const deleteCommentsUrl = "/api/v1/comments/".concat(id).concat("/");
    fetch(deleteCommentsUrl, { credentials: "same-origin", method: "DELETE" })
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
      })
      .then(() => {
        setComments(comments.filter((c) => c.commentid !== id));
      })
      .catch((error) => console.log(error));
  };

  const postCommentsUrl = "/api/v1/comments/?postid=".concat(postid);

  const handleCommentChange = (e) => {
    setText(e.target.value);
  };

  const handleCommentSubmit = (e) => {
    fetch(postCommentsUrl, {
      credentials: "same-origin",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ text }),
    })
      .then((response) => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then((data) => {
        setComments([...comments, data]);
        setText("");
      })
      .catch((error) => console.log(error));

    e.preventDefault();
  };

  const commentRows = comments.map((cm) => (
    <div className="post_single_comment" key={cm.commentid}>
      <div className="post_single_comment_name">
        <a href={cm.ownerShowUrl}>{cm.owner}</a>
      </div>
      <div className="comment-text">{cm.text}</div>
      <div>
        {cm.lognameOwnsThis ? (
          <button
            type="button"
            className="delete-comment-button"
            onClick={() => handleDeleteClick(cm.url, cm.commentid)}
          >
            Delete comment
          </button>
        ) : null}
      </div>
    </div>
  ));

  // Render post image and post owner
  return imgUrl !== "" ? (
    <div className="post">
      <PostHeader
        ownerLink={ownerUrl}
        ownerImgUrl={ownerImg}
        owner={owner}
        time={postTime}
        postLink={postUrl}
      />
      <img
        src={imgUrl}
        alt="post_image"
        className="post_img"
        onDoubleClick={handleDoubleClick}
      />
      <div className="post_comments">
        <LikeWord likeNum={numLike} />
        <form onSubmit={handleLikeSubmit}>
          <button
            onClick={handleLikeClick}
            className="like-unlike-button"
            type="submit"
          >
            {isLike ? "UNLIKE" : "LIKE"}
          </button>
        </form>

        {commentRows}
        <form className="comment-form" onSubmit={handleCommentSubmit}>
          <input type="text" value={text} onChange={handleCommentChange} />
        </form>
      </div>
    </div>
  ) : (
    <div> loading...</div>
  );
}

Post.propTypes = {
  url: PropTypes.string.isRequired,
};

PostHeader.propTypes = {
  owner: PropTypes.string.isRequired,
  ownerLink: PropTypes.string.isRequired,
  ownerImgUrl: PropTypes.string.isRequired,
  postLink: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
};

LikeWord.propTypes = {
  likeNum: PropTypes.number.isRequired,
};
