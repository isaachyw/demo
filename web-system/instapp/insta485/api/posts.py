"""REST API for posts."""
import flask
from flask import session, url_for
import insta485
from insta485.views.posts import get_likes, get_post_owner, \
    get_comments, get_img_url, get_owner_img_url
from insta485.views.index import login__, InvalidUsage


@insta485.app.route('/api/v1/posts/', methods=['GET'])
def get_newest_posts_json():
    """Return JSON of the newest posts."""
    check_login()

    postid_lte = flask.request.args.get("postid_lte", default=0, type=int)
    size = flask.request.args.get("size", default=10, type=int)
    page = flask.request.args.get("page", default=0, type=int)
    url = flask.request.full_path
    if postid_lte == 0 and size == 10 and page == 0:
        url = "/api/v1/posts/"

    # Connect to database
    connection = insta485.model.get_db()
    # Query database
    logname = session['username']

    if postid_lte == 0:
        cur = connection.execute(
            "SELECT p.postid "
            "FROM posts p, users u "
            "WHERE p.owner = u.username AND (p.owner in "
            "(select username2 as owner from following where username1 = ?) "
            "or owner = ?) "
            "order by p.postid desc;",
            (logname, logname,)
        )
    else:
        cur = connection.execute(
            "SELECT p.postid "
            "FROM posts p, users u "
            "WHERE p.owner = u.username AND (p.owner in "
            "(select username2 as owner from following where username1 = ?) "
            "or owner = ?) and p.postid <= ? "
            "order by p.postid desc;",
            (logname, logname, postid_lte,)
        )
    data = cur.fetchall()

    results, next_url = get_next_and_results(
        page, size, postid_lte, data)

    # Add database info to context
    context = {"next": next_url, "results": results, "url": url}

    return flask.jsonify(**context)


def get_next_and_results(page, size, postid_lte, data):
    """Return next url and results info in get_newest_posts_json()."""
    results = []
    next_url = ""
    posts_len = len(data)
    latest_postid = data[0]['postid']
    if page < 0 or size < 0:
        raise InvalidUsage("Bad Request", 400)
    if posts_len >= size * (page + 1):  # Have next page
        if postid_lte == 0:
            next_url = f"/api/v1/posts/?size={size}" \
                       f"&page={page + 1}&postid_lte={latest_postid}"
            results = data[size * page:size * (page + 1)]
        else:
            next_url = f"/api/v1/posts/?size={size}" \
                       f"&page={page + 1}&postid_lte={postid_lte}"
            for i in data:
                if i['postid'] <= postid_lte:
                    results.append(i)
                    if len(results) >= size * (page + 1):
                        break
            results = results[size * page:]
    else:
        if postid_lte == 0:
            results = data[:]
        else:
            for i in data:
                if i['postid'] <= postid_lte:
                    results.append(i)
            results = results[size * page:]

    for i in results:
        i['url'] = f"/api/v1/posts/{i['postid']}/"

    return results, next_url


def check_login():
    """Check login in API."""
    if 'username' not in flask.session:
        if flask.request.authorization:
            username = flask.request.authorization['username']
            password = flask.request.authorization['password']
            if username is not None and password is not None:
                login__(username, password, True)
        else:
            raise InvalidUsage("forbidden", 403)


@insta485.app.route('/api/v1/posts/<int:postid_url_slug>/', methods=['GET'])
def get_post_json(postid_url_slug):
    """Return JSON of the post on postid."""
    check_login()
    logname = flask.session['username']
    connection = insta485.model.get_db()
    cursor = connection.execute(
        "SELECT created FROM posts WHERE postid = ?",
        (postid_url_slug,)
    )
    post = cursor.fetchone()
    if not post:
        raise InvalidUsage("Not Found", 404)

    context = {
        "created": post['created'],
        "postid": postid_url_slug,
        "postShowUrl": f"/posts/{postid_url_slug}/",
        "owner": get_post_owner(postid_url_slug),
        "ownerImgUrl": url_for('get_img', filename=get_owner_img_url(
            get_post_owner(postid_url_slug))),
        "ownerShowUrl": f"/users/{get_post_owner(postid_url_slug)}/",
        "imgUrl": url_for('get_img',
                          filename=get_img_url(postid_url_slug)),
        "likes": get_likes(postid_url_slug, logname),
        "comments": get_comments(postid_url_slug, logname),
        "comments_url": f"/api/v1/comments/?postid={postid_url_slug}",
        "url": f"/api/v1/posts/{postid_url_slug}/"
    }
    return flask.jsonify(**context)
