"""REST API for comments."""
import hashlib
import flask
import insta485
from insta485.api.posts import check_login


@insta485.app.route('/api/v1/comments/', methods=['POST'])
def create_comments():
    """Add one comment to a post."""
    check_login()
    connection = insta485.model.get_db()
    cursor = connection.execute(
        "SELECT commentid FROM comments WHERE postid = ? AND owner = ?",
        (flask.request.args.get("postid"), flask.session.get("username"), )
    )
    # create the comment for the specific post. Return 201 on success.
    cursor = connection.execute(
        "INSERT INTO comments (postid, owner, text) VALUES (?, ?, ?)",
        (flask.request.args.get("postid"),
         flask.session.get("username"),
         flask.request.get_json()['text'], )
    )
    connection.commit()
    commentid = cursor.lastrowid
    context = {
        "commentid": commentid,
        "lognameOwnsThis": True,
        "owner": flask.session.get("username"),
        "ownerShowUrl": "/users/" + flask.session.get("username") + "/",
        "text": flask.request.get_json()['text'],
        "url": flask.request.path,
    }
    return flask.jsonify(**context), 201


@insta485.app.errorhandler(403)
def handle_forbidden(error):
    """Handle 403 error."""
    context = {
        "message": "Forbidden",
        "status_code": error.code
    }
    return flask.jsonify(**context), 403


@insta485.app.errorhandler(404)
def handle_segfault(error):
    """Handle segfault."""
    context = {
        "message": "Not found",
        "status_code": error.code
    }
    return flask.jsonify(**context), 404


def check_password(password_db, input_password) -> bool:
    """Check password."""
    algorithm = password_db.split('$')[0]
    salt = password_db.split('$')[1]
    hashed_password = password_db.split('$')[2]
    password_salted = salt + input_password
    hash_obj = hashlib.new(algorithm)
    hash_obj.update(password_salted.encode('utf-8'))
    result = hash_obj.hexdigest()
    return result == hashed_password


def verify_credentials(username: str, password):
    """Verify credentials."""
    connection = insta485.model.get_db()
    password_db = connection.execute(
        "SELECT password "  # * must have whitespace!
        "FROM users "
        "WHERE username = ?",
        (username,)
    ).fetchone()['password']
    if not check_password(password_db, password):
        flask.abort(403)


@insta485.app.route('/api/v1/comments/<commentid>/', methods=['DELETE'])
def delete_comment(commentid):
    """Delete comment."""
    connection = insta485.model.get_db()
    if flask.request.authorization:
        logname = flask.request.authorization['username']
        password = flask.request.authorization['password']
        verify_credentials(logname, password)
    elif 'username' in flask.session:
        logname = flask.session['username']
    else:
        flask.abort(403)

    owner = connection.execute(
        "SELECT owner "
        "FROM comments "
        "WHERE commentid = ?",
        (commentid,)
    ).fetchone()

    if not owner:
        flask.abort(404)
    if owner['owner'] != logname:
        flask.abort(403)
    connection.execute(
        "DELETE "
        "FROM comments "
        "WHERE commentid =?",
        (commentid,)
    )
    return '', 204
