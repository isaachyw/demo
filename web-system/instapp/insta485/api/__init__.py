"""Insta485 REST API."""

import flask
from insta485.api.misc import *
from insta485.api.comments import *
from insta485.api.posts import *
from insta485.api.likes import *
