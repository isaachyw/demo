"""REST API for posts."""
import flask
import insta485
from insta485.api.posts import check_login


@insta485.app.route('/api/v1/likes/', methods=['POST'])
def add_likes():
    """Change like state."""
    check_login()

    postid = flask.request.args.get('postid')
    connection = insta485.model.get_db()
    logname = flask.session['username']
    print(logname)
    cur = connection.execute(
        "SELECT likeid FROM likes WHERE "
        "owner = ? AND postid = ?;",
        (logname, postid,)
    ).fetchone()

    is_new_add = True
    if cur is not None:
        likeid = cur['likeid']
        is_new_add = False
    else:
        connection.execute(
            "INSERT INTO likes (owner, postid) "
            "VALUES (?, ?);",
            (logname, postid,)
        )
        cur = connection.execute(
            "SELECT likeid FROM likes WHERE "
            "owner = ? AND postid = ?;",
            (logname, postid,)
        ).fetchone()
        likeid = cur['likeid']

    like = {"likeid": likeid, "url": f"/api/v1/likes/{likeid}/"}
    if not is_new_add:
        return flask.make_response(flask.jsonify(**like), 200)
    return flask.make_response(flask.jsonify(**like), 201)


@insta485.app.route('/api/v1/likes/<int:likeid>/', methods=['DELETE'])
def delete_likes(likeid):
    """Change like state."""
    check_login()

    connection = insta485.model.get_db()
    logname = flask.session['username']

    cur = connection.execute(
        "SELECT * FROM likes WHERE "
        "likeid = ?;",
        (likeid,)
    ).fetchone()
    if cur is None:
        flask.abort(404)

    cur = connection.execute(
        "SELECT * FROM likes WHERE "
        "owner = ? AND likeid = ?;",
        (logname, likeid,)
    ).fetchone()

    if cur is None:
        flask.abort(403)
    else:
        connection.execute(
            "DELETE FROM likes WHERE "
            "likeid = ?;",
            (likeid, )
        )

    return flask.make_response(flask.jsonify({}), 204)
