"""Views, one for each Insta485 page."""
from insta485.views.index import *
from insta485.views.users import *
from insta485.views.explore import *
from insta485.views.accounts import *
from insta485.views.posts import *
from insta485.views.rapi import *
