"""Search main."""
# model.py
import logging
import threading
import heapq
import sqlite3
import search
import flask
import requests

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def dict_factory(cursor, row):
    """Convert database row objects to a dictionary keyed on column name.

    This is useful for building dictionaries which are then used to render a
    template.  Note that this would be inefficient for large queries.
    """
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}


def get_db():
    """Open a new database connection.

    Flask docs:
    https://flask.palletsprojects.com/en/1.0.x/appcontext/#storing-data
    """
    if 'sqlite_db' not in flask.g:
        db_filename = search.app.config['DATABASE_FILENAME']
        flask.g.sqlite_db = sqlite3.connect(str(db_filename))
        flask.g.sqlite_db.row_factory = dict_factory

        # Foreign keys have to be enabled per-connection.  This is an sqlite3
        # backwards compatibility thing.
        flask.g.sqlite_db.execute("PRAGMA foreign_keys = ON")

    return flask.g.sqlite_db


@search.app.teardown_appcontext
def close_db(error):
    """Close the database at the end of a request.

    Flask docs:
    https://flask.palletsprojects.com/en/1.0.x/appcontext/#storing-data
    """
    assert error or not error  # Needed to avoid superfluous style error
    sqlite_db = flask.g.pop('sqlite_db', None)
    if sqlite_db is not None:
        sqlite_db.commit()
        sqlite_db.close()


def fetch(url, query, weight, results):
    """Fetch query from given url."""
    response = requests.get(url, params={'q': query, 'w': weight}, timeout=5)
    response.raise_for_status()  # Raise exception if not 200
    response_data = response.json()
    # Extract 'docid' from the response and fetch document details
    documents = response_data['hits']
    #  sort the documents by score
    documents.sort(key=lambda x: x["score"], reverse=True)
    results.append(documents)


def get_document_details(doc_id):
    """Get summary info of given doc id."""
    connection = get_db()
    sql_query = "SELECT * FROM Documents WHERE docid = ?"
    cursor = connection.execute(sql_query, (doc_id,))
    documents = cursor.fetchall()
    return documents


def search_documents(query, weight):
    """Search documents from txt."""
    index_urls = search.app.config['SEARCH_INDEX_SEGMENT_API_URLS']
    results = []
    threads = [threading.Thread(
        target=fetch, args=(url, query, weight, results))
        for url in index_urls]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    # Merge results from all threads
    merged_results = heapq.merge(*results, key=lambda x: x["score"])
    merged_results = list(merged_results)
    merged_results.sort(key=lambda x: x["score"], reverse=True)
    top_results = merged_results[:10]

    # Get document details from the database
    doc_ids = [doc['docid'] for doc in top_results]
    documents = [get_document_details(doc_id)[0] for doc_id in doc_ids]

    return documents
