"""HTML index."""
from flask import request, render_template
import search
import search.model


@search.app.route('/', methods=['GET'])
def index():
    """Render index."""
    query = request.args.get("q", default=None, type=str)
    weight = request.args.get("w", default='0.5', type=str)
    # print all config in search.config from object config
    # for key, value in search.app.config.items():
    #     print(key, value)
    if query:
        results = search.model.search_documents(query, float(weight))
    else:
        results = []

    return render_template(
        "index.html", query=query, weight=weight, results=results)
