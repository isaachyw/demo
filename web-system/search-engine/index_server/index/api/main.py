"""Main API module."""
import os
import re
import math
from collections import defaultdict
from pathlib import Path
import flask
import index

STOPWORDS = set()
PAGERANK = {}


def load_index():
    """Initialize inverted index, stopwords, and pagerank."""
    index.app.config["INDEX_PATH"] = os.getenv(
        "INDEX_PATH", "inverted_index_1.txt")
    with open("index_server/index/stopwords.txt", encoding="UTF-8") as file:
        for line in file:
            STOPWORDS.add(line.strip())
    with open(
            "index_server/index/pagerank.out", encoding="UTF-8") as file:
        for line in file:
            line = line.strip().split(',')
            PAGERANK[int(line[0])] = float(line[1])


@index.app.route("/api/v1/search", methods=["GET"])
def handle_service_available():
    """Handle service available."""
    context = {
        "hits": "/api/v1/hits/",
        "url": "/api/v1/"
    }
    return flask.jsonify(context), 200


@index.app.route("/api/v1/hits/", methods=["GET"])
def handle_hits():
    """Handle hits."""
    query = flask.request.args.get("q", default=None, type=str)
    query = query.split()
    query = [re.sub(r"[^a-zA-Z0-9 ]+", "", word) for word in query]
    query = [word.casefold() for word in query]
    query = [word.strip() for word in query if word.strip() not in STOPWORDS]
    # term  idfk di tf d    di tf d
    idf_query = []
    #  load the inverted index
    inverted_index_dict = {}
    with open(Path("index_server/index/inverted_index")
              / Path(index.app.config["INDEX_PATH"]),
              encoding="UTF-8") as file:
        for line in file:
            line = line.strip().split()
            inverted_index_dict[line[0]] = (line[1], line[2:])
    for word in query:
        if word in inverted_index_dict:
            idf_query.append(float(inverted_index_dict[word][0]))
        else:
            return flask.jsonify({"hits": []}), 200
    query_vector = [a * b for a,
                    b in zip([query.count(word) for word in query], idf_query)]
    docs_id = _generate_docs(query, inverted_index_dict)
    # find the term frequency of each document
    return flask.jsonify(**_fuckyou_plint(docs_id, query,
                                          inverted_index_dict, idf_query,
                                          query_vector)), 200


def _fuckyou_plint(docs_id, query, inverted_index_dict,
                   idf_query, query_vector):
    """Change code style."""
    tf_doc = defaultdict(list)  # {doc_id: [tf1, tf2, ...]}
    norm_doc = defaultdict(list)  # {doc_id: [norm1, norm2, ...]}
    for doc_id in docs_id:
        for word in query:
            for idx, val in enumerate(inverted_index_dict[word][1]):
                if idx % 3 == 0 and int(val) == doc_id:
                    tf_doc[doc_id].append(
                        int
                        (inverted_index_dict
                         [word][1][idx + 1]))
                    norm_doc[doc_id].append(
                        math.sqrt
                        (float(inverted_index_dict[word][1][idx + 2])))
    doc_vectors = defaultdict(list)
    for doc_id, _ in tf_doc.items():
        doc_vectors[doc_id] = [a * b for a,
                               b in zip(idf_query, tf_doc[doc_id])]
    # normalization
    query_norm = pow(sum(pow(float(x), 2) for x in query_vector), 0.5)
    tf_idf = defaultdict(float)
    pagerank_score = defaultdict(float)
    for doc_id in docs_id:
        tf_idf[doc_id] = _dot_product(
            [x / query_norm for x in query_vector],
            doc_vectors[doc_id]) / norm_doc[doc_id][0]
        pagerank_score[doc_id] = _score(doc_id, tf_idf[doc_id],
                                        flask.request.args.get(
                                            "w", default=0.5, type=float)
                                        )
    pagerank_score = dict(sorted(pagerank_score.items(),
                          key=lambda item: item[1], reverse=True))
    return _context(pagerank_score)


def _context(pagerank_score) -> set:
    """Generate return context."""
    context = defaultdict(list)
    for key, val in pagerank_score.items():
        context["hits"].append({"docid": key, "score": val})
    return context


def _dot_product(vector1, vector2) -> float:
    """Calculate the dot product of two vectors."""
    return sum(a * b for a, b in zip(vector1, vector2))


def _score(doc_id, tf_idf, weight) -> float:
    """Calculate the score of a document."""
    return (1 - weight) * tf_idf + weight * PAGERANK[doc_id]


def _generate_docs(query, inverted_index_dict) -> set:
    """Generate a set of documents."""
    single_doc = defaultdict(set)
    for word in query:
        if word in inverted_index_dict:
            for doc_id in inverted_index_dict[word][1][::3]:
                single_doc[word].add(int(doc_id))

    docs_id = set.intersection(*single_doc.values())
    return docs_id
