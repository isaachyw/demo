"""initialization file for the package."""
import flask

app = flask.Flask(__name__)


import index.api  # noqa: E402  pylint: disable=wrong-import-position

index.api.load_index()
# Load inverted index, stopwords, and pagerank into memory
