#!/usr/bin/env python3
"""Reduce 3."""
import sys
import itertools


def reduce_one_group(group):
    """Reduce one group."""
    last_word = ""
    for line in group:
        words = line.split()
        word = words[1]
        if last_word != word:
            if last_word == "":
                print(f'{words[1]} {words[-1]} '
                      f'{words[2]} {words[3]} {words[4]}', end='')
            else:
                # key idfk di tfik norm
                print(f'\n{words[1]} {words[-1]} '
                      f'{words[2]} {words[3]} {words[4]}', end='')
            last_word = word
        else:
            # di tfik norm
            print(f' {words[2]} {words[3]} {words[4]}', end='')


def keyfunc(line):
    """Return the key from a TAB-delimited key-value pair."""
    return line.partition("\t")[0]


def main():
    """Divide sorted lines into groups that share a key."""
    for _, group in itertools.groupby(sys.stdin, keyfunc):
        reduce_one_group(group)
    print("")


if __name__ == "__main__":
    main()
