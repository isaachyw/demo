#!/usr/bin/env python3
"""Map 0."""
import sys
import csv


if __name__ == "__main__":
    csv.field_size_limit(sys.maxsize)

    file = csv.reader(sys.stdin, delimiter=',')
    for row in file:
        print('1\t1')
