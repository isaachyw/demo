#!/usr/bin/env python3
"""Map 1."""
import sys
import csv
import re


if __name__ == "__main__":
    csv.field_size_limit(sys.maxsize)
    stopwords = {}
    with open("stopwords.txt", encoding="utf-8") as sw:
        for word in sw:
            w = word[:-1]
            stopwords[w] = 1

    s = csv.reader(sys.stdin, delimiter=',')
    for row in s:
        doc_id = row[0]
        text = row[1] + " " + row[2]
        text = re.sub(r"[^a-zA-Z0-9 ]+", "", text)
        text = text.casefold()

        text_list = text.split()
        for w in text_list:
            if not stopwords.get(w, None):
                print(f'{w}\t{doc_id}')
