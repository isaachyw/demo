#!/usr/bin/env python3
"""Map 3."""
import sys
import csv


if __name__ == "__main__":
    csv.field_size_limit(sys.maxsize)

    for line in sys.stdin:
        tl = line.split()
        key = int(tl[2]) % 3
        # key word ID tfik norm idfk
        print(f'{key}\t{tl[0]} {tl[2]} {tl[3]} {tl[4]} {tl[1]}')
