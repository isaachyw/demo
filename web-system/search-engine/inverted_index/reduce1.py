#!/usr/bin/env python3
"""Reduce 1."""
import sys
import itertools
import math


def reduce_one_group(key, group, total):
    """Reduce one group."""
    doc = {}
    for line in group:
        lis = line.partition("\t")
        if lis[0] == '\n':
            return 0
        doc_id = lis[2][:-1]
        if doc_id == '':
            return 0
        if doc.get(doc_id, None):
            doc[doc_id] += 1
        else:
            doc[doc_id] = 1
    n_i = len(doc)
    if n_i > 0:
        idfk = math.log(total/n_i, 10)
        for d_i in doc:
            tfik = doc.get(d_i)
            print(f'{d_i} {key} {tfik} {idfk}')
    return 1


def keyfunc(line):
    """Return the key from a TAB-delimited key-value pair."""
    return line.partition("\t")[0]


def main():
    """Divide sorted lines into groups that share a key."""
    total = 0
    with open('./total_document_count.txt',
              encoding="utf-8") as count_text:
        for line in count_text:
            lis = line.split()
            if lis[0] is not None:
                total = int(lis[0])

    for key, group in itertools.groupby(sys.stdin, keyfunc):
        reduce_one_group(key, group, total)


if __name__ == "__main__":
    main()
