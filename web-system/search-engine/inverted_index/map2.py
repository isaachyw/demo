#!/usr/bin/env python3
"""Map 2."""
import sys
import csv


if __name__ == "__main__":
    csv.field_size_limit(sys.maxsize)

    for line in sys.stdin:
        tl = line.split()
        wik = float(tl[2]) * float(tl[3])
        # ID word wik tfik norm
        print(f'{tl[0]}\t{tl[1]} {wik} {tl[2]} {tl[3]}')
