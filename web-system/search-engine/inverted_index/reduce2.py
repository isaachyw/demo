#!/usr/bin/env python3
"""Reduce 2."""
import sys
import itertools


def reduce_one_group(key, group):
    """Reduce one group."""
    norm = 0
    grp = list(group)
    for line in grp:
        words = line.split()
        wik = float(words[2])
        norm += wik * wik

    for line in grp:
        words = line.split()
        # word idfk di tfik norm
        print(f'{words[1]} {words[4]} '
              f'{key} {words[3]} {norm}')


def keyfunc(line):
    """Return the key from a TAB-delimited key-value pair."""
    return line.partition("\t")[0]


def main():
    """Divide sorted lines into groups that share a key."""
    for key, group in itertools.groupby(sys.stdin, keyfunc):
        reduce_one_group(key, group)


if __name__ == "__main__":
    main()
