#include "cpu.h"
#include <ucontext.h>
#include "util.h"
#include "thread.h"

void timer_handler() {
    interrupt_guard g;
    if (readyQ.empty())return;
    swapCT();
}

void ipi_handler() {}

cpu::cpu(thread_startfunc_t f, void *arg) {
    interrupt_vector_table[TIMER] = timer_handler;
    interrupt_vector_table[IPI] = ipi_handler;
    cpu::interrupt_enable();
    thread t((thread_startfunc_t) f,arg);
    cpu::interrupt_disable();
    if (!readyQ.empty()) {
        cpu::self()->ctx_ptr = readyQ.front();
        readyQ.pop();
        setcontext(cpu::self()->ctx_ptr->ucptr.get());
    }
}