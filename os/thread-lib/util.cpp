#include "util.h"
#include "cpu.h"
#include "thread.h"
#include <cassert>
#include <memory>


readyQ_t readyQ;
readyQ_t finishQ;

interrupt_guard::interrupt_guard() {
    cpu::interrupt_disable();
}

interrupt_guard::~interrupt_guard() {
    cpu::interrupt_enable();
}

void swapCT() { // wrapper for swap context. Free memory when swapcontext
    while (!finishQ.empty()) {
        finishQ.pop();
    }
    if (readyQ.empty()) {
        cpu::interrupt_enable_suspend();
    }
    readyQ.push(std::move(cpu::self()->ctx_ptr));
    cpu::self()->ctx_ptr = readyQ.front();
    readyQ.pop();
    swapcontext(readyQ.back()->ucptr.get(), cpu::self()->ctx_ptr->ucptr.get());
}

void stub(thread_startfunc_t fun, void *arg) {
    cpu::interrupt_enable();
    fun(arg);
    cpu::interrupt_disable();
    cpu::self()->ctx_ptr->is_finished = true;
    // disable interrupt
    //  put thread blocked by joining this thread
    while (!cpu::self()->ctx_ptr->join_queue.empty()) {
        readyQ.push(cpu::self()->ctx_ptr->join_queue.front());
        cpu::self()->ctx_ptr->join_queue.pop_front();
    }
    assert(cpu::self()->ctx_ptr != nullptr);
    if (readyQ.empty())
        cpu::interrupt_enable_suspend();
    cpu::self()->ctx_ptr = readyQ.front();
    readyQ.pop();
    setcontext(cpu::self()->ctx_ptr->ucptr.get());
}

Uctx_util::Uctx_util() {
    char *stack;
    try {
        this->ucptr = std::make_unique<ucontext_t>();
        stack = new char[STACK_SIZE];
        this->sp = std::unique_ptr<char>(stack);
    }
    catch (const std::bad_alloc &e) {
        throw std::bad_alloc();
    }
    ucptr->uc_stack.ss_sp = stack;
    ucptr->uc_stack.ss_size = STACK_SIZE;
    ucptr->uc_stack.ss_flags = 0;
    ucptr->uc_link = nullptr;
    // store the starter location of stack pointer
    is_finished = false;
    tid = cur_tid++;
}

Uctx_util::~Uctx_util() {
    sp.reset();
    ucptr.reset();
}