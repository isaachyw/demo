#include "thread.h"
#include <ucontext.h>
#include <cassert>
#include <iostream>
#include "cpu.h"

thread::thread(thread_startfunc_t fun, void *arg) {
    interrupt_guard g;
    readyQ.push(std::make_shared<Uctx_util>());
    this->ctx_ptr = readyQ.back();
    makecontext(this->ctx_ptr.lock()->ucptr.get(), reinterpret_cast<void (*)()>(stub), 2, fun, arg);
}

void thread::yield() {
    interrupt_guard g;
    if (readyQ.empty()) return;
    swapCT();
}

void thread::join() {
    interrupt_guard g;
    if (this->ctx_ptr.expired() || this->ctx_ptr.lock()->is_finished)return;
    ctx_ptr.lock()->join_queue.push_back(cpu::self()->ctx_ptr);
    cpu::self()->ctx_ptr = readyQ.front();
    readyQ.pop_front();
    swapcontext(impl_ptr->ctx_ptr.lock()->join_queue.back()->ucptr, cpu::self()->impl_ptr->ctx_ptr->ucptr);
}

thread::~thread() {
    interrupt_guard g;
    while (!finishQ.empty())finishQ.pop_front();
    delete impl_ptr;
}