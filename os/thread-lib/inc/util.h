#pragma once

#include<ucontext.h>
#include <queue>
#include <memory>
#include "thread.h"


class Uctx_util {
public:
    Uctx_util();

    ~Uctx_util();

    std::unique_ptr<ucontext_t> ucptr;
    std::unique_ptr<char> sp;// start of stack address
    std::deque<std::shared_ptr<Uctx_util>> join_queue;
    bool is_finished;

    static uint64_t cur_tid;
    uint64_t tid = 1;// reserve 0 for special purpose
};

using shareCtx_ptr = std::shared_ptr<Uctx_util>;
using readyQ_t = std::queue<shareCtx_ptr>;
// we can't use impl to wrap the ucontext pointer, thus use map

extern readyQ_t readyQ;
extern readyQ_t finishQ;

class interrupt_guard {// raii for cpu_interrupte
public:
    // acquire resources in constructor
    interrupt_guard();

    // release resources in destructor
    ~interrupt_guard();
};

void swapCT();

void stub(thread_startfunc_t fun, void *arg);
