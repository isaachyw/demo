#!/usr/bin/bash
# if there are two argument
# if fail exit
# set -Eeuo pipefail
# set -x
testname=app
# for all file start with test
for file in test*.cpp; do
    echo $file
    cp $file app.cpp
    cd build
    make -j
    cd ..
    # run the fs in the background on port 8000 and redirect output to correct.log
    touch result.txt
    touch my.txt
    # kill any process use port 8000
    fuser -k 8000/tcp
    ./build/createfs
    ./build/correctfs 8000 >result.txt &
    pid=$!
    sleep 0.1
    ./build/${testname} localhost 8000
    sleep 0.1
    # kill the process
    kill -9 $pid
    sleep 0.1
    ./showfs >correct.fs

    #do the same thing for fs
    ./build/createfs
    ./build/fs 8000 >my.txt &
    pid=$!
    sleep 0.1
    ./build/${testname} localhost 8000
    sleep 0.1
    kill -9 $pid
    ./showfs >my.fs

    # grep the line start with @@@ in the my.txt and correct.txt to direct my.log and correct.log
    grep -a "@@@" my.txt >my.log
    grep -a "@@@" result.txt >correct.log
    # compare the two log file and print the result
    # diff my.log correct.log
    diff my.fs correct.fs
    if [ $? -eq 0 ]; then
        echo "test $file passed"
    else
        echo "test $file failed"
        exit 1
    fi
done
