/*
Each client program you submit may be accompanied by an optional file system
image with the same base name; e.g., client program test1.cpp may be accompanied
by file system image test1.fs. Client programs that are accompanied by a .fs
file will be run on a file system initialized with that file system image; i.e.,
the autograder will run createfs on that image before starting the file server).
Client programs that are not accompanied by a .fs file will be run on an empty
file system.

See the spec Test Cases section for more details.
*/

/*
This include should work. Configure the "Include Path" of your IDE if it yells
about header file can't be found. Do NOT write "../inc/handout/fs_client.h". It
will NOT work with autograder.
*/
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>

using std::cout;

int main(int argc, char *argv[]) {
  char *server;
  int server_port;

  const char *writedata =
      "01\023456789012345678901234567890123456789012345678901234567890123456789"
      "01"
      "23456789012345678901234567890123456789"
      "012345678901234567890123456789012345678901234567890123456789012345678901"
      "23456789012345678901234567890123456789"
      "012345678901234567890123456789012345678901234567890123456789012345678901"
      "23456789012345678901234567890123456789"
      "012345678901234567890123456789012345678901234567890123456789012345678901"
      "23456789012345678901234567890123456789"
      "01234567890123456789012345678901234567890123456789012345678901234567890";

  char readdata[FS_BLOCKSIZE];
  int status;

  if (argc != 3) {
    cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
    exit(1);
  }
  server = argv[1];
  server_port = atoi(argv[2]);

  fs_clientinit(server, server_port);

  status = fs_create("user1", "/dir", 'd');

  status = fs_create("user1", "/dir/file@", 'f');

  status = fs_create("user2", "/testfile", 'f');

  status = fs_writeblock("user1", "/dir/file@", 0, writedata);
  for (int i = 0; i < 16; i++) {
    status = fs_writeblock("user2", "/testfile", i, writedata);
    status = fs_writeblock("user2", "/testfile", i + 1, writedata);
    status = fs_readblock("user2", "/testfile", i - 10, readdata);
  }

  status = fs_readblock("user2", "/testfile", 0, readdata);
  // cout << readdata << "\n";

  status = fs_delete("user2", "/testfile");

  status = fs_delete("user1", "/dir/file@");

  status = fs_delete("user1", "/dir");
}
