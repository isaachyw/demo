#include "fs_param.h"
#include "fs_server.h"
#include "header.h"
#include <arpa/inet.h> // htons()
#include <boost/regex.hpp>
#include <boost/regex/v4/regex_match.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/pthread/mutex.hpp>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <queue>
#include <sstream>
#include <stdexcept>
#include <stdio.h>  // printf(), perror()
#include <stdlib.h> // atoi()
#include <string>
#include <sys/socket.h> // socket(), bind(), listen(), accept(), send(), recv()
#include <sys/types.h>
#include <unistd.h> // close()
using boost::regex, boost::regex_match;
using std::cout, std::endl;
using std::runtime_error;
// #define DEBUG
constexpr int QUEUE_SIZE = 30;
constexpr int MAX_MESSAGE_SIZE = FS_MAXPATHNAME + FS_MAXUSERNAME + 10;

void SAFE_COUT(const std::string &str) {
  boost::unique_lock<mutex> guard(cout_lock);
  std::cout << str << std::endl;
}

void dispatcher(int sockfd, const std::vector<std::string> &req) {
  // dispatch the request to different function
  if (req[0] == "FS_READBLOCK") {
    read_handler(req[1], req[2], uint(std::stoi(req[3])), sockfd);
  } else if (req[0] == "FS_WRITEBLOCK") {
    // cout << "data\n" << req[4] << endl;
    write_handler(req[1], req[2], uint(std::stoi(req[3])), req[4].data(),
                  sockfd);
  } else if (req[0] == "FS_CREATE") {
    create_handler(req[1], req[2], req[3].data()[0], sockfd);
  } else {
    assert(req[0] == "FS_DELETE");
    delete_handler(req[1].c_str(), req[2], sockfd);
  }
}

std::vector<std::string> parseReq(const std::string &message, size_t endpos) {
  // parse the request to command, username, path, if invalid, throw error
  string command = message.substr(0, endpos);
  vector<string> req;
  std::istringstream iss(command);
  for (std::string s; iss >> s;) {
    req.push_back(s);
  }
  if (req.size() < 3) {
    throw runtime_error("not enough argument");
  }
  boost::regex valid_types(
      "^(FS_READBLOCK|FS_WRITEBLOCK|FS_CREATE|FS_DELETE)$");

  // Check if the first element of req is a valid type
  if (!boost::regex_match(req[0], valid_types)) {
    throw std::runtime_error("[Error] Invalid request type");
  }
  if (req[1].length() > FS_MAXUSERNAME || req[1].empty()) {
    throw runtime_error("[Error]Username too long / empty");
  }
  if (req[2].length() > FS_MAXPATHNAME || req[2].empty()) {
    throw runtime_error("[Error]Pathname too long / empty");
  }
  if (req[2][0] != '/') {
    throw runtime_error("[Error]Pathname must start with '/'");
  }
  if (req[2][req[2].length() - 1] == '/') {
    throw runtime_error("[Error]Pathname must end with alphanumeric character");
  }

  if (req[0] == "FS_READBLOCK" ||
      req[0] == "FS_WRITEBLOCK") { // FS_READBLOCK or FS_WRITEBLOCK
    if (req.size() != 4) {
      throw runtime_error("[Error]Invalid number of arguments");
    }
    if (static_cast<uint>(stoi(req[3])) >= FS_MAXFILEBLOCKS) {
      throw runtime_error("[Error]File block number too large");
    }
    regex pattern("[^\\s\\0]+\\s+[^\\s\\0]+\\s+\\d+");
    string str = req[1] + " " + req[2] + " " + req[3];
    if (!regex_match(str, pattern) ||
        req[3] != std::to_string(
                      stoi(req[3]))) { // second part to check no leading zeros
      throw runtime_error("[Error]invalid format");
    }
  } else if (req[0] == "FS_CREATE") { // FS_CREATE
    if (req.size() != 4) {
      throw runtime_error("[Error]Invalid number of arguments");
    }
    if (req[3] != "f" && req[3] != "d") {
      throw runtime_error("[Error]Invalid file type");
    }
  } else { // FS_DELETE
    if (req.size() != 3) {
      throw runtime_error("[Error]Invalid number of arguments");
    }
  }
  return req;
}

void requestThread(int client_sockfd) {
  // main function for handle request
  std::string message;
  char msg[64];
  ssize_t recvd = 0;
  ssize_t rval;
  size_t endpos = 0; // end position for the last null character
  bool receivednull = false;
  bool valid = true;
  do {
    memset(msg, 0, 64);
    endpos = 0;
    rval = recv(client_sockfd, msg, 64, 0);
    if (rval <= 0) {
      break;
    }
    if (rval + recvd > MAX_MESSAGE_SIZE + 64) {
      valid = false;
      break;
    }
    recvd += rval;
    message += std::string(msg, static_cast<size_t>(rval));
    for (const auto c : message) {
      if (c == '\0') {
        receivednull = true;
        break;
      }
      endpos++;
    }
    // SAFE_COUT(message);
  } while (!receivednull);
  if (!valid || !receivednull) {
    throw std::runtime_error(
        "either message is too long or message is not null terminated");
  }
  // SAFE_COUT("start parse");
  auto req = parseReq(message, endpos);
  // if the request is fs writeblock, continue accept the data
  if (req[0] == "FS_WRITEBLOCK") {

    std::string data = message.substr(endpos + 1);
    req.push_back(data);
    recvd = static_cast<ssize_t>(req.back().size());
    size_t remain = FS_BLOCKSIZE - req.back().size();
    if (remain != 0) {
      vector<char> buffer(remain);
      rval = recv(client_sockfd, buffer.data(), remain, MSG_WAITALL);
      if (rval != static_cast<ssize_t>(remain)) {
        throw std::runtime_error("not enough data");
      }
      req.back().append(buffer.data(), remain);
    }
  }
  dispatcher(client_sockfd, req);
}

void requestHandler(int client_sockfd) {
  try {
    requestThread(client_sockfd);
  } catch (const std::exception &e) {
    SAFE_COUT(e.what());
  }
  close(client_sockfd);
}

int make_server_sockaddr(struct sockaddr_in *addr, int port) {
  addr->sin_family = AF_INET;
  addr->sin_addr.s_addr = INADDR_ANY;
  addr->sin_port = htons(static_cast<uint16_t>(port));
  return 0;
}

int get_port_number(int sockfd) {
  struct sockaddr_in addr;
  socklen_t length = sizeof(addr);
  if (getsockname(sockfd, (struct sockaddr *)&addr, &length) == -1) {
    perror("Error getting port of socket");
    return -1;
  }
  // Use ntohs to convert from network byte order to host byte order.
  return ntohs(addr.sin_port);
}

int run_server(int port, int queue) {
  int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
  if (sockfd == -1) {
    perror("Error opening stream socket");
    return -1;
  }
  int yesval = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yesval, sizeof(yesval)) ==
      -1) {
    perror("Error setting socket option");
    return -1;
  }
  struct sockaddr_in addr {};
  if (make_server_sockaddr(&addr, port) == -1) {
    perror("Error making server socket address");
    return -1;
  }
  if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("Error binding stream socket");
    return -1;
  }
  port = get_port_number(sockfd);
#ifndef DEBUG
  print_port(static_cast<uint>(port));
#endif
#ifdef DEBUG
  SAFE_COUT("@@@ port " + std::to_string(port));
#endif
  if (listen(sockfd, queue) == -1) {
    perror("Error listening on stream socket");
    return -1;
  }
  while (true) {
    struct sockaddr_in client_addr {};
    socklen_t client_addr_len = sizeof(client_addr);
    int client_sockfd =
        accept(sockfd, (struct sockaddr *)&client_addr, &client_addr_len);
    if (client_sockfd == -1) {
      continue;
    }
    boost::thread t(requestHandler, client_sockfd);
    t.detach();
  }
}

int main(int argc, const char **argv) {
  if (argc > 2) {
    std::cout << "Usage: " << argv[0] << " [port]" << std::endl;
    exit(1);
  }
  int port = 0;
  if (argc == 2) {
    port = atoi(argv[1]);
  }
  disk_block.initFS();
  run_server(port, QUEUE_SIZE);
  return 0;
}