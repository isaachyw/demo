#include "fs_param.h"
#include "header.h"
#include <algorithm>
#include <array>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/pthread/shared_mutex.hpp>
#include <cassert>
#include <cstdint>
#include <cstring>
#include <fs_server.h>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/socket.h>
using boost::unique_lock, boost::shared_mutex, boost::shared_lock;
using std::ostringstream, std::stringstream;
using std::runtime_error;
using std::string;
std::unordered_map<uint, boost::shared_mutex> block2mtx;
DiskBlock disk_block;

uint traverse_one(const string &username, const string &subdir,
                  const uint parent_block) {
  fs_inode parent_inode;
  disk_readblock(parent_block, &parent_inode);
  if (parent_block != 0 && strcmp(parent_inode.owner, username.c_str())) {
    throw runtime_error("Permission denied");
  }
  if (parent_inode.type != 'd') {
    throw runtime_error("Not a directory in the traversal path");
  }
  for (uint i = 0; i < parent_inode.size; i++) {
    fs_direntry entries[FS_DIRENTRIES];
    disk_readblock(parent_inode.blocks[i], entries);
    for (uint j = 0; j < FS_DIRENTRIES; j++) {
      if (entries[j].inode_block != 0 &&
          !strcmp(entries[j].name, subdir.c_str())) {
        return entries[j].inode_block;
      }
    }
  }
  throw runtime_error("No such file or directory");
}

vector<string> split_path(string path) {
  // get a copy of the path
  vector<string> path_vector;
  stringstream ss(path);
  string token;
  getline(ss, token, '/'); // extract the root
  while (getline(ss, token, '/')) {
    if (token.empty())
      throw std::runtime_error("invalid path");
    if (token.length() > FS_MAXFILENAME)
      throw std::runtime_error("filename too long");
    path_vector.push_back(token);
  }
  return path_vector;
}

uint traverse(const string &username, const vector<string> &path_vec,
              shared_lock<shared_mutex> &lock, bool need_child) {
  // traverse the path and give the inode block of the last entry
  assert(lock.owns_lock());
  uint block_num = 0;
  vector<string> path_vec_par(path_vec.begin(), path_vec.end() - 1);
  for (const auto &subdir : (need_child ? path_vec : path_vec_par)) {
    block_num = traverse_one(username, subdir, block_num);
    shared_lock<shared_mutex> sub_node_lock(block2mtx[block_num]);
    lock.swap(sub_node_lock);
  }
  return block_num;
}

void read_handler(const string &username, const string &path,
                  const uint block_num, const int sockfd) {
  char buf[FS_BLOCKSIZE];
  {
    shared_lock<shared_mutex> lock(block2mtx[0]);
    const vector<string> path_vec = split_path(path);
    uint target_block = traverse(username, path_vec, lock, true);
    fs_inode target_inode;
    disk_readblock(target_block, &target_inode);
    if (target_inode.type != 'f') {
      throw runtime_error("Not a file");
    }
    if (block_num >= target_inode.size) {
      throw runtime_error("Block number out of range");
    }
    if (strcmp(target_inode.owner, username.c_str())) {
      throw runtime_error("Permission denied");
    }
    uint data_block = target_inode.blocks[block_num];
    disk_readblock(data_block, buf);
  }
  {
    std::ostringstream oss;
    oss << "FS_READBLOCK " << username << " " << path << " "
        << std::to_string(block_num);
    // make a dynamic array of char of size of oss.str().length() + 1 +
    // FS_BLOCKSIZE and copy the oss.str() to the first part of the array and
    // then copy the buf
    std::vector<char> send_buf(oss.str().length() + 1 + FS_BLOCKSIZE);
    memcpy(send_buf.data(), oss.str().c_str(), oss.str().length() + 1);
    memcpy(send_buf.data() + oss.str().length() + 1, buf, FS_BLOCKSIZE);
    send(sockfd, send_buf.data(), send_buf.size(),
         MSG_NOSIGNAL); // -1 to exclude the null terminator
  }
}

void write_handler(const string &username, const string &path,
                   const uint block_num, const char *buf, const int sockfd) {
  {
    shared_lock<shared_mutex> lock(block2mtx[0]);
    const vector<string> path_vec = split_path(path);
    uint target_inode_block = traverse(username, path_vec, lock, false);
    target_inode_block =
        traverse_one(username, path_vec.back(), target_inode_block);
    unique_lock<shared_mutex> write_lock(block2mtx[target_inode_block]);
    lock.unlock();
    fs_inode target_inode;
    disk_readblock(target_inode_block, &target_inode);
    if (target_inode.type != 'f') {
      throw runtime_error("Not a file in the write handler");
    }
    if (target_inode_block != 0 &&
        strcmp(target_inode.owner, username.c_str())) {
      throw runtime_error("Permission denied during write");
    }

    if (block_num > target_inode.size) {
      throw runtime_error("Block number out of range in write");
    } else if (block_num < target_inode.size) {
      uint data_block = target_inode.blocks[block_num];
      disk_writeblock(data_block, buf);
    } else {
      if (block_num >= FS_MAXFILEBLOCKS) {
        throw runtime_error("Block number out of range in write");
      }
      uint data_block = disk_block.get_free_block();
      target_inode.blocks[block_num] = data_block;
      target_inode.size++;
      disk_writeblock(data_block, buf);
      disk_writeblock(target_inode_block, &target_inode);
    }
  }
  { // extra scope to make sure the lock is released before sending the message
    std::ostringstream oss;
    oss << "FS_WRITEBLOCK " << username << " " << path << " "
        << std::to_string(block_num);
    send(sockfd, oss.str().c_str(), oss.str().size() + 1,
         MSG_NOSIGNAL); // +1 to include the null terminator
  }
}

void create_callee(const string &username, const vector<string> &path_vec,
                   const char type, const int sockfd,
                   const uint parent_inode_block, const string &path) {
  fs_inode child_inode{type}, parent_inode{};
  child_inode.type = type;
  memset(child_inode.blocks, 0, FS_MAXFILEBLOCKS * sizeof(uint));
  strcpy(child_inode.owner, username.c_str());
  fs_direntry shallow_entry[FS_DIRENTRIES]{};
  disk_readblock(parent_inode_block, &parent_inode);
  if (parent_inode_block != 0 && strcmp(parent_inode.owner, username.c_str())) {
    throw runtime_error("Permission denied during create");
  }
  if (parent_inode.type != 'd') {
    throw runtime_error("Not a directory in the create handler");
  }
  // traverse the parent inode's blocks entries to see if the name already
  // exists and record the first empty entry
  bool find = false;
  uint free_block_num = disk_block.get_free_block();
  uint target_entry_idx = 0;
  for (uint32_t i = 0; i < parent_inode.size; i++) {
    fs_direntry entries[FS_DIRENTRIES];
    disk_readblock(parent_inode.blocks[i], entries);
    for (uint32_t j = 0; j < FS_DIRENTRIES; j++) {
      if (entries[j].inode_block == 0 && !find) {
        // set up the shallow entry
        target_entry_idx = i;
        entries[j].inode_block = free_block_num;
        strcpy(entries[j].name, path_vec.back().c_str());
        memcpy(shallow_entry, entries, FS_BLOCKSIZE);
        // target direntry idx = i
        find = true;
      } else {
        if (!strcmp(entries[j].name, path_vec.back().c_str()) &&
            entries[j].inode_block != 0) {
          disk_block.free_block(free_block_num);
          throw runtime_error("File or directory already exists");
        }
      }
    }
  }
  if (!find) {
    // need to add extra inode for whole dir entry and then put the new file's
    // inode under the first direntry
    if (parent_inode.size >= FS_MAXFILEBLOCKS || disk_block.check_full()) {
      disk_block.free_block(free_block_num);
      throw runtime_error("No more space for directory entry");
    }
    parent_inode.size++;
    parent_inode.blocks[parent_inode.size - 1] =
        disk_block
            .get_free_block(); // get extra free block for the new direntry
    fs_direntry entries[FS_DIRENTRIES]{};
    entries[0].inode_block = free_block_num;
    strcpy(entries[0].name, path_vec.back().c_str());
    disk_writeblock(free_block_num, &child_inode);
    disk_writeblock(parent_inode.blocks[parent_inode.size - 1], entries);
    disk_writeblock(parent_inode_block, &parent_inode);
  } else {
    // write the shallow entry back to the disk and then write the child inode
    // to the disk
    disk_writeblock(free_block_num, &child_inode);
    // SAFE_COUT(std::to_string(parent_inode.blocks[target_entry_idx]));
    disk_writeblock(parent_inode.blocks[target_entry_idx], shallow_entry);
  }
}

void create_handler(const string &username, const string &path, const char type,
                    const int sockfd) {
  {
    if (disk_block.check_full()) {
      throw runtime_error("Disk is full try to create");
    }
    uint parent_inode_block = 0;
    const vector<string> path_vec = split_path(path);
    if (path_vec.size() == 1) {
      unique_lock<shared_mutex> write_root_lock(block2mtx[0]);
      create_callee(username, path_vec, type, sockfd, 0, path);
    } else if (path_vec.size() > 1) {
      shared_lock<shared_mutex> lock(block2mtx[0]);
      vector<string> path_vec_par(path_vec.begin(), path_vec.end() - 1);
      if (path_vec.size() > 2) {
        parent_inode_block = traverse(username, path_vec_par, lock, false);
      }
      parent_inode_block =
          traverse_one(username, path_vec_par.back(), parent_inode_block);
      unique_lock<shared_mutex> parent_wl(block2mtx[parent_inode_block]);
      lock.unlock();
      create_callee(username, path_vec, type, sockfd, parent_inode_block, path);
    }
  }
  {
    std::ostringstream oss;
    oss << "FS_CREATE " << username << " " << path << " " << type;
    send(sockfd, oss.str().c_str(), oss.str().size() + 1,
         MSG_NOSIGNAL); // +1 to include the null terminator
  }
}

void delete_callee(const char *username, const string &path,
                   const uint parent_inode_block,
                   const string &delete_node_name, const int sockfd) {
  fs_inode parent_inode{};
  uint32_t entries_idx = 0;
  fs_direntry shallow_copy[FS_DIRENTRIES]{};
  uint delete_inode_block = 0;
  disk_readblock(parent_inode_block, &parent_inode);
  if (parent_inode.type != 'd') {
    throw runtime_error("Not a directory in the delete handler");
  }
  if (parent_inode_block != 0 && strcmp(parent_inode.owner, username)) {
    throw runtime_error("Permission denied during delete");
  }
  // if a whole direntry is deleted, the inode block shold shift
  bool find = false;
  for (uint32_t i = 0; i < parent_inode.size; i++) {
    fs_direntry entries[FS_DIRENTRIES];
    disk_readblock(parent_inode.blocks[i], entries);
    for (uint32_t j = 0; j < FS_DIRENTRIES; j++) {
      if (entries[j].inode_block != 0 &&
          !strcmp(entries[j].name, delete_node_name.c_str())) {
        entries_idx = i;
        delete_inode_block = entries[j].inode_block;
        entries[j].inode_block = 0;
        strcpy(entries[j].name, "");
        memcpy(shallow_copy, entries, FS_BLOCKSIZE);
        find = true;
        break;
      }
    }
    if (find) {
      break;
    }
  }
  if (!find) {
    throw runtime_error("No such file or directory in the delete handler");
  }
  // delete the inode block
  unique_lock<shared_mutex> delete_node_guard(block2mtx[delete_inode_block]);
  fs_inode delete_inode{};
  disk_readblock(delete_inode_block, &delete_inode);
  if (delete_inode.size > 0 && delete_inode.type == 'd') {
    // delete the whole directory
    throw runtime_error("Delete non-empty directory");
  }
  if (strcmp(delete_inode.owner, username)) {
    throw runtime_error("Permission denied during delete");
  }
  if (std::any_of(
          shallow_copy, shallow_copy + FS_DIRENTRIES,
          [](fs_direntry const &entry) { return entry.inode_block != 0; })) {
    // just delete the inode block
    disk_writeblock(parent_inode.blocks[entries_idx], &shallow_copy);
  } else {
    // delete the whole direntry
    uint dir_entry_block2free = parent_inode.blocks[entries_idx];
    for (uint i = entries_idx; i < parent_inode.size - 1; i++) {
      parent_inode.blocks[i] = parent_inode.blocks[i + 1];
    }
    parent_inode.size--;
    // SAFE_COUT(std::to_string(parent_inode_block));
    disk_writeblock(parent_inode_block, &parent_inode);
    disk_block.free_block(dir_entry_block2free);
  }
  // clean up the actual data block of the delete inode
  disk_block.free_block(delete_inode_block);
  if (delete_inode.type == 'f') {
    for (uint i = 0; i < delete_inode.size; i++) {
      disk_block.free_block(delete_inode.blocks[i]);
    }
  }
}

void delete_handler(const char *username, const string &path,
                    const int sockfd) {
  {
    uint parent_inode_block = 0;
    const vector<string> path_vec = split_path(path);
    if (path_vec.size() == 1) {
      unique_lock<shared_mutex> write_root_lock(block2mtx[0]);
      delete_callee(username, path, 0, path_vec.back(), sockfd);
    } else if (path_vec.size() > 1) {
      shared_lock<shared_mutex> lock(block2mtx[0]);
      vector<string> path_vec_par(path_vec.begin(), path_vec.end() - 1);
      if (path_vec.size() > 2) {
        parent_inode_block = traverse(username, path_vec_par, lock, false);
      }
      parent_inode_block =
          traverse_one(username, path_vec_par.back(), parent_inode_block);
      unique_lock<shared_mutex> parent_wl(block2mtx[parent_inode_block]);
      lock.unlock();
      delete_callee(username, path, parent_inode_block, path_vec.back(),
                    sockfd);
    }
  }
  {
    std::ostringstream oss;
    oss << "FS_DELETE " << username << " " << path;
    send(sockfd, oss.str().c_str(), oss.str().size() + 1,
         MSG_NOSIGNAL); // +1 to include the null terminator
  }
}
