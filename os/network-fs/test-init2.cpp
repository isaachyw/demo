/*
Each client program you submit may be accompanied by an optional file system
image with the same base name; e.g., client program test1.cpp may be accompanied
by file system image test1.fs. Client programs that are accompanied by a .fs
file will be run on a file system initialized with that file system image; i.e.,
the autograder will run createfs on that image before starting the file server).
Client programs that are not accompanied by a .fs file will be run on an empty
file system.

See the spec Test Cases section for more details.
*/

/*
This include should work. Configure the "Include Path" of your IDE if it yells
about header file can't be found. Do NOT write "../inc/handout/fs_client.h". It
will NOT work with autograder.
*/
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

int main(int argc, char **argv) {
  /* parsing server name and port */
  if (argc == 3) {
    char *server = argv[1];
    int server_port = atoi(argv[2]);
    fs_clientinit(server, server_port);
  } else {
    char server[16] = "localhost";
    int server_port = 8888;
    fs_clientinit(server, server_port);
  }

  // const char *writedata =
  //     "We hold these truths to be self-evident, that all men are created
  //     equal, that they are endowed by their Creator with certain unalienable
  //     Rights, that among these are Life, Liberty and the pursuit of
  //     Happiness. -- That to secure these rights, Governments are instituted
  //     among Men, deriving their just powers from the consent of the governed,
  //     -- That whenever any Form of Government becomes destructive of these
  //     ends, it is the Right of the People to alter or to abolish it, and to
  //     institute new Government, laying its foundation on such principles and
  //     organizing its powers in such form, as to them shall seem most likely
  //     to effect their Safety and Happiness.";

  int status;

  status = fs_create("user1", "/dir", 'd');
  //   assert(!status);
  status = fs_create("user1", "/di", 'd');
  //   assert(!status);
  status = fs_create("user1", "/dir/abc", 'd');
  //   assert(!status);
  status = fs_create("user2", "/bcd", 'f');
  //   assert(!status);
  status = fs_create("user2", "/bcde", 'd');
  //   assert(!status);
  status = fs_create("user2", "/bcde/ef", 'd');
  //   assert(!status);

  // delete
  status = fs_delete("user1", "/dddd//");
  //   assert(status == -1);
  status = fs_delete("user1", "/dir");
  //   assert(status == -1);
  status = fs_delete("user1", "/dir/ab");
  //   assert(status == -1);
  status = fs_delete("user2", "/dir/abc");
  //   assert(status == -1);
  status = fs_delete("user2d", "/bcde/ef");
  //   assert(status == -1);
  status = fs_delete("user2", "/bcde/ef");
  //   assert(!status);
  status = fs_delete("user2", "/bcde");
  //   assert(!status);
  status = fs_delete("user2", "/bcd");
  //   assert(!status);
  status = fs_delete("user1", "/di");
  //   assert(!status);
  status = fs_delete("user1", "/");
  //   assert(status == -1);
}