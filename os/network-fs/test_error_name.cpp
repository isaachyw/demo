#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>

using std::cout, std::string, std::to_string;

int main(int argc, char *argv[]) {
  if (argc == 3) {
    char *server = argv[1];
    int server_port = atoi(argv[2]);
    fs_clientinit(server, server_port);
  } else {
    char server[16] = "localhost";
    int server_port = 8888;
    fs_clientinit(server, server_port);
  }

  int status;

  // create a file
  status = fs_create("user1", "/file1", 'f');
  // write data to the file
  const char *writedata1 = "data.......";
  status = fs_writeblock("user1", "/file1", 0, writedata1);
  // write to second block
  status = fs_writeblock("user1", "/file1", 1, writedata1);
  // read 001 block
  char readdata1[FS_BLOCKSIZE];
  status = fs_readblock("user1000000000000000", "/file1", 1, readdata1);
  std::cout << status << std::endl;
  // delete root
  status = fs_delete("user1", "/");
  std::cout << status << std::endl;
  // create a directory
  status = fs_create("user1", "/dir1", 'd');
  // create same directory
  status = fs_create("user2", "/dir1", 'd');
  // delete using wrong user
  status = fs_delete("user2", "/dir1");
  // read a directory
  status = fs_readblock("user1", "/dir1", 0, readdata1);
  // username of length 10
  std::string username = "useruser11";
  // filename of length59
  std::string pathname = "/";
  for (int i = 0; i < 59; i++) {
    pathname += "d";
  }
  // create this dir
  status = fs_create(username.c_str(), pathname.c_str(), 'd');
  // again
  pathname += "/";
  for (int i = 0; i < 59; i++) {
    pathname += "d";
  }
  // create this dir
  status = fs_create(username.c_str(), pathname.c_str(), 'd');
  pathname += "/";

  std::string anotherpath = pathname;
  for (int i = 0; i < 7; i++) {
    pathname += "d";
  }
  // create a file
  status = fs_create(username.c_str(), pathname.c_str(), 'f');
  // write to 101 block
  for (int i = 0; i < 101; i++) {
    status = fs_writeblock(username.c_str(), pathname.c_str(), i, writedata1);
    // assert(!status);
  }
  // append 8 more chars
  for (int i = 0; i < 8; i++) {
    anotherpath += "d";
  }
  // create a file
  status = fs_create(username.c_str(), anotherpath.c_str(), 'f');
  // create 3 nested directories
  for (int i = 0; i < 2; i++) {
    string path = "/dir4";
    for (int j = 0; j < i; j++) {
      path += "/dir";
    }
    status = fs_create("user4", path.c_str(), 'd');
    // assert(!status);
    if (i == 1) {
      // create 20 files
      for (int k = 0; k < 20; k++) {
        string file = path + "/file" + to_string(k);
        status = fs_create("user4", file.c_str(), 'f');
      }
      // delete last 10 files
      for (int k = 10; k < 18; k++) {
        string file = path + "/file" + to_string(k);
        status = fs_delete("user4", file.c_str());
        // assert(!status);
      }
      // delete again
      for (int k = 10; k < 20; k++) {
        string file = path + "/file" + to_string(k);
        status = fs_delete("user4", file.c_str());
      }
      // create first 10 files again
      for (int k = 0; k < 10; k++) {
        string file = path + "/file" + to_string(k);
        status = fs_create("user4", file.c_str(), 'f');
      }
      // delete first 10 files
      for (int k = 0; k < 10; k++) {
        string file = path + "/file" + to_string(k);
        status = fs_writeblock("user4", file.c_str(), k, "");
        status = fs_delete("user4", file.c_str());
      }
    }
  }
}