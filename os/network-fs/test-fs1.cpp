#include <iostream>
#include <cassert>
#include <cstdlib>
#include "fs_client.h"

using std::cout;

int main(int argc, char *argv[]) {
    if (argc == 3) {
        char *server = argv[1];
        int server_port = atoi(argv[2]);
        fs_clientinit(server, server_port);
    } else {
        char server[16] = "localhost";
        int server_port = 8888;
        fs_clientinit(server, server_port);
    }
    int status;

    // delete /dir0/file950 - 991
    for (int i = 960; i < 992; i++) {
        std::string path = "/dir0/file" + std::to_string(i);
        status = fs_delete("user1", path.c_str());
        std::cout << status << std::endl;
    }
}