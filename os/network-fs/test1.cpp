/*
This include should work. Configure the "Include Path" of your IDE if it yells
about header file can't be found. Do NOT write "../inc/handout/fs_client.h". It
will NOT work with autograder.
*/
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

int main(int argc, char *argv[]) {
  char *server;
  int server_port;

  int status;

  if (argc != 3) {
    cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
    exit(1);
  }
  server = argv[1];
  server_port = atoi(argv[2]);

  fs_clientinit(server, server_port);

  const char *writedata =
      "We hold these truths to be self-evident, that all men are created "
      "equal, that they are endowed by their ";

  const char *writedata1 =
      "Creator with certain unalienable Rights, that among these are Life, "
      "Liberty and the pursuit of Happiness. -- ";

  const char *writedata2 =
      "that all men are created equal, that they are endowed by their ";

  const char *writedata3 = "Creator lish it, and to institute new Government, "
                           "laying its foundation on such ";

  char readdata[FS_BLOCKSIZE];

  fs_create("user1", "/dir_1", 'd');
  fs_create("user1", "/file_1", 'f');

  fs_create("user1", "/dir_1/dir_2", 'd');
  fs_create("user1", "/dir_1/file_2", 'f');

  fs_create("user1", "/dir_1_1", 'd');
  fs_create("user1", "/dir_1_1/dir_2", 'd');
  fs_create("user1", "/dir_1_1/dir_2_1", 'd');

  for (int i = 0; i < 10; ++i) {
    fs_create("user1", "/dir_1/dir_2/dir_3", 'd');
    fs_create("user1", "/dir_1/dir_2/file_3", 'f');

    fs_create("user1", "/dir_1/dir_2/dir_3/Fourth", 'd');
    fs_create("user1", "/dir_1/dir_2/dir_3/file_4", 'f');

    fs_writeblock("user1", "/dir_1/dir_2/file_3", 0, writedata);
    fs_writeblock("user1", "/dir_1/dir_2/file_3", 1, writedata1);

    fs_readblock("user1", "/dir_1/dir_2/file_3", 0, readdata);
    fs_readblock("user1", "/dir_1/dir_2/file_3", 1, readdata);

    fs_create("user1", "/dir_1/dir_2/file_3_1", 'f');
    fs_writeblock("user1", "/dir_1/dir_2/file_3_1", 0, writedata2);
    fs_writeblock("user1", "/dir_1/dir_2/file_3_1", 1, writedata3);

    fs_create("user1", "/dir_1/dir_2_2", 'd');
    fs_create("user1", "/dir_1/dir_2_2/file_3", 'f');
    fs_writeblock("user1", "/dir_1/dir_2_2/file_3", 0, writedata);
    fs_writeblock("user1", "/dir_1/dir_2_2/file_3", 1, writedata1);

    fs_create("user1", "/dir_1/dir_2_2/file_3_1", 'f');
    fs_writeblock("user1", "/dir_1/dir_2_2/file_3_1", 0, writedata2);
    fs_writeblock("user1", "/dir_1/dir_2_2/file_3_1", 1, writedata3);

    fs_delete("user1", "/dir_1/dir_2_2/file_3_1");
    fs_delete("user1", "/dir_1/dir_2_2/file_3");
    fs_delete("user1", "/dir_1/dir_2_2");
    fs_delete("user1", "/dir_1/dir_2/file_3_1");
    fs_delete("user1", "/dir_1/dir_2/file_3");
    fs_delete("user1", "/dir_1/dir_2/dir_3/Fourth");
    fs_delete("user1", "/dir_1/dir_2/dir_3/file_4");
    fs_delete("user1", "/dir_1/dir_2/dir_3");
  }
  return 0;
}