

/*
This include should work. Configure the "Include Path" of your IDE if it yells about header file can't be found.
Do NOT write "../inc/handout/fs_client.h". It will NOT work with autograder.
*/
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>

using std::cout;
using std::string;

int main(int argc, char *argv[]) {
    char *server;
    int server_port;

    char readdata[512];
    const char *writedata =
        "That to secure these rights, Governments are instituted among Men, deriving their just powers from the "
        "consent of the governed, -- That whenever";
    int status;

    if (argc != 3) {
        cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
        exit(1);
    }
    server = argv[1];
    server_port = atoi(argv[2]);

    fs_clientinit(server, server_port);

    // valid create
    status = fs_create("user1", "/First", 'd');
    assert(!status);
    status = fs_create("user1", "/First/Second", 'd');
    assert(!status);
    for (int i = 0; i < 5; i++) {
        string file_name = "/First/first" + std::to_string(i);
        status = fs_create("user1", file_name.c_str(), 'f');
        assert(!status);
    }
    for (int i = 0; i < 5; i++) {
        string file_name = "/First/Second/second" + std::to_string(i);
        status = fs_create("user1", file_name.c_str(), 'f');
        assert(!status);
    }

    for (int i = 0; i < 3; i++) {
        string file_name = "/First/Second/second" + std::to_string(i + 2);
        status = fs_delete("user1", file_name.c_str());
        assert(!status);
    }
    for (int i = 3; i < 6; i++) {
        string file_name = "/First/Second/second" + std::to_string(i + 2);
        status = fs_delete("user1", file_name.c_str());
        assert(status);
    }
    for (int i = 0; i < 5; i++) {
        string file_name = "/First/firstt" + std::to_string(i);
        status = fs_delete("user1", file_name.c_str());
        assert(status);
    }
    // test error
    status = fs_readblock("user1", "/", 0, readdata);
    assert(status);

    status = fs_writeblock("user1", "/", 0, writedata);
    assert(status);

    status = fs_create("user1", "/", 'f');
    assert(status);

    status = fs_delete("user1", "/");
    assert(status);

    status = fs_readblock("user1", "//First", 0, readdata);
    assert(status);

    status = fs_writeblock("user1", "//First", 0, writedata);
    assert(status);

    status = fs_create("user1", "//First", 'f');
    assert(status);

    status = fs_delete("user1", "//First");
    assert(status);

    status = fs_readblock("user1", "/First/wrong", 0, readdata);
    assert(status);

    status = fs_writeblock("user1", "/First/wrong", 0, writedata);
    assert(status);

    status = fs_readblock("user1", "First/wrong", 0, readdata);
    assert(status);

    status = fs_writeblock("user1", "First/wrong", 0, writedata);
    assert(status);

    status = fs_create("user1", "First/wrong", 'f');
    assert(status);

    status = fs_delete("user1", "First/wrong");
    assert(status);

    status = fs_readblock("user1", "/First/b/", 0, readdata);
    assert(status);

    status = fs_writeblock("user1", "/First/b/", 0, writedata);
    assert(status);

    status = fs_create("user1", "/First/b/", 'f');
    assert(status);

    status = fs_delete("user1", "/First/b/");
    assert(status);
}
