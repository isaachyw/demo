#include "fs_param.h"
#include "fs_server.h"
#include <algorithm>
#include <boost/thread.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/pthread/mutex.hpp>
#include <boost/thread/pthread/shared_mutex.hpp>
#include <iostream>
#include <iterator>
#include <queue>
#include <string>
#include <sys/types.h>
#include <unordered_map>
#include <vector>
using boost::mutex, boost::shared_mutex, boost::shared_lock, boost::unique_lock;
using boost::unique_lock;
using std::string;
using std::unordered_map;
using std::vector;
extern unordered_map<uint, shared_mutex> block2mtx;

void SAFE_COUT(const std::string &str);
class DiskBlock {

private:
  vector<bool> block;
  uint free_num;
  shared_mutex block_mutex;

public:
  DiskBlock() : block(FS_DISKSIZE, true), free_num(FS_DISKSIZE - 1) {
    block[0] = false; // 0 is reserved for root
  }
  uint get_free_block() {
    // if no free block, throw exception
    unique_lock<shared_mutex> lock(block_mutex);
    if (free_num == 0) {
      throw std::runtime_error("No free block");
    }
    auto it = std::find(block.begin(), block.end(), true);
    *it = false;
    this->free_num--;
    uint free_block = static_cast<uint>(std::distance(block.begin(), it));
    return free_block;
  }
  bool check_full() {
    shared_lock<shared_mutex> guard(block_mutex);
    bool full = (free_num <= 0);
    return full;
  }
  void free_block(uint block_num) {
    unique_lock<shared_mutex> lock(block_mutex);
    block[block_num] = true;
    free_num++;
  }
  bool check_block(uint block_num) {
    shared_lock<shared_mutex> guard(block_mutex);
    return block[block_num];
  }
  void initFS() {
    // bfs to search the whole file system to set the used block
    std::queue<uint> inode_to_search;
    assert(!check_block(0)); // first block is reserved for root
    inode_to_search.push(0);
    while (!inode_to_search.empty()) {
      fs_inode inode{};
      uint inode_num = inode_to_search.front();
      inode_to_search.pop();
      disk_readblock(inode_num, &inode);
      if (inode.type == 'f') {
        for (uint i = 0; i < inode.size; i++) {
          if (inode.blocks[i] != 0) {
            block[inode.blocks[i]] = false;
            free_num--;
          }
        }
      } else {
        assert(inode.type == 'd');
        fs_direntry direntry[FS_DIRENTRIES]{};
        for (uint i = 0; i < inode.size; i++) {
          disk_readblock(inode.blocks[i], &direntry);
          block[inode.blocks[i]] = false;
          free_num--;
          for (uint j = 0; j < FS_DIRENTRIES; j++) {
            if (direntry[j].inode_block != 0) {
              inode_to_search.push(direntry[j].inode_block);
              block[direntry[j].inode_block] = false;
              free_num--;
            }
          }
        }
      }
    }
  }
};

void read_handler(const string &username, const string &path,
                  const uint block_num, const int sockfd);

void write_handler(const string &username, const string &path,
                   const uint block_num, const char *buf, const int sockfd);
void create_handler(const string &username, const string &path, const char type,
                    const int sockfd);
void delete_handler(const char *username, const string &path, const int sockfd);

// global disk block
extern DiskBlock disk_block;