// test6 continue tests some error handling
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>

using std::cout;

int main(int argc, char *argv[]) {
  char *server;
  int server_port;

  int status;

  if (argc != 3) {
    cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
    exit(1);
  }
  server = argv[1];
  server_port = atoi(argv[2]);

  fs_clientinit(server, server_port);

  status = fs_create("user1", "/pear", 'd');
  assert(!status);

  // create a file into a file
  status = fs_create("user1", "/pear/file", 'f');
  status = fs_create("user1", "/pear/file/file2", 'f');
  assert(status);

  // read a file that does not exist
  char readdata[FS_BLOCKSIZE];
  status = fs_readblock("user1", "/pear/file3", 0, readdata);
  assert(status);

  // try to read a dir
  status = fs_readblock("user1", "/pear", 0, readdata);
  assert(status);

  // normal write
  const char *writedata =
      "We hold these truths to be self-evident, that all men are created "
      "equal, that they are endowed by their Creator with certain unalienable "
      "Rights, that among these are Life, Liberty and the pursuit of "
      "Happiness. -- That to secure these rights, Governments are instituted "
      "among Men, deriving their just powers from the consent of the governed, "
      "-- That whenever any Form of Government becomes destructive of these "
      "ends, it is the Right of the People to alter or to abolish it, and to "
      "institute new Government, laying its foundation on such principles and "
      "organizing its powers in such form, as to them shall seem most likely "
      "to effect their Safety and Happiness.";
  status = fs_writeblock("user1", "/pear/file", 0, writedata);
  assert(!status);

  // read exceeds the size of the file
  status = fs_readblock("user1", "/pear/file", 1, readdata);
  assert(status);

  // normal write
  status = fs_writeblock("user1", "/pear/file", 1, writedata);
  assert(!status);

  // write the block that exceeds the file size
  status = fs_writeblock("user1", "/pear/file", 3, writedata);
  assert(status);

  // delete not exist file
  status = fs_delete("user1", "/pear/file4");
  assert(status);

  // delete no empty directory
  status = fs_delete("user1", "/pear");
  assert(status);

  // write to a directory
  status = fs_writeblock("user1", "/pear", 0, writedata);
  assert(status);

  // have no right to read a dir
  status = fs_writeblock("-", "/pear/file", 0, writedata);
  assert(status);

  // have no right to delete a dir
  status = fs_delete("-", "/pear/file");
  assert(status);

  status = fs_delete("user1", "/pear/file");
  assert(!status);

  status = fs_delete("user1", "/pear/file");
  assert(status);

  // write the block that is deleted
  status = fs_writeblock("user1", "/pear/file", 0, writedata);
  assert(status);

  status = fs_delete("user2", "/pear");
  assert(status);

  status = fs_delete("user1", "/pear");
  assert(!status);

  status = fs_create("u", "/pear", 'd');
  assert(!status);

  status = fs_create("u", "/pear/pear", 'd');
  assert(!status);

  status = fs_create("u", "/pear/pear/1", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/2", 'f');
  assert(!status);

  status = fs_create("9", "/pear/pear/3", 'f');
  assert(status);

  status = fs_create("u", "/pear/pear/3", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/4", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/5", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/6", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/7", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/8", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/9", 'f');
  assert(!status);

  status = fs_delete("u1", "/pear/pear/3");
  assert(status);

  status = fs_create("u", "/pear/pear/new3", 'f');
  assert(!status);

  status = fs_delete("u", "/pear/pear/3");
  assert(!status);

  status = fs_create("u", "/pear/pear/3", 'f');
  assert(!status);

  status = fs_create("u", "/pear/pear/2", 'd');
  assert(status);

  status = fs_create("u", "/pear/pear/pear", 'd');
  assert(!status);

  status = fs_delete("u1", "/pear/pear/pear");
  assert(status);

  status = fs_writeblock("user1", "/pear/pear/pear", 0, writedata);
  assert(status);

  status = fs_delete("u", "/pear/pear/pear");
  assert(!status);

  status = fs_create("u", "/pear/pear/pear", 'f');
  assert(!status);

  status = fs_writeblock("u", "/pear/pear/pear", 0, writedata);
  assert(!status);
}