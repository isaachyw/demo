#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>

using std::cout;

int main(int argc, char *argv[]) {
  char *server;
  int server_port;

  const char *data1 = "hello,";
  const char *data2 = "world!";
  const char *data3 = "hello,";
  const char *data4 = "eecs482!";

  int status;

  if (argc != 3) {
    cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
    exit(1);
  }

  server = argv[1];
  server_port = atoi(argv[2]);

  fs_clientinit(server, static_cast<uint16_t>(server_port));

  // Generator
  status = fs_create("user_1", "/dir_1", 'd');
  assert(!status);
  status = fs_create("user_1", "/dir_1/dir_1_1", 'd');
  assert(!status);
  status = fs_create("user_1", "/dir_1/dir_1_1/file1", 'f');
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_1/file1", 0, data1);
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_1/file1", 1, data2);
  assert(!status);
  status = fs_create("user_1", "/dir_1/dir_1_1/file2", 'f');
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_1/file2", 0, data3);
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_1/file2", 1, data4);
  assert(!status);
  status = fs_create("user_1", "/dir_1/dir_1_2", 'd');
  assert(!status);
  status = fs_create("user_1", "/dir_1/dir_1_2/file1", 'f');
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_2/file1", 0, data1);
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_2/file1", 1, data2);
  assert(!status);
  status = fs_create("user_1", "/dir_1/dir_1_2/file2", 'f');
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_2/file2", 0, data3);
  assert(!status);
  status = fs_writeblock("user_1", "/dir_1/dir_1_2/file2", 1, data4);
  assert(!status);
  status = fs_create("user_1", "/dir_2", 'd');
  assert(!status);

  status = fs_create("user_1", "/dir_2/dir_2_1", 'd');
  assert(!status);

  status = fs_create("user_1", "/dir_2/dir_2_2", 'd');
  assert(!status);

  status = fs_create("user_1", "/dir_3", 'd');
  assert(!status);

  status = fs_create("user_2", "/dir_4", 'd');
  assert(!status);

  // Test
  for (int i = 0; i < 16; ++i) {
    status = fs_create("user_1", "/dir_1/file3", 'f');
    assert(!status);

    status = fs_create("user_1", "/dir_1/dir_1_3", 'd');
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_1/file1", 1, data1);
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_1/file1", 0, data2);
    assert(!status);

    status = fs_create("user_1", "/dir_1/dir_1_3/file3", 'f');
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_1/file2", 1, data3);
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_1/file2", 0, data4);
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_2/file2", 1, data3);
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_2/file2", 0, data4);
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_2/file1", 1, data1);
    assert(!status);

    status = fs_writeblock("user_1", "/dir_1/dir_1_2/file1", 0, data2);
    assert(!status);

    status = fs_delete("user_1", "/dir_1/dir_1_3/file3");
    assert(!status);

    status = fs_delete("user_1", "/dir_1/dir_1_3");
    assert(!status);

    status = fs_delete("user_1", "/dir_1/file3");
    assert(!status);
  }
}