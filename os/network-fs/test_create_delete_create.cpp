

/*
This include should work. Configure the "Include Path" of your IDE if it yells about header file can't be found.
Do NOT write "../inc/handout/fs_client.h". It will NOT work with autograder.
*/
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>

using std::cout;
using std::string;

int main(int argc, char *argv[]) {
    char *server;
    int server_port;

    int status;

    if (argc != 3) {
        cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
        exit(1);
    }
    server = argv[1];
    server_port = atoi(argv[2]);

    fs_clientinit(server, server_port);

    // valid create
    status = fs_create("user1", "/First", 'd');
    assert(!status);
    status = fs_create("user1", "/First/Second", 'd');
    assert(!status);
    for (int i = 0; i < 15; i++) {
        string file_name = "/First/first" + std::to_string(i);
        status = fs_create("user1", file_name.c_str(), 'f');
        assert(!status);
    }
    for (int i = 0; i < 15; i++) {
        string file_name = "/First/Second/second" + std::to_string(i);
        status = fs_create("user1", file_name.c_str(), 'f');
        assert(!status);
    }

    for (int i = 5; i < 15; i++) {
        string file_name = "/First/Second/second" + std::to_string(i);
        status = fs_delete("user1", file_name.c_str());
        assert(!status);
    }
    for (int i = 5; i < 15; i++) {
        string file_name = "/First/first" + std::to_string(i);
        status = fs_delete("user1", file_name.c_str());
        assert(!status);
    }
    for (int i = 5; i < 15; i++) {
        string file_name = "/First/first" + std::to_string(i);
        status = fs_create("user1", file_name.c_str(), 'f');
        assert(!status);
    }
    for (int i = 5; i < 15; i++) {
        string file_name = "/First/Second/second" + std::to_string(i);
        status = fs_create("user1", file_name.c_str(), 'f');
        assert(!status);
    }
}
