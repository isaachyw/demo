/*
Each client program you submit may be accompanied by an optional file system
image with the same base name; e.g., client program test1.cpp may be accompanied
by file system image test1.fs. Client programs that are accompanied by a .fs
file will be run on a file system initialized with that file system image; i.e.,
the autograder will run createfs on that image before starting the file server).
Client programs that are not accompanied by a .fs file will be run on an empty
file system.

See the spec Test Cases section for more details.
*/

/*
This include should work. Configure the "Include Path" of your IDE if it yells
about header file can't be found. Do NOT write "../inc/handout/fs_client.h". It
will NOT work with autograder.
*/
#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>

using std::cout;

int main(int argc, char *argv[]) {
  char *server;
  int server_port;

  const char *writedata =
      "We hold these truths to be self-evident, that all men are created "
      "equal, that they are endowed by their "
      "Creator with certain unalienable Rights, that among these are Life, "
      "Liberty and the pursuit of Happiness. -- "
      "That to secure these rights, Governments are instituted among Men, "
      "deriving their just powers from the "
      "consent of the governed, -- That whenever any Form of Government "
      "becomes destructive of these ends, it is the "
      "Right of the People to alter or to abolish it, and to institute new "
      "Government, laying its foundation on such "
      "principles and organizing its powers in such form, as to them shall "
      "seem most likely to effect their Safety "
      "and Happiness.";

  char readdata[FS_BLOCKSIZE];
  int status;

  if (argc != 3) {
    cout << "error: usage: " << argv[0] << " <server> <serverPort>\n";
    exit(1);
  }
  server = argv[1];
  server_port = atoi(argv[2]);

  fs_clientinit(server, server_port);

  status = fs_create("user2", "/dir/file", 'f');
  //   assert(status);

  status = fs_create("user1", "/dir", 'd');
  //   assert(!status);

  status = fs_create("user2", "/dir/file", 'f');
  //   assert(status);

  status = fs_create("user1", "/dir/file", 'f');
  //   assert(!status);

  status = fs_create("user1", "/dir/file", 'f');
  //   assert(status);

  status = fs_create("user2", "/testfile", 'f');
  //   assert(!status);

  status = fs_writeblock("user1", "/dir/file", 0, writedata);
  //   assert(!status);

  status = fs_writeblock("user2", "/dir/file", 1, writedata);
  //   assert(status);
  fs_writeblock("\0", "//dir/file", 01, writedata);
  fs_writeblock(
      "I am "
      "logggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
      "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg",
      "/dir/file", 19, "");
  fs_writeblock(
      "I am "
      "logggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
      "ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg",
      "/dir/file", 1, "here");

  status = fs_readblock("user2", "/dir/file", 1, readdata);
  //   assert(status);

  status = fs_writeblock("user2", "/testfile", 0, writedata);
  //   assert(!status);

  status = fs_readblock("user1", "/testfile", 0, readdata);
  //   assert(status);

  status = fs_readblock("user2", "/testfile", 0, readdata);
  //   assert(!status);
  //   cout << readdata << "\n";

  status = fs_delete("user2", "/testfile");
  //   assert(!status);

  status = fs_delete("user2", "/dir/file");
  //   assert(status);

  status = fs_delete("user1", "/dir/file");
  //   assert(!status);

  status = fs_delete("user1", "/dir/file");
  //   assert(status);

  status = fs_delete("user1", "/dir");
  //   assert(!status);
  fs_delete("user1", "/");
  fs_delete("user1", "//dir");
}
