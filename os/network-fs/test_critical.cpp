#include "fs_client.h"
#include <cassert>
#include <cstdlib>
#include <iostream>

using std::cout;

int main(int argc, char *argv[]) {
  if (argc == 3) {
    char *server = argv[1];
    int server_port = atoi(argv[2]);
    fs_clientinit(server, server_port);
  } else {
    char server[16] = "localhost";
    int server_port = 8888;
    fs_clientinit(server, server_port);
  }
  int status;

  for (int i = 0; i < 1; i++) {
    char dir[10];
    sprintf(dir, "/dir%d", i);
    status = fs_create("user1", dir, 'd');
    // create 8*124 files
    for (int j = 0; j < 8; j++) {
      for (int k = 0; k < 15; k++) {
        char filename[100];
        sprintf(filename, "%s/file%d", dir, j * 124 + k);
        status = fs_create("user1", filename, 'f');
      }
    }
  }

  status = fs_create("user1", "/dir/file4096", 'f');
  assert(status == -1);
}