cmake_minimum_required(VERSION 3.16)

project(EECS482_P1)
set(CMAKE_CXX_STANDARD 20)
set(THREADS_PREFER_PTHREAD_FLAG ON)

find_package(Threads REQUIRED)

add_library(libthread STATIC IMPORTED)

message(${CMAKE_CURRENT_LIST_DIR})

set_target_properties(libthread PROPERTIES IMPORTED_LOCATION ${CMAKE_CURRENT_LIST_DIR}/libthread.o)

# set_target_properties(libthread PROPERTIES IMPORTED_LOCATION ${CMAKE_CURRENT_LIST_DIR}/libthread.o)

# Disk scheduler build target (customize this with the source files for your disk scheduler)
add_executable(disk schedule.cpp)
target_link_libraries(disk libthread ${CMAKE_DL_LIBS} Threads::Threads)
add_custom_command(POST_BUILD TARGET disk COMMAND ${CMAKE_CURRENT_LIST_DIR}/autotag.sh push)
