#include "thread.h"
#include "disk.h"
#include "cpu.h"
#include "cv.h"
#include <fstream>
#include <vector>
#include <string>
#include <limits>
#include <iostream>
#include <cassert>

using std::string;
using std::vector;
using std::cout, std::endl,std::cerr;

uint32_t num_max = 0;
uint32_t num_live_request = 0;
char **ARGV;
vector<int> disk_queue;
uint32_t queue_capacity = 0;

//condition variable
cv wait_server;
cv wait_requester;

// mutex
mutex disk_queue_mutex;

class disk_queue_mutex_guard {
public:
    disk_queue_mutex_guard() {
        disk_queue_mutex.lock();
    }

    ~disk_queue_mutex_guard() {
        disk_queue_mutex.unlock();
    }
};

int find_requester(int &curr_address) {
    int min_dist = std::numeric_limits<int>::max();
    int request_id = -1;
    for (size_t i = 0; i < disk_queue.size(); i++) {
        // cout<<disk_queue[i]<<"  ";
        if (disk_queue[i] != -1) {
            if (abs(curr_address - disk_queue[i]) < min_dist) {
                request_id = static_cast<int>(i);
                min_dist = abs(curr_address - disk_queue[i]);
            }
        }
    }
    // cout<<endl;
    if (request_id == -1) { return -1; }
    curr_address = disk_queue[request_id];
    return request_id;
}

void requester(void *id) {
    string disk_address;
    int identifier = static_cast<int>(reinterpret_cast<intptr_t>(id));
    std::ifstream file(ARGV[identifier + 2]);
    while (getline(file, disk_address)) {
        int address_int = static_cast<int>(stoul(disk_address));
        {
            disk_queue_mutex_guard g;
            while (disk_queue[(size_t) identifier] != -1 || queue_capacity >= num_max) {
                wait_server.wait(disk_queue_mutex);
            }
            queue_capacity++;
            disk_queue[(size_t) identifier] = address_int;
            print_request(static_cast<uint>(identifier), static_cast<uint>(address_int));
            wait_requester.signal();
        }
    }
    {
        disk_queue_mutex_guard g;
        num_live_request--;
    }
    file.close();
}

void server(void *a) {
    // init all requester
    int num_to_create = static_cast<int>((reinterpret_cast<intptr_t>(a)));
    for (int i = 0; i < num_to_create; i++) {
        thread t(reinterpret_cast<thread_startfunc_t>(requester), reinterpret_cast<void *> (i));
    }
    num_live_request = (uint32_t) num_to_create;
    int curr_address = 0;
    while (num_live_request > 0) {
        disk_queue_mutex_guard g;
        while (queue_capacity < (std::min(num_live_request, num_max))) {
            wait_requester.wait(disk_queue_mutex);
        }
        int id = find_requester(curr_address);
        if (id == -1) { continue; }
        print_service((uint) id, (uint) disk_queue[(size_t) id]);
        queue_capacity--;
        disk_queue[(size_t) id] = -1;
        wait_server.broadcast();
    }
    while (true) {// all requester are dead, handle redundant data in queue.
        int rq_id = find_requester(curr_address);
        if (rq_id == -1) { break; }
        print_service((uint) rq_id, (uint) disk_queue[(size_t) rq_id]);
        queue_capacity--;
        disk_queue[(size_t) rq_id] = -1;
    }
}

int main(int argc, char **argv) {
    ARGV = argv;
    num_max = static_cast<uint32_t>(atoi(argv[1]));
    disk_queue.resize((size_t) (argc - 2), -1);
    cpu::boot(reinterpret_cast<thread_startfunc_t>(server), reinterpret_cast<void *>(static_cast<uint32_t>(argc - 2)),
              0);
    return 0;
}