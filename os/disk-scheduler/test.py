#!/usr/bin/python3
import subprocess

# run ten times "./scheduler 3 disk.in0 disk.in1 disk.in2 disk.in3 disk.in4"
for i in range(1, 10):
    s = subprocess.run(["./scheduler", str(i), "disk.in0", "disk.in1",
                        "disk.in2", "disk.in3", "disk.in4"], stdout=subprocess.PIPE)
    # assert every subprocess returns 0
    print(i)
    assert s.returncode == 0
