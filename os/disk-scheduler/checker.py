from typing import Dict,List
import os
import sys
import re
disk_map : Dict[int, List[int]] ={}

serve_seq: List[int]=[]
req_seq: List[int]=[]

def main():
    with open("output", "r") as f:
        for i,line in enumerate(f):
            if line.startswith("service"):
                server = int(line.split()[4])
                serve_seq.append(server)
            elif line.startswith("request"):
                request = int(line.split()[3])
                req_seq.append(request)


main()