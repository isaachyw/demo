#include <iostream>
#include <cstring>
#include <unistd.h>
#include "vm_app.h"
using namespace std;

int main()
{   
    for (int i = 1; i < 5; i++) {
        char *f1 = (char*)vm_map(nullptr, 0);
        char *f2 = (char*)vm_map(nullptr, 0);
        f1[65535] = 'd'; 
        memcpy(f2, "ataX.bin", 9);
        f2[3] = '0' + i;
        
        char *file = (char*)vm_map( f1 + 65535, 0 );
        memcpy(file, "123123", 6);

        char* f3 = (char*)vm_map(nullptr,0);
        *f3 = 'a';
        file = (char*)vm_map( f1 + 65535, 0 );
        *f3 = 'a';
        file = (char*)vm_map( f1 + 65535, 0 );
    }

}