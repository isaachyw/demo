#include <iostream>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include "vm_app.h"
#include <cassert>
using namespace std;

using std::cout;
using std::endl;

uint vmpgsz = 65536;

int main()
{
    char *str1 = (char *)vm_map(nullptr, 0);
    char *str2 = (char *)vm_map(nullptr, 0);
    char *filename = str1 + vmpgsz - 3;
    strcpy(filename, "lampson83.txt");

    if (fork())
    {
        cout << "parent" << endl;
        char *f1 = (char *)vm_map(filename, 0);
        f1[0] = 'p';

        cout << "yield to child" << endl;
        vm_yield();
    }
    else
    {
        char *f2 = (char *)vm_map(filename, 0);
        assert(f2[0] == 'p');
        f2[0] = 'c';
        cout << "child end" << endl;
    }
}
