#include <iostream>
#include <cstring>
#include <string>
#include <signal.h>
#include <unistd.h>
#include "vm_app.h"

using std::cout;
using std::endl;    
    
int main()
{
    char *temp[100];
    temp[0] = (char *)vm_map(nullptr, 0);
    temp[0][0] = 'a';
    unsigned int id = 8;
    while (id % 9 && fork()) {
        --id;
    }
    /* All children has id from 1 to 2. Parent has id 0 */
    if (id == 0) {
        for (unsigned int i = 1; i < 4; ++i) {
            temp[i] = (char *)vm_map(nullptr, 0);
            temp[i][0] = 'a' + i;
            temp[i][i] = 'f' + i;
        }
        vm_yield();
        cout << temp[0][0] << endl;
    } else {
        vm_yield();
    }
}
