#include "vm_app.h"
#include <iostream>
#include <cstring>

using std::cout;

int main() {
    char *str1 = (char *) vm_map(nullptr, 0);
    strcpy(str1, "data1.bin");
    char *str2 = (char *) vm_map(nullptr, 0);
    strcpy(str2, "data2.bin");
    char *str3 = (char *) vm_map(nullptr, 0);
    strcpy(str3, "data3.bin");
    char *str4 = (char *) vm_map(nullptr, 0);
    strcpy(str4, "data4.bin");
    char *file1 = (char *) vm_map(str1, 0);
    char *file2 = (char *) vm_map(str2, 0);
    char *file3 = (char *) vm_map(str3, 0);
    char *file4 = (char *) vm_map(str4, 0);
    strcpy(file1, "file-back 1");
    for (uint i=0; i<20; i++) {
        cout << file1[i];
    }
    cout<<"\n";
    strcpy(file2, "file-back 2");
    for (uint i=0; i<20; i++) {
        cout << file2[i];
    }
    cout<<"\n";
    strcpy(file3, " file-back 3");
    for (uint i=0; i<20; i++) {
        cout << file3[i];
    }
    cout<<"\n";
    strcpy(file4,  "file-back 4");
    for (uint i=0; i<20; i++) {
        cout << file4[i];
    }
}
