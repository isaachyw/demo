#include <iostream>
#include <cstring>
#include <unistd.h>
#include "vm_app.h"

using std::cout;

int main()
{
    char *str1 = (char *) vm_map(nullptr, 0);
    strcpy(str1,"str1");
    char *str2 = (char *) vm_map(nullptr, 0);
    strcpy(str2,"str2");
    char *str3 = (char *) vm_map(nullptr, 0);    
    strcpy(str3,"str3");
    char *str4 = (char *) vm_map(nullptr, 0);    
    strcpy(str4,"str4");
    
    if(fork()==0){
        strcpy(str1,"str1-child");
        strcpy(str2,"str2-child");
        strcpy(str3,"str3-child");
        strcpy(str4,"str4-child");
        printf("Child str1:%s\n",str1);
        printf("Child str2:%s\n",str2);
        printf("Child str3:%s\n",str3);
        printf("Child str4:%s\n",str4);
    } else {
        printf("Parent str1:%s\n",str1);
        printf("Parent str2:%s\n",str2);
        printf("Parent str3:%s\n",str3);
        printf("Parent str4:%s\n",str4);
        vm_yield();
    }
}
