#include <iostream>
#include <cstring>
#include <unistd.h>
#include "vm_app.h"
using namespace std;


int main() {
    if (fork() == 0) {
        char *filename = (char *) vm_map(nullptr, 0);
        strcpy(filename, "data1.bin");

        char *p1 = (char *) vm_map(filename, 0);
        strcpy(p1, "456");

        cout << p1[0] << p1[1] << p1[2] << endl;
        cout << p1[10] << p1[11] << p1[12] << endl;
    } else {
        char *filename = (char *) vm_map(nullptr, 0);
        strcpy(filename, "data1.bin");

        char *p1 = (char *) vm_map(filename, 0);
        strcpy(p1, "123");

        cout << p1[0] << p1[1] << p1[2] << endl;
        cout << p1[10] << p1[11] << p1[12] << endl;
    }
}