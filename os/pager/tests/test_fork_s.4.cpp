#include <iostream>
#include <cstring>
#include <unistd.h>
#include "vm_app.h"
#include <cassert>
using namespace std;

using std::cout;
using std::endl;

uint vmpgsz = 65536;

int main()
{
    char *str1 = (char *)vm_map(nullptr, 0);
    char *str2 = (char *)vm_map(nullptr, 0);
    char *strptr = str1 + vmpgsz - 3;
    memcpy(strptr, "xxxxxx", 6);
    if (fork())
    {
        if (fork())
        {
            cout << "parent" << endl;
            for (int i = 0; i < 6; i++)
            {
                strptr[i] = 'a';
            }
            cout << "parent: " << strptr << endl;
            cout << "yield to child" << endl;
            vm_yield();
            cout << "back to parent: " << strptr << endl;
            for (int i = 0; i < 6; i++)
            {
                cout << strptr[i];
            }
            cout << "parent end" << endl;
        }
        else
        {
            cout << "child begin" << endl;
            for (int i = 0; i < 6; i++)
            {
                assert(strptr[i] == 'a');
            }
            for (int i = 0; i < 6; i++)
            {
                strptr[i] = 'f';
            }
            cout << "child: " << strptr << endl;
            cout << "child end" << endl;
        }
        cout << "parent" << endl;
        for (int i = 0; i < 6; i++)
        {
            strptr[i] = 'a';
        }
        cout << "parent: " << strptr << endl;
        cout << "yield to child" << endl;
        vm_yield();
        cout << "back to parent: " << strptr << endl;
        for (int i = 0; i < 6; i++)
        {
            cout << strptr[i];
        }
        cout << "parent end" << endl;
    }
    else
    {
        cout << "child begin" << endl;
        for (int i = 0; i < 6; i++)
        {
            assert(strptr[i] == 'a');
        }
        for (int i = 0; i < 6; i++)
        {
            strptr[i] = 'f';
        }
        cout << "child: " << strptr << endl;
        cout << "child end" << endl;
    }
}
