#include <iostream>
#include <cstring>
#include <unistd.h>
#include "vm_app.h"
using namespace std;

uint vmpgsz = 65536;

int main()
{
    uint arena_size = 256;
    char *temp[arena_size + 20];
    for (uint i = 0; i < arena_size + 20; i++)
    {
        temp[i] = (char *)vm_map(nullptr, 0);
        if (i < arena_size)
            memset(temp[i], 'a', vmpgsz);
    }
    temp[233][333] = '\0';
    char *p = temp[100];
    cout << ".." << endl;
    char *fileback = (char *)vm_map(p, 0);
    cout << (void *)fileback << endl;
    char *swap = (char *)vm_map(nullptr, 0);
    cout << (void *)swap << endl;
}
