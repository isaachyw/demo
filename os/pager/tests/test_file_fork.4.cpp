#include <iostream>
#include <cstring>
#include <unistd.h>
#include "vm_app.h"
using namespace std;

int main()
{
    char *filename1 = (char *)vm_map(nullptr, 0);
    char *filename2 = (char *)vm_map(nullptr, 0);
    strcpy(filename1, "data1.bin");
    strcpy(filename2, "data2.bin");
    for (int i = 0; i < 3; i++)
    {
        char *temp1 = (char *)vm_map(filename1, 1);
        char *temp2 = (char *)vm_map(filename1, 2);
        char *temp3 = (char *)vm_map(filename1, 3);
        char *temp4 = (char *)vm_map(filename1, 4);
        char *temp5 = (char *)vm_map(filename2, 1);
        char *temp6 = (char *)vm_map(filename2, 2);
        char *temp7 = (char *)vm_map(filename2, 3);
        char *temp8 = (char *)vm_map(filename2, 4);
        if (fork())
        {
            cout << *filename1 << endl;
            cout << *filename2 << endl;
            char *block11 = (char *)vm_map(filename1, 1);
            char *block22 = (char *)vm_map(filename1, 2);
            char *block33 = (char *)vm_map(filename1, 3);
            cout << *block11 << *block22 << *block33 << endl;
            cout << *temp1 << *temp2 << *temp3 << *temp4 << endl;
        }
        else
        {
            char *block11 = (char *)vm_map(filename1, 1);
            char *block22 = (char *)vm_map(filename1, 2);
            char *block33 = (char *)vm_map(filename1, 3);
            cout << *block11 << *block22 << *block33 << endl;
            cout << *temp1 << *temp2 << *temp3 << *temp4 << endl;
            cout << *filename1 << endl;
            cout << *filename2 << endl;
        }
    }
    char *block1 = (char *)vm_map(filename1, 1);
    char *block2 = (char *)vm_map(filename1, 2);
    char *block3 = (char *)vm_map(filename1, 3);
    char *block4 = (char *)vm_map(filename1, 4);
    char *block5 = (char *)vm_map(filename2, 1);
    char *block6 = (char *)vm_map(filename2, 2);
    char *block7 = (char *)vm_map(filename2, 3);
    char *block8 = (char *)vm_map(filename2, 4);
    int pid = fork();
    if (!pid)
    {
        cout << *filename1 << endl;
        cout << *filename2 << endl;
        char *block11 = (char *)vm_map(filename1, 1);
        char *block22 = (char *)vm_map(filename1, 2);
        char *block33 = (char *)vm_map(filename1, 3);
        cout << *block11 << *block22 << *block33 << endl;
        cout << *block1 << *block2 << *block3 << *block4 << endl;
    }
    else
    {
        int pid2 = fork();
        if (!pid2)
        {
            char *block11 = (char *)vm_map(filename1, 1);
            char *block22 = (char *)vm_map(filename1, 2);
            char *block33 = (char *)vm_map(filename1, 3);
            char *block44 = (char *)vm_map(filename1, 4);
            cout << *block11 << *block22 << *block33 << *block44 << endl;
            block44 = (char *)vm_map(filename1, 5);
            cout << *block5 << endl;
            cout << *block1 << endl;
            cout << *block4 << endl;
            cout << *block5 << endl;
        }
        else
        {
            char *block5 = (char *)vm_map(filename1, 5);
            cout << *block1 << endl;
            *block1 = 'a';
            cout << *block2 << endl;
            *block3 = 'a';
            cout << *block1 << endl;
            cout << *block2 << endl;
            cout << *block5 << endl;
            *block3 = 'a';
            *block4 = 'a';
            *filename1 = 'a';
            cout << *filename1 << endl;
            cout << *block5 << endl;
            *block3 = 'a';
            *block4 = 'a';
            *block5 = 'a';
        }
    }
    return 0;
}
