#include <cassert>
#include <cstring>
#include <deque>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "vm_pager.h"

using std::cout, std::endl;
using std::deque;
using std::map;
using std::pair;
using std::queue;
using std::set;
using std::string;
using std::unordered_map;
using std::vector;

// Number of pages in an arena / table
#define NUM_PAGES (VM_ARENA_SIZE / VM_PAGESIZE)

struct page_table_t {
  page_table_entry_t ptes[VM_ARENA_SIZE / VM_PAGESIZE];
};

// From "vm_pager.cpp":
// struct page_table_entry_t {
//     unsigned int ppage : 20;            /* bit 0-19 */
//     unsigned int read_enable : 1;       /* bit 20 */
//     unsigned int write_enable : 1;      /* bit 21 */
// };
// struct page_table_t {
//     page_table_entry_t ptes[VM_ARENA_SIZE/VM_PAGESIZE];
// };

class page_status_t {
public:
  string filename;     // not used if swap_backed
  unsigned long block; // not used if swap_backed
  bool file_backed;
  bool dirty;
  bool valid;
  bool resident;
  bool referenced;

  page_status_t()
      : filename(""), block(0), file_backed(false), dirty(false), valid(false),
        resident(false), referenced(false) {}
};

struct page_status_table_t {
  page_status_t *pst[NUM_PAGES];
};

class process_t {
public:
  pid_t pid;
  page_table_t page_table;               // hw page table
  page_status_table_t page_status_table; // sw page table

  process_t(pid_t pid) : pid(pid) {
    for (auto &i : page_status_table.pst)
      i = nullptr;
  }

  ~process_t() = default;
};

process_t *current_process;
unordered_map<pid_t, process_t *> process_map; // pid -> process

unsigned int free_mem_page;
unsigned int free_blocks; // free swap blocks

typedef pair<string, uint> file_block_t;
typedef pair<pid_t, uint> vpage_t;

vector<page_table_entry_t *> sw_pages;
vector<page_status_t> mem_pages;
vector<set<vpage_t>> vpage_mapped_t; // the set of vpages mapped to this page

void handle_read(unsigned int vpage_index);
void handle_write(unsigned int vpage_index);

int evict();

queue<uint> clock_queue;

int fetch_page(const uint v_page);

int get_empty_p_page();

typedef pair<string, uint> file_block_t;
typedef pair<pid_t, uint> vpage_t;

map<file_block_t, set<vpage_t>> file_table;
map<file_block_t, uint> file_mem_table;
