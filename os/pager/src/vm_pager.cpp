#include "vm_pager.h"
#include "structure.h"
#include <cstring>

void vm_init(unsigned int memory_pages, unsigned int swap_blocks) {
  free_mem_page = memory_pages - 1;
  free_blocks = swap_blocks;
  sw_pages.resize(swap_blocks);
  mem_pages.resize(memory_pages);
  vpage_mapped_t.resize(memory_pages);
  // init the pinning page
  (void)memset(vm_physmem, 0, VM_PAGESIZE); // Init the first page to 0
  mem_pages[0].valid = true;
  mem_pages[0].resident = true;
  mem_pages[0].referenced = true;
}

int vm_create(pid_t parent_pid, pid_t child_pid) {
  // no need to check eager reservation
  auto *child_process = new process_t(child_pid);
  for (auto &pte : child_process->page_table.ptes) {
    pte.ppage = 0;
    pte.read_enable = 0;
    pte.write_enable = 0;
  }
  process_map[child_pid] = child_process;
  return 0;
}

void vm_switch(pid_t pid) {
  current_process = process_map[pid];
  page_table_base_register = &(current_process->page_table.ptes[0]);
}

void *vm_map(const char *filename, unsigned int block) {
  int avail_page_idx = 0;
  for (uintptr_t i = 0; i <= NUM_PAGES; i++) {
    if (i == NUM_PAGES) {
      return nullptr;
    }
    if (!current_process->page_status_table.pst[i] ||
        !current_process->page_status_table.pst[i]->valid) {
      avail_page_idx = static_cast<int>(i);
      break;
    }
  }

  if (!filename) {
    // Swap-backed pages
    if (free_blocks == 0)
      return nullptr;

    free_blocks--;

    // Not referenced, not dirty
    current_process->page_status_table.pst[avail_page_idx] = new page_status_t;
    page_table_entry_t *pte =
        &(current_process->page_table.ptes[avail_page_idx]);
    page_status_t *pste =
        current_process->page_status_table.pst[avail_page_idx];

    pte->read_enable = 1;
    pte->write_enable = 0;
    pte->ppage = 0;
    pste->file_backed = false;
    pste->valid = true;
    pste->resident = true;

    int sw_index = 0;
    // find unused sw_pages index
    for (auto it = sw_pages.begin(); it != sw_pages.end(); it++) {
      if (*it == nullptr)
        break;
      sw_index++;
    }
    pste->block = static_cast<unsigned long>(sw_index);
    vpage_t vpage_block = std::make_pair(current_process->pid, avail_page_idx);
    vpage_mapped_t[0].insert(vpage_block);
    sw_pages[static_cast<unsigned long>(sw_index)] =
        &current_process->page_table.ptes[avail_page_idx];
  } else {
    // File-backed pages
    if (filename < VM_ARENA_BASEADDR ||
        filename >= (char *)VM_ARENA_BASEADDR + VM_ARENA_SIZE)
      return nullptr;
    // Translate filename string
    string filename_str = "";

    unsigned int virtual_index =
        (unsigned int)(filename - (char *)VM_ARENA_BASEADDR) / VM_PAGESIZE;
    unsigned int offset =
        (unsigned int)(filename - (char *)VM_ARENA_BASEADDR) % VM_PAGESIZE;

    while (true) {
      if (current_process->page_status_table.pst[virtual_index] == nullptr ||
          !current_process->page_status_table.pst[virtual_index]->valid) {
        return nullptr;
      }

      if (!current_process->page_table.ptes[virtual_index].read_enable) {
        int flag = vm_fault(
            (char *)VM_ARENA_BASEADDR + virtual_index * VM_PAGESIZE, false);
        if (flag == -1)
          return nullptr;
      }

      unsigned int physical_page =
          current_process->page_table.ptes[virtual_index].ppage;
      char *filename_read =
          ((char *)vm_physmem + physical_page * VM_PAGESIZE + offset);
      while (offset < VM_PAGESIZE) {
        if (*filename_read == '\0') {
          filename_read++;
          break;
        }
        filename_str += *filename_read;
        filename_read++;
        offset++;
      }
      filename_read--;

      if (*filename_read == '\0')
        break;
      offset = 0;
      virtual_index++;
      physical_page = current_process->page_table.ptes[virtual_index].ppage;
      filename_read =
          ((char *)vm_physmem + physical_page * VM_PAGESIZE + offset);
    }

    current_process->page_status_table.pst[avail_page_idx] =
        new page_status_t; // new page_status_t;
    // initialize file-back virtual page
    page_table_entry_t *pte =
        &(current_process->page_table.ptes[avail_page_idx]);
    page_status_t *pste =
        current_process->page_status_table.pst[avail_page_idx];
    pste->filename = filename_str;
    pste->file_backed = true;
    pste->block = block;
    pste->valid = true;
    pste->resident = false;
    pte->read_enable = pte->write_enable = 0;

    // check if the file-back page is already in the physical memory
    file_block_t file_block = std::make_pair(filename_str, block);
    auto fmt_it = file_mem_table.find(file_block);
    // the file-back page is already in the physical memory
    if (fmt_it != file_mem_table.end()) {
      uint phy_mem_index = fmt_it->second;
      pste->resident = true;
      pste->referenced = mem_pages[phy_mem_index].referenced;

      pte->read_enable = mem_pages[phy_mem_index].referenced;
      pte->write_enable =
          mem_pages[phy_mem_index].referenced && mem_pages[phy_mem_index].dirty;
      pte->ppage = phy_mem_index;

      vpage_t vpage_block =
          std::make_pair(current_process->pid, avail_page_idx);
      vpage_mapped_t[phy_mem_index].insert(vpage_block);
    }

    file_table[file_block].insert(
        std::make_pair(current_process->pid, avail_page_idx));
  }
  return (void *)((char *)VM_ARENA_BASEADDR + avail_page_idx * VM_PAGESIZE);
}

void handle_read(unsigned int vpage_index) {
  page_table_entry_t *pte = &(current_process->page_table.ptes[vpage_index]);
  page_status_t *pste = current_process->page_status_table.pst[vpage_index];
  pte->read_enable = 1;
  mem_pages[pte->ppage].referenced = true;
  if (mem_pages[pte->ppage].dirty)
    pte->write_enable = 1;
  if (pste->file_backed) {
    for (auto i : file_table[std::make_pair(pste->filename, pste->block)]) {
      page_table_entry_t *tpte =
          &(process_map[i.first]->page_table.ptes[i.second]);
      tpte->ppage = pte->ppage;
      tpte->read_enable = 1;
      tpte->write_enable = mem_pages[pte->ppage].dirty;
      vpage_mapped_t[pte->ppage].insert(i);
    }
  }
}

void handle_write(unsigned int vpage_index) {
  page_table_entry_t *pte = &(current_process->page_table.ptes[vpage_index]);
  page_status_t *pste = current_process->page_status_table.pst[vpage_index];
  pte->read_enable = pte->write_enable = 1;
  uint mem_index; // the index of the physical memory page

  if (pte->ppage == 0) { // write on pining page
    mem_index = static_cast<uint>(get_empty_p_page());
    page_status_t *meme = &mem_pages[mem_index];
    memset((char *)vm_physmem + mem_index * VM_PAGESIZE, 0, VM_PAGESIZE);
    meme->filename = "";
    meme->referenced = true;
    meme->file_backed = false;
    meme->block = pste->block;
    meme->valid = true;
    meme->dirty = true;
    vpage_mapped_t[0].erase({current_process->pid, vpage_index});
    vpage_mapped_t[mem_index].insert({current_process->pid, vpage_index});
    pte->ppage = mem_index;
    clock_queue.push(mem_index);
  } else { // write on non-pining page
    mem_index = pte->ppage;
    mem_pages[mem_index].dirty = true;
    mem_pages[mem_index].referenced = true;
    if (pste->file_backed) {
      for (auto i : file_table[std::make_pair(pste->filename, pste->block)]) {
        auto *pt = &(process_map[i.first]->page_table.ptes[i.second]);
        pt->ppage = mem_index;
        pt->read_enable = pt->write_enable = 1;
        vpage_mapped_t[mem_index].insert(i);
      }
    }
  }
}

int vm_fault(const void *addr, bool write_flag) {
  if (addr < VM_ARENA_BASEADDR ||
      addr >= static_cast<char *>(VM_ARENA_BASEADDR) + VM_ARENA_SIZE)
    return -1;
  unsigned int vpage_index =
      ((uintptr_t)addr - (uintptr_t)VM_ARENA_BASEADDR) / VM_PAGESIZE;
  // check if the page is mapped
  if (current_process->page_status_table.pst[vpage_index] == nullptr)
    return -1;

  if (!current_process->page_status_table.pst[vpage_index]
           ->resident) { // if not in physical memory
    if (fetch_page(vpage_index) != 0)
      return -1;
  }

  if (!write_flag)
    handle_read(vpage_index);
  else
    handle_write(vpage_index);

  return 0;
}

void vm_destroy() { // process destroying
  page_status_table_t *pst = &current_process->page_status_table;
  // free all pages
  for (auto i = 0; i < NUM_PAGES; i++) {
    if (pst->pst[i] != nullptr) { // if been mapped
      auto *pte = &(current_process->page_table.ptes[i]);
      auto *pste = pst->pst[i];
      if (pste->file_backed) {
        auto fit = file_table.find({pste->filename, pste->block});
        fit->second.erase({current_process->pid, i});
        if (pste->resident) {
          vpage_mapped_t[pte->ppage].erase({current_process->pid, i});
        }
      } else {
        sw_pages[pste->block] = nullptr;
        free_blocks++;
        if (pte->ppage != 0 && pste->resident) { // currently in mem
          auto *phy_e = &mem_pages[pte->ppage];
          phy_e->valid = false;
          phy_e->dirty = false;
          phy_e->referenced = false;
          vpage_mapped_t[pte->ppage].clear();
          free_mem_page++;
        }
      }
    }
  }
  // clear the invalid element in clock queue
  auto origin_size = clock_queue.size();
  for (uint i = 0; i < origin_size;
       i++) { // clear the invalid element in clock queue
    uint vpage = clock_queue.front();
    clock_queue.pop();
    if (mem_pages[vpage].valid)
      clock_queue.push(vpage);
  }
  for (auto &i : pst->pst) { // free all page status table
    if (i) {
      delete i;
      i = nullptr;
    }
  }
  pid_t pid = current_process->pid;
  delete process_map[pid];
  process_map.erase(pid);
}

int evict() {
  while (true) { // find the evict target
    uint ev_target = clock_queue.front();
    clock_queue.pop();
    if (mem_pages[ev_target]
            .referenced) { // if referenced, set it to unreferenced and push it
                           // to the end of the clock queue
      mem_pages[ev_target].referenced = false;
      for (auto i : vpage_mapped_t[ev_target]) {
        page_table_entry_t *pte =
            &(process_map[i.first]->page_table.ptes[i.second]);
        pte->read_enable = pte->write_enable = 0;
      }
      clock_queue.push(ev_target);
    } else {                            // find the evict target
      if (mem_pages[ev_target].dirty) { // if dirty, write back to disk
        void *buf_addr =
            (void *)((char *)vm_physmem + (ev_target * VM_PAGESIZE));
        if (mem_pages[ev_target].file_backed) {
          if (file_write(mem_pages[ev_target].filename.c_str(),
                         mem_pages[ev_target].block, buf_addr) < 0)
            return -1;
        } else {
          if (file_write(nullptr, mem_pages[ev_target].block, buf_addr) < 0)
            return -1;
        }
        mem_pages[ev_target].dirty = false;
      }
      if (mem_pages[ev_target].file_backed)
        file_mem_table.erase(
            {mem_pages[ev_target].filename, mem_pages[ev_target].block});
      free_mem_page++;
      mem_pages[ev_target].valid = false;
      mem_pages[ev_target].referenced = false;
      for (auto i : vpage_mapped_t[ev_target]) {
        page_table_entry_t *pte =
            &(process_map[i.first]->page_table.ptes[i.second]);
        page_status_t *pste =
            process_map[i.first]->page_status_table.pst[i.second];
        pte->read_enable = pte->write_enable = 0;
        pste->resident = false;
      }
      vpage_mapped_t[ev_target].clear();
      return static_cast<int>(ev_target);
    } // end of non-reference
  }
}

int fetch_page(const uint v_page) { // fetch page from disk to physical memory
  page_table_entry_t *pte = &(current_process->page_table.ptes[v_page]);
  page_status_t *pste = (current_process->page_status_table.pst[v_page]);
  uint block = pste->block;
  uint target_mem_index =
      get_empty_p_page(); // evict page return the empty block index
  void *buf_addr =
      (void *)((char *)vm_physmem + (target_mem_index * VM_PAGESIZE));
  if (pste->file_backed) {
    if (file_read(pste->filename.c_str(), block, buf_addr) < 0)
      return -1;
    mem_pages[target_mem_index].file_backed = true;
    file_mem_table[{pste->filename, block}] = target_mem_index;
  } // end of file-backed
  else {
    if (file_read(nullptr, block, buf_addr) < 0)
      return -1;
    mem_pages[target_mem_index].file_backed = false;
  } // end of non-file backed
  page_status_t *meme = &mem_pages[target_mem_index];
  vpage_mapped_t[target_mem_index].insert({current_process->pid, v_page});
  meme->filename = pste->filename;
  meme->dirty = false;
  meme->referenced = false;
  meme->valid = true;
  meme->block = block;

  if (meme->file_backed) { // if file backed, update the file table
    auto ft_itr = file_table.find({meme->filename, meme->block});
    for (auto j : ft_itr->second) {
      pid_t pid = j.first;
      uint vpage = j.second;
      page_table_entry_t *tpte = &(process_map[pid]->page_table.ptes[vpage]);
      page_status_t *tpste = (process_map[pid]->page_status_table.pst[vpage]);
      tpte->ppage = target_mem_index;
      tpste->resident = true;
      vpage_mapped_t[target_mem_index].insert({pid, vpage});
    }
  }
  clock_queue.push(target_mem_index);
  free_mem_page--;
  pste->resident = true;
  pte->ppage = target_mem_index;
  return 0;
} // end of fetch page

int get_empty_p_page() { // get the index of first empty page in mempages
  int empty_index = -1;
  for (int i = 0; i < mem_pages.size(); i++) {
    if (!mem_pages[i].valid) {
      empty_index = i;
      break;
    }
  }
  if (empty_index == -1)
    empty_index = evict();

  return empty_index;
}